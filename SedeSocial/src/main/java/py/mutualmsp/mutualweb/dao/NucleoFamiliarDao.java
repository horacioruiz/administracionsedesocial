/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import py.mutualmsp.mutualweb.entities.NucleoFamiliar;

/**
 *
 * @author Dbarreto
 */
@Stateless
public class NucleoFamiliarDao implements Serializable {

    @PersistenceContext
    private EntityManager em;

    public List<NucleoFamiliar> listaNucleoFamiliar() {
        List<NucleoFamiliar> lista = new ArrayList<>();

        try {
            lista = em.createNamedQuery("NucleoFamiliar.findAll").getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }

    public void guardar(NucleoFamiliar cargo) {
        try {
            em.merge(cargo);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void borrar(NucleoFamiliar cargo) {
        try {
            em.remove(em.contains(cargo) ? cargo : em.merge(cargo));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<NucleoFamiliar> getListNucleoFamiliar(String valor) {
        List<NucleoFamiliar> lista = new ArrayList<>();
        try {
            if (valor != null && !valor.equals("")) {
                lista = em.createNativeQuery("SELECT * FROM subordinado "
                        + "                   where  id=" + valor + " AND habilitado=TRUE;", NucleoFamiliar.class)
                        .getResultList();
            } else {
                lista = em.createQuery("select c from NucleoFamiliar c WHERE c.habilitado=TRUE")
                        .getResultList();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }

    public List<NucleoFamiliar> getNucleoFamiliarByDescripcion(String valor) {
        List<NucleoFamiliar> lista = new ArrayList<>();
        try {
            if (valor != null && !valor.equals("")) {
                lista = em.createNativeQuery("SELECT * FROM subordinado "
                        + "                   where lower(descripcion) LIKE '" + valor + "%' AND habilitado=TRUE;", NucleoFamiliar.class)
                        .getResultList();
            } else {
                lista = em.createQuery("select c from NucleoFamiliar c WHERE c.habilitado=TRUE")
                        .getResultList();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }

    public List<NucleoFamiliar> listarNucleoFamiliarPorID(String valor) {
        List<NucleoFamiliar> lista = new ArrayList<>();
        try {
            if (valor != null && !valor.equals("")) {
                lista = em.createNativeQuery("SELECT * FROM subordinado "
                        + "                   where idsocio=" + valor + " AND habilitado=TRUE", NucleoFamiliar.class)
                        .getResultList();
            } else {
                lista = em.createQuery("select c from NucleoFamiliar c WHERE c.habilitado=TRUE")
                        .getResultList();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }

    public List<NucleoFamiliar> listarNucleoFamiliarPorCI(String valor) {
        List<NucleoFamiliar> lista = new ArrayList<>();
        try {
            if (valor != null && !valor.equals("")) {
                lista = em.createNativeQuery("SELECT * FROM subordinado "
                        + "                   where cedula='" + valor + "' AND habilitado=TRUE", NucleoFamiliar.class)
                        .getResultList();
            } else {
                lista = em.createQuery("select c from NucleoFamiliar c WHERE c.habilitado=TRUE")
                        .getResultList();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }

    public List<NucleoFamiliar> listarNucleoFamiliarData(String valor, String ci) {
        List<NucleoFamiliar> lista = new ArrayList<>();

        try {
            if (!valor.equalsIgnoreCase("") && !ci.equalsIgnoreCase("")) {
                lista = em.createNativeQuery("SELECT * FROM subordinado nf left join public.socio s on s.id=nf.idsocio "
                        + "                   where nf.cedula='" + ci + "' AND s.cedula='" + valor + "' AND nf.habilitado=TRUE", NucleoFamiliar.class)
                        .getResultList();
            } else if (!valor.equalsIgnoreCase("")) {
                lista = em.createNativeQuery("SELECT * FROM subordinado nf left join public.socio s on s.id=nf.idsocio "
                        + "                   where s.cedula='" + valor + "' AND nf.habilitado=TRUE", NucleoFamiliar.class)
                        .getResultList();
            } else if (!ci.equalsIgnoreCase("")) {
                lista = em.createNativeQuery("SELECT * FROM subordinado "
                        + "                   where cedula='" + ci + "' AND habilitado=TRUE", NucleoFamiliar.class)
                        .getResultList();
            } else {
                lista = em.createNativeQuery("SELECT * FROM subordinado WHERE habilitado=TRUE "
                        + "   ", NucleoFamiliar.class)
                        .getResultList();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }

    public NucleoFamiliar getNinguno() {
        NucleoFamiliar ninguno = null;
        try {
            ninguno = (NucleoFamiliar) em.createNativeQuery("SELECT * FROM subordinado "
                    + "               where  lower(descripcion) like lower('%NINGUNO%') AND habilitado=TRUE;", NucleoFamiliar.class)
                    .getSingleResult();
        } catch (Exception e) {
            e.printStackTrace();
            ninguno = (NucleoFamiliar) em.createQuery("select c from NucleoFamiliar c WHERE c.habilitado=TRUE")
                    .getResultList();
        }
        return ninguno;
    }

    public NucleoFamiliar getById(long parseLong) {
        NucleoFamiliar ninguno = null;
        try {
            String sql = "SELECT * FROM subordinado "
//                    + " where id=" + parseLong +" AND habilitado=TRUE";
                    + " where id=" + parseLong +"";
            System.out.println("SQLDATA -> " + sql);
            ninguno = (NucleoFamiliar) em.createNativeQuery(sql, NucleoFamiliar.class).setMaxResults(1)
                    .getSingleResult();
        } catch (Exception e) {
            e.printStackTrace();

        }
        return ninguno;
    }
}
