/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import py.mutualmsp.mutualweb.entities.DatosFamiliaresAyuda;
import py.mutualmsp.mutualweb.entities.DatosFamiliaresAyuda;

/**
 *
 * @author Dbarreto
 */
@Stateless
public class DatoFamiliarAyudaDao implements Serializable {

    @PersistenceContext
    private EntityManager em;

    public List<DatosFamiliaresAyuda> listaDatosFamiliaresAyuda() {
        List<DatosFamiliaresAyuda> lista = new ArrayList<>();

        try {
            lista = em.createNamedQuery("DatosFamiliaresAyuda.findAll").getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }

    public void guardar(DatosFamiliaresAyuda ciudad) {
        try {
            em.merge(ciudad);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void borrar(DatosFamiliaresAyuda ciudad) {
        try {
            em.remove(em.contains(ciudad) ? ciudad : em.merge(ciudad));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<DatosFamiliaresAyuda> getListDatosFamiliaresAyuda(String valor) {
        List<DatosFamiliaresAyuda> lista = new ArrayList<>();
        try {
            if (valor != null && !valor.equals("")) {
                lista = em.createNativeQuery("SELECT * FROM public.datos_familiares_ayuda "
                        + "                   where  id=" + valor + ";", DatosFamiliaresAyuda.class)
                        .getResultList();
            } else {
                lista = em.createQuery("select c from DatosFamiliaresAyuda c")
                        .getResultList();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }

    public List<DatosFamiliaresAyuda> getListDatosFamiliaresAyudaBySolicitud(String valor) {
        List<DatosFamiliaresAyuda> lista = new ArrayList<>();
        try {
            if (valor != null && !valor.equals("")) {
                lista = em.createNativeQuery("SELECT * FROM public.datos_familiares_ayuda "
                        + "                   where  id_solicitud=" + valor + ";", DatosFamiliaresAyuda.class)
                        .getResultList();
            } else {
                lista = new ArrayList<>();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }

}
