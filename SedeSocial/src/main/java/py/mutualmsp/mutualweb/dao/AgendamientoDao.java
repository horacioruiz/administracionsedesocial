/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.dao;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import py.mutualmsp.mutualweb.entities.Agendamiento;
import py.mutualmsp.mutualweb.entities.AgendamientoDetalle;
import py.mutualmsp.mutualweb.util.DateUtils;

/**
 *
 * @author Dbarreto
 */
@Stateless
public class AgendamientoDao implements Serializable {

    @PersistenceContext
    private EntityManager em;

    public List<Agendamiento> listaAgendamiento() {
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        String fecha = formatter.format(new Date());
        List<Agendamiento> lista = new ArrayList<>();
        try {
            String sql = "SELECT * FROM sedesocial.agendamiento "
                    + "                   where estado isnull OR upper(estado) LIKE 'PENDIENTE' AND fecha>='" + fecha + "';";
            System.out.println("SQL ==> "+sql);
            lista = em.createNativeQuery(sql, Agendamiento.class)
                    .getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }

    public Agendamiento listaAgendamientoById(Long id) {
        Agendamiento agen = new Agendamiento();
        try {
            agen = (Agendamiento) em.createNativeQuery("SELECT * FROM sedesocial.agendamiento "
                    + "                   where id=" + id, Agendamiento.class)
                    .getSingleResult();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return agen;
    }

    public void guardar(Agendamiento cargo) {
        try {
            em.merge(cargo);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void borrar(Agendamiento cargo) {
        try {
            em.remove(em.contains(cargo) ? cargo : em.merge(cargo));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<Agendamiento> getListAgendamiento(String valor) {
        List<Agendamiento> lista = new ArrayList<>();
        try {
            if (valor != null && !valor.equals("")) {
                lista = em.createNativeQuery("SELECT * FROM sedesocial.agendamiento "
                        + "                   where  id=" + valor + ";", Agendamiento.class)
                        .getResultList();
            } else {
                lista = em.createQuery("select c from Agendamiento c")
                        .getResultList();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }

    public List<Agendamiento> getAgendamientoByDescripcion(String valor) {
        List<Agendamiento> lista = new ArrayList<>();
        try {
            if (valor != null && !valor.equals("")) {
                lista = em.createNativeQuery("SELECT * FROM sedesocial.agendamiento "
                        + "                   where lower(descripcion) LIKE '" + valor + "%';", Agendamiento.class)
                        .getResultList();
            } else {
                lista = em.createQuery("select c from Agendamiento c")
                        .getResultList();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }

    public Agendamiento getNinguno() {
        Agendamiento ninguno = null;
        try {
            ninguno = (Agendamiento) em.createNativeQuery("SELECT * FROM sedesocial.agendamiento "
                    + "               where  lower(descripcion) like lower('%NINGUNO%');", Agendamiento.class)
                    .getSingleResult();
        } catch (Exception e) {
            e.printStackTrace();
            ninguno = (Agendamiento) em.createQuery("select c from Agendamiento c")
                    .getResultList();
        }
        return ninguno;
    }

    public List<AgendamientoDetalle> listarPorIdAgendamiento(Long id, String invitado) {
        List<AgendamientoDetalle> lista = new ArrayList<>();
        if (invitado.toUpperCase().equalsIgnoreCase("INVITADO")) {
            try {
                lista = em.createNativeQuery("SELECT * FROM sedesocial.agendamiento_detalle "
                        + "                   where idnucleofamiliar isnull AND idagendamiento=" + id + ";", AgendamientoDetalle.class)
                        .getResultList();
            } catch (Exception e) {
                lista = new ArrayList<>();
                e.printStackTrace();
            }
        } else if (invitado.toUpperCase().equalsIgnoreCase("FAMILIAR")) {
            try {
                lista = em.createNativeQuery("SELECT * FROM sedesocial.agendamiento_detalle "
                        + "                   where idinvitado isnull AND idagendamiento=" + id + ";", AgendamientoDetalle.class)
                        .getResultList();
            } catch (Exception e) {
                lista = new ArrayList<>();
                e.printStackTrace();
            }
        }

        return lista;
    }

    public List<Agendamiento> listaAgendamientoParametros(String tipo, String ci, LocalDate desde, LocalDate hasta, String estado, String horario) {
        List<Agendamiento> lista = new ArrayList<>();
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        String fechaDesdeText = formatter.format(DateUtils.asDate(desde));
        String fechaHastaText = formatter.format(DateUtils.asDate(hasta));

        String tipoData = "";
        if (!tipo.equalsIgnoreCase("*** Todos ***")) {
            if (tipo.equalsIgnoreCase("Sede Social")) {
                tipoData = " AND a.tipo='usufructo_sede'";
            } else if (tipo.equalsIgnoreCase("Quincho Tatare")) {
                tipoData = " AND a.tipo='usufructo_quincho_tatare'";
            } else if (tipo.equalsIgnoreCase("Salón de Evento Santa Rita")) {
                tipoData = " AND a.tipo='usufructo_eventos'";
            }
        }

        String estadoData = "";
        if (!estado.equalsIgnoreCase("*** Todos ***")) {
            if (estado.equalsIgnoreCase("Pendientes")) {
                estadoData = " AND (a.estado isnull OR upper(a.estado) LIKE 'PENDIENTE')";
            } else if (estado.equalsIgnoreCase("Confirmados")) {
                estadoData = " AND upper(a.estado)='CONFIRMADO'";
            } else if (estado.equalsIgnoreCase("Rechazados")) {
                estadoData = " AND upper(a.estado)='CANCELADO'";
            }
        }

        String horarioData = "";
        if (!horario.equalsIgnoreCase("*** Todos ***")) {
            horarioData = " AND a.horario='" + horario + "'";
        }

        String sql = "";
        if (!ci.equalsIgnoreCase("")) {
            sql = "SELECT * FROM sedesocial.agendamiento a left join public.socio s on s.id=a.idsocio "
                    + " where s.cedula like '%" + ci + "%' AND (a.fecha BETWEEN '" + fechaDesdeText + " 00:00:00' AND '" + fechaHastaText + " 23:59:59') " + tipoData + estadoData + horarioData;
        } else {
            sql = "SELECT * FROM sedesocial.agendamiento a "
                    + " where a.fecha BETWEEN '" + fechaDesdeText + " 00:00:00' AND '" + fechaHastaText + " 23:59:59' " + tipoData + estadoData + horarioData;
        }
        sql += " ORDER BY a.id DESC";
        try {
            System.out.println("SQL HERE -> " + sql);
            lista = em.createNativeQuery(sql, Agendamiento.class)
                    .getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return lista;
    }

}
