package py.mutualmsp.mutualweb.dao;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;
import py.mutualmsp.mutualweb.entities.DatosFamiliaresAyuda;
import py.mutualmsp.mutualweb.entities.Formulario;
import py.mutualmsp.mutualweb.entities.SolicitudCredito;
import py.mutualmsp.mutualweb.entities.Usuario;

/**
 * Created by Alfre on 8/6/2016.
 */
@Stateless
public class SolicitudCreditoDao {

    @PersistenceContext
    private EntityManager em;

    public SolicitudCredito guardarSolicitudCredito(SolicitudCredito usuarios) {
        if (usuarios.getId() == null) {
            em.persist(usuarios);
        } else {
            em.merge(usuarios);
        }
        em.flush();
        return usuarios;
    }

    public List<SolicitudCredito> listadeSolicitudCredito(int firstRow, int pageSize, String filter) {
        try {
            String sql = "select p ";
            sql += "from SolicitudCredito p ";
            if (filter != null && !filter.equals("")) {
                sql += "where upper(p.descripcion) like :param order by p.id ASC";
            }
            Query query = em.createQuery(sql, SolicitudCredito.class);
            if (filter != null && !filter.equals("")) {
                query.setParameter("param", filter.toUpperCase() + "%");
            }
            int i = query.getResultList().size();
            System.out.print(i);
            List<SolicitudCredito> results = query.getResultList();
            return results;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<SolicitudCredito> listadeSolicitudCredito() {
        try {
            String sql = "select u from SolicitudCredito u ";
            Query query = em.createQuery(sql);
            int i = query.getResultList().size();
            System.out.print(i);
            return query.getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<SolicitudCredito> listarAdministradores() {
        try {
            String sql = "select p ";
            sql += "from SolicitudCredito p JOIN FETCH p.socio f WHERE p.administrador=TRUE";
            Query query = em.createQuery(sql, SolicitudCredito.class);
            int i = query.getResultList().size();
            System.out.print(i);
            List<SolicitudCredito> results = query.getResultList();
            return results;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public SolicitudCredito getByIdUsuario(Long idusuario) {
        return (SolicitudCredito) em.find(SolicitudCredito.class, idusuario);
    }

    public List<SolicitudCredito> listadeSolicitudCreditoByFormulario(int init, int finish, String filter) {
        try {
            String sql = "select p ";
            sql += "from SolicitudCredito p";
            if (filter != null && !filter.equals("")) {
                sql += " JOIN FETCH p.parametro f where upper(f.descripcion) like :param order by p.id ASC";
            }
            Query query = em.createQuery(sql, SolicitudCredito.class);
            if (filter != null && !filter.equals("")) {
                query.setParameter("param", filter.toUpperCase());
            }
            int i = query.getResultList().size();
            System.out.print(i);
            List<SolicitudCredito> results = query.getResultList();
            return results;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public void borrar(SolicitudCredito cargo) {
        try {
            em.remove(em.contains(cargo) ? cargo : em.merge(cargo));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<SolicitudCredito> listarPorFormulario(Formulario value) {
        try {
            String sql = "select p ";
            sql += "from SolicitudCredito p JOIN FETCH p.parametro f WHERE f.id=:idForm";
            Query query = em.createQuery(sql, SolicitudCredito.class).setParameter("idForm", value.getId());
            int i = query.getResultList().size();
            System.out.print(i);
            List<SolicitudCredito> results = query.getResultList();
            return results;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public SolicitudCredito listarPorIdFuncionario(Long idsocio) {
        try {
            return (SolicitudCredito) em.createQuery("select hf from SolicitudCredito hf JOIN FETCH hf.socio f WHERE f.id=:id")
                    .setParameter("id", idsocio)
                    .getSingleResult();
        } catch (Exception e) {
            return null;
        } finally {
        }
    }

    public SolicitudCredito getById(Long id) {
        SolicitudCredito marcacion = null;
        try {
            marcacion = (SolicitudCredito) em.createNativeQuery("SELECT * FROM public.parametro "
                    + " where idreloj=" + id + ";", SolicitudCredito.class)
                    .getSingleResult();
        } catch (Exception e) {
            e.printStackTrace();
            marcacion = new SolicitudCredito();
        }
        return marcacion;
    }

    public SolicitudCredito listarPorIdReloj(Long idReloj) {
        try {
            return (SolicitudCredito) em.createQuery("select hf from SolicitudCredito hf JOIN FETCH hf.socio f WHERE hf.idreloj=:id")
                    .setParameter("id", idReloj)
                    .getSingleResult();
        } catch (Exception e) {
            return null;
        } finally {
        }
    }

    public List<SolicitudCredito> listarPorFechas(Date fechaDesde, Date fechaHasta, Long idsocio) {
        String filterFuncionario = "";
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        String fechaDesdeText = formatter.format(fechaDesde);
        String fechaHastaText = formatter.format(fechaHasta);
        if (idsocio != null) {
            filterFuncionario = "AND f.idsocio=" + idsocio;
        }
        try {
            String sql = "select * ";
            sql += "from licencias p LEFT join socio f on p.idsocio=f.idsocio WHERE p.fecha BETWEEN '" + fechaDesdeText + "' AND '" + fechaHastaText + "' " + filterFuncionario + " ORDER BY p.id DESC";
            Query query = em.createNativeQuery(sql, SolicitudCredito.class);
            return query.getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ArrayList<>();
    }

    public List<SolicitudCredito> listadeSolicitudCreditoSinConfirmar() {
        try {
            String sql = "select u from SolicitudCredito u ORDER BY u.id DESC";
            Query query = em.createQuery(sql);
            int i = query.getResultList().size();
            System.out.print(i);
            return query.getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public SolicitudCredito listarPorId(Long id) {
        try {
            return (SolicitudCredito) em.createQuery("select r from SolicitudCredito r JOIN FETCH r.socio f WHERE r.id=:id ORDER BY r.id DESC")
                    .setParameter("id", id)
                    .getSingleResult();
        } catch (Exception e) {
            System.out.println("-> " + e.getLocalizedMessage());
            System.out.println("-> " + e.fillInStackTrace());
            //return (SolicitudCredito) em.createQuery("select r from SolicitudCredito r JOIN FETCH r.socio f JOIN FETCH r.parametro form ORDER BY r.id DESC")
            //.setParameter("id", id)
            ///      .getSingleResult();
            return new SolicitudCredito();
        } finally {
        }
    }

    public SolicitudCredito listarPorIdHere(Long id) {
        try {
            return (SolicitudCredito) em.createQuery("select r from SolicitudCredito r JOIN FETCH r.socio f WHERE f.cedula=:id ORDER BY r.id DESC")
                    .setParameter("id", id)
                    .getSingleResult();
        } catch (Exception e) {
            return new SolicitudCredito();
        } finally {
        }
    }

    public SolicitudCredito listarPorCiSocio(String id) {
        try {
            return (SolicitudCredito) em.createQuery("select r from SolicitudCredito r JOIN FETCH r.socio f WHERE f.cedula=:id ORDER BY r.id DESC")
                    .setParameter("id", id)
                    .getSingleResult();
        } catch (Exception e) {
            return new SolicitudCredito();
        } finally {
        }
    }

    private List<SolicitudCredito> listadeLicenciaSinFuncionario(String filtroFuncionario, String filtroAprobado, String filtroFormulario, Usuario usuario, String fechaDesde, String fechaHasta, String filtroRegional) {
        // String filterFechas = " (p.fecha>='" + fechaDesde + "' AND p.fechafin<='" + fechaHasta + "') ";
        String filterFechas = " (p.fecha::date>='" + fechaDesde + "' AND p.fecha::date<='" + fechaHasta + "') ";
        try { //Si teiene nivel jefe, encargado o administrador para que liste todos

            String sql = "select * ";
            if (filtroAprobado == null) {
                String parametro = filtroFormulario == null ? "%%" : "%" + filtroFormulario.toUpperCase() + "%";
                String regional = filtroRegional == null ? "%%" : "" + filtroRegional.toUpperCase() + "%";
                sql += "from solicitud_credito p left JOIN necesidad_ayuda form on form.id=p.id_necesidad";
                sql += " WHERE upper(form.descripcion) LIKE '" + parametro + "' AND " + filterFechas + " order by p.id ASC ";
            } else if (filtroAprobado != null) {
                long val = 0L;
                if (filtroAprobado.equalsIgnoreCase("APROBADO")) {
                    val = 1L;
                } else if (filtroAprobado.equalsIgnoreCase("RECHAZADO")) {
                    val = 2L;
                }
                String regional = filtroRegional == null ? "%%" : "" + filtroRegional.toUpperCase() + "%";
                String parametro = filtroFormulario == null ? "%%" : "" + filtroFormulario.toUpperCase() + "%";

                sql += "from solicitud_credito p left JOIN necesidad_ayuda form on form.id=p.id_necesidad";
                sql += " WHERE upper(form.descripcion) LIKE '" + parametro + "' and " + filterFechas + " AND UPPER(p.estado)='" + filtroAprobado.toUpperCase() + "' AND UPPER(p.estado) LIKE '" + filtroAprobado.toUpperCase() + "%' AND UPPER(reg.descripcion) LIKE '" + regional + "' order by p.id ASC ";
            }
            Query query = em.createNativeQuery(sql, SolicitudCredito.class);
            int i = query.getResultList().size();
            System.out.print(i);
            List<SolicitudCredito> results = query.getResultList();
            return results;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<SolicitudCredito> listadeSolicitudCreditoByFuncionarioAndUsuario(int init, int finish, String filtroFuncionario, String filtroAprobado, String filtroFormulario, Usuario usuario, Date fechaDesde, Date fechaHasta, String filtroRegional, String cedula, String dondeSeEntero, Long idRepresentante) {
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

        String filterFechas = "";
        if (fechaDesde != null || fechaHasta != null) {
            String fechaDesdeText = formatter.format(fechaDesde);
            String fechaHastaText = formatter.format(fechaHasta);
            filterFechas = " AND (p.fecha BETWEEN '" + fechaDesdeText + " 00:00:01' AND '" + fechaHastaText + " 23:59:59') ";
        }
        String dondeEntero = "";
        if (dondeSeEntero != null && !dondeSeEntero.equalsIgnoreCase("")) {
            dondeEntero = " AND UPPER(p.retiro)='" + dondeSeEntero.toUpperCase() + "' ";
        }
        String representante = "";
        if (idRepresentante != null) {
            representante = " AND (p.idrepresentante)=" + idRepresentante + " ";
        }
        //String filterFechas = " (p.fecha>='" + fechaDesdeText + "' AND p.fechafin<='" + fechaHastaText + "') ";

        if (filtroFormulario != null && filtroFormulario.equalsIgnoreCase("SOLICITUD DE PRODUCCIÓN")) {
            return null; //listadeLicenciaSinFuncionario(filtroFuncionario, filtroAprobado, filtroFormulario, usuario, fechaDesdeText, fechaHastaText, filtroRegional);
        } else {
            try {
                //Si teiene nivel jefe, encargado o administrador para que liste todos

                String ced = "";
                if (cedula != null && !cedula.equalsIgnoreCase("")) {
                    ced = " AND func.cedula LIKE '" + cedula.toUpperCase() + "' ";
                }
                String sql = "select * ";
                if (filtroAprobado == null) {
                    String socio = filtroFuncionario == null ? "%%" : "%" + filtroFuncionario.toUpperCase() + "%";
                    String parametro = filtroFormulario == null ? "%%" : "" + filtroFormulario.toUpperCase() + "%";
                    String regional = filtroRegional == null ? "%%" : "" + filtroRegional.toUpperCase() + "%";
                    sql += "from solicitud_credito p left join regional reg on p.idregional=reg.id ";
                    sql += " left join socio func on p.idsocio=func.id where ((concat(func.nombre, ' ', func.apellido) like '" + socio + "') OR func is null) AND UPPER(reg.descripcion) LIKE '" + regional + "' " + ced
                            + filterFechas + dondeEntero + representante;
                    sql += " order by p.id ASC ";
                } else if (filtroAprobado != null) {
                    long val = 0L;
                    if (filtroAprobado.equalsIgnoreCase("APROBADO")) {
                        val = 1L;
                    } else if (filtroAprobado.equalsIgnoreCase("RECHAZADO")) {
                        val = 2L;
                    }

                    String socio = filtroFuncionario == null ? "%%" : "%" + filtroFuncionario.toUpperCase() + "%";
                    String parametro = filtroFormulario == null ? "%%" : "" + filtroFormulario.toUpperCase() + "%";
                    String regional = filtroRegional == null ? "%%" : "" + filtroRegional.toUpperCase() + "%";
                    sql += "from solicitud_credito p left join regional reg on p.idregional=reg.id ";
                    sql += " left join socio func on p.idsocio=func.id where ((concat(func.nombre, ' ', func.apellido) like '" + socio + "') OR func is null) AND UPPER(p.estado) LIKE '" + filtroAprobado.toUpperCase() + "%' AND UPPER(reg.descripcion) LIKE '" + regional + "' " + ced
                            + filterFechas + dondeEntero + representante;
                    sql += " order by p.id ASC ";
                }
                Query query = em.createNativeQuery(sql, SolicitudCredito.class);
                int i = query.getResultList().size();
                System.out.print(i);
                List<SolicitudCredito> results = query.getResultList();
                return results;

            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    public SolicitudCredito listarPorIdgetAll(Long id) {
        try {
            return (SolicitudCredito) em.createQuery("select r from SolicitudCredito r JOIN FETCH r.socio f JOIN FETCH r.motivoSolicitudCredito m JOIN FETCH r.necesidadAyudaDos n WHERE r.id=:id ORDER BY r.id DESC")
                    .setParameter("id", id)
                    .getSingleResult();
        } catch (Exception e) {
            return new SolicitudCredito();
        } finally {
        }
    }

    public List<DatosFamiliaresAyuda> getSCredBySolicitud(String string) {
        return null;
    }
}
