/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import py.mutualmsp.mutualweb.entities.Analisis;
import py.mutualmsp.mutualweb.entities.Giraduria;

/**
 *
 * @author hectorvillalba
 */

@Stateless
public class AnalisisDao implements Serializable {
    
    @PersistenceContext
    EntityManager em;
    
    
    public List<Analisis> listaAnalisis() {
        List<Analisis> lista = new ArrayList<>();
        
        try {
            lista = em.createNamedQuery("select g from Analisis g order by g.id desc ")
                    .setMaxResults(50)
                    .getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }
 }
