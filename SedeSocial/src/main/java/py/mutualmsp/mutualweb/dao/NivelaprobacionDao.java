/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import py.mutualmsp.mutualweb.entities.Nivelaprobacion;


/**
 *
 * @author Dbarreto
 */
@Stateless
public class NivelaprobacionDao implements Serializable{
    @PersistenceContext
    private EntityManager em;
    
    public List<Nivelaprobacion> listaNivelaprobacion() {
        List<Nivelaprobacion> lista = new ArrayList<>();
        try {
            lista = em.createNamedQuery("Nivelaprobacion.findAll").getResultList();  
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }
    
    public void guardar(Nivelaprobacion nivelaprobacion){
        try {
            em.merge(nivelaprobacion);
        } catch (Exception e) {
            e.printStackTrace();
        }
                
    }
    
    public void borrar(Nivelaprobacion nivelaprobacion) {
        try {
            em.remove(em.contains(nivelaprobacion) ? nivelaprobacion : em.merge(nivelaprobacion));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<Nivelaprobacion> getListNivelaprobacion(String valor) {
        List<Nivelaprobacion> lista = new ArrayList<>();
       try {
           if (valor != null && !valor.equals("")) {
               lista = em.createNativeQuery("SELECT * FROM public.nivelaprobacion " +
"                   where  lower(descripcion) like lower('%"+valor+"%');", Nivelaprobacion.class)
                   .getResultList();
           }else{
               lista = em.createQuery("select n from Nivelaprobacion n")
                   .getResultList();
           }
       } catch (Exception e) {
           e.printStackTrace();
       }
       return lista;
    }
    
    public Nivelaprobacion getNinguno() {
        Nivelaprobacion ninguno = null;
        try {
            ninguno = (Nivelaprobacion) em.createNativeQuery("SELECT * FROM public.nivelaprobacion " +
            "               where  lower(descripcion) like lower('%NINGUNO%');", Nivelaprobacion.class)
                   .getSingleResult();       
        } catch (Exception e) {
            e.printStackTrace();
            ninguno = (Nivelaprobacion) em.createQuery("select n from Nivelaprobacion n")
                .getResultList();
        }
        return ninguno;
    }
}
