/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.dao;

import com.vaadin.ui.TextField;
import java.io.Serializable;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import py.mutualmsp.mutualweb.entities.AgendamientoDetalle;
import py.mutualmsp.mutualweb.util.DateUtils;

/**
 *
 * @author Dbarreto
 */
@Stateless
public class AgendamientoDetalleDao implements Serializable {

    @PersistenceContext
    private EntityManager em;

    public List<AgendamientoDetalle> listaAgendamientoDetallePorIdAgendamiento(Long idAgen) {
        List<AgendamientoDetalle> lista = new ArrayList<>();
        try {
            String sql = "SELECT * FROM sedesocial.agendamiento_detalle "
                    + "                   where idagendamiento=" + idAgen;
            System.out.println("AGEN DET: " + sql);
            lista = em.createNativeQuery(sql, AgendamientoDetalle.class)
                    .getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }

    public List<AgendamientoDetalle> listaUsoPiscinaPorIdAgendamiento(Long idAgen) {
        List<AgendamientoDetalle> lista = new ArrayList<>();
        try {
            String sql = "SELECT * FROM sedesocial.agendamiento_detalle ad LEFT JOIN sedesocial.agendamiento a  "
                    + " ON ad.idagendamiento=a.id where (a.confirmado=TRUE OR a.confirmado is NULL) AND ad.usopiscina=true AND ad.idagendamiento=" + idAgen;
            System.out.println("SQL HERE -> " + sql);
            lista = em.createNativeQuery(sql, AgendamientoDetalle.class)
                    .getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }

    public void guardar(AgendamientoDetalle cargo) {
        try {
            em.merge(cargo);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void borrar(AgendamientoDetalle cargo) {
        try {
            em.remove(em.contains(cargo) ? cargo : em.merge(cargo));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<AgendamientoDetalle> getListAgendamientoDetalle(String valor) {
        List<AgendamientoDetalle> lista = new ArrayList<>();
        try {
            if (valor != null && !valor.equals("")) {
                lista = em.createNativeQuery("SELECT * FROM sedesocial.agendamiento_detalle "
                        + "                   where  id=" + valor + ";", AgendamientoDetalle.class)
                        .getResultList();
            } else {
                lista = em.createQuery("select c from AgendamientoDetalle c")
                        .getResultList();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }

    public List<AgendamientoDetalle> getAgendamientoDetalleByDescripcion(String valor) {
        List<AgendamientoDetalle> lista = new ArrayList<>();
        try {
            if (valor != null && !valor.equals("")) {
                lista = em.createNativeQuery("SELECT * FROM sedesocial.agendamiento_detalle "
                        + "                   where lower(descripcion) LIKE '" + valor + "%';", AgendamientoDetalle.class)
                        .getResultList();
            } else {
                lista = em.createQuery("select c from AgendamientoDetalle c")
                        .getResultList();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }

    public AgendamientoDetalle getNinguno() {
        AgendamientoDetalle ninguno = null;
        try {
            ninguno = (AgendamientoDetalle) em.createNativeQuery("SELECT * FROM sedesocial.agendamiento_detalle "
                    + "               where  lower(descripcion) like lower('%NINGUNO%');", AgendamientoDetalle.class)
                    .getSingleResult();
        } catch (Exception e) {
            e.printStackTrace();
            ninguno = (AgendamientoDetalle) em.createQuery("select c from AgendamientoDetalle c")
                    .getResultList();
        }
        return ninguno;
    }

    public AgendamientoDetalle getByIdAgendamientoIdNucleo(String idAgendamiento, String idFliar) {
        AgendamientoDetalle lista = new AgendamientoDetalle();
        try {
            String sql = "SELECT * FROM sedesocial.agendamiento_detalle "
                    + " where  idagendamiento=" + idAgendamiento + " AND idsubordinado=" + idFliar + ";";
            System.out.println("SQL -->> " + sql);
            lista = (AgendamientoDetalle) em.createNativeQuery(sql, AgendamientoDetalle.class)
                    .getSingleResult();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }

    public AgendamientoDetalle getByIdAgendamientoIdInvitado(String idAgendamiento, String idinvitado) {
        AgendamientoDetalle lista = new AgendamientoDetalle();
        try {
            String sql = "SELECT * FROM sedesocial.agendamiento_detalle "
                    + " where  idagendamiento=" + idAgendamiento + " AND idinvitado=" + idinvitado + ";";
            System.out.println("SQL -->> " + sql);
            lista = (AgendamientoDetalle) em.createNativeQuery(sql, AgendamientoDetalle.class)
                    .getSingleResult();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }

    public List<AgendamientoDetalle> listaAgendamientoPiscinaDetallePorIdAgendamiento(Long idAgen) {
        List<AgendamientoDetalle> lista = new ArrayList<>();
        try {
            lista = em.createNativeQuery("SELECT * FROM sedesocial.agendamiento_detalle "
                    + "                   where usopiscina=TRUE AND idagendamiento=" + idAgen, AgendamientoDetalle.class)
                    .getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }

    public long getIdAgendamiento(String ced, LocalDate fechahoy) {
        long id = 0;
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        String fechaDesdeText = formatter.format(DateUtils.asDate(fechahoy));
        try {
            String sql = "SELECT ad.idagendamiento FROM sedesocial.agendamiento_detalle ad left join sedesocial.invitado inv on inv.id=ad.idinvitado "
                    + " left join sedesocial.agendamiento ag on ag.id=ad.idagendamiento "
                    + " left join subordinado sub on sub.id=ad.idsubordinado where (inv.cedula='" + ced + "' or sub.cedula='" + ced + "') and a.fecha'" + fechaDesdeText + " 00:00:00' limit 1";
            System.out.println("SQL -->> " + sql);
            BigDecimal result = (BigDecimal) em.createNativeQuery(sql)
                    .getSingleResult();
            id = result.longValue();
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("--> " + e.getLocalizedMessage());
            System.out.println("-> " + e.fillInStackTrace());
        }
        return id;
    }
}
