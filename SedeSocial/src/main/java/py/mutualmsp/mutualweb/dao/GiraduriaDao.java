/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import py.mutualmsp.mutualweb.entities.Giraduria;
import py.mutualmsp.mutualweb.entities.Rubro;

/**
 *
 * @author hectorvillalba
 */

@Stateless
public class GiraduriaDao implements Serializable {

    @PersistenceContext
    private EntityManager em;
    
    public List<Giraduria> listaGiraduria() {
        List<Giraduria> lista = new ArrayList<>();
        
        try {
            lista = em.createQuery("select g from Giraduria g ").getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }
}
