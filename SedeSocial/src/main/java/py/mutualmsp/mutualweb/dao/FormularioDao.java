/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import py.mutualmsp.mutualweb.entities.Cargo;
import py.mutualmsp.mutualweb.entities.Formulario;

/**
 *
 * @author Dbarreto
 */
@Stateless
public class FormularioDao implements Serializable {

    @PersistenceContext
    private EntityManager em;

    public List<Formulario> listaFormulario() {
        List<Formulario> lista = new ArrayList<>();

        try {
            lista = em.createQuery("select c from Formulario c").getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }

    public void guardar(Formulario cargo) {
        try {
            em.merge(cargo);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void borrar(Formulario cargo) {
        try {
            em.remove(em.contains(cargo) ? cargo : em.merge(cargo));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<Formulario> getListFormulario(String valor) {
        List<Formulario> lista = new ArrayList<>();
        try {
            if (valor != null && !valor.equals("")) {
                lista = em.createNativeQuery("SELECT * FROM public.formulario "
                        + "                   where  id=" + valor + ";", Cargo.class)
                        .getResultList();
            } else {
                lista = em.createQuery("select c from Formulario c")
                        .getResultList();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }

    public List<Formulario> getFormularioByDescripcion(String valor) {
        List<Formulario> lista = new ArrayList<>();
        try {
            if (valor != null && !valor.equals("")) {
                lista = em.createNativeQuery("SELECT * FROM public.formulario "
                        + "                   where lower(descripcion) LIKE '%" + valor + "%';", Formulario.class)
                        .getResultList();
            } else {
                lista = em.createQuery("select c from Formulario c")
                        .getResultList();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }

    public Formulario getNinguno() {
        Formulario ninguno = null;
        try {
            ninguno = (Formulario) em.createNativeQuery("SELECT * FROM public.formulario "
                    + "               where  lower(descripcion) like lower('%NINGUNO%');", Formulario.class)
                    .getSingleResult();
        } catch (Exception e) {
            e.printStackTrace();
            ninguno = (Formulario) em.createQuery("select c from Formulario c")
                    .getResultList();
        }
        return ninguno;
    }
}
