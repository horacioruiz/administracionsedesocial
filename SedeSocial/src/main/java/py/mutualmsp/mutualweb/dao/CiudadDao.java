/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import py.mutualmsp.mutualweb.entities.Ciudad;


/**
 *
 * @author Dbarreto
 */
@Stateless
public class CiudadDao implements Serializable{
    @PersistenceContext
    private EntityManager em;
    
    public List<Ciudad> listaCiudad() {
        List<Ciudad> lista = new ArrayList<>();
        
        try {
            lista = em.createNamedQuery("Ciudad.findAll").getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }
    
    public void guardar(Ciudad ciudad){
        try {
            em.merge(ciudad);
        } catch (Exception e) {
            e.printStackTrace();
        }
                
    }
    
    public void borrar(Ciudad ciudad) {
        try {
            em.remove(em.contains(ciudad) ? ciudad : em.merge(ciudad));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<Ciudad> getListCiudad(String valor) {
        List<Ciudad> lista = new ArrayList<>();
       try {
           if (valor != null && !valor.equals("")) {
               lista = em.createNativeQuery("SELECT * FROM public.ciudad " +
"                   where  id="+valor+";", Ciudad.class)
                   .getResultList();
           }else{
               lista = em.createQuery("select c from Ciudad c")
                   .getResultList();
           }
       } catch (Exception e) {
           e.printStackTrace();
       }
       return lista;
    }
    
    
}
