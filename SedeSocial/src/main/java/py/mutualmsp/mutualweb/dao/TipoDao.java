/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import py.mutualmsp.mutualweb.entities.Tipo;

/**
 *
 * @author Dbarreto
 */
@Stateless
public class TipoDao implements Serializable {

    @PersistenceContext
    private EntityManager em;

    public List<Tipo> listaTipo() {
        List<Tipo> lista = new ArrayList<>();

        try {
            lista = em.createNamedQuery("Tipo.findAll").getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }

    public void guardar(Tipo cargo) {
        try {
            em.merge(cargo);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void borrar(Tipo cargo) {
        try {
            em.remove(em.contains(cargo) ? cargo : em.merge(cargo));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<Tipo> getListTipo(String valor) {
        List<Tipo> lista = new ArrayList<>();
        try {
            if (valor != null && !valor.equals("")) {
                lista = em.createNativeQuery("SELECT * FROM public.tipo "
                        + "                   where  id=" + valor + ";", Tipo.class)
                        .getResultList();
            } else {
                lista = em.createQuery("select c from Tipo c")
                        .getResultList();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }

    public List<Tipo> getTipoByDescripcion(String valor) {
        List<Tipo> lista = new ArrayList<>();
        try {
            if (valor != null && !valor.equals("")) {
                lista = em.createNativeQuery("SELECT * FROM public.tipo "
                        + "                   where lower(descripcion) LIKE '" + valor + "%';", Tipo.class)
                        .getResultList();
            } else {
                lista = em.createQuery("select c from Tipo c")
                        .getResultList();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }

    public Tipo getNinguno() {
        Tipo ninguno = null;
        try {
            ninguno = (Tipo) em.createNativeQuery("SELECT * FROM public.tipo "
                    + "               where  lower(descripcion) like lower('%NINGUNO%');", Tipo.class)
                    .getSingleResult();
        } catch (Exception e) {
            e.printStackTrace();
            ninguno = (Tipo) em.createQuery("select c from Tipo c")
                    .getResultList();
        }
        return ninguno;
    }
}
