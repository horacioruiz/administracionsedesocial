/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import py.mutualmsp.mutualweb.entities.Socio;

/**
 *
 * @author Dbarreto
 */
@Stateless
public class SocioDao implements Serializable {

    @PersistenceContext
    private EntityManager em;

    public List<Socio> listaSocio() {
        List<Socio> lista = new ArrayList<>();
        try {
            //lista = em.createNamedQuery("Socio.findAll").getResultList();  
            lista = em.createNativeQuery("SELECT * FROM public.socio;", Socio.class)
                    .getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }

    public List<Socio> listaVerificador() {
        List<Socio> lista = new ArrayList<>();
        try {
            //lista = em.createNamedQuery("Socio.findAll").getResultList();  
            lista = em.createNativeQuery("SELECT * FROM public.socio "
                    + "                   where  idestado=13 and iddependencia=13;", Socio.class)
                    .getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }

    public Socio guardar(Socio socio) {
        if (socio.getId() == null) {
            em.persist(socio);
        } else {
            em.merge(socio);
        }
        em.flush();
        return socio;
    }

    public void borrar(Socio socio) {
        try {
            em.remove(em.contains(socio) ? socio : em.merge(socio));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<Socio> getListSocio(String value) {
        List<Socio> lista = new ArrayList<>();
        try {
            if (value != null && !value.equals("")) {
                lista = em.createNativeQuery("SELECT * FROM public.socio "
                        + "                   where  lower(cedula || nombre || apellido) like lower('%" + value + "%') AND idestado=13;", Socio.class)
                        .getResultList();
            } else {
                lista = em.createQuery("select f from Socio f WHERE f.idestado=13")
                        .getResultList();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }

    public Socio getNinguno() {
        Socio ninguno = null;
        try {
            ninguno = (Socio) em.createNativeQuery("SELECT * FROM public.socio "
                    + " where  lower(nombre) like lower('%NINGUNO%') AND idestado=13;", Socio.class)
                    .getSingleResult();
        } catch (Exception e) {
            e.printStackTrace();
            ninguno = (Socio) em.createQuery("select f from Socio f WHERE f.idestado=13")
                    .getResultList();
        }
        return ninguno;
    }

    public Socio getSocio(String valor) {
        Socio socio = null;
        try {
            socio = (Socio) em.createNativeQuery("SELECT * FROM public.socio f "
                    + "left join usuario u ON  f.idsocio = u.idsocio "
                    + "where  lower(u.usuario) like lower('%" + valor + "%') AND f.idestado=13;", Socio.class)
                    .getSingleResult();
        } catch (Exception e) {
            e.printStackTrace();
            socio = (Socio) em.createQuery("select f from Socio f WHERE f.idestado=13")
                    .getResultList();
        }
        return socio;
    }

    public Socio listarSocioPorCI(String value) {
        Socio lista = new Socio();
        try {
            lista = (Socio) em.createQuery("select c from Socio c WHERE c.cedula=:ci")
                    .setParameter("ci", value).getResultList().get(0);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("--> " + e.getLocalizedMessage());
            System.out.println("-> " + e.fillInStackTrace());
        }
        return lista;
    }
    public Socio listarSocioPorId(String value) {
        Socio lista = new Socio();
        try {
            lista = (Socio) em.createQuery("select c from Socio c WHERE c.id=:ci")
                    .setParameter("ci", Long.parseLong(value)).getResultList().get(0);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("--> " + e.getLocalizedMessage());
            System.out.println("-> " + e.fillInStackTrace());
        }
        return lista;
    }

    public List<Socio> ListarPorDependencia(Long iddependencia) {
        List<Socio> lista = new ArrayList<>();
        try {
            lista = em.createQuery("select c from Socio c JOIN FETCH c.dependencia dep WHERE dep.iddependencia=:idDep AND c.idestado=13")
                    .setParameter("idDep", iddependencia).getResultList();
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("--> " + e.getLocalizedMessage());
            System.out.println("-> " + e.fillInStackTrace());
        }
        return lista;
    }

    public Socio listarPorNombreApellido(String nombreApellido) {
        String[] parts = nombreApellido.split(" ");
        Socio lista = new Socio();
        try {
            lista = (Socio) em.createQuery("select func from Socio func WHERE UPPER(func.nombre) LIKE :nombre AND UPPER(func.apellido) LIKE :apellido AND func.idestado=13")
                    .setParameter("nombre", "%" + parts[0] + "%")
                    .setParameter("apellido", "%" + parts[1] + "%")
                    .getResultList().get(0);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("--> " + e.getLocalizedMessage());
            System.out.println("-> " + e.fillInStackTrace());
        }
        return lista;
    }

    public Socio listarPorApellidoNombre(String apellidoNombre) {
        String[] parts = apellidoNombre.split(" ");
        Socio lista = new Socio();
        try {
            lista = (Socio) em.createQuery("select func from Socio func WHERE UPPER(func.nombre) LIKE :nombre AND UPPER(func.apellido) LIKE :apellido AND func.idestado=13")
                    .setParameter("nombre", "%" + parts[1] + "%")
                    .setParameter("apellido", "%" + parts[0] + "%")
                    .getResultList().get(0);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("--> " + e.getLocalizedMessage());
            System.out.println("-> " + e.fillInStackTrace());
        }
        return lista;
    }
}
