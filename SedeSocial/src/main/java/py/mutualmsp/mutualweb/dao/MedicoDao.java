/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import py.mutualmsp.mutualweb.entities.Medico;

/**
 *
 * @author Dbarreto
 */
@Stateless
public class MedicoDao implements Serializable {

    @PersistenceContext
    private EntityManager em;

    public List<Medico> listaMedico() {
        List<Medico> lista = new ArrayList<>();
        try {
            //lista = em.createNamedQuery("Medico.findAll").getResultList();  
            lista = em.createNativeQuery("SELECT * FROM sedesocial.medico;", Medico.class)
                    .getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }

    
    public Medico guardar(Medico medico) {
        if (medico.getId() == null) {
            em.persist(medico);
        } else {
            em.merge(medico);
        }
        em.flush();
        return medico;
    }

    public void borrar(Medico medico) {
        try {
            em.remove(em.contains(medico) ? medico : em.merge(medico));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<Medico> getListMedico(String value) {
        List<Medico> lista = new ArrayList<>();
        try {
            if (value != null && !value.equals("")) {
                lista = em.createNativeQuery("SELECT * FROM sedesocial.medico "
                        + "                   where  lower( nombre || apellido) like lower('%" + value + "%') AND activo=true;", Medico.class)
                        .getResultList();
            } else {
                lista = em.createQuery("select m from Medico m WHERE m.activo=true")
                        .getResultList();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }

    public Medico getNinguno() {
        Medico ninguno = null;
        try {
            ninguno = (Medico) em.createNativeQuery("SELECT * FROM sedesocial.medico "
                    + " where  lower(nombre) like lower('%NINGUNO%') AND activo=true;", Medico.class)
                    .getSingleResult();
        } catch (Exception e) {
            e.printStackTrace();
            ninguno = (Medico) em.createQuery("select m from Medico m WHERE m.activo=true")
                    .getResultList();
        }
        return ninguno;
    }

    
}
