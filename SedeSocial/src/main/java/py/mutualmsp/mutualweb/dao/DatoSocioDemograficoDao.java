/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import py.mutualmsp.mutualweb.entities.DatosFamiliaresAyuda;
import py.mutualmsp.mutualweb.entities.DatosSocioDemograficos;

/**
 *
 * @author Dbarreto
 */
@Stateless
public class DatoSocioDemograficoDao implements Serializable {

    @PersistenceContext
    private EntityManager em;

    public List<DatosSocioDemograficos> listaDatosSocioDemograficos() {
        List<DatosSocioDemograficos> lista = new ArrayList<>();

        try {
            lista = em.createNamedQuery("DatosSocioDemograficos.findAll").getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }

    public void guardar(DatosSocioDemograficos cargo) {
        try {
            em.merge(cargo);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void borrar(DatosSocioDemograficos cargo) {
        try {
            em.remove(em.contains(cargo) ? cargo : em.merge(cargo));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<DatosSocioDemograficos> getListDatosSocioDemograficos(String valor) {
        List<DatosSocioDemograficos> lista = new ArrayList<>();
        try {
            if (valor != null && !valor.equals("")) {
                lista = em.createNativeQuery("SELECT * FROM public.datos_socio_demograficos "
                        + "                   where  id=" + valor + ";", DatosSocioDemograficos.class)
                        .getResultList();
            } else {
                lista = em.createQuery("select c from DatosSocioDemograficos c")
                        .getResultList();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }

    public List<DatosSocioDemograficos> getDatosSocioDemograficosByDescripcion(String valor) {
        List<DatosSocioDemograficos> lista = new ArrayList<>();
        try {
            if (valor != null && !valor.equals("")) {
                lista = em.createNativeQuery("SELECT * FROM public.datos_socio_demograficos "
                        + "                   where lower(descripcion) LIKE '" + valor + "%';", DatosSocioDemograficos.class)
                        .getResultList();
            } else {
                lista = em.createQuery("select c from DatosSocioDemograficos c")
                        .getResultList();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }

    public DatosSocioDemograficos getNinguno() {
        DatosSocioDemograficos ninguno = null;
        try {
            ninguno = (DatosSocioDemograficos) em.createNativeQuery("SELECT * FROM public.datos_socio_demograficos "
                    + "               where  lower(descripcion) like lower('%NINGUNO%');", DatosSocioDemograficos.class)
                    .getSingleResult();
        } catch (Exception e) {
            e.printStackTrace();
            ninguno = (DatosSocioDemograficos) em.createQuery("select c from DatosSocioDemograficos c")
                    .getResultList();
        }
        return ninguno;
    }

    public List<DatosSocioDemograficos> listarPorIdSolicitud(Long id) {
        List<DatosSocioDemograficos> lista = new ArrayList<>();
        try {
            lista = em.createNativeQuery("SELECT * FROM public.datos_socio_demograficos "
                    + "                   where id_solicitud_ayuda=" + id + ";", DatosSocioDemograficos.class)
                    .getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }
}
