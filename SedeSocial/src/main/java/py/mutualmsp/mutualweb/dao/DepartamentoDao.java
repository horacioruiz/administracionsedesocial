/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import py.mutualmsp.mutualweb.entities.Departamento;

/**
 *
 * @author Dbarreto
 */
@Stateless
public class DepartamentoDao implements Serializable {

    @PersistenceContext
    private EntityManager em;

    public List<Departamento> listaDepartamento() {
        List<Departamento> lista = new ArrayList<>();

        try {
            lista = em.createNamedQuery("Departamento.findAll").getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }

    public void guardar(Departamento cargo) {
        try {
            em.merge(cargo);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void borrar(Departamento cargo) {
        try {
            em.remove(em.contains(cargo) ? cargo : em.merge(cargo));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<Departamento> getListDepartamento(String valor) {
        List<Departamento> lista = new ArrayList<>();
        try {
            if (valor != null && !valor.equals("")) {
                lista = em.createNativeQuery("SELECT * FROM public.departamento "
                        + "                   where  id=" + valor + ";", Departamento.class)
                        .getResultList();
            } else {
                lista = em.createQuery("select c from Departamento c")
                        .getResultList();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }

    public List<Departamento> getDepartamentoByDescripcion(String valor) {
        List<Departamento> lista = new ArrayList<>();
        try {
            if (valor != null && !valor.equals("")) {
                lista = em.createNativeQuery("SELECT * FROM public.departamento "
                        + "                   where lower(descripcion) LIKE '" + valor + "%';", Departamento.class)
                        .getResultList();
            } else {
                lista = em.createQuery("select c from Departamento c")
                        .getResultList();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }

    public Departamento getNinguno() {
        Departamento ninguno = null;
        try {
            ninguno = (Departamento) em.createNativeQuery("SELECT * FROM public.departamento "
                    + "               where  lower(descripcion) like lower('%NINGUNO%');", Departamento.class)
                    .getSingleResult();
        } catch (Exception e) {
            e.printStackTrace();
            ninguno = (Departamento) em.createQuery("select c from Departamento c")
                    .getResultList();
        }
        return ninguno;
    }
}
