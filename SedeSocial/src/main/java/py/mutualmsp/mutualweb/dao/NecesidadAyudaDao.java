/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import py.mutualmsp.mutualweb.entities.NecesidadAyuda;

/**
 *
 * @author Dbarreto
 */
@Stateless
public class NecesidadAyudaDao implements Serializable {

    @PersistenceContext
    private EntityManager em;

    public List<NecesidadAyuda> listaNecesidadAyuda() {
        List<NecesidadAyuda> lista = new ArrayList<>();

        try {
            lista = em.createNamedQuery("NecesidadAyuda.findAll").getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }

    public void guardar(NecesidadAyuda necesidad_ayuda) {
        try {
            em.merge(necesidad_ayuda);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void borrar(NecesidadAyuda necesidad_ayuda) {
        try {
            em.remove(em.contains(necesidad_ayuda) ? necesidad_ayuda : em.merge(necesidad_ayuda));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<NecesidadAyuda> getListNecesidadAyuda(String valor) {
        List<NecesidadAyuda> lista = new ArrayList<>();
        try {
            if (valor != null && !valor.equals("")) {
                lista = em.createNativeQuery("SELECT * FROM public.necesidad_ayuda "
                        + "                   where  id=" + valor + ";", NecesidadAyuda.class)
                        .getResultList();
            } else {
                lista = em.createQuery("select c from NecesidadAyuda c")
                        .getResultList();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }

    public List<NecesidadAyuda> getNecesidadAyudaByDescripcion(String valor) {
        List<NecesidadAyuda> lista = new ArrayList<>();
        try {
            if (valor != null && !valor.equals("")) {
                lista = em.createNativeQuery("SELECT * FROM public.necesidad_ayuda "
                        + "                   where lower(descripcion) LIKE '" + valor + "%';", NecesidadAyuda.class)
                        .getResultList();
            } else {
                lista = em.createQuery("select c from NecesidadAyuda c")
                        .getResultList();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }

    public NecesidadAyuda getNinguno() {
        NecesidadAyuda ninguno = null;
        try {
            ninguno = (NecesidadAyuda) em.createNativeQuery("SELECT * FROM public.necesidad_ayuda "
                    + "               where  lower(descripcion) like lower('%NINGUNO%');", NecesidadAyuda.class)
                    .getSingleResult();
        } catch (Exception e) {
            e.printStackTrace();
            ninguno = (NecesidadAyuda) em.createQuery("select c from NecesidadAyuda c")
                    .getResultList();
        }
        return ninguno;
    }
}
