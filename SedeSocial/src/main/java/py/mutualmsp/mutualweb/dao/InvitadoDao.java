/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.dao;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import py.mutualmsp.mutualweb.dto.InvitadoDto;
import py.mutualmsp.mutualweb.entities.Invitado;
import py.mutualmsp.mutualweb.entities.Socio;

/**
 *
 * @author Dbarreto
 */
@Stateless
public class InvitadoDao implements Serializable {

    @PersistenceContext
    private EntityManager em;

    public List<Invitado> listaInvitado() {
        List<Invitado> lista = new ArrayList<>();

        try {
            lista = em.createNamedQuery("Invitado.findAll").getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }

    public void guardar(Invitado cargo) {
        try {
            em.merge(cargo);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void borrar(Invitado cargo) {
        try {
            em.remove(em.contains(cargo) ? cargo : em.merge(cargo));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<Invitado> getListInvitado(String valor) {
        List<Invitado> lista = new ArrayList<>();
        try {
            if (valor != null && !valor.equals("")) {
                lista = em.createNativeQuery("SELECT * FROM public.cargo "
                        + "                   where  id=" + valor + ";", Invitado.class)
                        .getResultList();
            } else {
                lista = em.createQuery("select c from Invitado c")
                        .getResultList();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }

    public List<Invitado> getInvitadoByDescripcion(String valor) {
        List<Invitado> lista = new ArrayList<>();
        try {
            if (valor != null && !valor.equals("")) {
                lista = em.createNativeQuery("SELECT * FROM public.cargo "
                        + "                   where lower(descripcion) LIKE '" + valor + "%';", Invitado.class)
                        .getResultList();
            } else {
                lista = em.createQuery("select c from Invitado c")
                        .getResultList();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }

    public Invitado getNinguno() {
        Invitado ninguno = null;
        try {
            ninguno = (Invitado) em.createNativeQuery("SELECT * FROM public.cargo "
                    + "               where  lower(descripcion) like lower('%NINGUNO%');", Invitado.class)
                    .getSingleResult();
        } catch (Exception e) {
            e.printStackTrace();
            ninguno = (Invitado) em.createQuery("select c from Invitado c")
                    .getResultList();
        }
        return ninguno;
    }

    public Invitado getById(long parseLong) {
        Invitado ninguno = null;
        try {
            String sql = "SELECT * FROM sedesocial.invitado "
                    + " where id=" + parseLong;
            System.out.println("SQL -> " + sql);
            ninguno = (Invitado) em.createNativeQuery(sql, Invitado.class)
                    .getSingleResult();
        } catch (Exception e) {
            e.printStackTrace();

        }
        return ninguno;
    }

    public List<Invitado> listarInvitadoData(String valor, String ci) {
        List<Invitado> lista = new ArrayList<>();

        try {
            if (!valor.equalsIgnoreCase("") && !ci.equalsIgnoreCase("")) {
                lista = em.createNativeQuery("SELECT * FROM sedesocial.invitado inv left join public.socio s on s.id=inv.idsocio "
                        + "                   where inv.cedula='" + ci + "' AND s.cedula='" + valor + "' AND inv.habilitado=TRUE", Invitado.class)
                        .getResultList();
            } else if (!valor.equalsIgnoreCase("")) {
                lista = em.createNativeQuery("SELECT * FROM sedesocial.invitado inv left join public.socio s on s.id=inv.idsocio "
                        + " where s.cedula='" + valor + "' AND inv.habilitado=TRUE", Invitado.class)
                        .getResultList();
            } else if (!ci.equalsIgnoreCase("")) {
                lista = em.createNativeQuery("SELECT * FROM sedesocial.invitado inv left join public.socio s on s.id=inv.idsocio "
                        + " where inv.cedula='" + ci + "' AND inv.habilitado=TRUE", Invitado.class)
                        .getResultList();
            } else {
                lista = em.createNativeQuery("SELECT * FROM sedesocial.invitado WHERE inv.habilitado=TRUE", Invitado.class)
                        .getResultList();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }
    public List<Invitado> listarInvitadoPorCI(String ci) {
        List<Invitado> lista = new ArrayList<>();

        try {
            lista = em.createNativeQuery("SELECT * FROM sedesocial.invitado inv left join public.socio s on s.id=inv.idsocio "
                        + " where inv.cedula='" + ci + "'", Invitado.class)
                        .getResultList();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }

    public List<InvitadoDto> listaInvitadoDto() {
        List<Invitado> lista = new ArrayList<>();
        List<InvitadoDto> listaDto = new ArrayList<>();
        try {
            lista = em.createNamedQuery("Invitado.findAll").getResultList();

            for (Invitado invi : lista) {
                try {
                    Socio soc = (Socio) em.createNativeQuery("SELECT * FROM public.socio WHERE id=" + invi.getIdsocio(), Socio.class).getSingleResult();

                    InvitadoDto inviDto = new InvitadoDto();
                    inviDto.setId(invi.getId());
                    inviDto.setCisocio(soc.getCedula());
                    inviDto.setCedula(invi.getCedula());
                    inviDto.setParentesco(invi.getParentesco());
                    inviDto.setNombre(invi.getNombre());
                    inviDto.setApellido(invi.getApellido());

                    inviDto.setDoc1(invi.getDoc1());
                    inviDto.setDoc2(invi.getDoc2());
                    inviDto.setDoc3(invi.getDoc3());
                    inviDto.setHabilitado(invi.getHabilitado());
                    inviDto.setObservacion(invi.getObservacion());
                    inviDto.setCelular(invi.getCelular());
                    listaDto.add(inviDto);
                } catch (Exception e) {
                } finally {
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return listaDto;
    }

    public Socio consultarSocio(BigInteger idSocio) {
        try {
            return (Socio) em.createNativeQuery("SELECT * FROM public.socio WHERE id=" + idSocio, Socio.class).getSingleResult();
        } catch (Exception e) {
            return new Socio();
        } finally {
        }
    }
}
