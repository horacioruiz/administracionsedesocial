/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.dao;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TemporalType;
import py.mutualmsp.mutualweb.entities.Inspeccion;
import py.mutualmsp.mutualweb.entities.Invitado;
import py.mutualmsp.mutualweb.entities.NucleoFamiliar;
import py.mutualmsp.mutualweb.entities.Parentesco;
import py.mutualmsp.mutualweb.entities.Socio;
import py.mutualmsp.mutualweb.entities.Tipoincidencia;
import py.mutualmsp.mutualweb.util.DateUtils;

/**
 *
 * @author Dbarreto
 */
@Stateless
public class InspeccionDao implements Serializable {

    @PersistenceContext
    private EntityManager em;

    public List<Inspeccion> listaInspeccion() {
        List<Inspeccion> lista = new ArrayList<>();
        try {
//            lista = em.createNamedQuery("Inspeccion.findAll").getResultList();
            lista = em.createNativeQuery("select * from sedesocial.inspeccion ORDER BY id DESC LIMIT 150", Inspeccion.class)
                    .getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }

    public List<Inspeccion> getInspeccionByFecha(java.util.Date fdesde, java.util.Date fhasta) {
        List<Inspeccion> lista = new ArrayList<>();
        try {
            lista = em.createQuery("select * from sedesocial.inspeccion e where e.fecha between :fdesde and :fhasta ORDER BY e.id DESC")
                    .setMaxResults(150)
                    .setParameter("fdesde", fdesde, TemporalType.DATE)
                    .setParameter("fhasta", fhasta, TemporalType.DATE)
                    .getResultList();
        } catch (Exception e) {
        }
        return lista;
    }

    public List<Inspeccion> listarInspeccionPorCI(String valor) {
        List<Inspeccion> lista = new ArrayList<>();
        try {
            if (valor != null && !valor.equals("")) {
                lista = em.createNativeQuery("SELECT * FROM sedesocial.enfermeria "
                        + "                   where  cedula='" + valor + "';", Inspeccion.class)
                        .getResultList();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }

    public Inspeccion listarInspeccionPorId(String valor) {
        Inspeccion lista = new Inspeccion();
        try {
            if (valor != null && !valor.equals("")) {
                lista = (Inspeccion) em.createNativeQuery("SELECT * FROM sedesocial.inspeccion "
                        + "                   where  id=" + valor + ";", Inspeccion.class)
                        .getSingleResult();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }

    public void guardar(Inspeccion cargo) {
        try {
            cargo.setNombre(cargo.getNombre().toUpperCase());
            cargo.setApellido(cargo.getApellido().toUpperCase());
            em.merge(cargo);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void borrar(Inspeccion cargo) {
        try {
            em.remove(em.contains(cargo) ? cargo : em.merge(cargo));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<Inspeccion> getListInspeccion(String valor) {
        List<Inspeccion> lista = new ArrayList<>();
        try {
            if (valor != null && !valor.equals("")) {
                lista = em.createNativeQuery("SELECT * FROM sedesocial.enfermeria "
                        + "                   where  id=" + valor + ";", Inspeccion.class)
                        .getResultList();
            } else {
                lista = em.createQuery("select c from Inspeccion c")
                        .getResultList();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }
    
    public String recuperarParentesco(String cedula) {
        try {
            NucleoFamiliar nf = (NucleoFamiliar) em.createNativeQuery("SELECT * FROM subordinado where cedula='" + cedula + "'", NucleoFamiliar.class).setMaxResults(0).getSingleResult();
            Parentesco par = (Parentesco) em.createNativeQuery("SELECT * FROM parentesco where id=" + nf.getIdparentesco(), Parentesco.class).setMaxResults(0).getSingleResult();
            Socio soc = (Socio) em.createNativeQuery("SELECT * FROM socio where id=" + nf.getIdsocio(), Socio.class).setMaxResults(0).getSingleResult();
//            inspeccionDTO.setSocio(soc.getCedula() + " | " + soc.getNombreCompleto().toUpperCase());
            return par.getDescripcion().toUpperCase();
        } catch (Exception e) {
            try {
                Invitado nf = (Invitado) em.createNativeQuery("SELECT * FROM sedesocial.invitado where cedula='" + cedula + "'", Invitado.class).setMaxResults(0).getSingleResult();
                Socio soc = (Socio) em.createNativeQuery("SELECT * FROM socio where id=" + nf.getIdsocio(), Socio.class).setMaxResults(0).getSingleResult();

//                inspeccionDTO.setSocio(soc.getCedula() + " | " + soc.getNombreCompleto().toUpperCase());
                return nf.getParentesco().toUpperCase();
            } catch (Exception ec) {
                Socio soc = (Socio) em.createNativeQuery("SELECT * FROM socio where cedula LIKE '%" + cedula + "%'", Socio.class).setMaxResults(0).getSingleResult();
//                inspeccionDTO.setSocio(soc.getCedula() + " | " + soc.getNombreCompleto().toUpperCase());
                return "SOCIO";
            } finally {
            }
        } finally {
        }
    }

    public List<Inspeccion> recuperarInspeccion(String valor) {
        List<Inspeccion> lista = new ArrayList<>();
        try {
            if (valor != null && !valor.equals("")) {
                lista = em.createNativeQuery("SELECT * FROM sedesocial.inspeccion "
                        + "                   where cedula='" + valor + "';", Inspeccion.class)
                        .getResultList();
            } else {
                lista = em.createQuery("select c from Inspeccion c")
                        .getResultList();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }

    public List<Inspeccion> getInspeccionByDescripcion(String valor) {
        List<Inspeccion> lista = new ArrayList<>();
        try {
            if (valor != null && !valor.equals("")) {
                lista = em.createNativeQuery("SELECT * FROM sedesocial.enfermeria "
                        + "                   where lower(descripcion) LIKE '" + valor + "%';", Inspeccion.class)
                        .getResultList();
            } else {
                lista = em.createQuery("select c from Inspeccion c")
                        .getResultList();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }

    public List<Inspeccion> getInspeccionByTipoIncidencia(Long id) {
        List<Inspeccion> lista = new ArrayList<>();
        try {
            lista = em.createQuery("select e from Inspeccion e where e.idtipoIncidencia = :id ")
                    .setParameter("id", id)
                    .getResultList();
        } catch (Exception e) {
            e.printStackTrace();;
        }
        return lista;
    }

    public Inspeccion getNinguno() {
        Inspeccion ninguno = null;
        try {
            ninguno = (Inspeccion) em.createNativeQuery("SELECT * FROM sedesocial.enfermeria "
                    + "               where  lower(descripcion) like lower('%NINGUNO%');", Inspeccion.class)
                    .getSingleResult();
        } catch (Exception e) {
            e.printStackTrace();
            ninguno = (Inspeccion) em.createQuery("select c from Inspeccion c")
                    .getResultList();
        }
        return ninguno;
    }

    public List<Inspeccion> getDataFromFilter(String cedula, LocalDate desde, LocalDate hasta, Tipoincidencia incidencia) {
        List<Inspeccion> lista = new ArrayList<>();
        try {

            String ced = "";
            if (!cedula.equalsIgnoreCase("")) {
                ced = " e.cedula='" + cedula + "'";
            }
            String inci = "";
            try {
                if (incidencia.getId() != null) {
                    inci = " tp.id=" + incidencia.getId() + "";
                }
            } catch (Exception e) {
            } finally {
            }
            String fechaDesdeText = "";
            String fechaHastaText = "";
            try {
                DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                fechaDesdeText = formatter.format(DateUtils.asDate(desde));
                fechaHastaText = formatter.format(DateUtils.asDate(hasta));
            } catch (Exception e) {
            } finally {
            }

            if (!ced.equalsIgnoreCase("") && !inci.equalsIgnoreCase("") && !fechaDesdeText.equalsIgnoreCase("")) {
                lista = em.createQuery("select e from Inspeccion e where "
                        + " (e.fecha BETWEEN '" + fechaDesdeText + "' AND '" + fechaHastaText + "') AND " + ced + " AND " + inci)
                        .getResultList();
            } else if (!ced.equalsIgnoreCase("") && !inci.equalsIgnoreCase("")) {
                lista = em.createQuery("select e from Inspeccion e where "
                        + " " + ced + " AND " + inci)
                        .getResultList();
            } else if (!ced.equalsIgnoreCase("") && !fechaDesdeText.equalsIgnoreCase("")) {
                lista = em.createQuery("select e from Inspeccion e where "
                        + " " + ced + " AND (e.fecha BETWEEN '" + fechaDesdeText + "' AND '" + fechaHastaText + "')")
                        .getResultList();
            } else if (!fechaDesdeText.equalsIgnoreCase("")) {
                lista = em.createQuery("select e from Inspeccion e where "
                        + " (e.fecha BETWEEN '" + fechaDesdeText + "' AND '" + fechaHastaText + "')")
                        .getResultList();
            } else if (!ced.equalsIgnoreCase("")) {
                lista = em.createQuery("select e from Inspeccion e where "
                        + " " + ced + "")
                        .getResultList();
            } else if (!inci.equalsIgnoreCase("")) {
                lista = em.createQuery("select e from Inspeccion e where "
                        + " " + inci + "")
                        .getResultList();
            } else {
                lista = em.createQuery("select e from Inspeccion e ")
                        .getResultList();
            }

        } catch (Exception e) {
            e.printStackTrace();;
        }
        return lista;
    }
}
