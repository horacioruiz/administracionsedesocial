/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import py.mutualmsp.mutualweb.entities.Region;

/**
 *
 * @author Dbarreto
 */
@Stateless
public class RegionDao implements Serializable{
    @PersistenceContext
    private EntityManager em;
    
    public List<Region> listaRegion() {
        List<Region> lista = new ArrayList<>();
        try {
            lista = em.createNamedQuery("Region.findAll").getResultList();  
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }
    
    public void guardar(Region region){
        try {
            em.merge(region);
        } catch (Exception e) {
            e.printStackTrace();
        }
                
    }
    
    public void borrar(Region region) {
        try {
            em.remove(em.contains(region) ? region : em.merge(region));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<Region> getListRegion(String value) {
        List<Region> lista = new ArrayList<>();
       try {
           if (value != null && !value.equals("")) {
               lista = em.createNativeQuery("SELECT * FROM public.region " +
"                   where  lower(descripcion) like lower('%"+value+"%');", Region.class)
                   .getResultList();
           }else{
               lista = em.createQuery("select r from Region r")
                   .getResultList();
           }
       } catch (Exception e) {
           e.printStackTrace();
       }
       return lista;
    }
    
    public List<Region> getListRegionExistente(String value) {
        List<Region> lista = new ArrayList<>();
       try {
           if (value != null && !value.equals("")) {
               lista = em.createNativeQuery("SELECT * FROM public.region " +
                "where  lower(descripcion) like lower('%"+value+"%');", Region.class)
                   .getResultList();
           }
       } catch (Exception e) {
           e.printStackTrace();
       }
       return lista;
    }
}
