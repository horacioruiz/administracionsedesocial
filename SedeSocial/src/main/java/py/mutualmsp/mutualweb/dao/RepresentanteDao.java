/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import py.mutualmsp.mutualweb.entities.Representante;

/**
 *
 * @author Dbarreto
 */
@Stateless
public class RepresentanteDao implements Serializable {

    @PersistenceContext
    private EntityManager em;

    public List<Representante> listaRepresentante() {
        List<Representante> lista = new ArrayList<>();

        try {
            lista = em.createNamedQuery("Representante.findAll").getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }

    public void guardar(Representante cargo) {
        try {
            em.merge(cargo);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void borrar(Representante cargo) {
        try {
            em.remove(em.contains(cargo) ? cargo : em.merge(cargo));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<Representante> getListRepresentante(String valor) {
        List<Representante> lista = new ArrayList<>();
        try {
            if (valor != null && !valor.equals("")) {
                lista = em.createNativeQuery("SELECT * FROM public.representante "
                        + "                   where  id=" + valor + ";", Representante.class)
                        .getResultList();
            } else {
                lista = em.createQuery("select c from Representante c")
                        .getResultList();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }

    public List<Representante> getRepresentanteByDescripcion(String valor) {
        List<Representante> lista = new ArrayList<>();
        try {
            if (valor != null && !valor.equals("")) {
                lista = em.createNativeQuery("SELECT * FROM public.representante "
                        + "                   where lower(descripcion) LIKE '" + valor + "%';", Representante.class)
                        .getResultList();
            } else {
                lista = em.createQuery("select c from Representante c")
                        .getResultList();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }

    public Representante getNinguno() {
        Representante ninguno = null;
        try {
            ninguno = (Representante) em.createNativeQuery("SELECT * FROM public.representante "
                    + "               where  lower(descripcion) like lower('%NINGUNO%');", Representante.class)
                    .getSingleResult();
        } catch (Exception e) {
            e.printStackTrace();
            ninguno = (Representante) em.createQuery("select c from Representante c")
                    .getResultList();
        }
        return ninguno;
    }
}
