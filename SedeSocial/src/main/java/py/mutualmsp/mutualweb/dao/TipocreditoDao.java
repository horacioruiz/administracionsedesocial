/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import py.mutualmsp.mutualweb.entities.Tipocredito;

/**
 *
 * @author Dbarreto
 */
@Stateless
public class TipocreditoDao implements Serializable {

    @PersistenceContext
    private EntityManager em;

    public List<Tipocredito> listaTipocredito() {
        List<Tipocredito> lista = new ArrayList<>();

        try {
            lista = em.createNamedQuery("Tipocredito.findAll").getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }

    public void guardar(Tipocredito cargo) {
        try {
            em.merge(cargo);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void borrar(Tipocredito cargo) {
        try {
            em.remove(em.contains(cargo) ? cargo : em.merge(cargo));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<Tipocredito> getListTipocredito(String valor) {
        List<Tipocredito> lista = new ArrayList<>();
        try {
            if (valor != null && !valor.equals("")) {
                lista = em.createNativeQuery("SELECT * FROM public.tipocredito "
                        + "                   where  id=" + valor + ";", Tipocredito.class)
                        .getResultList();
            } else {
                lista = em.createQuery("select c from Tipocredito c")
                        .getResultList();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }

    public List<Tipocredito> getTipocreditoByDescripcion(String valor) {
        List<Tipocredito> lista = new ArrayList<>();
        try {
            if (valor != null && !valor.equals("")) {
                lista = em.createNativeQuery("SELECT * FROM public.tipocredito "
                        + "                   where lower(descripcion) LIKE '" + valor + "%';", Tipocredito.class)
                        .getResultList();
            } else {
                lista = em.createQuery("select c from Tipocredito c")
                        .getResultList();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }

    public Tipocredito getNinguno() {
        Tipocredito ninguno = null;
        try {
            ninguno = (Tipocredito) em.createNativeQuery("SELECT * FROM public.tipocredito "
                    + "               where  lower(descripcion) like lower('%NINGUNO%');", Tipocredito.class)
                    .getSingleResult();
        } catch (Exception e) {
            e.printStackTrace();
            ninguno = (Tipocredito) em.createQuery("select c from Tipocredito c")
                    .getResultList();
        }
        return ninguno;
    }
}
