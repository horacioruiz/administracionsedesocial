/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import py.mutualmsp.mutualweb.entities.EstadoSolicitudAyuda;

/**
 *
 * @author Dbarreto
 */
@Stateless
public class EstadoSolicitudAyudaDao implements Serializable {

    @PersistenceContext
    private EntityManager em;

    public List<EstadoSolicitudAyuda> listaEstadoSolicitudAyuda() {
        List<EstadoSolicitudAyuda> lista = new ArrayList<>();

        try {
            lista = em.createNamedQuery("EstadoSolicitudAyuda.findAll").getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }

    public void guardar(EstadoSolicitudAyuda cargo) {
        try {
            em.merge(cargo);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void borrar(EstadoSolicitudAyuda cargo) {
        try {
            em.remove(em.contains(cargo) ? cargo : em.merge(cargo));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<EstadoSolicitudAyuda> getListEstadoSolicitudAyuda(String valor) {
        List<EstadoSolicitudAyuda> lista = new ArrayList<>();
        try {
            if (valor != null && !valor.equals("")) {
                lista = em.createNativeQuery("SELECT * FROM public.estado_solicitud_ayuda "
                        + "                   where  id_solicitud=" + valor + " ORDER BY id;", EstadoSolicitudAyuda.class)
                        .getResultList();
            } else {
                lista = new ArrayList<>();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }

    public List<EstadoSolicitudAyuda> getEstadoSolicitudAyudaByDescripcion(String valor) {
        List<EstadoSolicitudAyuda> lista = new ArrayList<>();
        try {
            if (valor != null && !valor.equals("")) {
                lista = em.createNativeQuery("SELECT * FROM public.estado_solicitud_ayuda "
                        + "                   where lower(descripcion) LIKE '" + valor + "%';", EstadoSolicitudAyuda.class)
                        .getResultList();
            } else {
                lista = em.createQuery("select c from EstadoSolicitudAyuda c")
                        .getResultList();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }

    public EstadoSolicitudAyuda getNinguno() {
        EstadoSolicitudAyuda ninguno = null;
        try {
            ninguno = (EstadoSolicitudAyuda) em.createNativeQuery("SELECT * FROM public.estado_solicitud_ayuda "
                    + "               where  lower(descripcion) like lower('%NINGUNO%');", EstadoSolicitudAyuda.class)
                    .getSingleResult();
        } catch (Exception e) {
            e.printStackTrace();
            ninguno = (EstadoSolicitudAyuda) em.createQuery("select c from EstadoSolicitudAyuda c")
                    .getResultList();
        }
        return ninguno;
    }
}
