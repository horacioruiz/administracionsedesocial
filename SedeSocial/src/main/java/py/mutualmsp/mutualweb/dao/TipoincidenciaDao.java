/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import py.mutualmsp.mutualweb.entities.Tipoincidencia;

/**
 *
 * @author Dbarreto
 */
@Stateless
public class TipoincidenciaDao implements Serializable {

    @PersistenceContext
    private EntityManager em;

    public List<Tipoincidencia> listaTipoincidencia() {
        List<Tipoincidencia> lista = new ArrayList<>();
        try {
            lista = em.createNamedQuery("Tipoincidencia.findAll").getResultList();  
//            lista = em.createQuery("select * from tipoincidencia")
//                    .getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }

    public void guardar(Tipoincidencia tipoincidencia) {
        try {
            em.merge(tipoincidencia);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void borrar(Tipoincidencia tipoincidencia) {
        try {
            em.remove(em.contains(tipoincidencia) ? tipoincidencia : em.merge(tipoincidencia));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<Tipoincidencia> getListTipoincidencia(String valor) {
        List<Tipoincidencia> lista = new ArrayList<>();
        try {
            if (valor != null && !valor.equals("")) {
                lista = em.createNativeQuery("SELECT * FROM sedesocial.tipoincidencia "
                        + "                   where  lower(descripcion) like lower('%" + valor + "%');", Tipoincidencia.class)
                        .getResultList();
            } else {
                lista = em.createQuery("select t from Tipoincidencia t")
                        .getResultList();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }

    public Tipoincidencia getNinguno() {
        Tipoincidencia ninguno = null;
        try {
            ninguno = (Tipoincidencia) em.createNativeQuery("SELECT * FROM sedesocial.tipoincidencia "
                    + "               where  lower(descripcion) like lower('%NINGUNO%');", Tipoincidencia.class)
                    .getSingleResult();
        } catch (Exception e) {
            e.printStackTrace();
            ninguno = (Tipoincidencia) em.createQuery("select t from Tipoincidencia t")
                    .getResultList();
        }
        return ninguno;
    }
}
