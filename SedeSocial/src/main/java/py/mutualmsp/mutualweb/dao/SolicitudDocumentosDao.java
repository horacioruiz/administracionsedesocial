/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import py.mutualmsp.mutualweb.entities.SolicitudDocumentos;

/**
 *
 * @author Dbarreto
 */
@Stateless
public class SolicitudDocumentosDao implements Serializable {

    @PersistenceContext
    private EntityManager em;

    public List<SolicitudDocumentos> listaSolicitudDocumentos() {
        List<SolicitudDocumentos> lista = new ArrayList<>();

        try {
            lista = em.createNamedQuery("SolicitudDocumentos.findAll").getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }

    public void guardar(SolicitudDocumentos cargo) {
        try {
            em.merge(cargo);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void borrar(SolicitudDocumentos cargo) {
        try {
            em.remove(em.contains(cargo) ? cargo : em.merge(cargo));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<SolicitudDocumentos> getListSolicitudDocumentos(String valor) {
        List<SolicitudDocumentos> lista = new ArrayList<>();
        try {
            if (valor != null && !valor.equals("")) {
                lista = em.createNativeQuery("SELECT * FROM public.solicitud_documentos "
                        + "                   where  id=" + valor + ";", SolicitudDocumentos.class)
                        .getResultList();
            } else {
                lista = em.createQuery("select c from SolicitudDocumentos c")
                        .getResultList();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }

    public List<SolicitudDocumentos> getSolicitudDocumentosByDescripcion(String valor) {
        List<SolicitudDocumentos> lista = new ArrayList<>();
        try {
            if (valor != null && !valor.equals("")) {
                lista = em.createNativeQuery("SELECT * FROM public.solicitud_documentos "
                        + "                   where lower(descripcion) LIKE '" + valor + "%';", SolicitudDocumentos.class)
                        .getResultList();
            } else {
                lista = em.createQuery("select c from SolicitudDocumentos c")
                        .getResultList();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }

    public SolicitudDocumentos getNinguno() {
        SolicitudDocumentos ninguno = null;
        try {
            ninguno = (SolicitudDocumentos) em.createNativeQuery("SELECT * FROM public.solicitud_documentos "
                    + "               where  lower(descripcion) like lower('%NINGUNO%');", SolicitudDocumentos.class)
                    .getSingleResult();
        } catch (Exception e) {
            e.printStackTrace();
            ninguno = (SolicitudDocumentos) em.createQuery("select c from SolicitudDocumentos c")
                    .getResultList();
        }
        return ninguno;
    }

    public List<SolicitudDocumentos> getSCredBySolicitud(String id) {
        List<SolicitudDocumentos> lista = new ArrayList<>();
        try {
            if (id != null && !id.equals("")) {
                lista = em.createNativeQuery("SELECT * FROM public.solicitud_documentos "
                        + "                   where idsolicitudcredito=" + id + ";", SolicitudDocumentos.class)
                        .getResultList();
            } else {
                lista = new ArrayList<>();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }
}
