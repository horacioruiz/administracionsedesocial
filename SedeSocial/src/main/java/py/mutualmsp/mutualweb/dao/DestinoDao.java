/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import py.mutualmsp.mutualweb.entities.Destino;

/**
 *
 * @author Dbarreto
 */
@Stateless
public class DestinoDao implements Serializable {

    @PersistenceContext
    private EntityManager em;

    public List<Destino> listaDestino() {
        List<Destino> lista = new ArrayList<>();

        try {
            lista = em.createNamedQuery("Destino.findAll").getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }

    public void guardar(Destino cargo) {
        try {
            em.merge(cargo);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void borrar(Destino cargo) {
        try {
            em.remove(em.contains(cargo) ? cargo : em.merge(cargo));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<Destino> getListDestino(String valor) {
        List<Destino> lista = new ArrayList<>();
        try {
            if (valor != null && !valor.equals("")) {
                lista = em.createNativeQuery("SELECT * FROM public.destino "
                        + "                   where  id=" + valor + ";", Destino.class)
                        .getResultList();
            } else {
                lista = em.createQuery("select c from Destino c")
                        .getResultList();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }

    public List<Destino> getDestinoByDescripcion(String valor) {
        List<Destino> lista = new ArrayList<>();
        try {
            if (valor != null && !valor.equals("")) {
                lista = em.createNativeQuery("SELECT * FROM public.destino "
                        + "                   where lower(descripcion) LIKE '" + valor + "%';", Destino.class)
                        .getResultList();
            } else {
                lista = em.createQuery("select c from Destino c")
                        .getResultList();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }

    public Destino getNinguno() {
        Destino ninguno = null;
        try {
            ninguno = (Destino) em.createNativeQuery("SELECT * FROM public.destino "
                    + "               where  lower(descripcion) like lower('%NINGUNO%');", Destino.class)
                    .getSingleResult();
        } catch (Exception e) {
            e.printStackTrace();
            ninguno = (Destino) em.createQuery("select c from Destino c")
                    .getResultList();
        }
        return ninguno;
    }
}
