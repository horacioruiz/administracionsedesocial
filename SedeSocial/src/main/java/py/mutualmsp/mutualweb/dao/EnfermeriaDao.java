/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.dao;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TemporalType;
import py.mutualmsp.mutualweb.entities.Enfermeria;
import py.mutualmsp.mutualweb.entities.Tipoincidencia;
import py.mutualmsp.mutualweb.util.DateUtils;

/**
 *
 * @author Dbarreto
 */
@Stateless
public class EnfermeriaDao implements Serializable {

    @PersistenceContext
    private EntityManager em;

    public List<Enfermeria> listaEnfermeria() {
        List<Enfermeria> lista = new ArrayList<>();

        try {
//            lista = em.createNamedQuery("Enfermeria.findAll").getResultList();
            lista = em.createQuery("select e from Enfermeria e JOIN FETCH e.idtipoIncidencia t")
                    .getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }

    public List<Enfermeria> getEnfermeriaByFecha(java.util.Date fdesde, java.util.Date fhasta) {
        List<Enfermeria> lista = new ArrayList<>();
        try {
            lista = em.createQuery("select e from Enfermeria e where e.fecha between :fdesde and :fhasta")
                    .setParameter("fdesde", fdesde, TemporalType.DATE)
                    .setParameter("fhasta", fhasta, TemporalType.DATE)
                    .getResultList();
        } catch (Exception e) {
        }
        return lista;
    }

    public List<Enfermeria> listarEnfermeriaPorCI(String valor) {
        List<Enfermeria> lista = new ArrayList<>();
        try {
            if (valor != null && !valor.equals("")) {
                lista = em.createNativeQuery("SELECT * FROM sedesocial.enfermeria "
                        + "                   where  cedula='" + valor + "';", Enfermeria.class)
                        .getResultList();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }

    public void guardar(Enfermeria cargo) {
        try {
            cargo.setNombre(cargo.getNombre().toUpperCase());
            em.merge(cargo);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void borrar(Enfermeria cargo) {
        try {
            em.remove(em.contains(cargo) ? cargo : em.merge(cargo));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<Enfermeria> getListEnfermeria(String valor) {
        List<Enfermeria> lista = new ArrayList<>();
        try {
            if (valor != null && !valor.equals("")) {
                lista = em.createNativeQuery("SELECT * FROM sedesocial.enfermeria "
                        + "                   where  id=" + valor + ";", Enfermeria.class)
                        .getResultList();
            } else {
                lista = em.createQuery("select c from Enfermeria c")
                        .getResultList();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }

    public List<Enfermeria> getEnfermeriaByDescripcion(String valor) {
        List<Enfermeria> lista = new ArrayList<>();
        try {
            if (valor != null && !valor.equals("")) {
                lista = em.createNativeQuery("SELECT * FROM sedesocial.enfermeria "
                        + "                   where lower(descripcion) LIKE '" + valor + "%';", Enfermeria.class)
                        .getResultList();
            } else {
                lista = em.createQuery("select c from Enfermeria c")
                        .getResultList();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }

    public List<Enfermeria> getEnfermeriaByTipoIncidencia(Long id) {
        List<Enfermeria> lista = new ArrayList<>();
        try {
            lista = em.createQuery("select e from Enfermeria e where e.idtipoIncidencia = :id ")
                    .setParameter("id", id)
                    .getResultList();
        } catch (Exception e) {
            e.printStackTrace();;
        }
        return lista;
    }

    public Enfermeria getNinguno() {
        Enfermeria ninguno = null;
        try {
            ninguno = (Enfermeria) em.createNativeQuery("SELECT * FROM sedesocial.enfermeria "
                    + "               where  lower(descripcion) like lower('%NINGUNO%');", Enfermeria.class)
                    .getSingleResult();
        } catch (Exception e) {
            e.printStackTrace();
            ninguno = (Enfermeria) em.createQuery("select c from Enfermeria c")
                    .getResultList();
        }
        return ninguno;
    }

    public List<Enfermeria> getDataFromFilter(String cedula, LocalDate desde, LocalDate hasta, Tipoincidencia incidencia) {
        List<Enfermeria> lista = new ArrayList<>();
        try {

            String ced = "";
            if (!cedula.equalsIgnoreCase("")) {
                ced = " e.cedula='" + cedula + "'";
            }
            String inci = "";
            try {
                if (incidencia.getId() != null) {
                    inci = " tp.id=" + incidencia.getId() + "";
                }
            } catch (Exception e) {
            } finally {
            }
            String fechaDesdeText = "";
            String fechaHastaText = "";
            try {
                DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                fechaDesdeText = formatter.format(DateUtils.asDate(desde));
                fechaHastaText = formatter.format(DateUtils.asDate(hasta));
            } catch (Exception e) {
            } finally {
            }

            if (!ced.equalsIgnoreCase("") && !inci.equalsIgnoreCase("") && !fechaDesdeText.equalsIgnoreCase("")) {
                lista = em.createQuery("select e from Enfermeria e JOIN FETCH e.idtipoIncidencia tp where "
                        + " (e.fecha BETWEEN '" + fechaDesdeText + "' AND '" + fechaHastaText + "') AND " + ced + " AND " + inci)
                        .getResultList();
            } else if (!ced.equalsIgnoreCase("") && !inci.equalsIgnoreCase("")) {
                lista = em.createQuery("select e from Enfermeria e JOIN FETCH e.idtipoIncidencia tp where "
                        + " " + ced + " AND " + inci)
                        .getResultList();
            } else if (!ced.equalsIgnoreCase("") && !fechaDesdeText.equalsIgnoreCase("")) {
                lista = em.createQuery("select e from Enfermeria e JOIN FETCH e.idtipoIncidencia tp where "
                        + " " + ced + " AND (e.fecha BETWEEN '" + fechaDesdeText + "' AND '" + fechaHastaText + "')")
                        .getResultList();
            } else if (!fechaDesdeText.equalsIgnoreCase("")) {
                lista = em.createQuery("select e from Enfermeria e JOIN FETCH e.idtipoIncidencia tp where "
                        + " (e.fecha BETWEEN '" + fechaDesdeText + "' AND '" + fechaHastaText + "')")
                        .getResultList();
            } else if (!ced.equalsIgnoreCase("")) {
                lista = em.createQuery("select e from Enfermeria e JOIN FETCH e.idtipoIncidencia tp where "
                        + " " + ced + "")
                        .getResultList();
            } else if (!inci.equalsIgnoreCase("")) {
                lista = em.createQuery("select e from Enfermeria e JOIN FETCH e.idtipoIncidencia tp where "
                        + " " + inci + "")
                        .getResultList();
            } else {
                lista = em.createQuery("select e from Enfermeria e JOIN FETCH e.idtipoIncidencia tp ")
                        .getResultList();
            }
            
        } catch (Exception e) {
            e.printStackTrace();;
        }
        return lista;
    }
}
