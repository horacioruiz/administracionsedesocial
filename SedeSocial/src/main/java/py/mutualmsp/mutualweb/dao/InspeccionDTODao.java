/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.dao;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TemporalType;
import py.mutualmsp.mutualweb.dto.InspeccionDTO;
import py.mutualmsp.mutualweb.entities.Inspeccion;
import py.mutualmsp.mutualweb.entities.Invitado;
import py.mutualmsp.mutualweb.entities.NucleoFamiliar;
import py.mutualmsp.mutualweb.entities.Parentesco;
import py.mutualmsp.mutualweb.entities.Socio;
import py.mutualmsp.mutualweb.entities.Tipoincidencia;
import py.mutualmsp.mutualweb.util.DateUtils;

/**
 *
 * @author Dbarreto
 */
@Stateless
public class InspeccionDTODao implements Serializable {

    @PersistenceContext
    private EntityManager em;

    public List<InspeccionDTO> listaInspeccionDTO() {
        List<Inspeccion> lista = new ArrayList<>();
        List<InspeccionDTO> listaDTO = new ArrayList<>();
        try {
//            lista = em.createNamedQuery("InspeccionDTO.findAll").getResultList();
            lista = em.createNativeQuery("select * from sedesocial.inspeccion ORDER BY id DESC LIMIT 150", Inspeccion.class)
                    .getResultList();

            for (Inspeccion insp : lista) {
                InspeccionDTO inspeccionDTO = convertirInspDTO(insp);
                listaDTO.add(inspeccionDTO);
//                listaDTO.add(recuperarParentesco(inspeccionDTO));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return listaDTO;
    }

    public List<InspeccionDTO> getInspeccionDTOByFecha(java.util.Date fdesde, java.util.Date fhasta) {
        List<InspeccionDTO> lista = new ArrayList<>();
        try {
            lista = em.createQuery("select * from sedesocial.inspeccion e where e.fecha between :fdesde and :fhasta ORDER BY e.id DESC")
                    .setMaxResults(150)
                    .setParameter("fdesde", fdesde, TemporalType.DATE)
                    .setParameter("fhasta", fhasta, TemporalType.DATE)
                    .getResultList();
        } catch (Exception e) {
        }
        return lista;
    }

    public List<InspeccionDTO> listarInspeccionDTOPorCI(String valor) {
        List<InspeccionDTO> lista = new ArrayList<>();
        try {
            if (valor != null && !valor.equals("")) {
                lista = em.createNativeQuery("SELECT * FROM sedesocial.enfermeria "
                        + "                   where  cedula='" + valor + "';", Inspeccion.class)
                        .getResultList();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }

    public InspeccionDTO listarInspeccionDTOPorId(String valor) {
        InspeccionDTO lista = new InspeccionDTO();
        try {
            if (valor != null && !valor.equals("")) {
                lista = (InspeccionDTO) em.createNativeQuery("SELECT * FROM sedesocial.inspeccion "
                        + "                   where  id=" + valor + ";", Inspeccion.class)
                        .getSingleResult();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }

    public void guardar(InspeccionDTO cargo) {
        try {
            cargo.setNombre(cargo.getNombre().toUpperCase());
            cargo.setApellido(cargo.getApellido().toUpperCase());
            em.merge(cargo);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void borrar(InspeccionDTO cargo) {
        try {
            em.remove(em.contains(cargo) ? cargo : em.merge(cargo));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<InspeccionDTO> getListInspeccionDTO(String valor) {
        List<InspeccionDTO> lista = new ArrayList<>();
        try {
            if (valor != null && !valor.equals("")) {
                lista = em.createNativeQuery("SELECT * FROM sedesocial.enfermeria "
                        + "                   where  id=" + valor + ";", Inspeccion.class)
                        .getResultList();
            } else {
                lista = em.createQuery("select c from InspeccionDTO c")
                        .getResultList();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }

    public List<InspeccionDTO> recuperarInspeccionDTO(String valor, String socio) {
        List<InspeccionDTO> listaDTO = new ArrayList<>();
        listaDTO = recuperarInspeccionParentesco(valor, socio);

        return listaDTO;
    }

    public List<InspeccionDTO> recuperarInspeccionDTO(String valor, String socio, LocalDate desde, LocalDate hasta) {
        List<InspeccionDTO> listaDTO = new ArrayList<>();
        listaDTO = recuperarInspeccionParentesco(valor, socio, desde, hasta);

        return listaDTO;
    }

    public List<InspeccionDTO> getInspeccionDTOByDescripcion(String valor) {
        List<InspeccionDTO> lista = new ArrayList<>();
        try {
            if (valor != null && !valor.equals("")) {
                lista = em.createNativeQuery("SELECT * FROM sedesocial.enfermeria "
                        + "                   where lower(descripcion) LIKE '" + valor + "%';", Inspeccion.class)
                        .getResultList();
            } else {
                lista = em.createQuery("select c from InspeccionDTO c")
                        .getResultList();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }

    public List<InspeccionDTO> getInspeccionDTOByTipoIncidencia(Long id) {
        List<InspeccionDTO> lista = new ArrayList<>();
        try {
            lista = em.createQuery("select e from InspeccionDTO e where e.idtipoIncidencia = :id ")
                    .setParameter("id", id)
                    .getResultList();
        } catch (Exception e) {
            e.printStackTrace();;
        }
        return lista;
    }

    public InspeccionDTO getNinguno() {
        InspeccionDTO ninguno = null;
        try {
            ninguno = (InspeccionDTO) em.createNativeQuery("SELECT * FROM sedesocial.enfermeria "
                    + "               where  lower(descripcion) like lower('%NINGUNO%');", Inspeccion.class)
                    .getSingleResult();
        } catch (Exception e) {
            e.printStackTrace();
            ninguno = (InspeccionDTO) em.createQuery("select c from InspeccionDTO c")
                    .getResultList();
        }
        return ninguno;
    }

    public List<InspeccionDTO> getDataFromFilter(String cedula, LocalDate desde, LocalDate hasta, Tipoincidencia incidencia) {
        List<InspeccionDTO> lista = new ArrayList<>();
        try {

            String ced = "";
            if (!cedula.equalsIgnoreCase("")) {
                ced = " e.cedula='" + cedula + "'";
            }
            String inci = "";
            try {
                if (incidencia.getId() != null) {
                    inci = " tp.id=" + incidencia.getId() + "";
                }
            } catch (Exception e) {
            } finally {
            }
            String fechaDesdeText = "";
            String fechaHastaText = "";
            try {
                DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                fechaDesdeText = formatter.format(DateUtils.asDate(desde));
                fechaHastaText = formatter.format(DateUtils.asDate(hasta));
            } catch (Exception e) {
            } finally {
            }

            if (!ced.equalsIgnoreCase("") && !inci.equalsIgnoreCase("") && !fechaDesdeText.equalsIgnoreCase("")) {
                lista = em.createQuery("select e from InspeccionDTO e where "
                        + " (e.fecha BETWEEN '" + fechaDesdeText + "' AND '" + fechaHastaText + "') AND " + ced + " AND " + inci)
                        .getResultList();
            } else if (!ced.equalsIgnoreCase("") && !inci.equalsIgnoreCase("")) {
                lista = em.createQuery("select e from InspeccionDTO e where "
                        + " " + ced + " AND " + inci)
                        .getResultList();
            } else if (!ced.equalsIgnoreCase("") && !fechaDesdeText.equalsIgnoreCase("")) {
                lista = em.createQuery("select e from InspeccionDTO e where "
                        + " " + ced + " AND (e.fecha BETWEEN '" + fechaDesdeText + "' AND '" + fechaHastaText + "')")
                        .getResultList();
            } else if (!fechaDesdeText.equalsIgnoreCase("")) {
                lista = em.createQuery("select e from InspeccionDTO e where "
                        + " (e.fecha BETWEEN '" + fechaDesdeText + "' AND '" + fechaHastaText + "')")
                        .getResultList();
            } else if (!ced.equalsIgnoreCase("")) {
                lista = em.createQuery("select e from InspeccionDTO e where "
                        + " " + ced + "")
                        .getResultList();
            } else if (!inci.equalsIgnoreCase("")) {
                lista = em.createQuery("select e from InspeccionDTO e where "
                        + " " + inci + "")
                        .getResultList();
            } else {
                lista = em.createQuery("select e from InspeccionDTO e ")
                        .getResultList();
            }

        } catch (Exception e) {
            e.printStackTrace();;
        }
        return lista;
    }

    private String recuperarParentesco(String cedula) {
        try {
            NucleoFamiliar nf = (NucleoFamiliar) em.createNativeQuery("SELECT * FROM subordinado where cedula='" + cedula + "'", NucleoFamiliar.class).setMaxResults(0).getSingleResult();
            Parentesco par = (Parentesco) em.createNativeQuery("SELECT * FROM parentesco where id=" + nf.getIdparentesco(), Parentesco.class).setMaxResults(0).getSingleResult();
            Socio soc = (Socio) em.createNativeQuery("SELECT * FROM socio where id=" + nf.getIdsocio(), Socio.class).setMaxResults(0).getSingleResult();
//            inspeccionDTO.setSocio(soc.getCedula() + " | " + soc.getNombreCompleto().toUpperCase());
            return par.getDescripcion().toUpperCase();
        } catch (Exception e) {
            try {
                Invitado nf = (Invitado) em.createNativeQuery("SELECT * FROM sedesocial.invitado where cedula='" + cedula + "'", Invitado.class).setMaxResults(0).getSingleResult();
                Socio soc = (Socio) em.createNativeQuery("SELECT * FROM socio where id=" + nf.getIdsocio(), Socio.class).setMaxResults(0).getSingleResult();

//                inspeccionDTO.setSocio(soc.getCedula() + " | " + soc.getNombreCompleto().toUpperCase());
                return nf.getParentesco().toUpperCase();
            } catch (Exception ec) {
                Socio soc = (Socio) em.createNativeQuery("SELECT * FROM socio where cedula LIKE '%" + cedula + "%'", Socio.class).setMaxResults(0).getSingleResult();
//                inspeccionDTO.setSocio(soc.getCedula() + " | " + soc.getNombreCompleto().toUpperCase());
                return "SOCIO";
            } finally {
            }
        } finally {
        }
    }

    private InspeccionDTO convertirInspDTO(Inspeccion insp) {

        InspeccionDTO solicitud = new InspeccionDTO();
        try {
            solicitud.setId(insp.getId());
        } catch (Exception e) {
            solicitud.setId(0l);
        } finally {
        }
        try {
            solicitud.setPeso(insp.getPeso());
        } catch (Exception e) {
            solicitud.setPeso(0d);
        } finally {
        }
        try {
            solicitud.setParentesco(insp.getParentesco());
        } catch (Exception e) {
            solicitud.setParentesco("OTROS");
        } finally {
        }
        try {
            solicitud.setCedula(insp.getCedula());
        } catch (Exception e) {
            solicitud.setCedula("");
        } finally {
        }
        try {
            solicitud.setNombre(insp.getNombre());
        } catch (Exception e) {
            solicitud.setNombre("");
        } finally {
        }
        try {
            solicitud.setApellido(insp.getApellido());
        } catch (Exception e) {
            solicitud.setApellido("");
        } finally {
        }
        try {
            solicitud.setCedulasocio(insp.getCedulasocio());
        } catch (Exception e) {
            solicitud.setCedulasocio("");
        } finally {
        }
        try {
            solicitud.setNombresocio(insp.getNombresocio());
        } catch (Exception e) {
            solicitud.setNombresocio("");
        } finally {
        }
        try {
            solicitud.setApellidosocio(insp.getApellidosocio());
        } catch (Exception e) {
            solicitud.setApellidosocio("");
        } finally {
        }
        try {
            solicitud.setEdad(insp.getEdad());
        } catch (Exception e) {
            solicitud.setEdad(0);
        } finally {
        }
        try {
            solicitud.setGruposanguineo(insp.getGruposanguineo());
        } catch (Exception e) {
            solicitud.setGruposanguineo("");
        } finally {
        }
        try {
            solicitud.setSexo(insp.getSexo());
        } catch (Exception e) {
            solicitud.setSexo("");
        } finally {
        }
        try {
            solicitud.setSeguromedico(insp.getSeguromedico());
        } catch (Exception e) {
            solicitud.setSeguromedico("");
        } finally {
        }
        try {
            solicitud.setDst(insp.getDst());
        } catch (Exception e) {
            solicitud.setDst(false);
        } finally {
        }
        try {
            solicitud.setConv(insp.getConv());
        } catch (Exception e) {
            solicitud.setConv(false);
        } finally {
        }
        try {
            solicitud.setAsma(insp.getAsma());
        } catch (Exception e) {
            solicitud.setAsma(false);
        } finally {
        }
        try {
            solicitud.setHta(insp.getHta());
        } catch (Exception e) {
            solicitud.setHta(false);
        } finally {
        }
        try {
            solicitud.setCard(insp.getCard());
        } catch (Exception e) {
            solicitud.setCard(false);
        } finally {
        }
        try {
            solicitud.setEpoc(insp.getEpoc());
        } catch (Exception e) {
            solicitud.setEpoc(false);
        } finally {
        }
        try {
            solicitud.setAcv(insp.getAcv());
        } catch (Exception e) {
            solicitud.setAcv(false);
        } finally {
        }
        try {
            solicitud.setOtros(insp.getOtros());
        } catch (Exception e) {
            solicitud.setOtros(false);
        } finally {
        }
        try {
            solicitud.setAlergico(insp.getAlergico());
        } catch (Exception e) {
            solicitud.setAlergico(false);
        } finally {
        }
        try {
            solicitud.setComentarioalergico(insp.getComentarioalergico());
        } catch (Exception e) {
            solicitud.setComentarioalergico("");
        } finally {
        }
        try {
            solicitud.setMotivoconsulta(insp.getMotivoconsulta());
        } catch (Exception e) {
            solicitud.setMotivoconsulta("");
        } finally {
        }
        try {
            solicitud.setTa(insp.getTa());
        } catch (Exception e) {
            solicitud.setTa("");
        } finally {
        }
        try {
            solicitud.setFc(insp.getFc());
        } catch (Exception e) {
            solicitud.setFc("");
        } finally {
        }
        try {
            solicitud.setT(insp.getT());
        } catch (Exception e) {
            solicitud.setT("");
        } finally {
        }
        try {
            solicitud.setHallazgos(insp.getHallazgos());
        } catch (Exception e) {
            solicitud.setHallazgos("");
        } finally {
        }
        try {
            solicitud.setTratamiento(insp.getTratamiento());
        } catch (Exception e) {
            solicitud.setTratamiento("");
        } finally {
        }
        try {
            solicitud.setContactoemergencia(insp.getContactoemergencia());
        } catch (Exception e) {
            solicitud.setContactoemergencia("");
        } finally {
        }
        try {
            solicitud.setFecha(insp.getFecha());
        } catch (Exception e) {
            solicitud.setFecha(null);
        } finally {
        }
        try {
            solicitud.setFecharegistro(insp.getFecharegistro());
        } catch (Exception e) {
            solicitud.setFecharegistro(null);
        } finally {
        }
        solicitud.setMotivoConsulta(insp.getMotivoConsulta() == null ? "" : insp.getMotivoConsulta());
        solicitud.setMedicacionConsulta(insp.getMedicacionConsulta() == null ? "" :insp.getMedicacionConsulta() );
        solicitud.setObservacionConsulta(insp.getObservacionConsulta() == null ? "" : insp.getObservacionConsulta());
        
        return solicitud;
    }

    private List<InspeccionDTO> recuperarInspeccionParentesco(String valor, String socio) {
        List<InspeccionDTO> listaDTO = new ArrayList<>();
        try {
            List<Inspeccion> lista = new ArrayList<>();
            if (valor != null && !valor.equals("") && socio != null && !socio.equals("")) {
                lista = em.createNativeQuery("SELECT * FROM sedesocial.inspeccion "
                        + " where upper(cedula || nombre || apellido) LIKE '%" + valor.toUpperCase() + "%' AND "
                        + " upper(cedulasocio || nombresocio || apellidosocio) LIKE '%" + socio.toUpperCase() + "%'", Inspeccion.class)
                        .getResultList();
                for (Inspeccion insp : lista) {
                    InspeccionDTO inspeccionDTO = convertirInspDTO(insp);
                    listaDTO.add(inspeccionDTO);
//                    listaDTO.add(recuperarParentesco(inspeccionDTO));
                }
            } else if (valor != null && !valor.equals("")) {
                lista = em.createNativeQuery("SELECT * FROM sedesocial.inspeccion "
                        + " where upper(cedula || nombre || apellido) LIKE '%" + valor.toUpperCase() + "%' "
                        + "", Inspeccion.class)
                        .getResultList();
                for (Inspeccion insp : lista) {
                    InspeccionDTO inspeccionDTO = convertirInspDTO(insp);
                    listaDTO.add(inspeccionDTO);
//                    listaDTO.add(recuperarParentesco(inspeccionDTO));
                }
            } else if (socio != null && !socio.equals("")) {
                lista = em.createNativeQuery("SELECT * FROM sedesocial.inspeccion "
                        + " WHERE upper(cedulasocio || nombresocio || apellidosocio) LIKE '%" + socio.toUpperCase() + "%'", Inspeccion.class)
                        .getResultList();
                for (Inspeccion insp : lista) {
                    InspeccionDTO inspeccionDTO = convertirInspDTO(insp);
                    listaDTO.add(inspeccionDTO);
//                    listaDTO.add(recuperarParentesco(inspeccionDTO));
                }
            } else {
                try {
//            lista = em.createNamedQuery("InspeccionDTO.findAll").getResultList();
                    lista = em.createNativeQuery("select * from sedesocial.inspeccion ORDER BY id DESC LIMIT 150", Inspeccion.class)
                            .getResultList();

                    for (Inspeccion insp : lista) {
                        InspeccionDTO inspeccionDTO = convertirInspDTO(insp);
                        listaDTO.add(inspeccionDTO);
//                        listaDTO.add(recuperarParentesco(inspeccionDTO));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return listaDTO;
    }

    private List<InspeccionDTO> recuperarInspeccionSocio(String valor, String socio) {
        List<InspeccionDTO> listaDTO = new ArrayList<>();
        try {
            em.createNativeQuery("SELECT * FROM socio "
                    + " where upper(cedula || nombre || apellido) LIKE '%" + valor.toUpperCase() + "%';", Socio.class)
                    .getResultList();

            List<Inspeccion> lista = new ArrayList<>();
            if (valor != null && !valor.equals("")) {
                lista = em.createNativeQuery("SELECT * FROM sedesocial.inspeccion "
                        + " where upper(cedula || nombre || apellido) LIKE '%" + valor.toUpperCase() + "%';", Inspeccion.class)
                        .getResultList();
                for (Inspeccion insp : lista) {
                    InspeccionDTO inspeccionDTO = convertirInspDTO(insp);
                    listaDTO.add(inspeccionDTO);
//                    listaDTO.add(recuperarParentesco(inspeccionDTO));
                }
            } else {
                lista = em.createNativeQuery("SELECT * FROM sedesocial.inspeccion ORDER BY id DESC LIMIT 150", Inspeccion.class)
                        .getResultList();
                for (Inspeccion insp : lista) {
                    InspeccionDTO inspeccionDTO = convertirInspDTO(insp);
                    listaDTO.add(inspeccionDTO);
//                    listaDTO.add(recuperarParentesco(inspeccionDTO));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return listaDTO;
    }

    private List<InspeccionDTO> recuperarInspeccionParentesco(String valor, String socio, LocalDate desde, LocalDate hasta) {
        List<InspeccionDTO> listaDTO = new ArrayList<>();
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

        String fecha = "";
        String fechaDesdeText = "";
        String fechaHastaText = "";
        if (desde != null && hasta != null) {
            fechaDesdeText = formatter.format(DateUtils.asDate(desde));
            fechaHastaText = formatter.format(DateUtils.asDate(hasta));
            fecha = "   fecha<='" + fechaDesdeText + "' AND fecha>='" + fechaHastaText + "'";
        }
        try {
            List<Inspeccion> lista = new ArrayList<>();
            if (valor != null && !valor.equals("") && (desde == null || hasta== null) ) {
                lista = em.createNativeQuery("SELECT * FROM sedesocial.inspeccion \n" +
"                       where upper(cedula || nombre || apellido) LIKE '%" + valor.toUpperCase() + "%' \n" +
"                       or upper(cedulasocio || nombresocio  || apellidosocio) LIKE '%" + valor.toUpperCase() + "%' ", Inspeccion.class)
                        .getResultList();
                for (Inspeccion insp : lista) {
                    InspeccionDTO inspeccionDTO = convertirInspDTO(insp);
                    listaDTO.add(inspeccionDTO);
//                    listaDTO.add(recuperarParentesco(inspeccionDTO));
                }
            }else if (valor != null && !valor.equals("") && desde != null && hasta!= null) {
                String sql = "select * from sedesocial.inspeccion where fecharegistro  between '" + fechaDesdeText + "' AND '" + fechaHastaText + "'" +
                " and (upper(cedula || nombre || apellido) like '%" + valor.toUpperCase() + "%' " +
                " or upper(cedulasocio || nombresocio || apellidosocio) like '%" + valor.toUpperCase() + "%') ";
                System.out.println("query: " + sql);
                lista = em.createNativeQuery(sql, Inspeccion.class)
                        .getResultList();
                for (Inspeccion insp : lista) {
                    InspeccionDTO inspeccionDTO = convertirInspDTO(insp);
                    listaDTO.add(inspeccionDTO);
//                    listaDTO.add(recuperarParentesco(inspeccionDTO));
                }
            } else if ((valor == null || valor.equals("")) && desde != null && hasta!= null ) {
                try {
                    fecha = "WHERE fecharegistro  between '" + fechaDesdeText + "' AND '" + fechaHastaText + "'  order by id desc limit 150";
                    //fecha = " WHERE fecharegistro<='" + fechaDesdeText + "' AND fecha>='" + fechaHastaText + "'";
//            lista = em.createNamedQuery("InspeccionDTO.findAll").getResultList();
                    System.out.println("SQL ==> select * from sedesocial.inspeccion " + fecha + "");
                    lista = em.createNativeQuery("select * from sedesocial.inspeccion " + fecha + "", Inspeccion.class)
                            .getResultList();

                    for (Inspeccion insp : lista) {
                        InspeccionDTO inspeccionDTO = convertirInspDTO(insp);
                        listaDTO.add(inspeccionDTO);
//                        listaDTO.add(recuperarParentesco(inspeccionDTO));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return listaDTO;
    }
}
