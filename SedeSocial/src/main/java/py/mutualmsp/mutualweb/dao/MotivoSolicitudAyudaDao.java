/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import py.mutualmsp.mutualweb.entities.MotivoSolicitudAyuda;

/**
 *
 * @author Dbarreto
 */
@Stateless
public class MotivoSolicitudAyudaDao implements Serializable {

    @PersistenceContext
    private EntityManager em;

    public List<MotivoSolicitudAyuda> listaMotivoSolicitudAyuda() {
        List<MotivoSolicitudAyuda> lista = new ArrayList<>();

        try {
            lista = em.createNamedQuery("MotivoSolicitudAyuda.findAll").getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }

    public void guardar(MotivoSolicitudAyuda motivo_solicitud_ayuda) {
        try {
            em.merge(motivo_solicitud_ayuda);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void borrar(MotivoSolicitudAyuda motivo_solicitud_ayuda) {
        try {
            em.remove(em.contains(motivo_solicitud_ayuda) ? motivo_solicitud_ayuda : em.merge(motivo_solicitud_ayuda));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<MotivoSolicitudAyuda> getListMotivoSolicitudAyuda(String valor) {
        List<MotivoSolicitudAyuda> lista = new ArrayList<>();
        try {
            if (valor != null && !valor.equals("")) {
                lista = em.createNativeQuery("SELECT * FROM public.motivo_solicitud_ayuda "
                        + "                   where  id=" + valor + ";", MotivoSolicitudAyuda.class)
                        .getResultList();
            } else {
                lista = em.createQuery("select c from MotivoSolicitudAyuda c")
                        .getResultList();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }

    public List<MotivoSolicitudAyuda> getMotivoSolicitudAyudaByDescripcion(String valor) {
        List<MotivoSolicitudAyuda> lista = new ArrayList<>();
        try {
            if (valor != null && !valor.equals("")) {
                lista = em.createNativeQuery("SELECT * FROM public.motivo_solicitud_ayuda "
                        + "                   where lower(descripcion) LIKE '" + valor + "%';", MotivoSolicitudAyuda.class)
                        .getResultList();
            } else {
                lista = em.createQuery("select c from MotivoSolicitudAyuda c")
                        .getResultList();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }

    public MotivoSolicitudAyuda getNinguno() {
        MotivoSolicitudAyuda ninguno = null;
        try {
            ninguno = (MotivoSolicitudAyuda) em.createNativeQuery("SELECT * FROM public.motivo_solicitud_ayuda "
                    + "               where  lower(descripcion) like lower('%NINGUNO%');", MotivoSolicitudAyuda.class)
                    .getSingleResult();
        } catch (Exception e) {
            e.printStackTrace();
            ninguno = (MotivoSolicitudAyuda) em.createQuery("select c from MotivoSolicitudAyuda c")
                    .getResultList();
        }
        return ninguno;
    }
}
