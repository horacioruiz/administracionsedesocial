package py.mutualmsp.mutualweb.dao;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;
import py.mutualmsp.mutualweb.entities.Formulario;
import py.mutualmsp.mutualweb.entities.SolicitudAyuda;
import py.mutualmsp.mutualweb.entities.Usuario;

/**
 * Created by Alfre on 8/6/2016.
 */
@Stateless
public class SolicitudAyudaDao {

    @PersistenceContext
    private EntityManager em;

    public SolicitudAyuda guardarSolicitudAyuda(SolicitudAyuda usuarios) {
        if (usuarios.getId() == null) {
            em.persist(usuarios);
        } else {
            em.merge(usuarios);
        }
        em.flush();
        return usuarios;
    }

    public List<SolicitudAyuda> listadeSolicitudAyuda(int firstRow, int pageSize, String filter) {
        try {
            String sql = "select p ";
            sql += "from SolicitudAyuda p ";
            if (filter != null && !filter.equals("")) {
                sql += "where upper(p.descripcion) like :param order by p.id";
            }
            Query query = em.createQuery(sql, SolicitudAyuda.class);
            if (filter != null && !filter.equals("")) {
                query.setParameter("param", filter.toUpperCase() + "%");
            }
            int i = query.getResultList().size();
            System.out.print(i);
            List<SolicitudAyuda> results = query.getResultList();
            return results;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<SolicitudAyuda> listadeSolicitudAyuda() {
        try {
            String sql = "select u from SolicitudAyuda u ";
            Query query = em.createQuery(sql);
            int i = query.getResultList().size();
            System.out.print(i);
            return query.getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<SolicitudAyuda> listarAdministradores() {
        try {
            String sql = "select p ";
            sql += "from SolicitudAyuda p JOIN FETCH p.socio f WHERE p.administrador=TRUE";
            Query query = em.createQuery(sql, SolicitudAyuda.class);
            int i = query.getResultList().size();
            System.out.print(i);
            List<SolicitudAyuda> results = query.getResultList();
            return results;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public SolicitudAyuda getByIdUsuario(Long idusuario) {
        return (SolicitudAyuda) em.find(SolicitudAyuda.class, idusuario);
    }

    public List<SolicitudAyuda> listadeSolicitudAyudaByFormulario(int init, int finish, String filter) {
        try {
            String sql = "select p ";
            sql += "from SolicitudAyuda p";
            if (filter != null && !filter.equals("")) {
                sql += " JOIN FETCH p.parametro f where upper(f.descripcion) like :param order by p.id";
            }
            Query query = em.createQuery(sql, SolicitudAyuda.class);
            if (filter != null && !filter.equals("")) {
                query.setParameter("param", filter.toUpperCase());
            }
            int i = query.getResultList().size();
            System.out.print(i);
            List<SolicitudAyuda> results = query.getResultList();
            return results;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public void borrar(SolicitudAyuda cargo) {
        try {
            em.remove(em.contains(cargo) ? cargo : em.merge(cargo));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<SolicitudAyuda> listarPorFormulario(Formulario value) {
        try {
            String sql = "select p ";
            sql += "from SolicitudAyuda p JOIN FETCH p.parametro f WHERE f.id=:idForm";
            Query query = em.createQuery(sql, SolicitudAyuda.class).setParameter("idForm", value.getId());
            int i = query.getResultList().size();
            System.out.print(i);
            List<SolicitudAyuda> results = query.getResultList();
            return results;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public SolicitudAyuda listarPorIdFuncionario(Long idsocio) {
        try {
            return (SolicitudAyuda) em.createQuery("select hf from SolicitudAyuda hf JOIN FETCH hf.socio f WHERE f.id=:id")
                    .setParameter("id", idsocio)
                    .getSingleResult();
        } catch (Exception e) {
            return null;
        } finally {
        }
    }

    public SolicitudAyuda getById(Long id) {
        SolicitudAyuda marcacion = null;
        try {
            marcacion = (SolicitudAyuda) em.createNativeQuery("SELECT * FROM public.parametro "
                    + " where idreloj=" + id + ";", SolicitudAyuda.class)
                    .getSingleResult();
        } catch (Exception e) {
            e.printStackTrace();
            marcacion = new SolicitudAyuda();
        }
        return marcacion;
    }

    public SolicitudAyuda listarPorIdReloj(Long idReloj) {
        try {
            return (SolicitudAyuda) em.createQuery("select hf from SolicitudAyuda hf JOIN FETCH hf.socio f WHERE hf.idreloj=:id")
                    .setParameter("id", idReloj)
                    .getSingleResult();
        } catch (Exception e) {
            return null;
        } finally {
        }
    }

    public List<SolicitudAyuda> listarPorFechas(Date fechaDesde, Date fechaHasta, Long idsocio) {
        String filterFuncionario = "";
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        String fechaDesdeText = formatter.format(fechaDesde);
        String fechaHastaText = formatter.format(fechaHasta);
        if (idsocio != null) {
            filterFuncionario = "AND f.idsocio=" + idsocio;
        }
        try {
            String sql = "select * ";
            sql += "from licencias p LEFT join socio f on p.idsocio=f.idsocio WHERE p.fecha BETWEEN '" + fechaDesdeText + "' AND '" + fechaHastaText + "' " + filterFuncionario + " ORDER BY p.id DESC";
            Query query = em.createNativeQuery(sql, SolicitudAyuda.class);
            return query.getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ArrayList<>();
    }

    public List<SolicitudAyuda> listadeSolicitudAyudaSinConfirmar() {
        try {
            String sql = "select u from SolicitudAyuda u WHERE upper(u.estado)='PENDIENTE' ORDER BY u.id DESC";
            Query query = em.createQuery(sql);
            int i = query.getResultList().size();
            System.out.print(i);
            return query.getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public SolicitudAyuda listarPorId(Long id) {
        try {
            return (SolicitudAyuda) em.createQuery("select r from SolicitudAyuda r JOIN FETCH r.socio f WHERE r.id=:id ORDER BY r.id DESC")
                    .setParameter("id", id)
                    .getSingleResult();
        } catch (Exception e) {
            System.out.println("-> " + e.getLocalizedMessage());
            System.out.println("-> " + e.fillInStackTrace());
            //return (SolicitudAyuda) em.createQuery("select r from SolicitudAyuda r JOIN FETCH r.socio f JOIN FETCH r.parametro form ORDER BY r.id DESC")
            //.setParameter("id", id)
            ///      .getSingleResult();
            return new SolicitudAyuda();
        } finally {
        }
    }

    public SolicitudAyuda listarPorIdHere(Long id) {
        try {
            return (SolicitudAyuda) em.createQuery("select r from SolicitudAyuda r JOIN FETCH r.socio f WHERE f.cedula=:id ORDER BY r.id DESC")
                    .setParameter("id", id)
                    .getSingleResult();
        } catch (Exception e) {
            return new SolicitudAyuda();
        } finally {
        }
    }

    public SolicitudAyuda listarPorCiSocio(String id) {
        try {
            return (SolicitudAyuda) em.createQuery("select r from SolicitudAyuda r JOIN FETCH r.socio f WHERE f.cedula=:id ORDER BY r.id DESC")
                    .setParameter("id", id)
                    .getSingleResult();
        } catch (Exception e) {
            return new SolicitudAyuda();
        } finally {
        }
    }

    private List<SolicitudAyuda> listadeLicenciaSinFuncionario(String filtroFuncionario, String filtroAprobado, String filtroFormulario, Usuario usuario, String fechaDesde, String fechaHasta) {
        // String filterFechas = " (p.fecha>='" + fechaDesde + "' AND p.fechafin<='" + fechaHasta + "') ";
        String filterFechas = " (p.fecha::date>='" + fechaDesde + "' AND p.fecha::date<='" + fechaHasta + "') ";
        try { //Si teiene nivel jefe, encargado o administrador para que liste todos

            String sql = "select * ";
            if (filtroAprobado == null) {
                String parametro = filtroFormulario == null ? "%%" : "%" + filtroFormulario.toUpperCase() + "%";
                sql += "from solicitud_ayuda p left JOIN necesidad_ayuda form on form.id=p.id_necesidad";
                sql += " WHERE upper(form.descripcion) LIKE '" + parametro + "' AND " + filterFechas + " order by p.id DESC ";
            } else if (filtroAprobado != null) {
                long val = 0L;
                if (filtroAprobado.equalsIgnoreCase("APROBADO")) {
                    val = 1L;
                } else if (filtroAprobado.equalsIgnoreCase("RECHAZADO")) {
                    val = 2L;
                }

                String parametro = filtroFormulario == null ? "%%" : "" + filtroFormulario.toUpperCase() + "%";

                sql += "from solicitud_ayuda p left JOIN necesidad_ayuda form on form.id=p.id_necesidad";
                sql += " WHERE upper(form.descripcion) LIKE '" + parametro + "' and " + filterFechas + " AND UPPER(p.estado)='" + filtroAprobado.toUpperCase() + "' order by p.id DESC ";
            }
            Query query = em.createNativeQuery(sql, SolicitudAyuda.class);
            int i = query.getResultList().size();
            System.out.print(i);
            List<SolicitudAyuda> results = query.getResultList();
            return results;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<SolicitudAyuda> listadeSolicitudAyudaByFuncionarioAndUsuario(int init, int finish, String filtroFuncionario, String filtroAprobado, String filtroFormulario, Usuario usuario, Date fechaDesde, Date fechaHasta, String ciudad, String dpto, String ciSocio) {
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
//        String fechaDesdeText = formatter.format(fechaDesde);
//        String fechaHastaText = formatter.format(fechaHasta);
        //String filterFechas = " (p.fecha>='" + fechaDesdeText + "' AND p.fechafin<='" + fechaHastaText + "') ";
        String filterFechas = "";
        if (fechaDesde != null || fechaHasta != null) {
            String fechaDesdeText = formatter.format(fechaDesde);
            String fechaHastaText = formatter.format(fechaHasta);
            filterFechas = " AND (p.fecha BETWEEN '" + fechaDesdeText + " 00:00:01' AND '" + fechaHastaText + " 23:59:59') ";
        }
//        String filterFechas = " (p.fecha BETWEEN '" + fechaDesdeText + " 01:00:00' AND '" + fechaHastaText + " 23:59:59') ";
        if (filtroFormulario != null && filtroFormulario.equalsIgnoreCase("SOLICITUD DE PRODUCCIÓN")) {
            return null;//listadeLicenciaSinFuncionario(filtroFuncionario, filtroAprobado, filtroFormulario, usuario, fechaDesdeText, fechaHastaText);
        } else {

            String ced = "";
            if (ciSocio != null && !ciSocio.equalsIgnoreCase("")) {
                ced = " AND func.cedula='" + ciSocio.toUpperCase() + "'";
            }
            String depar = "";
            if (dpto != null && !dpto.equalsIgnoreCase("")) {
                depar = " AND dep.descripcion LIKE '" + dpto.toUpperCase() + "%'";
            }
            String ciud = "";
            if (ciudad != null && !ciudad.equalsIgnoreCase("")) {
                ciud = " AND UPPER(ciud.descripcion) LIKE '" + ciudad.toUpperCase() + "%'";
            }

            try {
                //Si teiene nivel jefe, encargado o administrador para que liste todos

                String sql = "select * ";
                if (filtroAprobado == null) {
                    String socio = filtroFuncionario == null ? "%%" : "%" + filtroFuncionario.toUpperCase() + "%";
                    String parametro = filtroFormulario == null ? "%%" : "" + filtroFormulario.toUpperCase() + "%";
                    sql += "from solicitud_ayuda p left JOIN necesidad_ayuda form on form.id=p.id_necesidad";
                    sql += " left JOIN departamento dep on p.iddepartamento=dep.id";
                    sql += " left JOIN ciudad ciud on p.idciudad=ciud.id";
                    sql += " left join socio func on p.idsocio=func.id where ((concat(func.nombre, ' ', func.apellido) like '" + socio + "') OR func is null) ";
                    sql += " " + filterFechas + " AND upper(form.descripcion) LIKE '" + parametro + "' " + ciud + ced + depar + " order by p.id DESC ";
                } else if (filtroAprobado != null) {
                    long val = 0L;
                    if (filtroAprobado.equalsIgnoreCase("APROBADO")) {
                        val = 1L;
                    } else if (filtroAprobado.equalsIgnoreCase("RECHAZADO")) {
                        val = 2L;
                    }

                    String socio = filtroFuncionario == null ? "%%" : "%" + filtroFuncionario.toUpperCase() + "%";
                    String parametro = filtroFormulario == null ? "%%" : "" + filtroFormulario.toUpperCase() + "%";

                    sql += "from solicitud_ayuda p left JOIN necesidad_ayuda form on form.id=p.id_necesidad";
                    sql += " left JOIN departamento dep on p.iddepartamento=dep.id";
                    sql += " left JOIN ciudad ciud on p.idciudad=ciud.id";
                    sql += " left join socio func on p.idsocio=func.id where ((concat(func.nombre, ' ', func.apellido) like '" + socio + "') OR func is null) ";
                    sql += " AND upper(form.descripcion) LIKE '" + parametro + "' " + filterFechas + " and UPPER(p.estado)='" + filtroAprobado.toUpperCase() + "' " + ciud + ced + depar + " order by p.id DESC ";
                }
                Query query = em.createNativeQuery(sql, SolicitudAyuda.class);
                int i = query.getResultList().size();
                System.out.print(i);
                List<SolicitudAyuda> results = query.getResultList();
                return results;

            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    public SolicitudAyuda listarPorIdgetAll(Long id) {
        try {
            return (SolicitudAyuda) em.createQuery("select r from SolicitudAyuda r JOIN FETCH r.socio f JOIN FETCH r.motivoSolicitudAyuda m JOIN FETCH r.necesidadAyudaDos n WHERE r.id=:id ORDER BY r.id DESC")
                    .setParameter("id", id)
                    .getSingleResult();
        } catch (Exception e) {
            return new SolicitudAyuda();
        } finally {
        }
    }
}
