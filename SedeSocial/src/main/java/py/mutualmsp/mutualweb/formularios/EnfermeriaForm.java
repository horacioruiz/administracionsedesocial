package py.mutualmsp.mutualweb.formularios;

import com.github.appreciated.material.MaterialTheme;
import com.vaadin.data.Binder;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.Page;
import com.vaadin.ui.*;
import java.time.LocalDate;
import java.util.List;

import java.util.function.Consumer;
import java.util.logging.Level;
import java.util.logging.Logger;
import py.mutualmsp.mutualweb.dao.AgendamientoDao;
import py.mutualmsp.mutualweb.dao.AgendamientoDetalleDao;
import py.mutualmsp.mutualweb.dao.EnfermeriaDao;
import py.mutualmsp.mutualweb.dao.InvitadoDao;
import py.mutualmsp.mutualweb.dao.MedicoDao;
import py.mutualmsp.mutualweb.dao.NucleoFamiliarDao;
import py.mutualmsp.mutualweb.dao.SocioDao;
import py.mutualmsp.mutualweb.dao.TipoincidenciaDao;
import py.mutualmsp.mutualweb.entities.Agendamiento;
import py.mutualmsp.mutualweb.entities.Enfermeria;
import py.mutualmsp.mutualweb.entities.Invitado;
import py.mutualmsp.mutualweb.entities.Medico;
import py.mutualmsp.mutualweb.entities.NucleoFamiliar;
import py.mutualmsp.mutualweb.entities.Socio;
import py.mutualmsp.mutualweb.entities.Tipoincidencia;
import py.mutualmsp.mutualweb.util.ResourceLocator;
import py.mutualmsp.mutualweb.util.Services;

/**
 * Created by Alfre on 15/6/2016.
 */
public class EnfermeriaForm extends CssLayout {

    DateField fechas = new DateField("Fecha Inicio");
    DateField fechavigencia = new DateField("Fecha Fin");
    TextField ci = new TextField("Nro. Documento");
    TextField nombres = new TextField("Nombres");
    Button btnSearch;
//    TextField apellidos = new TextField("Apellidos");
//    TextField agendamientos = new TextField("N° Agendamiento");
    TextField observacion = new TextField("Observación");
//    ComboBox<Agendamiento> agendamientos = new ComboBox<>("Agendamiento");
    ComboBox<Tipoincidencia> tipoincidencia = new ComboBox<>("Tipo de incidencia");
    ComboBox<Medico> medico = new ComboBox<>("Médico");
    CheckBox aprobo = new CheckBox("Aprobó");

    long idagendamiento = 0;

    Button save = new Button("Guardar");
    Button cancel = new Button("Cancelar");
    Button delete = new Button("Delete");
    Binder<Enfermeria> binder = new Binder<>(Enfermeria.class);
    AgendamientoDao agendamientoDao = ResourceLocator.locate(AgendamientoDao.class);
    AgendamientoDetalleDao agendamientoDetalleDao = ResourceLocator.locate(AgendamientoDetalleDao.class);
    EnfermeriaDao enfermeriaController = ResourceLocator.locate(EnfermeriaDao.class);
    InvitadoDao invitadoaController = ResourceLocator.locate(InvitadoDao.class);
    NucleoFamiliarDao nucleoController = ResourceLocator.locate(NucleoFamiliarDao.class);
    SocioDao socioController = ResourceLocator.locate(SocioDao.class);
    TipoincidenciaDao tipoincidenciaDao = ResourceLocator.locate(TipoincidenciaDao.class);
    MedicoDao medicoDao = ResourceLocator.locate(MedicoDao.class);

    private Consumer<Enfermeria> saveListener;
    private Consumer<Enfermeria> deleteListener;
    private Consumer<Enfermeria> cancelListener;
    Services ejb = ResourceLocator.locate(Services.class);
    Enfermeria funcionario;

    public EnfermeriaForm() {
        addStyleName("product-form-wrapper");
        addStyleName("product-form");
        VerticalLayout verticalLayout = createForm();
        addComponent(verticalLayout);

        fechas.setValue(LocalDate.now());
        binder.forField(fechas).bind(Enfermeria::getFecha, Enfermeria::setFecha);
        binder.forField(fechavigencia).bind(Enfermeria::getFechavigencia, Enfermeria::setFechavigencia);
        binder.bind(ci, Enfermeria::getCedula, Enfermeria::setCedula);
        binder.bind(nombres, Enfermeria::getNombre, Enfermeria::setNombre);
        binder.bind(observacion, Enfermeria::getObservacion, Enfermeria::setObservacion);
        List<Agendamiento> listaAgendamiento = agendamientoDao.listaAgendamiento();
//        nombres.setEnabled(false);
//        agendamientos.setItems(listaAgendamiento);
        //agendamientos.setItemCaptionGenerator(new SimpleDateFormat("MM/dd/yyyy").format(Agendamiento::getFecha));
//        agendamientos.setItemCaptionGenerator(Agendamiento::getDescripcionData);
//        binder.bind(agendamientos,
//                (Enfermeria source) -> source.getIdagendamiento(),
//                (Enfermeria bean, Agendamiento fieldvalue) -> {
//                    bean.setIdagendamiento(fieldvalue);
//                });
        List<Tipoincidencia> listatipoincidencia = tipoincidenciaDao.listaTipoincidencia();
        tipoincidencia.setItems(listatipoincidencia);
        tipoincidencia.setItemCaptionGenerator(Tipoincidencia::getDescripcion);
        binder.bind(tipoincidencia,
                (Enfermeria source) -> source.getIdtipoIncidencia(),
                (Enfermeria bean, Tipoincidencia fieldvalue) -> {
                    bean.setIdtipoIncidencia(fieldvalue);
                });
        List<Medico> listamedico = medicoDao.listaMedico();
        medico.setItems(listamedico);
        medico.setItemCaptionGenerator(Medico::getNombreCompleto);
        binder.bind(medico,
                (Enfermeria source) -> source.getIdmedico(),
                (Enfermeria bean, Medico fieldvalue) -> {
                    bean.setIdmedico(fieldvalue);
                });
//        binder.bind(agendamientos, Enfermeria::getNumeroagendamiento, Enfermeria::setNumeroagendamiento);
//        binder.bind(celular, Enfermeria::getCelular, Enfermeria::setCelular);
//        binder.bind(email, Enfermeria::getEmail, Enfermeria::setEmail);
        binder.bind(aprobo, Enfermeria::getAprobo, Enfermeria::setAprobo);
        binder.bindInstanceFields(this);

        save.addClickListener(cl -> {
            try {
                save();
            } catch (Exception e) {
                e.printStackTrace();
                Notification.show(e.getMessage());
            }
        });
        save.setEnabled(false);

        btnSearch.addClickListener(
                clickEvent -> buscarData()
        );

        nombres.addBlurListener(e -> {
            validate();
        });
        fechas.addBlurListener(e -> {
            validate();
        });
        fechavigencia.addBlurListener(e -> {
            validate();
        });
        ci.addBlurListener(e -> {
            validate();
        });
        tipoincidencia.addBlurListener(e -> {
            validate();
        });
        medico.addBlurListener(e -> {
            validate();
        });

        cancel.addClickListener(cl -> {
            try {
                setVisible(false);
                cancelListener.accept(funcionario);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

        delete.addClickListener(cl -> {
            enfermeriaController.borrar(funcionario);
            deleteListener.accept(funcionario);
            showForm(false);
        });

    }

    private VerticalLayout createForm() {
        VerticalLayout layout = new VerticalLayout();
        layout.addStyleName("form-layout");
//        nombres.setWidth("100%");
        HorizontalLayout hlayoutBar = new HorizontalLayout();

        btnSearch = new Button();
        btnSearch.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_PRIMARY);
        btnSearch.setIcon(FontAwesome.SEARCH);

        ci.setWidth("100%");
        hlayoutBar.addComponent(ci);
        hlayoutBar.addComponent(btnSearch);
        hlayoutBar.addComponent(aprobo);
        layout.addComponent(hlayoutBar);

        HorizontalLayout hlayoutBar2 = new HorizontalLayout();
        nombres.setWidth("100%");
        hlayoutBar2.addComponent(nombres);
//        agendamientos.setWidth("100%");
//        hlayoutBar2.addComponent(agendamientos);
        layout.addComponent(hlayoutBar2);

        HorizontalLayout hlayout = new HorizontalLayout();
        hlayout.addComponent(fechas);
        hlayout.addComponent(fechavigencia);
        layout.addComponent(hlayout);

        HorizontalLayout hlayout2 = new HorizontalLayout();
        tipoincidencia.setWidth("100%");
        hlayout2.addComponent(tipoincidencia);

        medico.setWidth("100%");
        hlayout2.addComponent(medico);
        layout.addComponent(hlayout2);

        observacion.setWidth("100%");
        layout.addComponent(observacion);

        CssLayout expander = new CssLayout();
        expander.setSizeFull();
        expander.setStyleName("expander");
        layout.addComponent(expander);
        layout.setExpandRatio(expander, 1.0F);

        save.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_FRIENDLY);
        cancel.addStyleName(MaterialTheme.BUTTON_ROUND);
        delete.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_DANGER);

        HorizontalLayout hl = new HorizontalLayout();
        hl.addComponents(save, cancel);

        layout.addComponent(hl);

        return layout;
    }

    private void validate() {
        boolean savedEnabled = true;
        if (fechas == null || fechas.isEmpty()) {
            savedEnabled = false;
        }
        if (nombres == null || nombres.isEmpty()) {
            savedEnabled = false;
        }
        if (tipoincidencia == null || tipoincidencia.isEmpty()) {
            savedEnabled = false;
        }
        if (medico == null || medico.isEmpty()) {
            savedEnabled = false;
        }
        if (ci == null || ci.isEmpty()) {
            savedEnabled = false;
        }
        save.setEnabled(savedEnabled);
    }

    private void showForm(boolean show) {
        if (show) {
            addStyleName("visible");
        } else {
            removeStyleName("visible");
        }

        setEnabled(show);
    }

    private void save() {
        try {
            enfermeriaController.guardar(funcionario);
            setVisible(false);
            saveListener.accept(funcionario);
        } catch (Exception ex) {
            Notification.show("Atención", "Ocurio un error al intentar registrar", Notification.Type.ERROR_MESSAGE);
            saveListener.accept(funcionario);
            Logger.getLogger(EnfermeriaForm.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void setEnfermeria(Enfermeria funcionario) {
        this.funcionario = funcionario;
        binder.setBean(funcionario);
        delete.setVisible(((funcionario.getId() != null)));
        setVisible(true);
        nombres.selectAll();
        // !!! ??? Scroll to the top as this is not a Panel, using JavaScript
        Page.getCurrent().getJavaScript().execute("window.document.getElementById('" + getId() + "').scrollTop = 0;");
    }

    public void setSaveListener(Consumer<Enfermeria> saveListener) {
        this.saveListener = saveListener;
    }

    public void setDeleteListener(Consumer<Enfermeria> deleteListener) {
        this.deleteListener = deleteListener;
    }

    public void setCancelListener(Consumer<Enfermeria> cancelListener) {
        this.cancelListener = cancelListener;
    }

    private void buscarData() {
        LocalDate fechahoy = LocalDate.now();
        List<Invitado> listInvitado = invitadoaController.listarInvitadoData("", ci.getValue().trim());
        if (listInvitado.size() <= 0) {
            List<NucleoFamiliar> listNucleoFamiliar = nucleoController.listarNucleoFamiliarData("", ci.getValue().trim());
            if (listNucleoFamiliar.size() <= 0) {
                Socio socio = socioController.listarSocioPorCI(ci.getValue().trim());

                String nombreapellido = socio.getNombre() + " " + socio.getApellido();
                if (!nombreapellido.equalsIgnoreCase("null null")) {
                    fechas.setValue(fechahoy);
                    fechavigencia.setValue(fechahoy.plusYears(1));
                    nombres.setValue(nombreapellido);
                    try {
                        idagendamiento = agendamientoDetalleDao.getIdAgendamiento(ci.getValue().trim(), fechahoy);
                    } catch (Exception e) {
                    } finally {
                    }

                } else {
                    nombres.setValue("");
                    idagendamiento = 0;
                }
            } else {
                String nombreapellido = listNucleoFamiliar.get(0).getNombre() + " " + listNucleoFamiliar.get(0).getApellido();
                if (!nombreapellido.equalsIgnoreCase("null null")) {
                    fechas.setValue(fechahoy);
                    fechavigencia.setValue(fechahoy.plusYears(1));
                    nombres.setValue(nombreapellido);

                    try {
                        idagendamiento = agendamientoDetalleDao.getIdAgendamiento(ci.getValue().trim(), fechahoy);
                    } catch (Exception e) {
                    } finally {
                    }
                } else {
                    nombres.setValue("");
                    idagendamiento = 0;
                }
            }
        } else {
            String nombreapellido = listInvitado.get(0).getNombre() + " " + listInvitado.get(0).getApellido();
            if (!nombreapellido.equalsIgnoreCase("null null")) {
                fechas.setValue(fechahoy);
                fechavigencia.setValue(fechahoy.plusYears(1));
                nombres.setValue(nombreapellido);

                try {
                    idagendamiento = agendamientoDao.listaAgendamientoParametros("", ci.getValue().trim(), fechahoy, fechahoy, "", "").get(0).getId();
                } catch (Exception e) {
                } finally {
                }
            } else {
                nombres.setValue("");
                idagendamiento = 0;
            }
        }
    }

    public void setEnfermeria(Enfermeria enfermeria, String ex, Long id) {
        this.funcionario = enfermeria;
        binder.setBean(funcionario);
        delete.setVisible(((funcionario.getId() != null)));
        setVisible(true);
//        try {
//            aprobo.setValue(enfermeria.getAprobo());
//        } catch (Exception e) {
//        }
        try {
            ci.setValue(enfermeria.getCedula());
        } catch (Exception e) {
        }
        try {
            nombres.setValue(enfermeria.getNombre());
        } catch (Exception e) {
        }
        try {
            fechas.setValue(enfermeria.getFecha());
        } catch (Exception e) {
        }
        try {
            fechavigencia.setValue(enfermeria.getFechavigencia());
        } catch (Exception e) {
        }
        try {
            tipoincidencia.setValue(enfermeria.getIdtipoIncidencia());
        } catch (Exception e) {
        }
        try {
            medico.setValue(enfermeria.getIdmedico());
        } catch (Exception e) {
        }
        try {
            observacion.setValue(enfermeria.getObservacion());
        } catch (Exception e) {
        }

        nombres.selectAll();
        // !!! ??? Scroll to the top as this is not a Panel, using JavaScript
        Page.getCurrent().getJavaScript().execute("window.document.getElementById('" + getId() + "').scrollTop = 0;");
    }
}
