package py.mutualmsp.mutualweb.formularios;

import com.github.appreciated.material.MaterialTheme;
import com.vaadin.data.ValueProvider;
import com.vaadin.event.ShortcutAction;
import com.vaadin.event.ShortcutListener;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.server.Setter;
import com.vaadin.ui.*;
import com.vaadin.ui.Button;
import com.vaadin.ui.TabSheet.SelectedTabChangeEvent;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.Window;
import java.io.File;
import java.io.FileOutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.temporal.ChronoField;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.function.Consumer;
import py.mutualmsp.mutualweb.dao.CargoDao;
import py.mutualmsp.mutualweb.dao.FormularioDao;
import py.mutualmsp.mutualweb.dao.FuncionarioDao;
import py.mutualmsp.mutualweb.dao.InspeccionDao;
import py.mutualmsp.mutualweb.dao.InvitadoDao;
import py.mutualmsp.mutualweb.dao.MotivosDao;
import py.mutualmsp.mutualweb.dao.NucleoFamiliarDao;
import py.mutualmsp.mutualweb.dao.SocioDao;
import py.mutualmsp.mutualweb.entities.Formulario;
import py.mutualmsp.mutualweb.entities.Funcionario;
import py.mutualmsp.mutualweb.entities.Inspeccion;
import py.mutualmsp.mutualweb.entities.Invitado;
import py.mutualmsp.mutualweb.entities.Motivos;
import py.mutualmsp.mutualweb.entities.NucleoFamiliar;
import py.mutualmsp.mutualweb.entities.Socio;
import py.mutualmsp.mutualweb.util.DateUtils;
import py.mutualmsp.mutualweb.util.ResourceLocator;

/**
 * Created by Alfre on 28/6/2016.
 */
public class InspeccionForm extends Window {

    private TextField txtCedulaFuncionario = new TextField("Ingrese cédula (*)");
    private TextField txtNombreFuncionario = new TextField("Nombres");
    private TextField txtApellidoFuncionario = new TextField("Apellidos");

    private TextField txtCedulaSocio = new TextField("Ingrese cédula Socio");
    private TextField txtNombreSocio = new TextField("Nombres Socio");
    private TextField txtApellidoSocio = new TextField("Apellidos Socio");

    private TextField txtSangre = new TextField("Grupo Sanguineo");
    private TextField txtEdad = new TextField("Edad");
    private TextField txtPeso = new TextField("Peso");
    ComboBox<String> cmbSexo = new ComboBox<>("Seleccione Sexo");
    ComboBox<String> cmbParentesco = new ComboBox<>("Seleccione Parentesco");
    private TextField txtSeguro = new TextField("Seguro Médico");
    private DateField fecha = new DateField("Fecha Fin Vigencia");
    private DateField fechaRegistro = new DateField("Fecha Registro");
//    Antecedentes
    private CheckBox chkDST = new CheckBox("DST");
    private CheckBox chkCONV = new CheckBox("CONV");
    private CheckBox chkASMA = new CheckBox("ASMA");
    private CheckBox chkHTA = new CheckBox("HTA");
    private CheckBox chkCARD = new CheckBox("CARD");
    private CheckBox chkEPOC = new CheckBox("EPOC");
    private CheckBox chkACV = new CheckBox("ACV");
    private CheckBox chkOTROS = new CheckBox("OTROS");
    private CheckBox chkAlergico = new CheckBox("Alergico");
    private TextArea txtAlergico = new TextArea("Especificar Alergia");
    private TextArea txtMotivoConsulta = new TextArea("Motivo Consulta");
//    Signos Vitales
    private TextField txtTA = new TextField("T.A.");
    private TextField txtFC = new TextField("F.C.");
    private TextField txtT = new TextField("T.");
    private TextField txtId = new TextField();

    Inspeccion inspeccion;

    private TextArea txtHallazgo = new TextArea("Hallazgos Positivos del Examen Físico (o datos negativos de importancia)");
    private TextArea txtTratamiento = new TextArea("Tratamientos Actuales");
    private TextArea txtContacto = new TextArea("Contactos de Emergencia");

    private TextField txtMotivo_Consulta = new TextField("Motivo de la consulta");
    private TextArea txtMedicacionConsulta = new TextArea("Medicacion o indicaciones");
    private TextArea txtObservacionConsulta = new TextArea("Observacion");
    
    boolean mostrarFechaAnterior = false;
    private int editar = 0;
    InspeccionDao controller = ResourceLocator.locate(InspeccionDao.class);
    NucleoFamiliarDao controllerNucleoFamiliarDao = ResourceLocator.locate(NucleoFamiliarDao.class);
    InvitadoDao controllerInvitadoDao = ResourceLocator.locate(InvitadoDao.class);
    SocioDao controllerSocioDao = ResourceLocator.locate(SocioDao.class);

    private Button guardar = new Button("Guardar");
    private Button cancelar = new Button("Cancelar");
    private Button btnSearch = new Button("");
    private Button btnSearchSocio = new Button("");

    final FormLayout form = new FormLayout();
    final HorizontalLayout mainLayout = new HorizontalLayout();
    long parametroVacaciones = 0;
    boolean booleanVacaciones = false;

    TextField txtEdadForm = new TextField("Cargo");
    TextField txtObservacionForm = new TextField("Descripción actividad (*)");
    DateTimeField fechaHoraForm = new DateTimeField("Fecha/Hora estipulada (*)");
    private Button btnAgregar = new Button("Agregar");

    private ComboBox<Motivos> motivo = new ComboBox<>("Motivos (*)");
    private ComboBox<Funcionario> encargado = new ComboBox<>("Encargado (*)");
//    private ComboBox<Funcionario> rrhh = new ComboBox<>("Funcionario RRHH");
//    private TextField txtOtroMotivo = new TextField("Otro motivo");
    private TextArea txtOtroMotivo = new TextArea("Otro motivo");
//    Upload upload = upload = new Upload("Subir archivo", receiver);
    final Image image = new Image("Imagen");
    Label labelUrl = new Label();
    String fileName = "";
    String ubicacion = "";
    String filename;
    byte[] content;
    public File file;
    String url = "";

    private Consumer<Inspeccion> saveListener;
    private Consumer<Inspeccion> deleteListener;
    private Consumer<Inspeccion> cancelListener;

    HashMap<Long, String> mapeo = new HashMap<>();
    int numRowSelected = 0;

    FuncionarioDao funcionarioDao = ResourceLocator.locate(FuncionarioDao.class);
    FormularioDao formularioDao = ResourceLocator.locate(FormularioDao.class);
    CargoDao cargoDao = ResourceLocator.locate(CargoDao.class);
    MotivosDao motivosDao = ResourceLocator.locate(MotivosDao.class);
    InspeccionDao solicitudDao = ResourceLocator.locate(InspeccionDao.class);

    private boolean enviarCorreos = false;
    private Inspeccion solicitud;
    String destinarariosString = "";
    TabSheet tabsheet = new TabSheet();

    // Create upload stream
    FileOutputStream fos = null; // Stream to write to
    // Implement both receiver that saves upload in a file and
    // listener for successful upload

    private void updateFormulario() {
//        if (cbFuncionario != null && cbFuncionario.getValue() != null) {
//            Funcionario func = funcionarioDao.listarFuncionarioPorCI(cbFuncionario.getValue().getCedula());
//            txtSangreForm.setValue(func.getDependencia().getDescripcion());
//            txtEdadForm.setValue(func.getCargo().getDescripcion());
//            txtNombreFuncionario.setValue(func.getNombreCompleto());
//        }
    }

    private void cargarGrilla() {
    }

    private boolean validateForm() {
        boolean savedEnabled = true;
        if (txtCedulaFuncionario.getValue().equalsIgnoreCase("") || txtCedulaFuncionario.isEmpty()) {
            savedEnabled = false;
        }
        if (txtNombreFuncionario.getValue().equalsIgnoreCase("") || txtNombreFuncionario.isEmpty()) {
            savedEnabled = false;
        }
        return savedEnabled;
    }

    public void setConsultaInspeccion(Inspeccion c) {
        try {
            try {
                this.solicitud = c;
                DateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
                String fecha = formatter.format(c.getFecha());
            } catch (Exception e) {
            } finally {
            }
            try {
                txtId.setValue(solicitud.getId() + "");
            } catch (Exception e) {
                txtId.setValue("");
            } finally {
            }
            try {
                txtPeso.setValue(solicitud.getPeso() + "");
            } catch (Exception e) {
                txtPeso.setValue("");
            } finally {
            }
            try {
                txtCedulaFuncionario.setValue(solicitud.getCedula());
            } catch (Exception e) {
                txtCedulaFuncionario.setValue("");
            } finally {
            }
            try {
                txtNombreFuncionario.setValue(solicitud.getNombre());
            } catch (Exception e) {
                txtNombreFuncionario.setValue("");
            } finally {
            }
            try {
                txtApellidoFuncionario.setValue(solicitud.getApellido());
            } catch (Exception e) {
                txtApellidoFuncionario.setValue("");
            } finally {
            }
            try {
                txtEdad.setValue(solicitud.getEdad() + "");
            } catch (Exception e) {
                txtEdad.setValue("");
            } finally {
            }

            try {
                txtCedulaSocio.setValue(solicitud.getCedulasocio());
            } catch (Exception e) {
                txtCedulaSocio.setValue("");
            } finally {
            }
            try {
                txtNombreSocio.setValue(solicitud.getNombresocio());
            } catch (Exception e) {
                txtNombreSocio.setValue("");
            } finally {
            }
            try {
                txtApellidoSocio.setValue(solicitud.getApellidosocio());
            } catch (Exception e) {
                txtApellidoSocio.setValue("");
            } finally {
            }

            try {
                txtSangre.setValue(solicitud.getGruposanguineo());
            } catch (Exception e) {
                txtSangre.setValue("");
            } finally {
            }
            try {
                cmbSexo.setValue(solicitud.getSexo().equalsIgnoreCase("M") ? "MASCULINO" : "FEMENINO");
            } catch (Exception e) {
                cmbSexo.setValue("");
            } finally {
            }
            try {
                txtSeguro.setValue(solicitud.getSeguromedico());
            } catch (Exception e) {
                txtSeguro.setValue("");
            } finally {
            }
            try {
                fecha.setValue(DateUtils.asLocalDate(solicitud.getFecha()));
            } catch (Exception e) {
                Date fechaHoy = new Date();
                fechaHoy.setHours(0);
                fechaHoy.setMinutes(0);
                fechaHoy.setSeconds(0);
                fecha.setValue(DateUtils.asLocalDate(fechaHoy));
            } finally {
            }
            try {
                fechaRegistro.setValue(DateUtils.asLocalDate(solicitud.getFecharegistro()));
            } catch (Exception e) {
                Date fechaHoy = new Date();
                fechaHoy.setHours(0);
                fechaHoy.setMinutes(0);
                fechaHoy.setSeconds(0);
                fechaRegistro.setValue(DateUtils.asLocalDate(fechaHoy));
            } finally {
            }
            try {
                chkDST.setValue(solicitud.getDst());
            } catch (Exception e) {
                chkDST.setValue(false);
            } finally {
            }
            try {
                chkCONV.setValue(solicitud.getConv());
            } catch (Exception e) {
                chkCONV.setValue(false);
            } finally {
            }
            try {
                chkASMA.setValue(solicitud.getAsma());
            } catch (Exception e) {
                chkASMA.setValue(false);
            } finally {
            }
            try {
                chkHTA.setValue(solicitud.getHta());
            } catch (Exception e) {
                chkHTA.setValue(false);
            } finally {
            }
            try {
                chkCARD.setValue(solicitud.getCard());
            } catch (Exception e) {
                chkCARD.setValue(false);
            } finally {
            }
            try {
                chkEPOC.setValue(solicitud.getEpoc());
            } catch (Exception e) {
                chkEPOC.setValue(false);
            } finally {
            }
            try {
                chkACV.setValue(solicitud.getAcv());
            } catch (Exception e) {
                chkACV.setValue(false);
            } finally {
            }
            try {
                chkOTROS.setValue(solicitud.getOtros());
            } catch (Exception e) {
                chkOTROS.setValue(false);
            } finally {
            }
            try {
                chkAlergico.setValue(solicitud.getAlergico());
            } catch (Exception e) {
                chkAlergico.setValue(false);
            } finally {
            }
            try {
                txtAlergico.setValue(solicitud.getComentarioalergico());
            } catch (Exception e) {
                txtAlergico.setValue("");
            } finally {
            }
            try {
                txtMotivoConsulta.setValue(solicitud.getMotivoconsulta());
            } catch (Exception e) {
                txtMotivoConsulta.setValue("");
            } finally {
            }
            try {
                txtTA.setValue(solicitud.getTa());
            } catch (Exception e) {
                txtTA.setValue("");
            } finally {
            }
            try {
                txtFC.setValue(solicitud.getFc());
            } catch (Exception e) {
                txtFC.setValue("");
            } finally {
            }
            try {
                txtT.setValue(solicitud.getT());
            } catch (Exception e) {
                txtT.setValue("");
            } finally {
            }
            try {
                txtHallazgo.setValue(solicitud.getHallazgos());
            } catch (Exception e) {
                txtHallazgo.setValue("");
            } finally {
            }
            try {
                txtTratamiento.setValue(solicitud.getTratamiento());
            } catch (Exception e) {
                txtTratamiento.setValue("");
            } finally {
            }
            try {
                txtContacto.setValue(solicitud.getContactoemergencia());
            } catch (Exception e) {
                txtContacto.setValue("");
            } finally {
            }
            try {
                cmbParentesco.setValue(solicitud.getParentesco().toUpperCase());
            } catch (Exception e) {
                cmbParentesco.setValue("");
            } finally {

            }
            txtMotivo_Consulta.setValue(solicitud.getMotivoConsulta() == null ? "":solicitud.getMotivoConsulta());
            txtMedicacionConsulta.setValue(solicitud.getMedicacionConsulta() == null ? "":solicitud.getMedicacionConsulta());
            txtObservacionConsulta.setValue(solicitud.getObservacionConsulta() == null ? "":solicitud.getObservacionConsulta());
            setVisible(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void guardarDetalleProduccion(Inspeccion solicitud) {
    }

    private void editarInspeccionProduccion(Inspeccion solicitud) {
        try {
            txtCedulaFuncionario.setValue(solicitud.getCedula());
        } catch (Exception e) {
            txtCedulaFuncionario.setValue("");
        } finally {
        }
        try {
            txtNombreFuncionario.setValue(solicitud.getNombre());
        } catch (Exception e) {
            txtApellidoFuncionario.setValue("");
        } finally {
        }
        try {
            txtApellidoFuncionario.setValue(solicitud.getNombre() + " " + solicitud.getApellido());
        } catch (Exception e) {
            txtApellidoFuncionario.setValue("");
        } finally {
        }
        try {
            txtEdad.setValue(solicitud.getEdad() + "");
        } catch (Exception e) {
            txtEdad.setValue("");
        } finally {
        }
        try {
            txtSangre.setValue(solicitud.getGruposanguineo());
        } catch (Exception e) {
            txtSangre.setValue("");
        } finally {
        }
        try {
            cmbSexo.setValue(solicitud.getSexo());
        } catch (Exception e) {
            cmbSexo.setValue("");
        } finally {
        }
        try {
            txtSeguro.setValue(solicitud.getSeguromedico());
        } catch (Exception e) {
            txtSeguro.setValue("");
        } finally {
        }
        try {
            chkDST.setValue(solicitud.getDst());
        } catch (Exception e) {
            chkDST.setValue(false);
        } finally {
        }
        try {
            chkCONV.setValue(solicitud.getConv());
        } catch (Exception e) {
            chkCONV.setValue(false);
        } finally {
        }
        try {
            chkASMA.setValue(solicitud.getAsma());
        } catch (Exception e) {
            chkASMA.setValue(false);
        } finally {
        }
        try {
            chkHTA.setValue(solicitud.getHta());
        } catch (Exception e) {
            chkHTA.setValue(false);
        } finally {
        }
        try {
            chkCARD.setValue(solicitud.getCard());
        } catch (Exception e) {
            chkCARD.setValue(null);
        } finally {
        }
        try {
            chkEPOC.setValue(solicitud.getEpoc());
        } catch (Exception e) {
            chkEPOC.setValue(null);
        } finally {
        }
        try {
            chkACV.setValue(solicitud.getAcv());
        } catch (Exception e) {
            chkACV.setValue(null);
        } finally {
        }
        try {
            chkOTROS.setValue(solicitud.getOtros());
        } catch (Exception e) {
            chkOTROS.setValue(null);
        } finally {
        }
        try {
            chkAlergico.setValue(solicitud.getAlergico());
        } catch (Exception e) {
            chkAlergico.setValue(false);
        } finally {
        }
        try {
            txtAlergico.setValue(solicitud.getComentarioalergico());
        } catch (Exception e) {
            txtAlergico.setValue("");
        } finally {
        }
        try {
            txtMotivoConsulta.setValue(solicitud.getMotivoconsulta());
        } catch (Exception e) {
            txtMotivoConsulta.setValue("");
        } finally {
        }
        try {
            txtTA.setValue(solicitud.getTa());
        } catch (Exception e) {
            txtTA.setValue("");
        } finally {
        }
        try {
            txtFC.setValue(solicitud.getFc());
        } catch (Exception e) {
            txtFC.setValue("");
        } finally {
        }
        try {
            txtT.setValue(solicitud.getT());
        } catch (Exception e) {
            txtT.setValue("");
        } finally {
        }
        try {
            txtHallazgo.setValue(solicitud.getHallazgos());
        } catch (Exception e) {
            txtHallazgo.setValue("");
        } finally {
        }
        try {
            txtTratamiento.setValue(solicitud.getTratamiento());
        } catch (Exception e) {
            txtTratamiento.setValue("");
        } finally {
        }
        try {
            txtContacto.setValue(solicitud.getContactoemergencia());
        } catch (Exception e) {
            txtContacto.setValue("");
        } finally {
        }
    }

    public InspeccionForm() {
        try {
//            VerticalLayout layout = createForm();
            VerticalLayout layout = new VerticalLayout(tabsheet);
            setContent(layout);
            setWidth("90%");
            setHeight("85%");
            setCaption("Agregar Inspección");
            //setWindowMode(WindowMode.MAXIMIZED);
            setModal(true);
            center();
            txtEdadForm.setEnabled(false);
            mainLayout.setVisible(false);

            Date fechaHoy = new Date();
            fechaHoy.setHours(0);
            fechaHoy.setMinutes(0);
            fechaHoy.setSeconds(0);
            fecha.setValue(DateUtils.asLocalDate(fechaHoy));

            txtCedulaFuncionario.addShortcutListener(new ShortcutListener("Shortcut", ShortcutAction.KeyCode.ENTER, null) {
                @Override
                public void handleAction(Object sender, Object target) {
                    // Do nice stuff
                    if (txtCedulaFuncionario.getValue() != null && !txtCedulaFuncionario.getValue().equals("")) {
//                        calcularCantDiaFuncionario();

                        try {
                            List<Inspeccion> lista = solicitudDao.listarInspeccionPorCI(txtCedulaFuncionario.getValue());
                            txtNombreFuncionario.setValue(lista.get(0).getNombre().toUpperCase());
                            txtApellidoFuncionario.setValue(lista.get(0).getApellido().toUpperCase());
                        } catch (Exception exp) {
                        } finally {
                        }
                    }
                }
            });
            txtNombreFuncionario.addValueChangeListener(n -> {
                txtNombreFuncionario.setValue(txtNombreFuncionario.getValue().toUpperCase());
            });
            txtApellidoFuncionario.addValueChangeListener(a -> {
                txtApellidoFuncionario.setValue(txtApellidoFuncionario.getValue().toUpperCase());
            });

            addCloseListener(closeEvent -> {
                close();
            });

//            fechaInicio.addValueChangeListener(e -> updateFecha());
//            fechaFin.addValueChangeListener(e -> updateFecha());
//            cbFuncionario.addValueChangeListener(e -> updateFormulario());
            guardar.addClickListener(cl -> {
                try {
                    save();
                } catch (Exception e) {
                    e.printStackTrace();
                    Notification.show(e.getMessage());
                }
            });
            btnSearch.addClickListener(cl -> {
                try {
                    buscarParentesco();
                } catch (Exception e) {
                    e.printStackTrace();
                    Notification.show(e.getMessage());
                }
            });
            btnSearchSocio.addClickListener(cl -> {
                try {
                    buscarSocio();
                } catch (Exception e) {
                    e.printStackTrace();
                    Notification.show(e.getMessage());
                }
            });
            btnAgregar.addClickListener(cl -> {
                try {
                    cargarGrilla();
                } catch (Exception e) {
                    e.printStackTrace();
                    Notification.show(e.getMessage());
                }
            });
//            guardar.setEnabled(false);
            cancelar.addClickListener(clickEvent -> {
                close();
            });

            List<String> datos = new ArrayList<>();
            datos.add("MASCULINO");
            datos.add("FEMENINO");
            cmbSexo.setItems(datos);
            cmbSexo.setPlaceholder("Seleccione estados");
            cmbSexo.setItems(datos);

            List<String> listaParentesco = new ArrayList<>();
            listaParentesco.add("Socio".toUpperCase());
            listaParentesco.add("Esposo".toUpperCase());
            listaParentesco.add("Hijo".toUpperCase());
            listaParentesco.add("Padre".toUpperCase());
            listaParentesco.add("Madre".toUpperCase());
            listaParentesco.add("Tio".toUpperCase());
            listaParentesco.add("Primo".toUpperCase());
            listaParentesco.add("Amigo".toUpperCase());
            listaParentesco.add("Hijo".toUpperCase());
            listaParentesco.add("Otros".toUpperCase());
            cmbParentesco.setItems(listaParentesco);
            cmbParentesco.setPlaceholder("Seleccione Parentesco");

// Create tab content dynamically when tab is selected
            tabsheet.addSelectedTabChangeListener(
                    new TabSheet.SelectedTabChangeListener() {
                public void selectedTabChange(SelectedTabChangeEvent event) {
                    // Find the tabsheet
                    TabSheet tabsheet = event.getTabSheet();

                    // Find the tab (here we know it's a layout)
                    Layout tab = (Layout) tabsheet.getSelectedTab();

                    // Get the tab caption from the tab object
                    String caption = tabsheet.getTab(tab).getCaption();

                    // Fill the tab content
                    tab.removeAllComponents();
                    VerticalLayout vl = new VerticalLayout();
                    vl.setWidth("100%");
//                    vl.addComponent(new Label("HOLA MUNDO ->>"+caption));
                    if (caption.equalsIgnoreCase("Datos Basicos")) {
                        vl.addComponent(createForm());
                    }else if(caption.equalsIgnoreCase("consultas")){
                        vl.addComponent(crearConsultaForm());
                    }else {
                        vl.addComponent(createFormMotivoInspeccion());
                    }

                    tab.addComponent(vl);
                }
            });

// Have some tabs
            String[] tabs = {"Datos Basicos", "Antecedentes", "Consultas"};
            for (String caption : tabs) {
                tabsheet.addTab(new VerticalLayout(), caption);
            }

        } catch (Exception e) {
            e.printStackTrace();

        }
    }

    private void buscarParentesco() {
        try {
            List<NucleoFamiliar> listNucleoFliar = controllerNucleoFamiliarDao.listarNucleoFamiliarPorCI(txtCedulaFuncionario.getValue());
            if (listNucleoFliar.size() > 0) {
                txtNombreFuncionario.setValue(listNucleoFliar.get(0).getNombre());
                txtApellidoFuncionario.setValue(listNucleoFliar.get(0).getApellido());

                Socio soc = controllerSocioDao.listarSocioPorId(listNucleoFliar.get(0).getIdsocio() + "");
                txtCedulaSocio.setValue(soc.getCedula());
                txtNombreSocio.setValue(soc.getNombre());
                txtApellidoSocio.setValue(soc.getApellido());
                cmbParentesco.setValue(controller.recuperarParentesco(txtCedulaFuncionario.getValue()));

            } else {
                List<Invitado> listInvitado = controllerInvitadoDao.listarInvitadoPorCI(txtCedulaFuncionario.getValue());
                if (listInvitado.size() > 0) {
                    txtNombreFuncionario.setValue(listInvitado.get(0).getNombre());
                    txtApellidoFuncionario.setValue(listInvitado.get(0).getApellido());

                    Socio soc = controllerSocioDao.listarSocioPorId(listInvitado.get(0).getIdsocio() + "");
                    txtCedulaSocio.setValue(soc.getCedula());
                    txtNombreSocio.setValue(soc.getNombre());
                    txtApellidoSocio.setValue(soc.getApellido());
                    
                    cmbParentesco.setValue(controller.recuperarParentesco(txtCedulaFuncionario.getValue()));
                } else {
                    txtNombreFuncionario.setValue("");
                    txtApellidoFuncionario.setValue("");
                    txtCedulaSocio.setValue("");
                    txtNombreSocio.setValue("");
                    txtApellidoSocio.setValue("");
                    
                    cmbParentesco.setValue("OTROS");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void buscarSocio() {
        try {
            Socio soc = controllerSocioDao.listarSocioPorCI(txtCedulaSocio.getValue());
            txtCedulaSocio.setValue(soc.getCedula());
            txtNombreSocio.setValue(soc.getNombre());
            txtApellidoSocio.setValue(soc.getApellido());
        } catch (Exception e) {
            txtNombreSocio.setValue("");
            txtApellidoSocio.setValue("");
        } finally {
        }
    }

    public static class No {

        public static <SOURCE, TARGET> ValueProvider<SOURCE, TARGET> getter() {
            return source -> null;
        }

        public static <BEAN, FIELDVALUE> Setter<BEAN, FIELDVALUE> setter() {
            return (bean, fieldValue) -> {
                //no op
            };
        }
    }

    private boolean validate() {
        boolean savedEnabled = true;
        return savedEnabled;
    }

    public void editarRegistro(Inspeccion solicitud) {
        setCaption("Editar Inspeccion");
        this.solicitud = solicitud;
    }

    private VerticalLayout crearConsultaForm(){
        VerticalLayout layout = new VerticalLayout();
                layout.addStyleName("crud-view");
        layout.setMargin(true);
        layout.setSpacing(true);
        guardar.setVisible(true);
        
        txtMotivo_Consulta.setWidth("100%");
        txtMedicacionConsulta.setWidth("100%");
        txtObservacionConsulta.setWidth("100%");
        layout.addComponent(txtMotivo_Consulta);
        layout.addComponent(txtMedicacionConsulta);
        layout.addComponent(txtObservacionConsulta);
        
        HorizontalLayout horizontalButton = new HorizontalLayout();
        guardar.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_PRIMARY);
        horizontalButton.addComponent(guardar);
        cancelar.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_FRIENDLY);
        horizontalButton.addComponent(cancelar);
        horizontalButton.setSpacing(true);
        layout.addComponent(horizontalButton);
        return layout;
    }
    
    private VerticalLayout createForm() {
        VerticalLayout layout = new VerticalLayout();
        layout.addStyleName("crud-view");
        layout.setMargin(true);
        layout.setSpacing(true);

        btnSearch.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_PRIMARY);
        btnSearch.setIcon(VaadinIcons.SEARCH);

        HorizontalLayout horizontal = new HorizontalLayout();
        horizontal.addComponent(txtCedulaFuncionario);
        horizontal.addComponent(btnSearch);
        horizontal.addComponent(txtNombreFuncionario);
        horizontal.addComponent(txtApellidoFuncionario);
        horizontal.addComponent(cmbParentesco);
        horizontal.addComponent(txtEdad);

        horizontal.setWidth("100%");
        horizontal.setSpacing(true);
        horizontal.setStyleName("top-bar");

        btnSearchSocio.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_PRIMARY);
        btnSearchSocio.setIcon(VaadinIcons.SEARCH);

        HorizontalLayout horizontal1 = new HorizontalLayout();
        horizontal1.addComponent(txtCedulaSocio);
        horizontal1.addComponent(btnSearchSocio);
        horizontal1.addComponent(txtNombreSocio);
        horizontal1.addComponent(txtApellidoSocio);
        horizontal1.addComponent(fechaRegistro);
        horizontal1.addComponent(fecha);

        horizontal1.setWidth("100%");
        horizontal1.setSpacing(true);
        horizontal1.setStyleName("top-bar");

        HorizontalLayout horizontal2 = new HorizontalLayout();
        horizontal2.addComponent(txtPeso);
        horizontal2.addComponent(cmbSexo);
        horizontal2.addComponent(txtSeguro);
        horizontal2.addComponent(txtSangre);

        horizontal2.setWidth("100%");
        horizontal2.setSpacing(true);
        horizontal2.setStyleName("top-bar");

        HorizontalLayout horizontal3 = new HorizontalLayout();
        horizontal3.addComponent(chkDST);
        horizontal3.addComponent(chkASMA);
        horizontal3.addComponent(chkHTA);
        horizontal3.addComponent(chkCARD);
        horizontal3.addComponent(chkEPOC);
        horizontal3.addComponent(chkACV);
        horizontal3.addComponent(chkOTROS);

        horizontal3.setWidth("100%");
        horizontal3.setSpacing(true);
        horizontal3.setStyleName("top-bar");

        layout.addComponent(horizontal);
        layout.addComponent(horizontal1);
        layout.addComponent(horizontal2);
        layout.addComponent(horizontal3);
//        layout.addComponent(txtObservacion);

        HorizontalLayout horizontalButton = new HorizontalLayout();
        cancelar.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_FRIENDLY);
        horizontalButton.addComponent(cancelar);
        horizontalButton.setSpacing(true);
        layout.addComponent(horizontalButton);
        return layout;
    }

    private VerticalLayout createFormMotivoInspeccion() {
        guardar.setVisible(true);
        VerticalLayout layout = new VerticalLayout();

        HorizontalLayout horizontal4 = new HorizontalLayout();
        horizontal4.addComponent(txtTA);
        horizontal4.addComponent(txtFC);
        horizontal4.addComponent(txtT);
        horizontal4.addComponent(chkAlergico);
        horizontal4.addComponent(txtAlergico);
        horizontal4.addComponent(txtMotivoConsulta);

        horizontal4.setWidth("100%");
        horizontal4.setSpacing(true);
        horizontal4.setStyleName("top-bar");

        HorizontalLayout horizontal5 = new HorizontalLayout();
        horizontal5.addComponent(txtHallazgo);
        horizontal5.addComponent(txtTratamiento);
        horizontal5.addComponent(txtContacto);
        horizontal5.setWidth("100%");
        horizontal5.setSpacing(true);
        horizontal5.setStyleName("top-bar");

        txtHallazgo.setWidth("100%");
        txtTratamiento.setWidth("100%");
        txtContacto.setWidth("100%");

        layout.addComponent(horizontal4);
        layout.addComponent(horizontal5);

        HorizontalLayout horizontalButton = new HorizontalLayout();
        guardar.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_PRIMARY);
        horizontalButton.addComponent(guardar);
        cancelar.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_FRIENDLY);
        horizontalButton.addComponent(cancelar);
        horizontalButton.setSpacing(true);
        layout.addComponent(horizontalButton);
        return layout;
        //return new VerticalLayout();
    }

    public void setSaveListener(Consumer<Inspeccion> saveListener) {
        this.saveListener = saveListener;
    }

    public void setDeleteListener(Consumer<Inspeccion> deleteListener) {
        this.deleteListener = deleteListener;
    }

    public void setCancelListener(Consumer<Inspeccion> cancelListener) {
        this.cancelListener = cancelListener;
    }

    public boolean isEnviarCorreos() {
        return enviarCorreos;
    }

    public void setEnviarCorreos(boolean enviarCorreos) {
        this.enviarCorreos = enviarCorreos;
    }

    public static long calcWeekDays(final Date start, final Date end) {
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        Date date1 = start;
        Date date2 = end;
        Calendar cal1 = Calendar.getInstance();
        Calendar cal2 = Calendar.getInstance();
        cal1.setTime(date1);
        cal2.setTime(date2);

        int numberOfDays = 0;
        while (cal1.before(cal2)) {
//            if ((Calendar.SATURDAY != cal1.get(Calendar.DAY_OF_WEEK)) && (Calendar.SUNDAY != cal1.get(Calendar.DAY_OF_WEEK))) {
            if ((Calendar.SUNDAY != cal1.get(Calendar.DAY_OF_WEEK))) {
                numberOfDays++;
                cal1.add(Calendar.DATE, 1);
            } else {
                cal1.add(Calendar.DATE, 1);
            }
        }
        return numberOfDays;
    }

    private void updateList(String value) {
        try {
            if (value.equalsIgnoreCase("")) {
                txtNombreFuncionario.setValue("");
                txtSangre.setValue("");
                txtEdad.setValue("");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void cargarPorFormulario(Formulario value) {
        try {
            motivo.setValue(null);
            if (value != null) {
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

//    public Date convertToDateViaInstant(LocalDate dateToConvert) {
//        return java.util.Date.from(dateToConvert.atStartOfDay()
//                .atZone(ZoneId.systemDefault())
//                .toInstant());
//    }
    public static boolean isWeekendSunday(LocalDate date) {
        DayOfWeek dayOfWeek = DayOfWeek.of(date.get(ChronoField.DAY_OF_WEEK));
        switch (dayOfWeek) {
            case SUNDAY:
                return true;
            default:
                return false;
        }
    }

    public static boolean isWeekend(LocalDate date) {
        DayOfWeek dayOfWeek = DayOfWeek.of(date.get(ChronoField.DAY_OF_WEEK));
        switch (dayOfWeek) {
            case SUNDAY:
                return true;
            default:
                return false;
        }
    }

    public static boolean isWeekendSaturday(LocalDate date) {
        DayOfWeek dayOfWeek = DayOfWeek.of(date.get(ChronoField.DAY_OF_WEEK));
        switch (dayOfWeek) {
            case SATURDAY:
                return true;
            default:
                return false;
        }
    }

    public void nuevoRegistro() {
        solicitud = new Inspeccion();
//        solicitud.setFechacreacion(new Date());
    }

    private void save() {
        if (validate()) {
            boolean value = true;
            Date fechaHoy = new Date();
            fechaHoy.setHours(0);
            fechaHoy.setMinutes(0);
            fechaHoy.setSeconds(0);
            java.sql.Date sqlDateHoy = java.sql.Date.valueOf(DateUtils.asLocalDate(fechaHoy));
            solicitud = new Inspeccion();
            if (txtCedulaFuncionario.getValue().equalsIgnoreCase("")) {
                Notification.show("Atención", "Completar datos de cedula", Notification.Type.ERROR_MESSAGE);
            } else {
                solicitud = new Inspeccion();
                try {
                    solicitud.setId(Long.parseLong(txtId.getValue()));
                } catch (Exception e) {
                    solicitud.setId(0l);
                } finally {
                }
                try {
                    solicitud.setPeso(Double.parseDouble(txtPeso.getValue()));
                } catch (Exception e) {
                    solicitud.setPeso(0d);
                } finally {
                }
                try {
                    solicitud.setCedula(txtCedulaFuncionario.getValue());
                } catch (Exception e) {
                    solicitud.setCedula("");
                } finally {
                }
                try {
                    solicitud.setNombre(txtNombreFuncionario.getValue());
                } catch (Exception e) {
                    solicitud.setNombre("");
                } finally {
                }
                try {
                    solicitud.setApellido(txtApellidoFuncionario.getValue());
                } catch (Exception e) {
                    solicitud.setApellido("");
                } finally {
                }
                try {
                    solicitud.setEdad(Integer.parseInt(txtEdad.getValue()));
                } catch (Exception e) {
                    solicitud.setEdad(0);
                } finally {
                }

                try {
                    solicitud.setCedulasocio(txtCedulaSocio.getValue());
                } catch (Exception e) {
                    solicitud.setCedulasocio("");
                } finally {
                }
                try {
                    solicitud.setNombresocio(txtNombreSocio.getValue());
                } catch (Exception e) {
                    solicitud.setNombresocio("");
                } finally {
                }
                try {
                    solicitud.setApellidosocio(txtApellidoSocio.getValue());
                } catch (Exception e) {
                    solicitud.setApellidosocio("");
                } finally {
                }

                try {
                    solicitud.setGruposanguineo(txtSangre.getValue());
                } catch (Exception e) {
                    solicitud.setGruposanguineo("");
                } finally {
                }
                try {
                    solicitud.setSexo(cmbSexo.getValue().equalsIgnoreCase("MASCULINO") ? "M" : "F");
                } catch (Exception e) {
                    solicitud.setSexo("");
                } finally {
                }
                try {
                    solicitud.setSeguromedico(txtSeguro.getValue());
                } catch (Exception e) {
                    solicitud.setSeguromedico("");
                } finally {
                }
                try {
                    solicitud.setDst(chkDST.getValue());
                } catch (Exception e) {
                    chkDST.setValue(false);
                } finally {
                }
                try {
                    solicitud.setConv(chkCONV.getValue());
                } catch (Exception e) {
                    solicitud.setConv(false);
                } finally {
                }
                try {
                    solicitud.setAsma(chkASMA.getValue());
                } catch (Exception e) {
                    solicitud.setAsma(false);
                } finally {
                }
                try {
                    solicitud.setHta(chkHTA.getValue());
                } catch (Exception e) {
                    solicitud.setHta(false);
                } finally {
                }
                try {
                    solicitud.setCard(chkCARD.getValue());
                } catch (Exception e) {
                    solicitud.setCard(false);
                } finally {
                }
                try {
                    solicitud.setEpoc(chkEPOC.getValue());
                } catch (Exception e) {
                    solicitud.setEpoc(false);
                } finally {
                }
                try {
                    solicitud.setAcv(chkACV.getValue());
                } catch (Exception e) {
                    solicitud.setAcv(false);
                } finally {
                }
                try {
                    solicitud.setOtros(chkOTROS.getValue());
                } catch (Exception e) {
                    solicitud.setOtros(false);
                } finally {
                }
                try {
                    solicitud.setAlergico(chkAlergico.getValue());
                } catch (Exception e) {
                    solicitud.setAlergico(false);
                } finally {
                }
                try {
                    solicitud.setComentarioalergico(txtAlergico.getValue());
                } catch (Exception e) {
                    solicitud.setComentarioalergico("");
                } finally {
                }
                try {
                    solicitud.setMotivoconsulta(txtMotivoConsulta.getValue());
                } catch (Exception e) {
                    solicitud.setMotivoconsulta("");
                } finally {
                }
                try {
                    solicitud.setTa(txtTA.getValue());
                } catch (Exception e) {
                    solicitud.setTa("");
                } finally {
                }
                try {
                    solicitud.setFc(txtFC.getValue());
                } catch (Exception e) {
                    solicitud.setFc("");
                } finally {
                }
                try {
                    solicitud.setT(txtT.getValue());
                } catch (Exception e) {
                    solicitud.setT("");
                } finally {
                }
                try {
                    solicitud.setHallazgos(txtHallazgo.getValue());
                } catch (Exception e) {
                    solicitud.setHallazgos("");
                } finally {
                }
                try {
                    solicitud.setTratamiento(txtTratamiento.getValue());
                } catch (Exception e) {
                    solicitud.setTratamiento("");
                } finally {
                }
                try {
                    solicitud.setContactoemergencia(txtContacto.getValue());
                } catch (Exception e) {
                    solicitud.setContactoemergencia("");
                } finally {
                }
                try {
                    solicitud.setParentesco(cmbParentesco.getValue());
                } catch (Exception e) {
                    solicitud.setParentesco("Otros".toUpperCase());
                } finally {
                }
                try {
                    solicitud.setFecha(DateUtils.asDate(fecha.getValue()));
                } catch (Exception e) {
                    solicitud.setFecha(null);
                } finally {
                }
                try {
                    solicitud.setFecharegistro(DateUtils.asDate(fechaRegistro.getValue()));
                } catch (Exception e) {
                    solicitud.setFecharegistro(null);
                } finally {
                }
                solicitud.setMotivoConsulta(txtMotivo_Consulta.getValue() == null ? "":txtMotivo_Consulta.getValue() );
                solicitud.setMedicacionConsulta(txtMedicacionConsulta.getValue() == null ? "":txtMedicacionConsulta.getValue() );
                solicitud.setObservacionConsulta(txtObservacionConsulta.getValue() == null ? "":txtObservacionConsulta.getValue() );
                solicitudDao.guardar(solicitud);
                setVisible(false);
                saveListener.accept(solicitud);
                Notification.show("Mensaje del Sistema", "Datos registrados correctamente", Notification.Type.HUMANIZED_MESSAGE);
            }
        } else {
            Notification.show("Atención", "Completar los campos obligatorios", Notification.Type.ERROR_MESSAGE);
        }

    }

    private void guardarDetalle(Inspeccion solicitud) {
    }

    private void cargarVacacionesDisponible() {
    }
}
