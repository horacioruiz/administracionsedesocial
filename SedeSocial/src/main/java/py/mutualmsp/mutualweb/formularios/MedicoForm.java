package py.mutualmsp.mutualweb.formularios;

import com.github.appreciated.material.MaterialTheme;
import com.vaadin.data.Binder;
import com.vaadin.data.converter.StringToLongConverter;
import com.vaadin.ui.*;
import java.util.function.Consumer;
import java.util.logging.Level;
import java.util.logging.Logger;
import py.mutualmsp.mutualweb.dao.MedicoDao;
import py.mutualmsp.mutualweb.entities.Medico;
import py.mutualmsp.mutualweb.util.ResourceLocator;

/**
 * @author Dbarreto
 */
public class MedicoForm extends FormLayout {

    TextField txtfNumeroregistro = new TextField("Reg. Mro.", "");
    TextField txtfNombre = new TextField("Nombre", "");
    TextField txtfApellido = new TextField("Apellido", "");
    CheckBox chkActivo = new CheckBox("Activo");

    Button btnGuardar = new Button("Guardar");
    Button btnCancelar = new Button("Cancelar");
    Button btnBorrar = new Button("Borrar");

    //Binder<Medico> fieldGroup = new Binder<>(Medico.class);
    MedicoDao medicoDao = ResourceLocator.locate(MedicoDao.class);
    Binder<Medico> binder = new Binder<>(Medico.class);
    
    Medico medico;
    
    private Consumer<Medico> guardarListener;
    private Consumer<Medico> borrarListener;
    private Consumer<Medico> cancelarListener;
    
    private String viejo = "";
    private String nuevo = "";
    private String operacion = "";
    public static final String NOMBRE_TABLA = "medico";
    
        
    public MedicoForm() {
        try {
            addStyleName("product-form-wrapper");
            addStyleName("product-form");
            setSizeUndefined();
            HorizontalLayout botones = new HorizontalLayout();
            botones.addComponents(btnGuardar, btnBorrar, btnCancelar);
            
            btnGuardar.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_PRIMARY);
            btnBorrar.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_DANGER);
            btnCancelar.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_FRIENDLY);

            txtfNumeroregistro.setWidth("100%");
            txtfNombre.setWidth("100%");
            txtfApellido.setWidth("100%");
            
            CssLayout expander1 = new CssLayout();
            expander1.setSizeFull();
            expander1.setStyleName("expander");


            addComponents(txtfNumeroregistro, txtfNombre, txtfApellido, chkActivo, expander1, botones);

            binder.forField(txtfNumeroregistro)
                    .withNullRepresentation("")
                    .withConverter(new StringToLongConverter(Long.valueOf(0), "Long only"))
                    .bind(Medico::getNumeroregistro, Medico::setNumeroregistro);
            
            binder.bind(txtfNombre, Medico::getNombre, Medico::setNombre);
            binder.forField(txtfNombre).withNullRepresentation("")
                    .bind(Medico::getNombre, Medico::setNombre);
            
            binder.bind(txtfApellido, Medico::getNombre, Medico::setNombre);
            binder.forField(txtfApellido).withNullRepresentation("")
                    .bind(Medico::getNombre, Medico::setNombre);
            
            chkActivo.setValue(Boolean.TRUE);
            binder.bind(chkActivo, Medico::getActivo, Medico::setActivo);
            
            binder.bindInstanceFields(this);

            txtfNombre.addValueChangeListener(n -> { txtfNombre.setValue(txtfNombre.getValue().toUpperCase());});
            txtfApellido.addValueChangeListener(a -> { txtfApellido.setValue(txtfApellido.getValue().toUpperCase());});

            
            btnGuardar.addClickListener(e -> {
                if (validateOpcion()) {
                    guardar();
                } else {
                    Notification.show("Atención", "Todos los campos son obligatorios", Notification.Type.ERROR_MESSAGE);
                }
            });

            btnBorrar.addClickListener(e -> {
                borrar();
            });
            
            btnCancelar.addClickListener(e -> {
                try {
                    cancelarListener.accept(medico);
                    setVisible(false);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            });
            
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void guardar() {
        try {
            //medico.setNombre(txtfNombre.getValue());
            //medico.setActivo(chkActivo.getValue());
            medicoDao.guardar(medico);
            setVisible(false);
            guardarListener.accept(medico);
            Notification.show("Información", "Se ha guardado correctamente.", Notification.Type.HUMANIZED_MESSAGE);
        } catch (Exception ex) {
            Notification.show("Atención", "Ocurio un error al intentar guardar el registro", Notification.Type.ERROR_MESSAGE);
            guardarListener.accept(medico);
            Logger.getLogger(MedicoForm.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void borrar() {
        try {
            medicoDao.borrar(medico);
            setVisible(false);
            borrarListener.accept(medico);
            Notification.show("Advertencia", "Se borraron los datos.", Notification.Type.HUMANIZED_MESSAGE);
            operacion = "E";
            nuevo = "";
            //AuditoriaRegistrar.guardarAuditoria(NOMBRE_TABLA, operacion, viejo, nuevo);
        } catch (Exception e) {
            e.printStackTrace();
            Notification.show("Advertencia", "No se pudo borrar.", Notification.Type.ERROR_MESSAGE);
            borrarListener.accept(medico);
        }
    }
    
    public void setGuardarListener(Consumer<Medico> guardarListener) {
        this.guardarListener = guardarListener;
    }

    public void setBorrarListener(Consumer<Medico> borrarListener) {
        this.borrarListener = borrarListener;
    }

    public void setCancelarListener(Consumer<Medico> cancelarListener) {
        this.cancelarListener = cancelarListener;
    }

    
    public void setViejo() {
        if (medico.getId() != null) {
            this.viejo = "(" + medico.getId() + "," + medico.getNumeroregistro()+ ")";
        }
    }

    public void setMedico(Medico m) {
        try {
            this.medico = m;
            binder.setBean(m);
            btnBorrar.setVisible((medico.getId() != null));
            setVisible(true);
            txtfNombre.selectAll();
            this.viejo = "";
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private boolean validateOpcion() {
        boolean savedEnabled = true;
        if (txtfNumeroregistro == null || txtfNumeroregistro.isEmpty()) {
            savedEnabled = false;
        }
        if (txtfNombre == null || txtfNombre.isEmpty()) {
            savedEnabled = false;
        }
        if (txtfApellido == null || txtfApellido.isEmpty()) {
            savedEnabled = false;
        }
        return savedEnabled;
    }
}
