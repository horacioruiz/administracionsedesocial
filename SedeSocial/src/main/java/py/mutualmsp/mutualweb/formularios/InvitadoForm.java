package py.mutualmsp.mutualweb.formularios;

import com.github.appreciated.material.MaterialTheme;
import com.vaadin.data.Binder;
import com.vaadin.server.Page;
import com.vaadin.ui.*;
import java.math.BigInteger;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import java.util.function.Consumer;
import java.util.logging.Level;
import java.util.logging.Logger;
import py.mutualmsp.mutualweb.dao.InvitadoDao;
import py.mutualmsp.mutualweb.dao.SocioDao;
import py.mutualmsp.mutualweb.entities.Invitado;
import py.mutualmsp.mutualweb.entities.Socio;
import py.mutualmsp.mutualweb.util.ResourceLocator;
import py.mutualmsp.mutualweb.util.Services;

/**
 * Created by Alfre on 15/6/2016.
 */
public class InvitadoForm extends CssLayout {

    DateField fechas = new DateField("Fecha");
    TextField ci = new TextField("CI Invitado");
    TextField nombres = new TextField("Nombres");
    TextField apellidos = new TextField("Apellidos");
    TextField celular = new TextField("Telefono");
    TextField agendamientos = new TextField("N° Agendamiento");

    ComboBox<String> cbParentesco = new ComboBox<>("Parentesco");
    TextField ciSocio = new TextField("CI del Socio");

    Button save = new Button("Guardar");
    Button cancel = new Button("Cancelar");
    //Button delete = new Button("Delete");
    Binder<Invitado> binder = new Binder<>(Invitado.class);
    InvitadoDao enfermeriaController = ResourceLocator.locate(InvitadoDao.class);
    SocioDao socioDao = ResourceLocator.locate(SocioDao.class);

    private Consumer<Invitado> saveListener;
    private Consumer<Invitado> deleteListener;
    private Consumer<Invitado> cancelListener;
    Services ejb = ResourceLocator.locate(Services.class);
    Invitado funcionario;
    String operacion;

    public InvitadoForm() {
        addStyleName("product-form-wrapper");
        addStyleName("product-form");
        VerticalLayout verticalLayout = createForm();
        addComponent(verticalLayout);

        fechas.setValue(LocalDate.now());
        binder.forField(fechas).bind(Invitado::getFechanacimiento, Invitado::setFechanacimiento);
        binder.forField(cbParentesco).asRequired().bind(Invitado::getParentesco, Invitado::setParentesco);
        binder.bind(ci, Invitado::getCedula, Invitado::setCedula);
        binder.bind(nombres, Invitado::getNombre, Invitado::setNombre);
        binder.bind(apellidos, Invitado::getApellido, Invitado::setApellido);
        binder.bind(celular, Invitado::getCelular, Invitado::setCelular);
        //binder.bind(agendamientos, Invitado::getNumeroagendamiento, Invitado::setNumeroagendamiento);
//        binder.bind(celular, Invitado::getCelular, Invitado::setCelular);
//        binder.bind(email, Invitado::getEmail, Invitado::setEmail);
        binder.bindInstanceFields(this);

        List<String> lista = new ArrayList<>();
        lista.add("Tio/a");
        lista.add("Primo/a");
        lista.add("Amigo/a");
        lista.add("Hijo/a");

        cbParentesco.setItems(lista);
        cbParentesco.setValue("Tio/a");

        save.addClickListener(cl -> {
            try {
                if (validate() == false) {
                    save();
                } else {
                    Notification.show("Atención", "Debes completar todos los campos!.", Notification.Type.ERROR_MESSAGE);
                }

            } catch (Exception e) {
                e.printStackTrace();
                Notification.show(e.getMessage());
            }
        });
//        save.setEnabled(false);

        nombres.addBlurListener(e -> {
//            validate();
        });
//        fechas.addBlurListener(e -> {
//            validate();
//        });
        ci.addBlurListener(e -> {
//            validate();
        });
        agendamientos.addBlurListener(e -> {
//            validate();
        });

        cancel.addClickListener(cl -> {
            try {
                setVisible(false);
                cancelListener.accept(funcionario);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

//        delete.addClickListener(cl -> {
//            enfermeriaController.borrar(funcionario);
//            deleteListener.accept(funcionario);
//            showForm(false);
//        });
    }

    private VerticalLayout createForm() {
        VerticalLayout layout = new VerticalLayout();
        HorizontalLayout hlayout = new HorizontalLayout();
        HorizontalLayout hlayout2 = new HorizontalLayout();
        layout.addStyleName("form-layout");
//        nombres.setWidth("100%");
//        layout.addComponent(fechas);

        ciSocio.setWidth("100%");

        hlayout.addComponent(fechas);
        hlayout.addComponent(ciSocio);

        layout.addComponent(hlayout);

        cbParentesco.setWidth("100%");
//        layout.addComponent(cbParentesco);

        ci.setWidth("100%");
//        layout.addComponent(ci);
        hlayout2.addComponent(ci);
        hlayout2.addComponent(cbParentesco);

        layout.addComponent(hlayout2);

        nombres.setWidth("100%");
        layout.addComponent(nombres);

        apellidos.setWidth("100%");
        layout.addComponent(apellidos);

        celular.setWidth("100%");
        layout.addComponent(celular);

        CssLayout expander = new CssLayout();
        expander.setSizeFull();
        expander.setStyleName("expander");
        layout.addComponent(expander);
        layout.setExpandRatio(expander, 1.0F);

        save.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_FRIENDLY);
        cancel.addStyleName(MaterialTheme.BUTTON_ROUND);
//        delete.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_DANGER);

        HorizontalLayout hl = new HorizontalLayout();
//        hl.addComponents(save, cancel, delete);
        hl.addComponents(save, cancel);

        layout.addComponent(hl);

        return layout;
    }

    private boolean validate() {
        boolean savedEnabled = false;
        if (nombres == null || nombres.isEmpty()) {
            savedEnabled = true;
        }
        if (apellidos == null || apellidos.isEmpty()) {
            savedEnabled = true;
        }
//        if (fechas == null || fechas.isEmpty()) {
//            savedEnabled = true;
//        }
        if (celular == null || celular.isEmpty()) {
            savedEnabled = true;
        }
        if (ci == null || ci.isEmpty()) {
            savedEnabled = true;
        }
        if (cbParentesco == null || cbParentesco.isEmpty()) {
            savedEnabled = true;
        }
//        if (operacion.equalsIgnoreCase("A")) {
//            if (ciSocio == null || ciSocio.isEmpty()) {
//                savedEnabled = true;
//            }
//            
//        }
        return savedEnabled;
    }

    private void showForm(boolean show) {
        if (show) {
            addStyleName("visible");
        } else {
            removeStyleName("visible");
        }

        setEnabled(show);
    }

    private void save() {
        try {
            funcionario.setFechanacimiento(fechas.getValue());
            funcionario.setCedula(ci.getValue());
            funcionario.setNombre(nombres.getValue());
            funcionario.setApellido(apellidos.getValue());
            funcionario.setCelular(celular.getValue());

            if (operacion.equalsIgnoreCase("A")) {
                Socio soc = socioDao.listarSocioPorCI(ciSocio.getValue());
                funcionario.setIdsocio(BigInteger.valueOf(soc.getId()));
            }
            if (!cbParentesco.getValue().equalsIgnoreCase("")) {
                funcionario.setParentesco(cbParentesco.getValue());
            }
            try {
                Socio soc = socioDao.listarSocioPorCI(ciSocio.getValue());
                funcionario.setIdsocio(BigInteger.valueOf(soc.getId()));
            } catch (Exception e) {
            } finally {
            }

            enfermeriaController.guardar(funcionario);
            setVisible(false);
            saveListener.accept(funcionario);
        } catch (Exception ex) {
            Notification.show("Atención", "Ocurio un error al intentar borrar el registro", Notification.Type.ERROR_MESSAGE);
            saveListener.accept(funcionario);
            Logger.getLogger(InvitadoForm.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void setInvitado(Invitado funcionario, String dato, String cedulaSocio) {
        this.funcionario = funcionario;
        operacion = dato;
        ciSocio.setVisible(true);
        fechas.setValue(funcionario.getFechanacimiento());
//        if (dato.equalsIgnoreCase("A")) {
//            ciSocio.setVisible(true);
////            cbParentesco.setVisible(true);
//        } else {
//            ciSocio.setVisible(false);
////            cbParentesco.setVisible(false);
//        }
        ciSocio.setValue(cedulaSocio);
        celular.setValue(funcionario.getCelular());
        cbParentesco.setVisible(true);
        binder.setBean(funcionario);
//        delete.setVisible(((funcionario.getId() != null)));
        setVisible(true);
        nombres.selectAll();
        // !!! ??? Scroll to the top as this is not a Panel, using JavaScript
        Page.getCurrent().getJavaScript().execute("window.document.getElementById('" + getId() + "').scrollTop = 0;");
    }

    public void setSaveListener(Consumer<Invitado> saveListener) {
        this.saveListener = saveListener;
    }

    public void setDeleteListener(Consumer<Invitado> deleteListener) {
        this.deleteListener = deleteListener;
    }

    public void setCancelListener(Consumer<Invitado> cancelListener) {
        this.cancelListener = cancelListener;
    }
}
