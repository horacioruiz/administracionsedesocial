/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.formularios;

import com.vaadin.event.ShortcutAction;
import com.vaadin.event.ShortcutListener;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.DateField;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.function.Consumer;
import py.mutualmsp.mutualweb.dao.GiraduriaDao;
import py.mutualmsp.mutualweb.dao.RubroDao;
import py.mutualmsp.mutualweb.dao.SocioDao;
import py.mutualmsp.mutualweb.dto.DescuentoDisponibilidadDto;
import py.mutualmsp.mutualweb.entities.Analisis;
import py.mutualmsp.mutualweb.entities.Giraduria;
import py.mutualmsp.mutualweb.entities.MotivoSolicitudAyuda;
import py.mutualmsp.mutualweb.entities.Rubro;
import py.mutualmsp.mutualweb.entities.Socio;
import py.mutualmsp.mutualweb.entities.SolicitudCredito;
import py.mutualmsp.mutualweb.util.ResourceLocator;

/**
 *
 * @author hectorvillalba
 */
public class DisponibilidadForm extends Window implements Serializable {
    TextField txtCedula = new TextField("Cedula");
    TextField txtNombreSocio = new TextField("Nombre Socio");
    DateField fecha = new DateField("Fecha");
    ComboBox<Rubro> cbRubro = new ComboBox<>("Rubro");
    ComboBox<Giraduria> cbGiraduria = new ComboBox<>("Giraduria");
    TextField txtSueldoBruto = new TextField("Sueldo Bruto");
    TextField txtJubilacionPorcentaje = new TextField("%");
    TextField txtJubilacionMonto = new TextField("Jubilación Monto");
    TextField txtIvaPorcentaje = new TextField("%");
    TextField txtIvaMonto = new TextField("%");
    TextField txtSueldoLiquido = new TextField("Sueldo Líquido");
    TextField txtSueldoDisponiblePorcentaje = new TextField("50%");
    TextField txtSueldoDisponible = new TextField("50%");
    Grid<DescuentoDisponibilidadDto> gridDescuento = new Grid<>();
    TextField txtTotalDescuentoActual = new TextField("Total Descuento Act.");
    TextField txtTotalDescuentoSig = new TextField("Total Descuento Sig.");
    TextField txtSaldoActual = new TextField("Saldo Actual");
    TextField txtSaldoSig = new TextField("Saldo Sig.");
    
    TextField txtSaldoDisponibleAct = new TextField("Saldo Act. 75%");
    TextField txtSaldoDisponibleSig = new TextField("Saldo Sig. 75%");
    RubroDao rubroDao = ResourceLocator.locate(RubroDao.class);
    GiraduriaDao giraduriaDao = ResourceLocator.locate(GiraduriaDao.class);
    SocioDao socioDao = ResourceLocator.locate(SocioDao.class);
    
    private Consumer<Analisis> guardarListener;
    private Consumer<Analisis> borrarListener;
    private Consumer<Analisis> cancelarListener;

    public Consumer<Analisis> getGuardarListener() {
        return guardarListener;
    }

    public void setGuardarListener(Consumer<Analisis> guardarListener) {
        this.guardarListener = guardarListener;
    }

    public Consumer<Analisis> getBorrarListener() {
        return borrarListener;
    }

    public void setBorrarListener(Consumer<Analisis> borrarListener) {
        this.borrarListener = borrarListener;
    }

    public Consumer<Analisis> getCancelarListener() {
        return cancelarListener;
    }

    public void setCancelarListener(Consumer<Analisis> cancelarListener) {
        this.cancelarListener = cancelarListener;
    }
    
    
    
    public DisponibilidadForm(){
            VerticalLayout layout = new VerticalLayout();
            setContent(layout);
            setWidth("90%");
            setHeight("85%");
            setCaption("Disponibilidad");
            //setWindowMode(WindowMode.MAXIMIZED);
            setModal(true);
            center();
            
            HorizontalLayout hoSocio = new HorizontalLayout();
            hoSocio.addComponents(txtCedula, txtNombreSocio);
            hoSocio.setExpandRatio(txtNombreSocio, 1);
            
            HorizontalLayout hoFechaRubro = new HorizontalLayout();
            hoFechaRubro.addComponents(fecha, cbRubro);
            
            fecha.setValue(LocalDate.now());
            cbRubro.setItems(rubroDao.listaRubros());
            cbRubro.setItemCaptionGenerator(Rubro::getDescripcion);
            
            cbGiraduria.setItems(giraduriaDao.listaGiraduria());
            cbGiraduria.setItemCaptionGenerator(Giraduria::getDescripcion);
            
            HorizontalLayout hoJubilacion = new HorizontalLayout();
            hoJubilacion.addComponents(txtJubilacionPorcentaje, txtJubilacionMonto);
            
            HorizontalLayout hoIva = new HorizontalLayout();
            hoIva.addComponents(txtIvaPorcentaje, txtIvaMonto);

            HorizontalLayout hoSueldoDisponible = new HorizontalLayout();
            hoSueldoDisponible.addComponents(txtSueldoDisponiblePorcentaje, txtSueldoDisponible);
            
            
            HorizontalLayout hoTotalDesc = new HorizontalLayout();
            hoTotalDesc.addComponents(txtTotalDescuentoActual, txtTotalDescuentoSig);
            
            HorizontalLayout hoSaldo = new HorizontalLayout();
            hoSaldo.addComponents(txtSaldoActual, txtSaldoSig);
            
            HorizontalLayout hoSaldoDisponible = new HorizontalLayout();
            hoSaldoDisponible.addComponents(txtSaldoDisponibleAct, txtSaldoDisponibleSig);
            
//            txtCedula.addValueChangeListener(vcl -> {
//                if (txtCedula.getValue() != null) {
//                    Socio socio = socioDao.listarSocioPorCI(txtCedula.getValue());
//                    txtNombreSocio.setValue(socio.getNombreCompleto());
//                } else {
//                    Notification.show("Debe ingresar un número de cedula", Notification.Type.ASSISTIVE_NOTIFICATION);
//                }
//            });
            
            txtCedula.addShortcutListener(new ShortcutListener("Shortcut", ShortcutAction.KeyCode.ENTER, null) {
                @Override
                public void handleAction(Object sender, Object target) {
                    if (txtCedula.getValue() != null) {
                        Socio socio = socioDao.listarSocioPorCI(txtCedula.getValue());
                        txtNombreSocio.setValue(socio.getNombreCompleto());
                    } else {
                        Notification.show("Debe ingresar un número de cedula", Notification.Type.ASSISTIVE_NOTIFICATION);
                    }
                }
            });
            
            
            
            layout.addComponents(hoSocio,hoFechaRubro,cbGiraduria, txtSueldoBruto, 
                                 hoJubilacion, hoIva,txtSueldoLiquido,hoSueldoDisponible, 
                                 gridDescuento,hoTotalDesc, hoSaldo,hoSaldoDisponible);
            
    }
    
    
    public void setDisponibilidad(Analisis analisis){
        
    }
}
