/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.formularios;

import com.github.appreciated.material.MaterialTheme;
import com.vaadin.data.Binder;
import com.vaadin.ui.Button;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.TextField;
import java.util.function.Consumer;
import py.mutualmsp.mutualweb.dao.TipoincidenciaDao;
import py.mutualmsp.mutualweb.entities.Tipoincidencia;
import py.mutualmsp.mutualweb.util.AuditoriaRegistrar;
import py.mutualmsp.mutualweb.util.ResourceLocator;


/**
 *
 * @author Dbarreto
 */
public class TipoincidenciaForm extends FormLayout{
    TextField txtfDescripcion = new TextField("Descripción", "Ingrese descripción");
    Button btnGuardar = new Button("Guardar");
    Button btnCancelar = new Button("Cancelar");
    Button btnBorrar = new Button("Borrar");
    
    TipoincidenciaDao tipoincidenciaDao = ResourceLocator.locate(TipoincidenciaDao.class);
    Binder<Tipoincidencia> binder = new Binder<>(Tipoincidencia.class);
    
    Tipoincidencia tipoincidencia;
    
    private Consumer<Tipoincidencia> guardarListener;
    private Consumer<Tipoincidencia> borrarListener;
    private Consumer<Tipoincidencia> cancelarListener;
            
    private String viejo="";
    private String nuevo="";
    private String operacion="";
    public static final String NOMBRE_TABLA = "tipoincidencia";
            
    public TipoincidenciaForm() {
        try {
            addStyleName("product-form-wrapper");
            addStyleName("product-form");
            setSizeUndefined();
            HorizontalLayout botones = new HorizontalLayout();
            botones.addComponents(btnGuardar, btnBorrar, btnCancelar);
            
            btnGuardar.addStyleName(MaterialTheme.BUTTON_ROUND+" "+MaterialTheme.BUTTON_PRIMARY);
            btnBorrar.addStyleName(MaterialTheme.BUTTON_ROUND+" "+MaterialTheme.BUTTON_DANGER);
            btnCancelar.addStyleName(MaterialTheme.BUTTON_ROUND+" "+MaterialTheme.BUTTON_FRIENDLY);
            
            addComponents(txtfDescripcion, botones);
            
            binder.bind(txtfDescripcion, Tipoincidencia::getDescripcion, Tipoincidencia::setDescripcion);
            binder.forField(txtfDescripcion).withNullRepresentation("")
                    .bind(Tipoincidencia::getDescripcion, Tipoincidencia::setDescripcion);
            binder.bindInstanceFields(this);
            
            txtfDescripcion.addValueChangeListener(m -> {txtfDescripcion.setValue(txtfDescripcion.getValue().toUpperCase());});
            
            btnGuardar.addClickListener(e ->{
                guardar();
            });
            
            btnBorrar.addClickListener(e ->{
               borrar(); 
            });
            
            btnCancelar.addClickListener(e ->{
                setVisible(false);
                cancelarListener.accept(tipoincidencia);
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void guardar() {
        try {
            tipoincidenciaDao.guardar(tipoincidencia);
            setVisible(false);
            guardarListener.accept(tipoincidencia);
            Notification.show("Información", "Se ha guardado correctamente.", Notification.Type.HUMANIZED_MESSAGE);
            if(viejo.equals("")) operacion="I";
            else operacion="M";
            this.setNuevo();
            //AuditoriaRegistrar.guardarAuditoria(NOMBRE_TABLA, operacion, viejo, nuevo);
        } catch (Exception e) {
            e.printStackTrace();
            Notification.show("Advertencia", "No se pudo guardar", Notification.Type.ERROR_MESSAGE);
            guardarListener.accept(tipoincidencia);
        }
    }
    
    private void borrar() {
        try {
            tipoincidenciaDao.borrar(tipoincidencia);
            setVisible(false);
            borrarListener.accept(tipoincidencia);
            Notification.show("Advertencia", "Se borraron los datos.", Notification.Type.HUMANIZED_MESSAGE);
            operacion="E";
            nuevo="";
            //AuditoriaRegistrar.guardarAuditoria(NOMBRE_TABLA, operacion, viejo, nuevo);
        } catch (Exception e) {
            e.printStackTrace();
            Notification.show("Advertencia", "No se pudo borrar.", Notification.Type.ERROR_MESSAGE);
            borrarListener.accept(tipoincidencia);
        }
    }
    
    public void setGuardarListener(Consumer<Tipoincidencia> guardarListener) {
        this.guardarListener = guardarListener;
    }

    public void setBorrarListener(Consumer<Tipoincidencia> borrarListener) {
        this.borrarListener = borrarListener;
    }

    public void setCancelarListener(Consumer<Tipoincidencia> cancelarListener) {
        this.cancelarListener = cancelarListener;
    }

    public void setViejo(){
        if(tipoincidencia.getId()!=null) this.viejo = "("+tipoincidencia.getId()+","+tipoincidencia.getDescripcion()+")";
    }
    
    public void setNuevo() {
        this.nuevo = "("+tipoincidencia.getId()+","+tipoincidencia.getDescripcion()+")";
    }

    public void setTipoincidencia(Tipoincidencia t) {
        try {
            this.tipoincidencia = t;
            binder.setBean(t);
            btnBorrar.setVisible((tipoincidencia.getId()!=null));
            setVisible(true);
            txtfDescripcion.selectAll();
            this.viejo= "";
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
}
