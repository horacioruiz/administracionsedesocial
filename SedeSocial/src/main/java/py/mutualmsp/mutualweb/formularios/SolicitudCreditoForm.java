package py.mutualmsp.mutualweb.formularios;

import com.github.appreciated.material.MaterialTheme;
import com.vaadin.event.ShortcutAction;
import com.vaadin.event.ShortcutListener;
import com.vaadin.server.FileDownloader;
import com.vaadin.server.FileResource;
import com.vaadin.server.Page;
import com.vaadin.server.Sizeable.Unit;
import com.vaadin.server.StreamResource;
import com.vaadin.server.VaadinService;
import com.vaadin.ui.*;
import com.vaadin.ui.Button;
import com.vaadin.ui.TabSheet.SelectedTabChangeEvent;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.Window;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.temporal.ChronoField;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.function.Consumer;
import java.util.logging.Level;
import java.util.logging.Logger;
import py.mutualmsp.mutualweb.dao.CargoDao;
import py.mutualmsp.mutualweb.dao.DatoFamiliarAyudaDao;
import py.mutualmsp.mutualweb.dao.DestinoDao;
import py.mutualmsp.mutualweb.dao.FuncionarioDao;
import py.mutualmsp.mutualweb.dao.InstitucionDao;
//import py.mutualmsp.mutualweb.dao.MotivoSolicitudCreditoDao;
import py.mutualmsp.mutualweb.dao.MotivosDao;
import py.mutualmsp.mutualweb.dao.RegionalDao;
import py.mutualmsp.mutualweb.dao.RepresentanteDao;
import py.mutualmsp.mutualweb.dao.SocioDao;
import py.mutualmsp.mutualweb.dao.SolicitudCreditoDao;
import py.mutualmsp.mutualweb.dao.SolicitudDocumentosDao;
import py.mutualmsp.mutualweb.dao.TipocreditoDao;
import py.mutualmsp.mutualweb.dao.UsuarioDao;
import py.mutualmsp.mutualweb.entities.Cargo;
import py.mutualmsp.mutualweb.entities.Destino;
import py.mutualmsp.mutualweb.entities.Institucion;
import py.mutualmsp.mutualweb.entities.Regional;
import py.mutualmsp.mutualweb.entities.Representante;
import py.mutualmsp.mutualweb.entities.Socio;
import py.mutualmsp.mutualweb.entities.SolicitudCredito;
import py.mutualmsp.mutualweb.entities.SolicitudDocumentos;
import py.mutualmsp.mutualweb.entities.Tipocredito;
import py.mutualmsp.mutualweb.util.Constants;
import py.mutualmsp.mutualweb.util.DBConexion;
import py.mutualmsp.mutualweb.util.ResourceLocator;
import py.mutualmsp.mutualweb.util.UserHolder;

/**
 * Created by Alfre on 28/6/2016.
 */
public class SolicitudCreditoForm extends Window {

    ComboBox<String> departametos = new ComboBox<>("Derivar al Dpto.");
    TextArea observacionDerivarPor = new TextArea("Motivo por el cual se deriva a otro Dpto. (Interno)");
    private TextField txtCedulaSocio = new TextField("Ingrese CI socio (*)");
    private TextField txtNombreSocio = new TextField("Nombres");
    private TextField txtApellido = new TextField("Apellidos");
    private TextField txtTelefono = new TextField("Teléfono");
    private TextField txtDondeSeEntero = new TextField("Donde se entero?");

    private TextField txtParentezco1 = new TextField();
    private TextField edad1 = new TextField();
    private TextField ci1 = new TextField();

    private TextField txtParentezco2 = new TextField();
    private TextField edad2 = new TextField();
    private TextField ci2 = new TextField();

    private TextField txtParentezco3 = new TextField();
    private TextField edad3 = new TextField();
    private TextField ci3 = new TextField();

    private TextField txtParentezco4 = new TextField();
    private TextField edad4 = new TextField();
    private TextField ci4 = new TextField();

    private TextField txtParentezco5 = new TextField();
    private TextField edad5 = new TextField();
    private TextField ci5 = new TextField();

    private TextField txtParentezco6 = new TextField();
    private TextField edad6 = new TextField();
    private TextField ci6 = new TextField();

    private TextField txtMonto = new TextField("Monto Solicitado");
    private TextArea txtObservacionEstado = new TextArea("Observación");

    private TextField txtAtendidoPor = new TextField("Atendido por");
    private TextField txtMontoAprobado = new TextField("Monto Aprobado");
    private TextField txtPlazoAprobado = new TextField("Plazo Aprobado");

    private ComboBox<Cargo> cargo = new ComboBox<>("Cargo");
    private ComboBox<Regional> dpto = new ComboBox<>("Regional");
    private ComboBox<Institucion> institucion = new ComboBox<>("Institución");
    private TextField txtTipoContrato = new TextField("Tipo Contrato");

    private TextField txtTipoVivienda = new TextField("Tipo Vivienda");

    private TextArea txtObservacion = new TextArea("Observación (Esto lo visualiza el Socio en la App)");

//    private ComboBox<MotivoSolicitudCredito> motivoSolicitudCredito = new ComboBox<>("Motivo");
    private TextField txtMotivoObs = new TextField("Motivo Obs");
    private ComboBox<Tipocredito> tipoCredito = new ComboBox<>("Tipo Crédito");
    private ComboBox<Representante> representante = new ComboBox<>("Representante");
    private ComboBox<Destino> destino = new ComboBox<>("Destino");
    private ComboBox<String> retiro = new ComboBox<>("Retiro");
    private TextField origen = new TextField("Origen");
//    private TextField representante = new TextField("Necesidad Obs");
//    private TextField representante = new TextField("Necesidad Obs");
    private ComboBox<String> estado = new ComboBox<>("Estado");
    private TextField txtPlazo = new TextField("Plazo");
    private TextField txtPin = new TextField("PIN");

    private int editar = 0;

    private Button guardar = new Button("Guardar");
    private Button cancelar = new Button("Cancelar");

    //Grid<SolicitudCreditoProduccionDetalle> gridLicenciaCompensar = new Grid<>(SolicitudCreditoProduccionDetalle.class);
    final FormLayout form = new FormLayout();
    final HorizontalLayout mainLayout = new HorizontalLayout();

    private Button btnAgregar = new Button("Agregar");

//    private TextField txtOtroMotivo = new TextField("Otro motivo");
    //private TextArea txtOtroMotivo = new TextArea("Observación");
    ImageReceiver receiver = new ImageReceiver();
    Upload upload = upload = new Upload("Subir archivo", receiver);
    final Image image = new Image("Imagen");
    Label labelUrl = new Label();
    StreamResource myResource;
    String fileName = "";
    String ubicacion = "";
    String filename;
    byte[] content;
    public File file;
    String url = "";

    private Consumer<SolicitudCredito> saveListener;
    private Consumer<SolicitudCredito> deleteListener;
    private Consumer<SolicitudCredito> cancelListener;

    HashMap<Long, String> mapeo = new HashMap<>();
//    SolicitudCreditoProduccionDetalle spd = new SolicitudCreditoProduccionDetalle();
    int numRowSelected = 0;

    DatoFamiliarAyudaDao datoFliarAyudaDao = ResourceLocator.locate(DatoFamiliarAyudaDao.class);
    SolicitudDocumentosDao solicitudDocumentosDao = ResourceLocator.locate(SolicitudDocumentosDao.class);
    SocioDao socioDao = ResourceLocator.locate(SocioDao.class);
//    CiudadDao cargoDao = ResourceLocator.locate(CiudadDao.class);
    RegionalDao regionDao = ResourceLocator.locate(RegionalDao.class);
    InstitucionDao institucionDao = ResourceLocator.locate(InstitucionDao.class);
//    MotivoSolicitudCreditoDao motivoSolicitudCreditoDao = ResourceLocator.locate(MotivoSolicitudCreditoDao.class);
    TipocreditoDao tipoCreditoDao = ResourceLocator.locate(TipocreditoDao.class);
    RepresentanteDao representateDao = ResourceLocator.locate(RepresentanteDao.class);
    DestinoDao destinoDao = ResourceLocator.locate(DestinoDao.class);
    CargoDao cargoDao = ResourceLocator.locate(CargoDao.class);
    UsuarioDao usuarioDao = ResourceLocator.locate(UsuarioDao.class);
    FuncionarioDao funcDao = ResourceLocator.locate(FuncionarioDao.class);
    //DependenciaDao dptoDao = ResourceLocator.locate(DependenciaDao.class);
    MotivosDao motivosDao = ResourceLocator.locate(MotivosDao.class);
    SolicitudCreditoDao solicitudDao = ResourceLocator.locate(SolicitudCreditoDao.class);
    // SolicitudCreditoProduccionDetalleDao solicitudProduccionDetalleDao = ResourceLocator.locate(SolicitudCreditoProduccionDetalleDao.class);

    private boolean enviarCorreos = false;
    private SolicitudCredito solicitud;
    String destinarariosString = "";
    TabSheet tabsheet = new TabSheet();

    //List<SolicitudCreditoProduccionDetalle> listProduccionDetalle = new ArrayList<>();
    // Create upload stream
    FileOutputStream fos = null; // Stream to write to
    // Implement both receiver that saves upload in a file and
    // listener for successful upload
    String tmp = "";
    String tmp2 = "";
    String tmp3 = "";
    Image photo1 = new Image();
    Image photo2 = new Image();
    Image photo3 = new Image();
    Image photo4 = new Image();
    Image photo5 = new Image();
    Image photo6 = new Image();
    Image photo7 = new Image();

    Button button1 = new Button();
    Button button2 = new Button();
    Button button3 = new Button();
    Button button4 = new Button();
    Button button5 = new Button();
    Button button6 = new Button();
    Button button7 = new Button();

    String url1 = "";
    String url2 = "";
    String url3 = "";
    String url4 = "";
    String url5 = "";
    String url6 = "";
    String url7 = "";

    public SolicitudCreditoForm() {
        try {
            guardar.setVisible(false);
//            VerticalLayout layout = createForm();
            VerticalLayout layout = new VerticalLayout(tabsheet);
            setContent(layout);
            setWidth("90%");
            setHeight("85%");
            setCaption("Agregar Solicitud");
            //setWindowMode(WindowMode.MAXIMIZED);
            setModal(true);
            center();

            SimpleDateFormat formatFecs = new SimpleDateFormat("yyyy-MM-dd");
            SimpleDateFormat formatHora = new SimpleDateFormat("HH:mm");

            txtTipoVivienda.setVisible(false);
            txtMotivoObs.setVisible(false);

//            sample.setValue(LocalDateTime.now());
//            sample.setLocale(Locale.US);
//            sample.setResolution(DateTimeResolution.MINUTE);
            txtNombreSocio.setEnabled(false);
            txtApellido.setEnabled(false);
            //txtTipoContrato.setEnabled(false);
            //txtTelefono.setEnabled(false);
            mainLayout.setVisible(false);

            txtCedulaSocio.addBlurListener(e -> {
                validate();
            });

            departametos.setVisible(false);
            observacionDerivarPor.setVisible(false);
            txtObservacion.setVisible(false);

            txtParentezco1.setPlaceholder("Grado parentezo (1)");
            txtParentezco2.setPlaceholder("Grado parentezo (2)");
            txtParentezco3.setPlaceholder("Grado parentezo (3)");
            txtParentezco4.setPlaceholder("Grado parentezo (4)");
            txtParentezco5.setPlaceholder("Grado parentezo (5)");
            txtParentezco6.setPlaceholder("Grado parentezo (6)");

            edad1.setPlaceholder("Edad");
            edad2.setPlaceholder("Edad");
            edad3.setPlaceholder("Edad");
            edad4.setPlaceholder("Edad");
            edad5.setPlaceholder("Edad");
            edad6.setPlaceholder("Edad");

            ci1.setPlaceholder("Nro. Documento");
            ci2.setPlaceholder("Nro. Documento");
            ci3.setPlaceholder("Nro. Documento");
            ci4.setPlaceholder("Nro. Documento");
            ci5.setPlaceholder("Nro. Documento");
            ci6.setPlaceholder("Nro. Documento");

            cargo.setItems(cargoDao.listaCargo());
            cargo.setItemCaptionGenerator(Cargo::getDescripcion);

            txtMonto.addValueChangeListener(e -> agregarDecimales());

            dpto.setItems(regionDao.listaRegional());
            dpto.setItemCaptionGenerator(Regional::getDescripcion);

            institucion.setItems(institucionDao.listaInstitucion());
            institucion.setItemCaptionGenerator(Institucion::getDescripcion);

//            motivoSolicitudCredito.setItems(motivoSolicitudCreditoDao.listaMotivoSolicitudCredito());
//            motivoSolicitudCredito.setItemCaptionGenerator(MotivoSolicitudCredito::getDescripcion);
            tipoCredito.setItems(tipoCreditoDao.listaTipocredito());
            tipoCredito.setItemCaptionGenerator(Tipocredito::getDescripcion);

            representante.setItems(representateDao.listaRepresentante());
            representante.setItemCaptionGenerator(Representante::getNombreCompleto);

            List<String> listEstado = new ArrayList<>();
            listEstado.add("EN PROCESO DE ANALISIS");
            listEstado.add("PENDIENTE");
            listEstado.add("APROBADO");
            listEstado.add("RECHAZADO");
            estado.setItems(listEstado);

            List<String> listRetiro = new ArrayList<>();
            listRetiro.add("--");
            listRetiro.add("Sede Central");
            listRetiro.add("Secretaría Regional");
            retiro.setItems(listRetiro);

            destino.setItems(destinoDao.listaDestino());
            destino.setItemCaptionGenerator(Destino::getDescripcion);

            //cargarPorFormulario(cargoDao.listarPorTipoCodigo("licencias").get(0));
            //            rrhh.setItems(socioDao.listaFuncionario());
            //            rrhh.setItemCaptionGenerator(Funcionario::getNombreCompleto);
            txtCedulaSocio.focus();

            txtCedulaSocio.addValueChangeListener(e -> updateList(e.getValue()));

            txtCedulaSocio.addShortcutListener(new ShortcutListener("Shortcut", ShortcutAction.KeyCode.ENTER, null) {
                @Override
                public void handleAction(Object sender, Object target) {
                    // Do nice stuff
                    if (txtCedulaSocio.getValue() != null && !txtCedulaSocio.getValue().equals("")) {
                        try {
                            calcularCantDiaFuncionario();
                        } catch (Exception e) {
                        } finally {
                        }

//                    grid.setItems(controller.getFuncionario(filter.getValue().toUpperCase()));
                    }
                }

                private void calcularCantDiaFuncionario() {
                    Socio func = socioDao.listarSocioPorCI(txtCedulaSocio.getValue());
                    txtApellido.setValue(func.getApellido().toUpperCase());
                    txtNombreSocio.setValue(func.getNombre().toUpperCase());
                }
            });

            addCloseListener(closeEvent -> {
                close();
            });

            guardar.addClickListener(cl -> {
                try {
                    save();
                } catch (Exception e) {
                    e.printStackTrace();
                    Notification.show(e.getMessage());
                }
            });
            btnAgregar.addClickListener(cl -> {
                SimpleDateFormat sdfHM = new SimpleDateFormat("HH.mm");
                SimpleDateFormat sdf = new SimpleDateFormat("mm");
                long min = 0l;
            });
//            button1.addClickListener(cl -> {
//                descargarArchivo(new File(url1), button1);
//            });
//            button2.addClickListener(cl -> {
//                descargarArchivo(new File(url2), button2);
//            });
//            button3.addClickListener(cl -> {
//                descargarArchivo(new File(url3), button3);
//            });
//            button4.addClickListener(cl -> {
//                descargarArchivo(new File(url4), button4);
//            });
//            button5.addClickListener(cl -> {
//                descargarArchivo(new File(url5), button5);
//            });
////            guardar.setEnabled(false);
            cancelar.addClickListener(clickEvent -> {
                close();
            });

// Create tab content dynamically when tab is selected
            tabsheet.addSelectedTabChangeListener(
                    new TabSheet.SelectedTabChangeListener() {
                public void selectedTabChange(SelectedTabChangeEvent event) {
                    // Find the tabsheet
                    TabSheet tabsheet = event.getTabSheet();

                    // Find the tab (here we know it's a layout)
                    Layout tab = (Layout) tabsheet.getSelectedTab();

                    // Get the tab caption from the tab object
                    String caption = tabsheet.getTab(tab).getCaption();

                    // Fill the tab content
                    tab.removeAllComponents();
                    VerticalLayout vl = new VerticalLayout();
                    vl.setWidth("100%");
//                    vl.addComponent(new Label("HOLA MUNDO ->>"+caption));
                    if (caption.equalsIgnoreCase("Datos Socio")) {
                        vl.addComponent(createForm());
                    } else if (caption.equalsIgnoreCase("Imagenes")) {
                        vl.addComponent(createHojaImagenes());
//                        vl.addComponent(createHojaBotones());
                    } else if (caption.equalsIgnoreCase("Datos Fliares")) {
//                        vl.addComponent(createDatosFliares());
                    } else {
                        vl.addComponent(createFormMotivoSolicitud());
                    }

                    tab.addComponent(vl);
                }

                private VerticalLayout createForm() {
                    VerticalLayout layout = new VerticalLayout();
                    layout.addStyleName("crud-view");
                    layout.setMargin(true);
                    layout.setSpacing(true);

                    HorizontalLayout horizontal = new HorizontalLayout();
                    horizontal.addComponent(txtCedulaSocio);
                    horizontal.addComponent(txtNombreSocio);
                    horizontal.addComponent(txtApellido);
                    horizontal.addComponent(txtTelefono);

                    horizontal.setWidth("100%");
                    horizontal.setSpacing(true);
                    horizontal.setStyleName("top-bar");

                    HorizontalLayout horizontalSegundo = new HorizontalLayout();
                    horizontalSegundo.addComponent(cargo);
                    horizontalSegundo.addComponent(dpto);
                    horizontalSegundo.addComponent(institucion);
                    horizontalSegundo.addComponent(txtTipoContrato);

                    horizontalSegundo.setWidth("100%");
                    horizontalSegundo.setSpacing(true);
                    horizontalSegundo.setStyleName("top-bar");

                    HorizontalLayout horizontalTercero = new HorizontalLayout();
                    horizontalTercero.addComponent(txtTipoVivienda);
                    horizontalTercero.addComponent(txtPin);

                    horizontalTercero.setWidth("50%");
                    horizontalTercero.setSpacing(true);
                    horizontalTercero.setStyleName("top-bar");

                    layout.addComponent(horizontal);
                    layout.addComponent(horizontalSegundo);
                    layout.addComponent(horizontalTercero);

                    HorizontalLayout horizontalButton = new HorizontalLayout();
                    cancelar.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_FRIENDLY);
                    horizontalButton.addComponent(cancelar);
                    horizontalButton.setSpacing(true);
                    layout.addComponent(horizontalButton);
                    return layout;
                }

                private VerticalLayout createDatosFliares() {
                    VerticalLayout layout = new VerticalLayout();
                    layout.addStyleName("crud-view");
                    layout.setMargin(true);
                    layout.setSpacing(true);

                    HorizontalLayout horizontal = new HorizontalLayout();
                    horizontal.addComponent(txtParentezco1);
                    horizontal.addComponent(edad1);
                    horizontal.addComponent(ci1);

                    horizontal.setWidth("100%");
                    horizontal.setSpacing(true);
                    horizontal.setStyleName("top-bar");

                    HorizontalLayout horizontalSegundo = new HorizontalLayout();
                    horizontalSegundo.addComponent(txtParentezco2);
                    horizontalSegundo.addComponent(edad2);
                    horizontalSegundo.addComponent(ci2);

                    horizontalSegundo.setWidth("100%");
                    horizontalSegundo.setSpacing(true);
                    horizontalSegundo.setStyleName("top-bar");

                    HorizontalLayout horizontalTercero = new HorizontalLayout();
                    horizontalTercero.addComponent(txtParentezco3);
                    horizontalTercero.addComponent(edad3);
                    horizontalTercero.addComponent(ci3);

                    horizontalTercero.setWidth("100%");
                    horizontalTercero.setSpacing(true);
                    horizontalTercero.setStyleName("top-bar");

                    HorizontalLayout horizontal4to = new HorizontalLayout();
                    horizontal4to.addComponent(txtParentezco4);
                    horizontal4to.addComponent(edad4);
                    horizontal4to.addComponent(ci4);

                    horizontal4to.setWidth("100%");
                    horizontal4to.setSpacing(true);
                    horizontal4to.setStyleName("top-bar");

                    HorizontalLayout horizontal5to = new HorizontalLayout();
                    horizontal5to.addComponent(txtParentezco5);
                    horizontal5to.addComponent(edad5);
                    horizontal5to.addComponent(ci5);

                    horizontal5to.setWidth("100%");
                    horizontal5to.setSpacing(true);
                    horizontal5to.setStyleName("top-bar");

                    HorizontalLayout horizontal6to = new HorizontalLayout();
                    horizontal6to.addComponent(txtParentezco6);
                    horizontal6to.addComponent(edad6);
                    horizontal6to.addComponent(ci6);

                    horizontal6to.setWidth("100%");
                    horizontal6to.setSpacing(true);
                    horizontal6to.setStyleName("top-bar");

                    layout.addComponent(horizontal);
                    layout.addComponent(horizontalSegundo);
                    layout.addComponent(horizontalTercero);
                    layout.addComponent(horizontal4to);
                    layout.addComponent(horizontal5to);
                    layout.addComponent(horizontal6to);

                    HorizontalLayout horizontalButton = new HorizontalLayout();
                    guardar.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_PRIMARY);
                    horizontalButton.addComponent(guardar);
                    cancelar.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_FRIENDLY);
                    horizontalButton.addComponent(cancelar);
                    horizontalButton.setSpacing(true);
                    layout.addComponent(horizontalButton);
                    return layout;
                }

                private VerticalLayout createFormMotivoSolicitud() {
//                    guardar.setVisible(true);
                    guardar.setVisible(false);
                    VerticalLayout layout = new VerticalLayout();
                    layout.addStyleName("crud-view");
                    layout.setMargin(true);
                    layout.setSpacing(true);

                    HorizontalLayout horizontal = new HorizontalLayout();
                    horizontal.addComponent(txtMotivoObs);
                    horizontal.setComponentAlignment(txtMotivoObs, Alignment.TOP_LEFT);
                    txtMotivoObs.setWidth("80%");
                    layout.addComponent(horizontal);

                    HorizontalLayout horizontalSegundo = new HorizontalLayout();
                    horizontalSegundo.addComponent(tipoCredito);
                    horizontalSegundo.addComponent(destino);
                    horizontalSegundo.addComponent(retiro);
                    horizontalSegundo.addComponent(origen);
                    horizontalSegundo.setSpacing(true);
                    representante.setWidth("80%");
                    txtAtendidoPor.setEnabled(false);

                    HorizontalLayout horizontalTercero = new HorizontalLayout();
                    horizontalTercero.addComponent(representante);
                    horizontalTercero.addComponent(txtMonto);
                    horizontalTercero.addComponent(txtPlazo);
                    horizontalTercero.addComponent(txtDondeSeEntero);
//                    txtMonto.setWidth("15%");
                    txtPlazo.setWidth("15%");
                    txtDondeSeEntero.setWidth("50%");

                    HorizontalLayout horizontal4to = new HorizontalLayout();
                    horizontal4to.addComponent(estado);
                    horizontal4to.addComponent(txtAtendidoPor);
                    horizontal4to.addComponent(txtMontoAprobado);
                    horizontal4to.addComponent(txtPlazoAprobado);

                    HorizontalLayout horizontal5to = new HorizontalLayout();
                    horizontal5to.addComponent(txtObservacionEstado);
                    txtObservacionEstado.setWidth("80%");
                    horizontal5to.setWidth("80%");

                    departametos.setItems(Arrays.asList("Créditos", "Cobranzas", "Subsidios"));
                    horizontalTercero.addComponent(departametos);
                    txtAtendidoPor.setEnabled(false);

                    HorizontalLayout horizontalCuarto = new HorizontalLayout();
                    horizontalCuarto.addComponent(observacionDerivarPor);
                    observacionDerivarPor.setWidth("100%");
                    observacionDerivarPor.setRows(1);

                    HorizontalLayout horizontalQuinto = new HorizontalLayout();
                    horizontalQuinto.addComponent(txtObservacion);
                    //HorizontalLayout h4to = new HorizontalLayout();
                    //h4to.addComponent(txtAtendidoPor);

                    layout.addComponent(horizontalSegundo);
                    //layout.addComponent(txtAtendidoPor);
                    //layout.addComponent(h4to);
                    layout.addComponent(horizontalTercero);
                    layout.addComponent(horizontalCuarto);
                    layout.addComponent(horizontal4to);
                    layout.addComponent(horizontal5to);
                    layout.addComponent(horizontalQuinto);
                    txtObservacion.setWidth("100%");
                    txtObservacion.setRows(2);

//                    horizontalTercero.setWidth("100%");
//                    horizontalCuarto.setWidth("100%");
//                    horizontal4to.setWidth("100%");
//                    horizontal5to.setWidth("100%");
//                    horizontalQuinto.setWidth("100%");
                    image.setVisible(false);
                    upload.setButtonCaption("Seleccionar");
                    upload.addSucceededListener(receiver);

                    // Prevent too big downloads 0981752315
                    final long UPLOAD_LIMIT = 1000000l;
                    upload.addStartedListener(new Upload.StartedListener() {
                        private static final long serialVersionUID = 4728847902678459488L;

                        @Override
                        public void uploadStarted(Upload.StartedEvent event) {
                            if (event.getContentLength() > UPLOAD_LIMIT) {
                                Notification.show("El archivo es muy grande",
                                        Notification.Type.ERROR_MESSAGE);
                                upload.interruptUpload();
                            }
                        }
                    });

                    // Check the size also during progress
                    upload.addProgressListener(new Upload.ProgressListener() {
                        private static final long serialVersionUID = 8587352676703174995L;

                        @Override
                        public void updateProgress(long readBytes, long contentLength) {
                            if (readBytes > UPLOAD_LIMIT) {
                                Notification.show("El archivo es muy grande",
                                        Notification.Type.ERROR_MESSAGE);
                                upload.interruptUpload();
                            }
                        }
                    });
                    // Create uploads directory
                    File uploads = new File(Constants.UPLOAD_DIR_TEMP);
                    if (!uploads.exists() && !uploads.mkdir()) {
                        horizontal.addComponent(new Label("ERROR: No se pudo crear la carpeta"));
                    }

//                    horizontal.setWidth("70%");
//                    horizontalSegundo.setWidth("70%");
//                    horizontalTercero.setWidth("70%");
//                    horizontal4to.setWidth("70%");
//                    horizontal5to.setWidth("70%");
                    horizontal.setStyleName("top-bar");

                    btnAgregar.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_PRIMARY);
                    //form.addComponent(cbFuncionario);
                    form.addComponent(btnAgregar);
                    mainLayout.addComponent(form);
                    mainLayout.setWidth("100%");

                    layout.addComponent(mainLayout);

                    HorizontalLayout horizontalButton = new HorizontalLayout();
                    guardar.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_PRIMARY);
                    horizontalButton.addComponent(guardar);
                    cancelar.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_FRIENDLY);
                    horizontalButton.addComponent(cancelar);
                    horizontalButton.setSpacing(true);
                    layout.addComponent(horizontalButton);
                    return layout;
                }

                private VerticalLayout createHojaImagenes() {

                    button1.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_PRIMARY);
                    button1.setCaption("Descargar");

                    button2.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_PRIMARY);
                    button2.setCaption("Descargar");

                    button3.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_PRIMARY);
                    button3.setCaption("Descargar");

                    button4.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_PRIMARY);
                    button4.setCaption("Descargar");

                    button5.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_PRIMARY);
                    button5.setCaption("Descargar");

                    button6.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_PRIMARY);
                    button6.setCaption("Descargar");

                    button7.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_PRIMARY);
                    button7.setCaption("Descargar");
//                    guardar.setVisible(true);
                    guardar.setVisible(false);
                    HorizontalLayout hlayout = new HorizontalLayout();
                    VerticalLayout layout = new VerticalLayout();
                    layout.addStyleName("crud-view");
                    layout.setMargin(true);
                    layout.setSpacing(true);

                    //layout.addComponent(photo);
                    Panel sample1 = new Panel();
                    sample1.setHeight(100.0f, Unit.PERCENTAGE);
                    sample1.setWidth(105.0f, Unit.PERCENTAGE);

                    VerticalLayout ver1 = new VerticalLayout();
                    ver1.addComponents(photo1, button1);
                    sample1.setContent(ver1);

                    HorizontalLayout horizontal = new HorizontalLayout();
                    horizontal.addComponent(sample1);

                    Panel sample2 = new Panel();
                    sample2.setHeight(100.0f, Unit.PERCENTAGE);
                    sample2.setWidth(105.0f, Unit.PERCENTAGE);

                    VerticalLayout ver2 = new VerticalLayout();
                    ver2.addComponents(photo2, button2);
                    sample2.setContent(ver2);
                    horizontal.addComponent(sample2);

                    Panel sample3 = new Panel();
                    sample3.setHeight(100.0f, Unit.PERCENTAGE);
                    sample3.setWidth(105.0f, Unit.PERCENTAGE);

                    VerticalLayout ver3 = new VerticalLayout();
                    ver3.addComponents(photo3, button3);
                    sample3.setContent(ver3);
                    horizontal.addComponent(sample3);

                    Panel sample4 = new Panel();
                    sample4.setHeight(100.0f, Unit.PERCENTAGE);
                    sample4.setWidth(105.0f, Unit.PERCENTAGE);

                    VerticalLayout ver4 = new VerticalLayout();
                    ver4.addComponents(photo4, button4);
                    sample4.setContent(ver4);
                    horizontal.addComponent(sample4);
                    horizontal.setWidth("100%");
                    horizontal.setSpacing(true);
                    horizontal.setStyleName("top-bar");

                    Panel sample5 = new Panel();
                    sample5.setHeight(100.0f, Unit.PERCENTAGE);
                    sample5.setWidth(100.0f, Unit.PERCENTAGE);

                    VerticalLayout ver5 = new VerticalLayout();
                    ver5.addComponents(photo5, button5);
                    sample5.setContent(ver5);
                    hlayout.addComponent(sample5);
                    hlayout.setWidth("100%");
                    hlayout.setSpacing(true);
                    hlayout.setStyleName("top-bar");

                    Panel sample6 = new Panel();
                    sample6.setHeight(100.0f, Unit.PERCENTAGE);
                    sample6.setWidth(100.0f, Unit.PERCENTAGE);

                    VerticalLayout ver6 = new VerticalLayout();
                    ver6.addComponents(photo6, button6);
                    sample6.setContent(ver6);
                    hlayout.addComponent(sample6);
                    hlayout.setWidth("100%");
                    hlayout.setSpacing(true);
                    hlayout.setStyleName("top-bar");

                    Panel sample7 = new Panel();
                    sample7.setHeight(100.0f, Unit.PERCENTAGE);
                    sample7.setWidth(100.0f, Unit.PERCENTAGE);

                    VerticalLayout ver7 = new VerticalLayout();
                    ver7.addComponents(photo7, button7);
                    sample7.setContent(ver7);
                    hlayout.addComponent(sample7);
                    hlayout.setWidth("100%");
                    hlayout.setSpacing(true);
                    hlayout.setStyleName("top-bar");

//                    HorizontalLayout horizontalSegundo = new HorizontalLayout();
//                    //horizontalSegundo.addComponent(txtOtroMotivo);
//                    HorizontalLayout horizontalTercero = new HorizontalLayout();
////        horizontalSegundo.addComponent(txtOtroMotivo);
//                    //txtOtroMotivo.setWidth("100%");
//
//                    horizontalSegundo.setWidth("100%");
//                    horizontalSegundo.setSpacing(true);
//                    horizontalSegundo.setStyleName("top-bar");
//                    //txtDependencia.setWidth("100%");
//
//                    horizontalTercero.setWidth("100%");
//                    horizontalTercero.setSpacing(true);
//                    horizontalTercero.setStyleName("top-bar");
//                    btnAgregar.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_PRIMARY);
//                    //form.addComponent(cbFuncionario);
//                    form.addComponent(btnAgregar);
//                    mainLayout.addComponent(form);
//                    mainLayout.setWidth("100%");
//
//                    layout.addComponent(mainLayout);
                    btnAgregar.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_PRIMARY);
                    //form.addComponent(cbFuncionario);
                    form.addComponent(btnAgregar);
                    mainLayout.addComponent(form);
                    mainLayout.setWidth("100%");

                    layout.addComponent(mainLayout);

                    layout.addComponent(horizontal);

                    HorizontalLayout horizontalButton = new HorizontalLayout();
                    guardar.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_PRIMARY);
                    horizontalButton.addComponent(guardar);
                    cancelar.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_FRIENDLY);
                    horizontalButton.addComponent(cancelar);
                    horizontalButton.setSpacing(true);
                    layout.addComponent(hlayout);
                    layout.addComponent(horizontalButton);

//                    hlayout.addComponent(horizontalButton);
                    return layout;

//                    HorizontalLayout horizontalButton = new HorizontalLayout();
//                    guardar.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_PRIMARY);
//                    horizontalButton.addComponent(guardar);
//                    cancelar.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_FRIENDLY);
//                    horizontalButton.addComponent(cancelar);
//                    horizontalButton.setSpacing(true);
//                    layout.addComponent(horizontalButton);
//                    return layout;
                }

                private VerticalLayout createHojaBotones() {
//                    guardar.setVisible(true);
                    guardar.setVisible(false);
                    VerticalLayout layout = new VerticalLayout();
                    layout.addStyleName("crud-view");
                    layout.setMargin(true);
                    layout.setSpacing(true);

                    //layout.addComponent(photo);
                    Panel sample1 = new Panel();
//                    sample1.setHeight(100.0f, Unit.PERCENTAGE);
//                    sample1.setWidth(100.0f, Unit.PERCENTAGE);

//                    final VerticalLayout contentLayout = new VerticalLayout();
//                    contentLayout.setWidth(500, Unit.PIXELS);
//                    contentLayout.setSpacing(false);
//                    contentLayout.addComponent(photo1);
                    button1.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_PRIMARY);
                    button1.setCaption("Descargar");

                    button2.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_PRIMARY);
                    button2.setCaption("Descargar");

                    button3.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_PRIMARY);
                    button3.setCaption("Descargar");

                    button4.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_PRIMARY);
                    button4.setCaption("Descargar");

                    button5.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_PRIMARY);
                    button5.setCaption("Descargar");
                    sample1.setContent(button1);

                    HorizontalLayout horizontal = new HorizontalLayout();
                    horizontal.addComponent(sample1);

                    Panel sample2 = new Panel();
//                    sample2.setHeight(100.0f, Unit.PERCENTAGE);
//                    sample2.setWidth(100.0f, Unit.PERCENTAGE);

//                    final VerticalLayout contentLayout2 = new VerticalLayout();
//                    contentLayout2.setWidth(500, Unit.PIXELS);
//                    contentLayout2.setSpacing(false);
//                    contentLayout2.addComponent(photo2);
                    sample2.setContent(button2);
                    horizontal.addComponent(sample2);

                    Panel sample3 = new Panel();
//                    sample3.setHeight(100.0f, Unit.PERCENTAGE);
//                    sample3.setWidth(100.0f, Unit.PERCENTAGE);
//                    final VerticalLayout contentLayout3 = new VerticalLayout();
//                    contentLayout3.setWidth(500, Unit.PIXELS);
//                    contentLayout3.setSpacing(false);
//                    contentLayout3.addComponent(photo3);
                    sample3.setContent(button3);
                    horizontal.addComponent(sample3);

                    Panel sample4 = new Panel();
//                    sample4.setHeight(100.0f, Unit.PERCENTAGE);
//                    sample4.setWidth(100.0f, Unit.PERCENTAGE);

//                    final VerticalLayout contentLayout3 = new VerticalLayout();
//                    contentLayout3.setWidth(500, Unit.PIXELS);
//                    contentLayout3.setSpacing(false);
//                    contentLayout3.addComponent(photo3);
                    sample4.setContent(button4);
                    horizontal.addComponent(sample4);
                    horizontal.setWidth("100%");
                    horizontal.setSpacing(true);
                    horizontal.setStyleName("top-bar");

                    Panel sample5 = new Panel();
//                    sample5.setHeight(100.0f, Unit.PERCENTAGE);
//                    sample5.setWidth(100.0f, Unit.PERCENTAGE);

//                    final VerticalLayout contentLayout3 = new VerticalLayout();
//                    contentLayout3.setWidth(500, Unit.PIXELS);
//                    contentLayout3.setSpacing(false);
//                    contentLayout3.addComponent(photo3);
                    sample5.setContent(button5);
                    horizontal.addComponent(sample5);
                    horizontal.setWidth("100%");
                    horizontal.setSpacing(true);
                    horizontal.setStyleName("top-bar");

//                    HorizontalLayout horizontalSegundo = new HorizontalLayout();
//                    //horizontalSegundo.addComponent(txtOtroMotivo);
//                    HorizontalLayout horizontalTercero = new HorizontalLayout();
////        horizontalSegundo.addComponent(txtOtroMotivo);
//                    //txtOtroMotivo.setWidth("100%");
//
//                    horizontalSegundo.setWidth("100%");
//                    horizontalSegundo.setSpacing(true);
//                    horizontalSegundo.setStyleName("top-bar");
//                    //txtDependencia.setWidth("100%");
//
//                    horizontalTercero.setWidth("100%");
//                    horizontalTercero.setSpacing(true);
//                    horizontalTercero.setStyleName("top-bar");
                    btnAgregar.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_PRIMARY);
                    //form.addComponent(cbFuncionario);
                    form.addComponent(btnAgregar);
                    mainLayout.addComponent(form);
                    mainLayout.setWidth("100%");

                    layout.addComponent(mainLayout);

                    layout.addComponent(horizontal);

                    HorizontalLayout horizontalButton = new HorizontalLayout();
                    guardar.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_PRIMARY);
                    horizontalButton.addComponent(guardar);
                    cancelar.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_FRIENDLY);
                    horizontalButton.addComponent(cancelar);
                    horizontalButton.setSpacing(true);
                    layout.addComponent(horizontalButton);
                    return layout;
                }
            });

// Have some tabs
//            String[] tabs = {"Datos Socio", "Solicitud", "Imagenes", "Datos Fliares"};
            String[] tabs = {"Datos Socio", "Solicitud", "Imagenes"};
            for (String caption : tabs) {
                tabsheet.addTab(new VerticalLayout(), caption);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void descargarArchivo(File file, Button button) {
        try {
            myResource = createResource(file);
            FileDownloader fileDownloader = new FileDownloader(myResource);
            fileDownloader.extend(button);
        } catch (Exception e) {
        } finally {
        }
    }

    private StreamResource createResource(File file) {
        return new StreamResource(new StreamResource.StreamSource() {
            @Override
            public InputStream getStream() {
                try {
                    return new FileInputStream(file);
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        }, file.getName());
    }

    private void editarSolicitudProduccion(SolicitudCredito solicitud) {
        labelUrl.setValue("");

        upload.setVisible(false);

        txtCedulaSocio.setVisible(false);
        txtNombreSocio.setVisible(false);
        txtApellido.setVisible(false);
        txtTelefono.setVisible(false);
        txtObservacion.setVisible(false);

        mainLayout.setVisible(true);

        //cbDpto.setItems(dptoDao.listarRegionsPadres());
        //cbDpto.setValue(dptoDao.getDependenciaByDescripcion(solicitud.getAreafunc().toLowerCase()));
        //cbCargo.setValue(dptoDao.getDependenciaByDescripcion(solicitud.getCargofunc().toLowerCase()));
        mainLayout.setVisible(true);

    }

    private void cargarDetalleProduccion(SolicitudCredito solicitud) {
        SimpleDateFormat formatFec = new SimpleDateFormat("yyyy-MM-dd");
        String patternHora = "HH:mm";
        SimpleDateFormat simpleDateFormatHora = new SimpleDateFormat(patternHora);

    }

    public void editarRegistro(SolicitudCredito solicitud) {
        setCaption("Editar Solicitud");
        this.solicitud = solicitud;
        editar = 1;

        //editarSolicitudProduccion(solicitud);
        /* if (solicitud.getParametro().getDescripcion().equalsIgnoreCase("SOLICITUD DE PRODUCCIÓN")) {
            editar = 1;
            editarSolicitudProduccion(solicitud);
        } else {*/
        //List<SolicitudDetalle> listSolicitudDetalle = solicitudDetalleDao.listarPorIdSolicitud(solicitud.getId());
        //DATOS DEL SOCIO
        try {
            txtCedulaSocio.setValue(solicitud.getSocio().getCedula());
        } catch (Exception e) {
            txtCedulaSocio.setValue("");
        } finally {
        }
        try {
            txtNombreSocio.setValue(solicitud.getSocio().getNombre());
        } catch (Exception e) {
            txtNombreSocio.setValue("");
        } finally {
        }
        try {
            txtApellido.setValue(solicitud.getSocio().getApellido());
        } catch (Exception e) {
            txtApellido.setValue("");
        } finally {
        }
        try {
            txtTelefono.setValue(solicitud.getTelefono());
        } catch (Exception e) {
            txtTelefono.setValue("");
        } finally {
        }
        try {
            cargo.setValue(solicitud.getCargo());
        } catch (Exception e) {
            cargo.setValue(null);
        } finally {
        }
        try {
            dpto.setValue(solicitud.getRegional());
        } catch (Exception e) {
            dpto.setValue(null);
        } finally {
        }
        try {
            institucion.setValue(solicitud.getInstitucion());
        } catch (Exception e) {
            institucion.setValue(null);
        } finally {
        }
        try {
            txtTipoContrato.setValue(solicitud.getRubro());
        } catch (Exception e) {
            txtTipoContrato.setValue("");
        } finally {
        }
        try {
            txtPin.setValue(solicitud.getNropin());
        } catch (Exception e) {
            txtPin.setValue("");
        } finally {
        }
        //DATOS DE LA SOLICITUD    DESTINO, retiro, origen PARA MOSTRAR
        try {
            tipoCredito.setValue(solicitud.getTipoCredito());
        } catch (Exception e) {
            tipoCredito.setValue(null);
        } finally {
        }
        try {
            destino.setValue(solicitud.getDestino());
        } catch (Exception e) {
            destino.setValue(null);
        } finally {
        }
        try {
            if (solicitud.getRetiro() == null || solicitud.getRetiro().equalsIgnoreCase("")) {
                retiro.setValue("--");
            } else {
                retiro.setValue(solicitud.getRetiro());
            }

        } catch (Exception e) {
            retiro.setValue(null);
        } finally {
        }
        try {
            origen.setValue(solicitud.getOrigen());
        } catch (Exception e) {
            origen.setValue(null);
        } finally {
        }
        try {
            representante.setValue(solicitud.getRepresentante());
        } catch (Exception e) {
            representante.setValue(null);
        } finally {
        }
        try {
            DecimalFormatSymbols symbols = DecimalFormatSymbols.getInstance();
            symbols.setGroupingSeparator('.');
            DecimalFormat formatter = new DecimalFormat("###,###", symbols);

            txtMonto.setValue(formatter.format(Math.round(solicitud.getMonto())));
        } catch (Exception e) {
            txtMonto.setValue("");
        } finally {
        }
        try {
            txtPlazo.setValue(solicitud.getPlazo() + "");
        } catch (Exception e) {
            txtPlazo.setValue(null);
        } finally {
        }
        try {
            txtDondeSeEntero.setValue(solicitud.getDondeSeEntero());
        } catch (Exception e) {
            txtDondeSeEntero.setValue("");
        } finally {
        }
        try {
            estado.setValue(solicitud.getEstado().toUpperCase());
        } catch (Exception e) {
            estado.setValue("EN PROCESO DE ANALISIS");
        } finally {
        }
        estado.setEnabled(false);
        try {
            txtAtendidoPor.setValue(solicitud.getAtendidopor().toUpperCase());
        } catch (Exception e) {
            txtAtendidoPor.setValue("");
        } finally {
        }
        try {
            DecimalFormatSymbols symbols = DecimalFormatSymbols.getInstance();
            symbols.setGroupingSeparator('.');
            DecimalFormat formatter = new DecimalFormat("###,###", symbols);

            txtMontoAprobado.setValue(formatter.format(Math.round(solicitud.getMontoaprobado())));
        } catch (Exception e) {
            txtMontoAprobado.setValue("0");
        } finally {
        }
        try {
            txtPlazoAprobado.setValue(solicitud.getPlazoaprobado() == null ? "0" : solicitud.getPlazoaprobado() + "");
        } catch (Exception e) {
            txtPlazoAprobado.setValue("0");
        } finally {
        }
        try {
            txtObservacionEstado.setValue(solicitud.getObsestado());
        } catch (Exception e) {
            txtObservacionEstado.setValue("");
        } finally {
        }
        //DATOS DE IMAGENES
        List<SolicitudDocumentos> listData = solicitudDocumentosDao.getSCredBySolicitud(solicitud.getId() + "");
        int num = 0;
        if (listData.size() > 1) {

            String foto1 = "";
            String foto2 = "";
            String foto3 = "";
            String foto4 = "";
            String foto5 = "";
            String foto6 = "";
            String foto7 = "";

            for (SolicitudDocumentos sd : listData) {
                num++;

                switch (num) {
                    case 1:
                        try {
                            if (sd.getUrlCiFrontal() != null) {
                                foto1 = sd.getUrlCiFrontal();
                            }
                        } catch (Exception e) {
                        } finally {
                        }
                        try {
                            if (sd.getUrlCiDorso() != null) {
                                foto1 = sd.getUrlCiDorso();
                            }
                        } catch (Exception e) {
                        } finally {
                        }
                        try {
                            if (sd.getUrlLiquidacion() != null) {
                                foto1 = sd.getUrlLiquidacion();
                            }
                        } catch (Exception e) {
                        } finally {
                        }
                        try {
                            if (sd.getUrlServicio() != null) {
                                foto1 = sd.getUrlServicio();
                            }
                        } catch (Exception e) {
                        } finally {
                        }
                        try {
                            if (sd.getUrlSenepa() != null) {
                                foto1 = sd.getUrlSenepa();
                            }
                        } catch (Exception e) {
                        } finally {
                        }
                        try {
                            if (sd.getUrlcovid1() != null) {
                                foto1 = sd.getUrlcovid1();
                            }
                        } catch (Exception e) {
                        } finally {
                        }
                        try {
                            if (sd.getUrlcovid2() != null) {
                                foto1 = sd.getUrlcovid2();
                            }
                        } catch (Exception e) {
                        } finally {
                        }
                        break;
                    case 2:
                        try {
                            if (sd.getUrlCiFrontal() != null) {
                                foto2 = sd.getUrlCiFrontal();
                            }
                        } catch (Exception e) {
                        } finally {
                        }
                        try {
                            if (sd.getUrlCiDorso() != null) {
                                foto2 = sd.getUrlCiDorso();
                            }
                        } catch (Exception e) {
                        } finally {
                        }
                        try {
                            if (sd.getUrlLiquidacion() != null) {
                                foto2 = sd.getUrlLiquidacion();
                            }
                        } catch (Exception e) {
                        } finally {
                        }
                        try {
                            if (sd.getUrlServicio() != null) {
                                foto2 = sd.getUrlServicio();
                            }
                        } catch (Exception e) {
                        } finally {
                        }
                        try {
                            if (sd.getUrlSenepa() != null) {
                                foto2 = sd.getUrlSenepa();
                            }
                        } catch (Exception e) {
                        } finally {
                        }
                        try {
                            if (sd.getUrlcovid1() != null) {
                                foto2 = sd.getUrlcovid1();
                            }
                        } catch (Exception e) {
                        } finally {
                        }
                        try {
                            if (sd.getUrlcovid2() != null) {
                                foto2 = sd.getUrlcovid2();
                            }
                        } catch (Exception e) {
                        } finally {
                        }
                        break;
                    case 3:
                        try {
                            if (sd.getUrlCiFrontal() != null) {
                                foto3 = sd.getUrlCiFrontal();
                            }
                        } catch (Exception e) {
                        } finally {
                        }
                        try {
                            if (sd.getUrlCiDorso() != null) {
                                foto3 = sd.getUrlCiDorso();
                            }
                        } catch (Exception e) {
                        } finally {
                        }
                        try {
                            if (sd.getUrlLiquidacion() != null) {
                                foto3 = sd.getUrlLiquidacion();
                            }
                        } catch (Exception e) {
                        } finally {
                        }
                        try {
                            if (sd.getUrlServicio() != null) {
                                foto3 = sd.getUrlServicio();
                            }
                        } catch (Exception e) {
                        } finally {
                        }
                        try {
                            if (sd.getUrlSenepa() != null) {
                                foto3 = sd.getUrlSenepa();
                            }
                        } catch (Exception e) {
                        } finally {
                        }
                        try {
                            if (sd.getUrlcovid1() != null) {
                                foto3 = sd.getUrlcovid1();
                            }
                        } catch (Exception e) {
                        } finally {
                        }
                        try {
                            if (sd.getUrlcovid2() != null) {
                                foto3 = sd.getUrlcovid2();
                            }
                        } catch (Exception e) {
                        } finally {
                        }
                        break;
                    case 4:
                        try {
                            if (sd.getUrlCiFrontal() != null) {
                                foto4 = sd.getUrlCiFrontal();
                            }
                        } catch (Exception e) {
                        } finally {
                        }
                        try {
                            if (sd.getUrlCiDorso() != null) {
                                foto4 = sd.getUrlCiDorso();
                            }
                        } catch (Exception e) {
                        } finally {
                        }
                        try {
                            if (sd.getUrlLiquidacion() != null) {
                                foto4 = sd.getUrlLiquidacion();
                            }
                        } catch (Exception e) {
                        } finally {
                        }
                        try {
                            if (sd.getUrlServicio() != null) {
                                foto4 = sd.getUrlServicio();
                            }
                        } catch (Exception e) {
                        } finally {
                        }
                        try {
                            if (sd.getUrlSenepa() != null) {
                                foto4 = sd.getUrlSenepa();
                            }
                        } catch (Exception e) {
                        } finally {
                        }
                        try {
                            if (sd.getUrlcovid1() != null) {
                                foto4 = sd.getUrlcovid1();
                            }
                        } catch (Exception e) {
                        } finally {
                        }
                        try {
                            if (sd.getUrlcovid2() != null) {
                                foto4 = sd.getUrlcovid2();
                            }
                        } catch (Exception e) {
                        } finally {
                        }
                        break;
                    case 5:
                        try {
                            if (sd.getUrlCiFrontal() != null) {
                                foto5 = sd.getUrlCiFrontal();
                            }
                        } catch (Exception e) {
                        } finally {
                        }
                        try {
                            if (sd.getUrlCiDorso() != null) {
                                foto5 = sd.getUrlCiDorso();
                            }
                        } catch (Exception e) {
                        } finally {
                        }
                        try {
                            if (sd.getUrlLiquidacion() != null) {
                                foto5 = sd.getUrlLiquidacion();
                            }
                        } catch (Exception e) {
                        } finally {
                        }
                        try {
                            if (sd.getUrlServicio() != null) {
                                foto5 = sd.getUrlServicio();
                            }
                        } catch (Exception e) {
                        } finally {
                        }
                        try {
                            if (sd.getUrlSenepa() != null) {
                                foto5 = sd.getUrlSenepa();
                            }
                        } catch (Exception e) {
                        } finally {
                        }
                        try {
                            if (sd.getUrlcovid1() != null) {
                                foto5 = sd.getUrlcovid1();
                            }
                        } catch (Exception e) {
                        } finally {
                        }
                        try {
                            if (sd.getUrlcovid2() != null) {
                                foto5 = sd.getUrlcovid2();
                            }
                        } catch (Exception e) {
                        } finally {
                        }
                        break;

                    case 6:
                        try {
                            if (sd.getUrlCiFrontal() != null) {
                                foto6 = sd.getUrlCiFrontal();
                            }
                        } catch (Exception e) {
                        } finally {
                        }
                        try {
                            if (sd.getUrlCiDorso() != null) {
                                foto6 = sd.getUrlCiDorso();
                            }
                        } catch (Exception e) {
                        } finally {
                        }
                        try {
                            if (sd.getUrlLiquidacion() != null) {
                                foto6 = sd.getUrlLiquidacion();
                            }
                        } catch (Exception e) {
                        } finally {
                        }
                        try {
                            if (sd.getUrlServicio() != null) {
                                foto6 = sd.getUrlServicio();
                            }
                        } catch (Exception e) {
                        } finally {
                        }
                        try {
                            if (sd.getUrlSenepa() != null) {
                                foto6 = sd.getUrlSenepa();
                            }
                        } catch (Exception e) {
                        } finally {
                        }
                        try {
                            if (sd.getUrlcovid1() != null) {
                                foto6 = sd.getUrlcovid1();
                            }
                        } catch (Exception e) {
                        } finally {
                        }
                        try {
                            if (sd.getUrlcovid2() != null) {
                                foto6 = sd.getUrlcovid2();
                            }
                        } catch (Exception e) {
                        } finally {
                        }
                        break;

                    case 7:
                        try {
                            if (sd.getUrlCiFrontal() != null) {
                                foto7 = sd.getUrlCiFrontal();
                            }
                        } catch (Exception e) {
                        } finally {
                        }
                        try {
                            if (sd.getUrlCiDorso() != null) {
                                foto7 = sd.getUrlCiDorso();
                            }
                        } catch (Exception e) {
                        } finally {
                        }
                        try {
                            if (sd.getUrlLiquidacion() != null) {
                                foto7 = sd.getUrlLiquidacion();
                            }
                        } catch (Exception e) {
                        } finally {
                        }
                        try {
                            if (sd.getUrlServicio() != null) {
                                foto7 = sd.getUrlServicio();
                            }
                        } catch (Exception e) {
                        } finally {
                        }
                        try {
                            if (sd.getUrlSenepa() != null) {
                                foto7 = sd.getUrlSenepa();
                            }
                        } catch (Exception e) {
                        } finally {
                        }
                        try {
                            if (sd.getUrlcovid1() != null) {
                                foto7 = sd.getUrlcovid1();
                            }
                        } catch (Exception e) {
                        } finally {
                        }
                        try {
                            if (sd.getUrlcovid2() != null) {
                                foto7 = sd.getUrlcovid2();
                            }
                        } catch (Exception e) {
                        } finally {
                        }
                        break;
                    default:
                    // code block
                }
            }
            try {
                if (!foto1.equalsIgnoreCase("")) {
                    FileResource resource = new FileResource(new File(foto1));
//                FileResource resource = new FileResource(new File("C:\\uploads\\mutualmspybs\\1_" + solicitud.getId() + ".jpeg"));
                    photo1 = new Image("", resource);
                    photo1.setWidth("300px");
                    photo1.setHeight("200px");

                    url1 = foto1;
                    descargarArchivo(new File(url1), button1);
                } else {
                    String basepath = VaadinService.getCurrent()
                            .getBaseDirectory().getAbsolutePath();

                    FileResource resource = new FileResource(new File(basepath + "/WEB-INF/images/sin-imagen.png"));
                    photo1 = new Image("", resource);
                    photo1.setWidth("300px");
                    photo1.setHeight("200px");

                    url1 = "";
                }
            } catch (Exception e) {
                String basepath = VaadinService.getCurrent()
                        .getBaseDirectory().getAbsolutePath();

                FileResource resource = new FileResource(new File(basepath + "/WEB-INF/images/sin-imagen.png"));
                photo1 = new Image("", resource);
                photo1.setWidth("300px");
                photo1.setHeight("200px");

                url1 = "";
            } finally {
            }
            try {
                if (!foto2.equalsIgnoreCase("")) {
                    FileResource resource2 = new FileResource(new File(foto2));
//                FileResource resource2 = new FileResource(new File("C:\\uploads\\mutualmspybs\\2_" + solicitud.getId() + ".jpeg"));
                    photo2 = new Image("", resource2);
                    photo2.setWidth("300px");
                    photo2.setHeight("200px");

                    url2 = foto2;
                    descargarArchivo(new File(url2), button2);
                } else {
                    String basepath = VaadinService.getCurrent()
                            .getBaseDirectory().getAbsolutePath();

                    FileResource resource = new FileResource(new File(basepath
                            + "/WEB-INF/images/sin-imagen.png"));
                    photo2 = new Image("", resource);
                    photo2.setWidth("300px");
                    photo2.setHeight("200px");

                    url2 = "";
                }
            } catch (Exception e) {
                String basepath = VaadinService.getCurrent()
                        .getBaseDirectory().getAbsolutePath();

                FileResource resource = new FileResource(new File(basepath
                        + "/WEB-INF/images/sin-imagen.png"));
                photo2 = new Image("", resource);
                photo2.setWidth("300px");
                photo2.setHeight("200px");

                url2 = "";
            } finally {
            }
            try {
                if (!foto3.equalsIgnoreCase("")) {
                    FileResource resource3 = new FileResource(new File(foto3));
//                FileResource resource3 = new FileResource(new File("C:\\uploads\\mutualmspybs\\3_" + solicitud.getId() + ".jpeg"));
                    photo3 = new Image("", resource3);
                    photo3.setWidth("300px");
                    photo3.setHeight("200px");

                    url3 = foto3;
                    descargarArchivo(new File(url3), button3);
                } else {
                    String basepath = VaadinService.getCurrent()
                            .getBaseDirectory().getAbsolutePath();

                    FileResource resource = new FileResource(new File(basepath
                            + "/WEB-INF/images/sin-imagen.png"));
                    photo3 = new Image("", resource);
                    photo3.setWidth("300px");
                    photo3.setHeight("200px");

                    url3 = "";
                }
            } catch (Exception e) {
                String basepath = VaadinService.getCurrent()
                        .getBaseDirectory().getAbsolutePath();

                FileResource resource = new FileResource(new File(basepath
                        + "/WEB-INF/images/sin-imagen.png"));
                photo3 = new Image("", resource);
                photo3.setWidth("300px");
                photo3.setHeight("200px");

                url3 = "";
            } finally {
            }
            try {
                if (!foto4.equalsIgnoreCase("")) {
                    FileResource resource4 = new FileResource(new File(foto4));
//                FileResource resource3 = new FileResource(new File("C:\\uploads\\mutualmspybs\\3_" + solicitud.getId() + ".jpeg"));
                    photo4 = new Image("", resource4);
                    photo4.setWidth("300px");
                    photo4.setHeight("200px");

                    url4 = foto4;
                    descargarArchivo(new File(url4), button4);
                } else {
                    String basepath = VaadinService.getCurrent()
                            .getBaseDirectory().getAbsolutePath();

                    FileResource resource = new FileResource(new File(basepath
                            + "/WEB-INF/images/sin-imagen.png"));
                    photo4 = new Image("", resource);
                    photo4.setWidth("300px");
                    photo4.setHeight("200px");

                    url4 = "";
                }
            } catch (Exception e) {
                String basepath = VaadinService.getCurrent()
                        .getBaseDirectory().getAbsolutePath();

                FileResource resource = new FileResource(new File(basepath
                        + "/WEB-INF/images/sin-imagen.png"));
                photo4 = new Image("", resource);
                photo4.setWidth("300px");
                photo4.setHeight("200px");

                url4 = "";
            } finally {
            }
            try {
                if (!foto5.equalsIgnoreCase("")) {
                    FileResource resource5 = new FileResource(new File(foto5));
//                FileResource resource3 = new FileResource(new File("C:\\uploads\\mutualmspybs\\3_" + solicitud.getId() + ".jpeg"));
                    photo5 = new Image("", resource5);
                    photo5.setWidth("300px");
                    photo5.setHeight("200px");

                    url5 = foto5;
                    descargarArchivo(new File(url5), button5);
                } else {
                    String basepath = VaadinService.getCurrent()
                            .getBaseDirectory().getAbsolutePath();

                    FileResource resource = new FileResource(new File(basepath
                            + "/WEB-INF/images/sin-imagen.png"));
                    photo5 = new Image("", resource);
                    photo5.setWidth("300px");
                    photo5.setHeight("200px");

                    url5 = "";
                }

            } catch (Exception e) {
                String basepath = VaadinService.getCurrent()
                        .getBaseDirectory().getAbsolutePath();

                FileResource resource = new FileResource(new File(basepath
                        + "/WEB-INF/images/sin-imagen.png"));
                photo5 = new Image("", resource);
                photo5.setWidth("300px");
                photo5.setHeight("200px");

                url5 = "";
            } finally {
            }

            try {
                if (!foto6.equalsIgnoreCase("")) {
                    FileResource resource6 = new FileResource(new File(foto6));
//                FileResource resource3 = new FileResource(new File("C:\\uploads\\mutualmspybs\\3_" + solicitud.getId() + ".jpeg"));
                    photo6 = new Image("", resource6);
                    photo6.setWidth("280px");
                    photo6.setHeight("200px");

                    url6 = foto6;
                    descargarArchivo(new File(url6), button6);
                } else {
                    String basepath = VaadinService.getCurrent()
                            .getBaseDirectory().getAbsolutePath();

                    FileResource resource = new FileResource(new File(basepath
                            + "/WEB-INF/images/sin-imagen.png"));
                    photo6 = new Image("", resource);
                    photo6.setWidth("280px");
                    photo6.setHeight("200px");

                    url6 = "";
                }

            } catch (Exception e) {
                String basepath = VaadinService.getCurrent()
                        .getBaseDirectory().getAbsolutePath();

                FileResource resource = new FileResource(new File(basepath
                        + "/WEB-INF/images/sin-imagen.png"));
                photo6 = new Image("", resource);
                photo6.setWidth("280px");
                photo6.setHeight("200px");

                url6 = "";
            } finally {
            }
            try {
                if (!foto7.equalsIgnoreCase("")) {
                    FileResource resource7 = new FileResource(new File(foto7));
//                FileResource resource3 = new FileResource(new File("C:\\uploads\\mutualmspybs\\3_" + solicitud.getId() + ".jpeg"));
                    photo7 = new Image("", resource7);
                    photo7.setWidth("280px");
                    photo7.setHeight("200px");

                    url7 = foto7;
                    descargarArchivo(new File(url7), button7);
                } else {
                    String basepath = VaadinService.getCurrent()
                            .getBaseDirectory().getAbsolutePath();

                    FileResource resource = new FileResource(new File(basepath
                            + "/WEB-INF/images/sin-imagen.png"));
                    photo7 = new Image("", resource);
                    photo7.setWidth("280px");
                    photo7.setHeight("200px");

                    url7 = "";
                }
            } catch (Exception e) {
                String basepath = VaadinService.getCurrent()
                        .getBaseDirectory().getAbsolutePath();

                FileResource resource = new FileResource(new File(basepath
                        + "/WEB-INF/images/sin-imagen.png"));
                photo6 = new Image("", resource);
                photo6.setWidth("280px");
                photo6.setHeight("200px");

                url6 = "";
            } finally {
            }
        } else {
            for (SolicitudDocumentos sd : listData) {
                num++;
                try {
                    FileResource resource = new FileResource(new File(sd.getUrlCiFrontal()));
//                FileResource resource = new FileResource(new File("C:\\uploads\\mutualmspybs\\1_" + solicitud.getId() + ".jpeg"));
                    photo1 = new Image("", resource);
                    photo1.setWidth("250px");
                    photo1.setHeight("200px");

                    url1 = sd.getUrlCiFrontal();
                    descargarArchivo(new File(url1), button1);

                } catch (Exception e) {
                    String basepath = VaadinService.getCurrent()
                            .getBaseDirectory().getAbsolutePath();

                    FileResource resource = new FileResource(new File(basepath + "/WEB-INF/images/sin-imagen.png"));
                    photo1 = new Image("", resource);
                    photo1.setWidth("250px");
                    photo1.setHeight("200px");

                    url1 = "";
                } finally {
                }
                try {
                    FileResource resource2 = new FileResource(new File(sd.getUrlCiDorso()));
//                FileResource resource2 = new FileResource(new File("C:\\uploads\\mutualmspybs\\2_" + solicitud.getId() + ".jpeg"));
                    photo2 = new Image("", resource2);
                    photo2.setWidth("250px");
                    photo2.setHeight("200px");

                    url2 = sd.getUrlCiDorso();
                    descargarArchivo(new File(url2), button2);
                } catch (Exception e) {
                    String basepath = VaadinService.getCurrent()
                            .getBaseDirectory().getAbsolutePath();

                    FileResource resource = new FileResource(new File(basepath
                            + "/WEB-INF/images/sin-imagen.png"));
                    photo2 = new Image("", resource);
                    photo2.setWidth("250px");
                    photo2.setHeight("200px");

                    url2 = "";
                } finally {
                }
                try {
                    FileResource resource3 = new FileResource(new File(sd.getUrlLiquidacion()));
//                FileResource resource3 = new FileResource(new File("C:\\uploads\\mutualmspybs\\3_" + solicitud.getId() + ".jpeg"));
                    photo3 = new Image("", resource3);
                    photo3.setWidth("250px");
                    photo3.setHeight("200px");

                    url3 = sd.getUrlLiquidacion();
                    descargarArchivo(new File(url3), button3);
                } catch (Exception e) {
                    String basepath = VaadinService.getCurrent()
                            .getBaseDirectory().getAbsolutePath();

                    FileResource resource = new FileResource(new File(basepath
                            + "/WEB-INF/images/sin-imagen.png"));
                    photo3 = new Image("", resource);
                    photo3.setWidth("250px");
                    photo3.setHeight("200px");

                    url3 = "";
                } finally {
                }
                try {
                    FileResource resource4 = new FileResource(new File(sd.getUrlServicio()));
//                FileResource resource3 = new FileResource(new File("C:\\uploads\\mutualmspybs\\3_" + solicitud.getId() + ".jpeg"));
                    photo4 = new Image("", resource4);
                    photo4.setWidth("250px");
                    photo4.setHeight("200px");

                    url4 = sd.getUrlServicio();
                    descargarArchivo(new File(url4), button4);
                } catch (Exception e) {
                    String basepath = VaadinService.getCurrent()
                            .getBaseDirectory().getAbsolutePath();

                    FileResource resource = new FileResource(new File(basepath
                            + "/WEB-INF/images/sin-imagen.png"));
                    photo4 = new Image("", resource);
                    photo4.setWidth("250px");
                    photo4.setHeight("200px");

                    url4 = "";
                } finally {
                }
                try {
                    FileResource resource5 = new FileResource(new File(sd.getUrlSenepa()));
//                FileResource resource3 = new FileResource(new File("C:\\uploads\\mutualmspybs\\3_" + solicitud.getId() + ".jpeg"));
                    photo5 = new Image("", resource5);
                    photo5.setWidth("280px");
                    photo5.setHeight("200px");

                    url5 = sd.getUrlSenepa();
                    descargarArchivo(new File(url5), button5);
                } catch (Exception e) {
                    String basepath = VaadinService.getCurrent()
                            .getBaseDirectory().getAbsolutePath();

                    FileResource resource = new FileResource(new File(basepath
                            + "/WEB-INF/images/sin-imagen.png"));
                    photo5 = new Image("", resource);
                    photo5.setWidth("280px");
                    photo5.setHeight("200px");

                    url5 = "";
                } finally {
                }
                try {
                    FileResource resource6 = new FileResource(new File(sd.getUrlcovid1()));
//                FileResource resource3 = new FileResource(new File("C:\\uploads\\mutualmspybs\\3_" + solicitud.getId() + ".jpeg"));
                    photo6 = new Image("", resource6);
                    photo6.setWidth("280px");
                    photo6.setHeight("200px");

                    url6 = sd.getUrlcovid1();
                    descargarArchivo(new File(url6), button6);
                } catch (Exception e) {
                    String basepath = VaadinService.getCurrent()
                            .getBaseDirectory().getAbsolutePath();

                    FileResource resource = new FileResource(new File(basepath
                            + "/WEB-INF/images/sin-imagen.png"));
                    photo6 = new Image("", resource);
                    photo6.setWidth("280px");
                    photo6.setHeight("200px");

                    url6 = "";
                } finally {
                }
                try {
                    FileResource resource7 = new FileResource(new File(sd.getUrlcovid2()));
//                FileResource resource3 = new FileResource(new File("C:\\uploads\\mutualmspybs\\3_" + solicitud.getId() + ".jpeg"));
                    photo7 = new Image("", resource7);
                    photo7.setWidth("280px");
                    photo7.setHeight("200px");

                    url7 = sd.getUrlcovid2();
                    descargarArchivo(new File(url7), button7);
                } catch (Exception e) {
                    String basepath = VaadinService.getCurrent()
                            .getBaseDirectory().getAbsolutePath();

                    FileResource resource = new FileResource(new File(basepath
                            + "/WEB-INF/images/sin-imagen.png"));
                    photo7 = new Image("", resource);
                    photo7.setWidth("280px");
                    photo7.setHeight("200px");

                    url7 = "";
                } finally {
                }
            }
        }

        mainLayout.setVisible(false);
    }

    private boolean validate() {
        boolean savedEnabled = true;
        try {

            if (txtCedulaSocio == null || txtCedulaSocio.isEmpty()) {
                savedEnabled = false;
            }
            if (txtNombreSocio == null || txtNombreSocio.isEmpty()) {
                savedEnabled = false;
            }
            if (cargo == null || cargo.getValue() == null) {
                savedEnabled = false;
            }

        } catch (Exception e) {
        } finally {
        }
//        if (encargado == null || encargado.getValue() == null) {
//            savedEnabled = false;
//        }
//        guardar.setEnabled(savedEnabled);
        return savedEnabled;
    }

    private void updateParametro() {

    }

    private void cargarGrilla() {
        SimpleDateFormat formatFecs = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat formatHora = new SimpleDateFormat("HH:mm");

    }

    public static boolean isWeekendSaturday(LocalDate date) {
        DayOfWeek dayOfWeek = DayOfWeek.of(date.get(ChronoField.DAY_OF_WEEK));
        switch (dayOfWeek) {
            case SATURDAY:
                return true;
            default:
                return false;
        }
    }

    private void cargarGrillaSinCompensar() {
        SimpleDateFormat formatFecs = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat formatHora = new SimpleDateFormat("HH:mm");
        boolean val = false;

    }

    private boolean validateForm() {
        boolean savedEnabled = true;

        return savedEnabled;
    }

    private boolean validateFormSinCompensar() {
        boolean savedEnabled = true;
        return savedEnabled;
    }

    public void setSaveListener(Consumer<SolicitudCredito> saveListener) {
        this.saveListener = saveListener;
    }

    public void setDeleteListener(Consumer<SolicitudCredito> deleteListener) {
        this.deleteListener = deleteListener;
    }

    public void setCancelListener(Consumer<SolicitudCredito> cancelListener) {
        this.cancelListener = cancelListener;
    }

    public boolean isEnviarCorreos() {
        return enviarCorreos;
    }

    public void nuevoRegistro() {
        solicitud = new SolicitudCredito();
        solicitud.setFecha(new Date());
    }

    private void guardarDetalleProduccion(SolicitudCredito solicitud) {
    }

    private void editarSolicitudCreditoProduccion(SolicitudCredito solicitud) {
    }

    public static boolean isWeekendSunday(LocalDate date) {
        DayOfWeek dayOfWeek = DayOfWeek.of(date.get(ChronoField.DAY_OF_WEEK));
        switch (dayOfWeek) {
            case SUNDAY:
                return true;
            default:
                return false;
        }
    }

    public static long calcWeekDays(final Date start, final Date end) {
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        Date date1 = start;
        Date date2 = end;
        Calendar cal1 = Calendar.getInstance();
        Calendar cal2 = Calendar.getInstance();
        cal1.setTime(date1);
        cal2.setTime(date2);

        int numberOfDays = 0;
        while (cal1.before(cal2)) {
//            if ((Calendar.SATURDAY != cal1.get(Calendar.DAY_OF_WEEK)) && (Calendar.SUNDAY != cal1.get(Calendar.DAY_OF_WEEK))) {
            if ((Calendar.SUNDAY != cal1.get(Calendar.DAY_OF_WEEK))) {
                numberOfDays++;
                cal1.add(Calendar.DATE, 1);
            } else {
                cal1.add(Calendar.DATE, 1);
            }
        }
        return numberOfDays;
    }

    private void updateList(String value) {
        try {
            if (value.equalsIgnoreCase("")) {
                txtNombreSocio.setValue("");
                txtApellido.setValue("");
                txtTelefono.setValue("");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void save() {
        if (validate()) {
            try {
//                if (estado.getValue().equalsIgnoreCase("EN PROCESO DE ANALISIS")) {
//                    solicitud.setEstado("EN PROCESO DE ANALISIS");
//                } else if (estado.getValue().equalsIgnoreCase("APROBADO")) {
//                    solicitud.setEstado("APROBADO");
//                } else {
//                    solicitud.setEstado("RECHAZADO");
//                }

                try {
                    solicitud.setEstado(estado.getValue());
                } catch (Exception e) {
                    solicitud.setEstado(null);
                } finally {
                }
                try {
                    solicitud.setSocio(socioDao.listarSocioPorCI(txtCedulaSocio.getValue()));
                } catch (Exception e) {
                    solicitud.setSocio(null);
                } finally {
                }
                try {
                    solicitud.setMonto(Double.parseDouble(txtMonto.getValue().replace(".", "")));
                } catch (Exception e) {
                    solicitud.setMonto(0D);
                } finally {
                }
                try {
                    solicitud.setDestino(destino.getValue());
                } catch (Exception e) {
                    solicitud.setDestino(null);
                } finally {
                }
                try {
                    solicitud.setRetiro(retiro.getValue());
                } catch (Exception e) {
                    solicitud.setRetiro("");
                } finally {
                }
                try {
                    solicitud.setOrigen(origen.getValue());
                } catch (Exception e) {
                    solicitud.setOrigen("");
                } finally {
                }

                try {
                    solicitud.setTelefono(txtTelefono.getValue());
                } catch (Exception e) {
                    solicitud.setTelefono("");
                } finally {
                }
                try {
                    solicitud.setNropin(txtPin.getValue());
                } catch (Exception e) {
                    solicitud.setNropin("");
                } finally {
                }
                try {
                    solicitud.setTipoCredito(tipoCredito.getValue());
                } catch (Exception e) {
                    solicitud.setTipoCredito(null);
                } finally {
                }
                try {
                    solicitud.setRubro(txtTipoContrato.getValue());
                } catch (Exception e) {
                    solicitud.setRubro("");
                } finally {
                }
                try {
                    solicitud.setCargo(cargo.getValue());
                } catch (Exception e) {
                    solicitud.setCargo(null);
                } finally {
                }
                try {
                    solicitud.setInstitucion(institucion.getValue());
                } catch (Exception e) {
                    solicitud.setInstitucion(null);
                } finally {
                }
                try {
                    solicitud.setRegional(dpto.getValue());
                } catch (Exception e) {
                    solicitud.setRegional(null);
                } finally {
                }
                try {
                    solicitud.setRepresentante(representante.getValue());
                } catch (Exception e) {
                    solicitud.setRepresentante(null);
                } finally {
                }
                try {
                    solicitud.setPlazo(Long.parseLong(txtPlazo.getValue()));
                } catch (Exception e) {
                    solicitud.setPlazo(null);
                } finally {
                }
                try {
                    solicitud.setAtendidopor(UserHolder.get().getIdfuncionario().getNombreCompleto());
                } catch (Exception e) {
                    solicitud.setAtendidopor("");
                } finally {
                }
                try {
                    solicitud.setObsestado(txtObservacionEstado.getValue());
                } catch (Exception e) {
                    solicitud.setObsestado("");
                } finally {
                }
                try {
                    solicitud.setDondeSeEntero(txtDondeSeEntero.getValue());
                } catch (Exception e) {
                    solicitud.setObsestado("");
                } finally {
                }
//                FALTA DESTINO, retiro, origen PARA MOSTRAR

                solicitud = solicitudDao.guardarSolicitudCredito(solicitud);
//                cargarParentezco(solicitud);
//                if (!estado.getValue().equalsIgnoreCase("EN PROCESO DE ANALISIS")) {
//                    ConfirmButton confirmMessage = new ConfirmButton("");
//                    confirmMessage.openInModalPopup("Mensaje de confirmación", "¿Desea aprobar la solicitud?", "20%");
//                    confirmMessage.getOkButton().addClickListener(e -> {
////                        try {
////                            enviarSMS(solicitud.getSocio(), solicitud.getTelefono(), solicitud.getId(), solicitud.getUsuario().getIdfuncionario().getIdfuncionario());
////                        } catch (ClassNotFoundException ex) {
////                            System.out.println(ex.fillInStackTrace());
////                            System.out.println(ex.getLocalizedMessage());
////                        }
//                        saveListener.accept(solicitud);
//                        Notification.show("Mensaje del Sistema", "Datos registrados correctamente", Notification.Type.HUMANIZED_MESSAGE);
//                        confirmMessage.closePopup();
//                        setVisible(false);
//                    });
//                    confirmMessage.getCancelButton().addClickListener(e -> {
//                        confirmMessage.closePopup();
//                    });
//                } else {
                saveListener.accept(solicitud);
                Notification.show("Mensaje del Sistema", "Datos registrados correctamente", Notification.Type.HUMANIZED_MESSAGE);
                setVisible(false);
//                }
            } catch (Exception e) {
                Notification.show("Todos los campos son obligatorios.",
                        Notification.Type.ERROR_MESSAGE);
            } finally {
            }
        } else {
            Notification.show("Todos los campos son obligatorios.",
                    Notification.Type.ERROR_MESSAGE);
        }
    }

    private void guardarDetalle(SolicitudCredito solicitud) {
        if (editar == 0) {

        } else {

        }
    }

    private void enviarSMS(Socio socio, String telefono, Long id, Long idFuncionario) throws ClassNotFoundException {
        DBConexion dBConnection = new DBConexion();
        String res = "";
        try {
            Statement statement = dBConnection.getConnection().createStatement();
            String sql = "SELECT sms.programar_sms('" + telefono + "',"
                    + "'Sr/a " + socio.getNombre() + " " + socio.getApellido() + " su solicitud ha sido " + estado.getValue() + " con exito su Nro. de Referencia es el " + id + ". Mutual Nac. del MSP Y BS'" + "," + socio.getId() + "," + idFuncionario + ");";
            System.out.println(sql);
            ResultSet resultSet = statement.executeQuery(sql);
            while (resultSet.next()) {
                res = resultSet.getString(1);
                System.out.println("programar_sms: " + res);
            }
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        } catch (Exception ex) {
            Logger.getLogger(SolicitudCreditoForm.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void agregarDecimales() {
        System.out.println("-->> " + txtMonto.getValue());
        if (txtMonto.getValue() != null && !txtMonto.getValue().equals("")) {
            String monto = txtMonto.getValue();
            monto = monto.replace(".", "");

            System.out.println("-->> " + monto);
            DecimalFormatSymbols symbols = DecimalFormatSymbols.getInstance();
            symbols.setGroupingSeparator('.');
            DecimalFormat formatter = new DecimalFormat("###,###", symbols);
            if (monto.equals("")) {
                monto = "0";
            }
            txtMonto.setValue(formatter.format(Long.parseLong(monto)));
        }
    }

    class ImageReceiver implements Upload.Receiver, Upload.SucceededListener {

        private static final long serialVersionUID = -1276759102490466761L;

        public OutputStream receiveUpload(String filename,
                String mimeType) {

            try {
                // Open the file for writing.
                file = new File(Constants.UPLOAD_DIR + "/mutual-web/" + filename);
                url = Constants.PUBLIC_SERVER_URL + "/mutual-web/" + filename;
                fileName = filename;
                ubicacion = Constants.PUBLIC_SERVER_URL + "/mutual-web/";
                fos = new FileOutputStream(file);
            } catch (final java.io.FileNotFoundException e) {
                new Notification("Could not open file<br/>",
                        e.getMessage(),
                        Notification.Type.ERROR_MESSAGE)
                        .show(Page.getCurrent());
                return null;
            } catch (IOException ex) {
                Logger.getLogger(SolicitudCreditoForm.class.getName()).log(Level.SEVERE, null, ex);
            }
            return fos; // Return the output stream to write to
        }

        public void uploadSucceeded(Upload.SucceededEvent event) {
            // Show the uploaded file in the image viewer
            image.setVisible(true);
            image.setSource(new FileResource(file));
            labelUrl.setValue(file.getName());
        }
    };

}
