/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.formularios;

import com.github.appreciated.material.MaterialTheme;
import com.vaadin.data.Binder;
import com.vaadin.ui.Button;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.TextField;
import java.util.function.Consumer;
import py.mutualmsp.mutualweb.dao.MotivoSolicitudAyudaDao;
import py.mutualmsp.mutualweb.entities.MotivoSolicitudAyuda;
import py.mutualmsp.mutualweb.util.AuditoriaRegistrar;
import py.mutualmsp.mutualweb.util.ResourceLocator;

/**
 *
 * @author Dbarreto
 */
public class MotivoAyudaForm extends FormLayout {

    TextField txtfDescripcion = new TextField("Descripción", "Ingrese descripción");
    Button btnGuardar = new Button("Guardar");
    Button btnCancelar = new Button("Cancelar");
    Button btnBorrar = new Button("Borrar");

    MotivoSolicitudAyudaDao cargoDao = ResourceLocator.locate(MotivoSolicitudAyudaDao.class);
    Binder<MotivoSolicitudAyuda> binder = new Binder<>(MotivoSolicitudAyuda.class);

    MotivoSolicitudAyuda cargo;

    private Consumer<MotivoSolicitudAyuda> guardarListener;
    private Consumer<MotivoSolicitudAyuda> borrarListener;
    private Consumer<MotivoSolicitudAyuda> cancelarListener;

    private String viejo = "";
    private String nuevo = "";
    private String operacion = "";
    public static final String NOMBRE_TABLA = "cargo";

    public MotivoAyudaForm() {
        try {
            addStyleName("product-form-wrapper");
            addStyleName("product-form");
            setSizeUndefined();
            HorizontalLayout botones = new HorizontalLayout();
            botones.addComponents(btnGuardar, btnBorrar, btnCancelar);

            btnGuardar.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_PRIMARY);
            btnBorrar.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_DANGER);
            btnCancelar.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_FRIENDLY);

            addComponents(txtfDescripcion, botones);

            binder.bind(txtfDescripcion, MotivoSolicitudAyuda::getDescripcion, MotivoSolicitudAyuda::setDescripcion);
            binder.forField(txtfDescripcion).withNullRepresentation("")
                    .bind(MotivoSolicitudAyuda::getDescripcion, MotivoSolicitudAyuda::setDescripcion);
            binder.bindInstanceFields(this);

            txtfDescripcion.addValueChangeListener(m -> {
                txtfDescripcion.setValue(txtfDescripcion.getValue().toUpperCase());
            });

            btnGuardar.addClickListener(e -> {
                if (validate()) {
                    guardar();
                } else {
                    Notification.show("Atención", "Todos los campos son obligatorios", Notification.Type.ERROR_MESSAGE);
                }
            });

            btnBorrar.addClickListener(e -> {
                borrar();
            });

            btnCancelar.addClickListener(e -> {
                setVisible(false);
                cancelarListener.accept(cargo);
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private boolean validate() {
        boolean savedEnabled = true;
        if (txtfDescripcion == null || txtfDescripcion.isEmpty()) {
            savedEnabled = false;
        }
        return savedEnabled;
    }

    private void guardar() {
        try {
            cargoDao.guardar(cargo);
            setVisible(false);
            guardarListener.accept(cargo);
            Notification.show("Información", "Se ha guardado correctamente.", Notification.Type.HUMANIZED_MESSAGE);
            if (viejo.equals("")) {
                operacion = "I";
            } else {
                operacion = "M";
            }
            this.setNuevo();
            //AuditoriaRegistrar.guardarAuditoria(NOMBRE_TABLA, operacion, viejo, nuevo);
        } catch (Exception e) {
            e.printStackTrace();
            Notification.show("Advertencia", "No se pudo guardar", Notification.Type.ERROR_MESSAGE);
            guardarListener.accept(cargo);
        }
    }

    private void borrar() {
        try {
            cargoDao.borrar(cargo);
            setVisible(false);
            borrarListener.accept(cargo);
            Notification.show("Advertencia", "Se borraron los datos.", Notification.Type.HUMANIZED_MESSAGE);
            operacion = "E";
            nuevo = "";
            //AuditoriaRegistrar.guardarAuditoria(NOMBRE_TABLA, operacion, viejo, nuevo);
        } catch (Exception e) {
            e.printStackTrace();
            Notification.show("Advertencia", "No se pudo borrar.", Notification.Type.ERROR_MESSAGE);
            borrarListener.accept(cargo);
        }
    }

    public void setGuardarListener(Consumer<MotivoSolicitudAyuda> guardarListener) {
        this.guardarListener = guardarListener;
    }

    public void setBorrarListener(Consumer<MotivoSolicitudAyuda> borrarListener) {
        this.borrarListener = borrarListener;
    }

    public void setCancelarListener(Consumer<MotivoSolicitudAyuda> cancelarListener) {
        this.cancelarListener = cancelarListener;
    }

    public void setViejo() {
        if (cargo.getId() != null) {
            this.viejo = "(" + cargo.getId() + "," + cargo.getDescripcion() + ")";
        }
    }

    public void setNuevo() {
        this.nuevo = "(" + cargo.getId() + "," + cargo.getDescripcion() + ")";
    }

    public void setMotivoSolicitudAyuda(MotivoSolicitudAyuda c) {
        try {
            this.cargo = c;
            binder.setBean(c);
            btnBorrar.setVisible((cargo.getId() != null));
            setVisible(true);
            txtfDescripcion.selectAll();
            this.viejo = "";
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
