package py.mutualmsp.mutualweb.formularios;

import com.github.appreciated.material.MaterialTheme;
import com.vaadin.event.ShortcutAction;
import com.vaadin.event.ShortcutListener;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.server.FileDownloader;
import com.vaadin.server.FileResource;
import com.vaadin.server.Page;
import com.vaadin.server.StreamResource;
import com.vaadin.server.VaadinService;
import com.vaadin.ui.*;
import com.vaadin.ui.Button;
import com.vaadin.ui.TabSheet.SelectedTabChangeEvent;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.Window;
import com.vaadin.ui.themes.ValoTheme;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.temporal.ChronoField;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.function.Consumer;
import java.util.logging.Level;
import java.util.logging.Logger;
import py.mutualmsp.mutualweb.dao.CargoDao;
import py.mutualmsp.mutualweb.dao.CiudadDao;
import py.mutualmsp.mutualweb.dao.DatoFamiliarAyudaDao;
import py.mutualmsp.mutualweb.dao.DatoSocioDemograficoDao;
import py.mutualmsp.mutualweb.dao.DepartamentoDao;
import py.mutualmsp.mutualweb.dao.FuncionarioDao;
import py.mutualmsp.mutualweb.dao.InstitucionDao;
import py.mutualmsp.mutualweb.dao.MotivosDao;
import py.mutualmsp.mutualweb.dao.NecesidadAyudaDao;
import py.mutualmsp.mutualweb.dao.SocioDao;
import py.mutualmsp.mutualweb.dao.AgendamientoDao;
import py.mutualmsp.mutualweb.dao.AgendamientoDetalleDao;
import py.mutualmsp.mutualweb.dao.InvitadoDao;
import py.mutualmsp.mutualweb.dao.NucleoFamiliarDao;
import py.mutualmsp.mutualweb.dao.UsuarioDao;
import py.mutualmsp.mutualweb.entities.Ciudad;
import py.mutualmsp.mutualweb.entities.DatosFamiliaresAyuda;
import py.mutualmsp.mutualweb.entities.Departamento;
import py.mutualmsp.mutualweb.entities.Institucion;
import py.mutualmsp.mutualweb.entities.NecesidadAyuda;
import py.mutualmsp.mutualweb.entities.Socio;
import py.mutualmsp.mutualweb.entities.Agendamiento;
import py.mutualmsp.mutualweb.entities.AgendamientoDetalle;
import py.mutualmsp.mutualweb.entities.Invitado;
import py.mutualmsp.mutualweb.entities.NucleoFamiliar;
import py.mutualmsp.mutualweb.util.ConfirmButton;
import py.mutualmsp.mutualweb.util.Constants;
import py.mutualmsp.mutualweb.util.DBConexion;
import py.mutualmsp.mutualweb.util.DateUtils;
import py.mutualmsp.mutualweb.util.ResourceLocator;

/**
 * Created by Alfre on 28/6/2016.
 */
public class AgendamientoForm extends Window {
    
    private TextField txtTipoViviendaSocio = new TextField("Tipo vivienda");
    private TextField txtCondicionVivienda = new TextField("Condición vivienda");
    private TextField txtCantPersonasMayores = new TextField("Cant personas mayores a 60 años");
    private TextField txtCantPersonasMenores = new TextField("Cant personas menores a 18 años");
    private TextField txtCantPersonasTrabajando = new TextField("Cant personas trabajando");
    private TextField txtIngresoAproxHogar = new TextField("Ingreso aprox hogar");
    private TextField txtSeguroMedico = new TextField("Seguro médico");
//    private TextField txtPersonaEnferma = new TextField("Persona con enfermedad");
    ComboBox<String> txtPersonaEnferma = new ComboBox<>("Persona con enfermedad");
    private TextArea txtDescripcionEnfermedad = new TextArea("Descripción enfermedad");
    
    ComboBox<String> departametos = new ComboBox<>("Derivar al Dpto.");
    TextArea observacionDerivarPor = new TextArea("Motivo por el cual se deriva a otro Dpto. (Interno)");
    private TextField txtCedulaSocio = new TextField("Ingrese CI socio (*)");
    private TextField txtNombreSocio = new TextField("Nombres");
    private TextField txtApellido = new TextField("Apellidos");
    private TextField txtTelefono = new TextField("Teléfono");
    
    private TextField idInvitado1 = new TextField();
    private ComboBox<String> cbInvitadoParentezco1 = new ComboBox<>("Parentezco");
    private TextField nombreInvitado1 = new TextField("Nombre");
    private TextField apellidoInvitado1 = new TextField("Apellido");
    private TextField telefonoInvitado1 = new TextField("Telefono");
    private TextField ciInvitado1 = new TextField("Cedula");
    private TextField piscinaInvitado1 = new TextField("Piscina");
    private Button btnUpdateInvitado1 = new Button();
//    private Button btnDownloadInvitado1 = new Button();

//    private TextField txtParentezco2 = new TextField();
    private TextField idInvitado2 = new TextField();
    private ComboBox<String> cbInvitadoParentezco2 = new ComboBox<>();
    private TextField nombreInvitado2 = new TextField();
    private TextField apellidoInvitado2 = new TextField();
    private TextField telefonoInvitado2 = new TextField();
    private TextField ciInvitado2 = new TextField();
    private TextField piscinaInvitado2 = new TextField("Piscina");
    private Button btnUpdateInvitado2 = new Button();
//    private Button btnDownloadInvitado2 = new Button();

//    private TextField txtParentezco1 = new TextField();
    private ComboBox<String> cbParentezco1 = new ComboBox<>("Parentezco");
    private TextField idFliar1 = new TextField();
    private TextField obs1 = new TextField();
    private TextField nombre1 = new TextField("Nombre");
    private TextField apellido1 = new TextField("Apellido");
    private TextField telefono1 = new TextField("Telefono");
    private TextField ci1 = new TextField("Cedula");
    private TextField piscina1 = new TextField("Piscina");
    private Button btnUpdate1 = new Button();
    private Button btnDownload1 = new Button();
    
    private Button btnEliminar1 = new Button();
    private Button btnEliminar2 = new Button();
    private Button btnInvitadoEliminar1 = new Button();
    private Button btnInvitadoEliminar2 = new Button();
    private Button btnEliminar3 = new Button();
    private Button btnEliminar4 = new Button();
    private Button btnEliminar5 = new Button();
    private Button btnEliminar6 = new Button();
    private Button btnEliminar7 = new Button();
//    private TextField txtParentezco2 = new TextField();
    private TextField idFliar2 = new TextField();
    private TextField obs2 = new TextField();
    private ComboBox<String> cbParentezco2 = new ComboBox<>();
    private TextField nombre2 = new TextField();
    private TextField apellido2 = new TextField();
    private TextField telefono2 = new TextField();
    private TextField ci2 = new TextField();
    private TextField piscina2 = new TextField();
    private Button btnUpdate2 = new Button();
    private Button btnDownload2 = new Button();

//    private TextField txtParentezco3 = new TextField();
    private TextField idFliar3 = new TextField();
    private TextField obs3 = new TextField();
    private ComboBox<String> cbParentezco3 = new ComboBox<>();
    private TextField nombre3 = new TextField();
    private TextField apellido3 = new TextField();
    private TextField telefono3 = new TextField();
    private TextField ci3 = new TextField();
    private TextField piscina3 = new TextField();
    private Button btnUpdate3 = new Button();
    private Button btnDownload3 = new Button();

//    private TextField txtParentezco4 = new TextField();
    private TextField idFliar4 = new TextField();
    private TextField obs4 = new TextField();
    private ComboBox<String> cbParentezco4 = new ComboBox<>();
    private TextField nombre4 = new TextField();
    private TextField apellido4 = new TextField();
    private TextField telefono4 = new TextField();
    private TextField ci4 = new TextField();
    private TextField piscina4 = new TextField();
    private Button btnUpdate4 = new Button();
    private Button btnDownload4 = new Button();

//    private TextField txtParentezco5 = new TextField();
    private TextField idFliar5 = new TextField();
    private TextField obs5 = new TextField();
    private ComboBox<String> cbParentezco5 = new ComboBox<>();
    private TextField nombre5 = new TextField();
    private TextField apellido5 = new TextField();
    private TextField telefono5 = new TextField();
    private TextField ci5 = new TextField();
    private TextField piscina5 = new TextField();
    private Button btnUpdate5 = new Button();
    private Button btnDownload5 = new Button();

//    private TextField txtParentezco6 = new TextField();
    private TextField idFliar6 = new TextField();
    private TextField obs6 = new TextField();
    private ComboBox<String> cbParentezco6 = new ComboBox<>();
    private TextField nombre6 = new TextField();
    private TextField apellido6 = new TextField();
    private TextField telefono6 = new TextField();
    private TextField ci6 = new TextField();
    private TextField piscina6 = new TextField();
    private Button btnUpdate6 = new Button();
    private Button btnDownload6 = new Button();
    
    private TextField idFliar7 = new TextField();
    private TextField obs7 = new TextField();
    private ComboBox<String> cbParentezco7 = new ComboBox<>();
    private TextField nombre7 = new TextField();
    private TextField apellido7 = new TextField();
    private TextField telefono7 = new TextField();
    private TextField ci7 = new TextField();
    private TextField piscina7 = new TextField();
    private Button btnUpdate7 = new Button();
    private Button btnDownload7 = new Button();
    
    private TextField txtAtendidoPor = new TextField("Atendido por");
    
    private ComboBox<Ciudad> ciudad = new ComboBox<>("Ciudad");
    private ComboBox<Departamento> dpto = new ComboBox<>("Departamento");
    private ComboBox<Institucion> institucion = new ComboBox<>("Institución");
    private TextField txtCargo = new TextField("Cargo");
    
    private TextField txtTipoVivienda = new TextField("Tipo Vivienda");
    
    private TextArea txtObservacion = new TextArea("Observación (Esto lo visualiza el Socio en la App)");
    
    private TextArea txtMotivoObs = new TextArea("Motivo Obs");
    private ComboBox<NecesidadAyuda> necesidadAyuda = new ComboBox<>("Necesidad");
    private TextArea txtNecesidadObs = new TextArea("Necesidad Obs");
    private ComboBox<String> estado = new ComboBox<>("Estado");
    private TextField txtPin = new TextField("PIN");
    
    private int editar = 0;
    
    private Button guardar = new Button("Guardar");
    private Button cancelar = new Button("Cancelar");
    private Button verDerivaciones = new Button("");
    
    StreamResource myResource;

    //Grid<AgendamientoProduccionDetalle> gridLicenciaCompensar = new Grid<>(AgendamientoProduccionDetalle.class);
    final FormLayout form = new FormLayout();
    final HorizontalLayout mainLayout = new HorizontalLayout();
    
    private Button btnAgregar = new Button("Agregar");

//    private TextField txtOtroMotivo = new TextField("Otro motivo");
    //private TextArea txtOtroMotivo = new TextArea("Observación");
    ImageReceiver receiver = new ImageReceiver();
    Upload upload = upload = new Upload("Subir archivo", receiver);
    final Image image = new Image("Imagen");
    Label labelUrl = new Label();
    String fileName = "";
    String ubicacion = "";
    String filename;
    byte[] content;
    public File file;
    String url = "";
    
    private Consumer<Agendamiento> saveListener;
    private Consumer<Agendamiento> deleteListener;
    private Consumer<Agendamiento> cancelListener;
    
    HashMap<Long, String> mapeo = new HashMap<>();
//    AgendamientoProduccionDetalle spd = new AgendamientoProduccionDetalle();
    int numRowSelected = 0;
    
    DatoSocioDemograficoDao datoSocioDemograficoDao = ResourceLocator.locate(DatoSocioDemograficoDao.class);
    DatoFamiliarAyudaDao datoFliarAyudaDao = ResourceLocator.locate(DatoFamiliarAyudaDao.class);
    SocioDao socioDao = ResourceLocator.locate(SocioDao.class);
    CiudadDao ciudadDao = ResourceLocator.locate(CiudadDao.class);
    DepartamentoDao departamentoDao = ResourceLocator.locate(DepartamentoDao.class);
    InstitucionDao institucionDao = ResourceLocator.locate(InstitucionDao.class);
    NecesidadAyudaDao necesidadAyudaDao = ResourceLocator.locate(NecesidadAyudaDao.class);
    CargoDao cargoDao = ResourceLocator.locate(CargoDao.class);
    UsuarioDao usuarioDao = ResourceLocator.locate(UsuarioDao.class);
    FuncionarioDao funcDao = ResourceLocator.locate(FuncionarioDao.class);
    //DependenciaDao dptoDao = ResourceLocator.locate(DependenciaDao.class);
    MotivosDao motivosDao = ResourceLocator.locate(MotivosDao.class);
    AgendamientoDao AgendamientoController = ResourceLocator.locate(AgendamientoDao.class);
    AgendamientoDetalleDao AgendamientoDetalleController = ResourceLocator.locate(AgendamientoDetalleDao.class);
    
    NucleoFamiliarDao nucleoFamiliarDao = ResourceLocator.locate(NucleoFamiliarDao.class);
    InvitadoDao invitadoDao = ResourceLocator.locate(InvitadoDao.class);
    
    private DateField fechaAgendamiento = new DateField("Fecha");
    private TextField idAgendamiento = new TextField();
    private TextField idSocio = new TextField();
    private TextField nombreSocio = new TextField("Nombre");
    private TextField apellidoSocio = new TextField("Apellido");
    private TextField observacionSocio = new TextField("Observacion");
    private CheckBox asisteSocio = new CheckBox("Asiste");
    private CheckBox piscinaSocio = new CheckBox("Uso Piscina");
    private ComboBox<String> cbHorarios = new ComboBox<>("Horario");
    // AgendamientoProduccionDetalleDao solicitudProduccionDetalleDao = ResourceLocator.locate(AgendamientoProduccionDetalleDao.class);

    private boolean enviarCorreos = false;
    private Agendamiento solicitud;
    String destinarariosString = "";
    TabSheet tabsheet = new TabSheet();

    //List<AgendamientoProduccionDetalle> listProduccionDetalle = new ArrayList<>();
    // Create upload stream
    FileOutputStream fos = null; // Stream to write to
    // Implement both receiver that saves upload in a file and
    // listener for successful upload
    String tmp = "";
    String tmp2 = "";
    String tmp3 = "";
    Image photo1 = new Image();
    Image photo2 = new Image();
    Image photo3 = new Image();
    Image photo4 = new Image();
    
    public AgendamientoForm() {
        try {
//            VerticalLayout layout = createForm();
            VerticalLayout layout = new VerticalLayout(tabsheet);
            setContent(layout);
            setWidth("90%");
            setHeight("85%");
            setCaption("Agregar Solicitud");
            //setWindowMode(WindowMode.MAXIMIZED);
            setModal(true);
            center();
            
            SimpleDateFormat formatFecs = new SimpleDateFormat("yyyy-MM-dd");
            SimpleDateFormat formatHora = new SimpleDateFormat("HH:mm");
            
            idInvitado1.setVisible(false);
            idInvitado2.setVisible(false);
            idFliar1.setVisible(false);
            idFliar2.setVisible(false);
            idFliar3.setVisible(false);
            idFliar4.setVisible(false);
            idFliar5.setVisible(false);
            idFliar6.setVisible(false);
            idFliar7.setVisible(false);
            
            obs1.setVisible(false);
            obs2.setVisible(false);
            obs3.setVisible(false);
            obs4.setVisible(false);
            obs5.setVisible(false);
            obs6.setVisible(false);
            obs7.setVisible(false);
            
            cbParentezco1.setVisible(false);
            ci1.setVisible(false);
            nombre1.setVisible(false);
            apellido1.setVisible(false);
            telefono1.setVisible(false);
            piscina1.setVisible(false);
            btnUpdate1.setVisible(false);
            btnDownload1.setVisible(false);
            
            btnInvitadoEliminar1.setVisible(false);
            btnInvitadoEliminar2.setVisible(false);
            btnEliminar1.setVisible(false);
            btnEliminar2.setVisible(false);
            btnEliminar3.setVisible(false);
            btnEliminar4.setVisible(false);
            btnEliminar5.setVisible(false);
            btnEliminar6.setVisible(false);
            btnEliminar7.setVisible(false);
            
            cbParentezco2.setVisible(false);
            ci2.setVisible(false);
            nombre2.setVisible(false);
            apellido2.setVisible(false);
            telefono2.setVisible(false);
            piscina2.setVisible(false);
            btnUpdate2.setVisible(false);
            btnDownload2.setVisible(false);
            
            cbParentezco3.setVisible(false);
            ci3.setVisible(false);
            nombre3.setVisible(false);
            apellido3.setVisible(false);
            telefono3.setVisible(false);
            piscina3.setVisible(false);
            btnUpdate3.setVisible(false);
            btnDownload3.setVisible(false);
            
            cbParentezco4.setVisible(false);
            ci4.setVisible(false);
            nombre4.setVisible(false);
            apellido4.setVisible(false);
            telefono4.setVisible(false);
            piscina4.setVisible(false);
            btnUpdate4.setVisible(false);
            btnDownload4.setVisible(false);
            
            cbParentezco5.setVisible(false);
            ci5.setVisible(false);
            nombre5.setVisible(false);
            apellido5.setVisible(false);
            telefono5.setVisible(false);
            piscina5.setVisible(false);
            btnUpdate5.setVisible(false);
            btnDownload5.setVisible(false);
            
            cbParentezco6.setVisible(false);
            ci6.setVisible(false);
            nombre6.setVisible(false);
            apellido6.setVisible(false);
            telefono6.setVisible(false);
            piscina6.setVisible(false);
            btnUpdate6.setVisible(false);
            btnDownload6.setVisible(false);
            
            cbParentezco7.setVisible(false);
            ci7.setVisible(false);
            nombre7.setVisible(false);
            apellido7.setVisible(false);
            telefono7.setVisible(false);
            piscina7.setVisible(false);
            btnUpdate7.setVisible(false);
            btnDownload7.setVisible(false);
            
            cbInvitadoParentezco2.setWidth("80%");
            ciInvitado2.setWidth("70%");
            nombreInvitado2.setWidth("70%");
            apellidoInvitado2.setWidth("70%");
            telefonoInvitado2.setWidth("70%");
            
            cbInvitadoParentezco1.setVisible(false);
            ciInvitado1.setVisible(false);
            nombreInvitado1.setVisible(false);
            apellidoInvitado1.setVisible(false);
            telefonoInvitado1.setVisible(false);
            piscinaInvitado1.setVisible(false);
            btnUpdateInvitado1.setVisible(false);
//            btnDownloadInvitado1.setVisible(false);

            cbInvitadoParentezco2.setVisible(false);
            ciInvitado2.setVisible(false);
            nombreInvitado2.setVisible(false);
            apellidoInvitado2.setVisible(false);
            telefonoInvitado2.setVisible(false);
            piscinaInvitado2.setVisible(false);
            btnUpdateInvitado2.setVisible(false);
//            btnDownloadInvitado2.setVisible(false);

            verDerivaciones.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_FRIENDLY);
            verDerivaciones.setIcon(VaadinIcons.EYE);
            
            btnUpdate1.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_FRIENDLY);
            btnUpdate1.setIcon(VaadinIcons.EDIT);
            
            btnUpdate2.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_FRIENDLY);
            btnUpdate2.setIcon(VaadinIcons.EDIT);
            
            btnUpdate3.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_FRIENDLY);
            btnUpdate3.setIcon(VaadinIcons.EDIT);
            
            btnUpdate4.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_FRIENDLY);
            btnUpdate4.setIcon(VaadinIcons.EDIT);
            
            btnUpdate5.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_FRIENDLY);
            btnUpdate5.setIcon(VaadinIcons.EDIT);
            
            btnUpdate6.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_FRIENDLY);
            btnUpdate6.setIcon(VaadinIcons.EDIT);
            
            btnUpdate7.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_FRIENDLY);
            btnUpdate7.setIcon(VaadinIcons.EDIT);
            
            btnUpdateInvitado1.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_FRIENDLY);
            btnUpdateInvitado1.setIcon(VaadinIcons.EDIT);
            
            btnUpdateInvitado2.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_FRIENDLY);
            btnUpdateInvitado2.setIcon(VaadinIcons.EDIT);
            
            btnInvitadoEliminar1.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_DANGER);
            btnInvitadoEliminar1.setIcon(VaadinIcons.CLOSE);
            
            btnInvitadoEliminar2.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_DANGER);
            btnInvitadoEliminar2.setIcon(VaadinIcons.CLOSE);
            
            btnEliminar1.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_DANGER);
            btnEliminar1.setIcon(VaadinIcons.CLOSE);
            
            btnEliminar2.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_DANGER);
            btnEliminar2.setIcon(VaadinIcons.CLOSE);
            
            btnEliminar3.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_DANGER);
            btnEliminar3.setIcon(VaadinIcons.CLOSE);
            
            btnEliminar4.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_DANGER);
            btnEliminar4.setIcon(VaadinIcons.CLOSE);
            
            btnEliminar5.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_DANGER);
            btnEliminar5.setIcon(VaadinIcons.CLOSE);
            
            btnEliminar6.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_DANGER);
            btnEliminar6.setIcon(VaadinIcons.CLOSE);
            
            btnEliminar7.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_DANGER);
            btnEliminar7.setIcon(VaadinIcons.CLOSE);
            
            btnDownload1.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_DANGER);
            btnDownload1.setIcon(VaadinIcons.DOWNLOAD);
            
            btnDownload2.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_DANGER);
            btnDownload2.setIcon(VaadinIcons.DOWNLOAD);
            
            btnDownload3.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_DANGER);
            btnDownload3.setIcon(VaadinIcons.DOWNLOAD);
            
            btnDownload4.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_DANGER);
            btnDownload4.setIcon(VaadinIcons.DOWNLOAD);
            
            btnDownload5.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_DANGER);
            btnDownload5.setIcon(VaadinIcons.DOWNLOAD);
            
            btnDownload6.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_DANGER);
            btnDownload6.setIcon(VaadinIcons.DOWNLOAD);
            
            btnDownload7.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_DANGER);
            btnDownload7.setIcon(VaadinIcons.DOWNLOAD);

//            btnDownloadInvitado1.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_DANGER);
//            btnDownloadInvitado1.setIcon(VaadinIcons.DOWNLOAD);
//
//            btnDownloadInvitado2.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_DANGER);
//            btnDownloadInvitado2.setIcon(VaadinIcons.DOWNLOAD);
//            sample.setValue(LocalDateTime.now());
//            sample.setLocale(Locale.US);
//            sample.setResolution(DateTimeResolution.MINUTE);
            txtNombreSocio.setEnabled(false);
            txtApellido.setEnabled(false);
            //txtCargo.setEnabled(false);
            //txtTelefono.setEnabled(false);
            mainLayout.setVisible(false);
            
            txtCedulaSocio.addBlurListener(e -> {
                validate();
            });
            
            ciudad.setItems(ciudadDao.listaCiudad());
            ciudad.setItemCaptionGenerator(Ciudad::getDescripcion);
            
            dpto.setItems(departamentoDao.listaDepartamento());
            dpto.setItemCaptionGenerator(Departamento::getDescripcion);
            
            institucion.setItems(institucionDao.listaInstitucion());
            institucion.setItemCaptionGenerator(Institucion::getDescripcion);
            
            necesidadAyuda.setItems(necesidadAyudaDao.listaNecesidadAyuda());
            necesidadAyuda.setItemCaptionGenerator(NecesidadAyuda::getDescripcion);
            
            List<String> listHorarios = new ArrayList<>();
            listHorarios.add("10:00 a 13:00");
            listHorarios.add("13:30 a 16:30");
            listHorarios.add("17:00 a 19:00");
            
            cbHorarios.setItems(listHorarios);
            
            List<String> listParentezcoFliar = new ArrayList<>();
            listParentezcoFliar.add("Esposo/a");
            listParentezcoFliar.add("Hijo/a");
            listParentezcoFliar.add("Padres");
            
            cbParentezco1.setItems(listParentezcoFliar);
            cbParentezco2.setItems(listParentezcoFliar);
            cbParentezco3.setItems(listParentezcoFliar);
            cbParentezco4.setItems(listParentezcoFliar);
            cbParentezco5.setItems(listParentezcoFliar);
            cbParentezco6.setItems(listParentezcoFliar);
            cbParentezco7.setItems(listParentezcoFliar);
            
            List<String> listParentezcoInvitado = new ArrayList<>();
            listParentezcoInvitado.add("Tio/a");
            listParentezcoInvitado.add("Primo/a");
            listParentezcoInvitado.add("Amigo/a");
            listParentezcoInvitado.add("Esposo/a");
            listParentezcoInvitado.add("Hijo/a");
            listParentezcoInvitado.add("Padres");
            
            cbInvitadoParentezco1.setItems(listParentezcoInvitado);
            cbInvitadoParentezco2.setItems(listParentezcoInvitado);
            
            List<String> listEfermo = new ArrayList<>();
            listEfermo.add("SI");
            listEfermo.add("NO");
            txtPersonaEnferma.setItems(listEfermo);
            //cargarPorFormulario(ciudadDao.listarPorTipoCodigo("licencias").get(0));

//            rrhh.setItems(socioDao.listaFuncionario());
//            rrhh.setItemCaptionGenerator(Funcionario::getNombreCompleto);
            txtCedulaSocio.focus();
            
            txtCedulaSocio.addValueChangeListener(e -> updateList(e.getValue()));
            
            txtCedulaSocio.addShortcutListener(new ShortcutListener("Shortcut", ShortcutAction.KeyCode.ENTER, null) {
                @Override
                public void handleAction(Object sender, Object target) {
                    // Do nice stuff
                    if (txtCedulaSocio.getValue() != null && !txtCedulaSocio.getValue().equals("")) {
                        try {
                            calcularCantDiaFuncionario();
                        } catch (Exception e) {
                        } finally {
                        }

//                    grid.setItems(controller.getFuncionario(filter.getValue().toUpperCase()));
                    }
                }
                
                private void calcularCantDiaFuncionario() {
                    Socio func = socioDao.listarSocioPorCI(txtCedulaSocio.getValue());
                    txtApellido.setValue(func.getApellido().toUpperCase());
                    txtNombreSocio.setValue(func.getNombre().toUpperCase());
                }
            });
            
            addCloseListener(closeEvent -> {
                close();
            });
            
            btnUpdate1.addClickListener(cl -> {
                ConfirmButton confirmMessage = new ConfirmButton("");
                confirmMessage.openInModalPopup("Mensaje de confirmación", "¿Desea confirmar los cambios?", "25%");
                confirmMessage.getOkButton().addClickListener(e -> {
                    NucleoFamiliar nf = nucleoFamiliarDao.getById(Long.parseLong(idFliar1.getValue()));
                    nf.setNombre(nombre1.getValue());
                    nf.setApellido(apellido1.getValue());
                    nf.setCedula(ci1.getValue());
                    nf.setParentesco(cbParentezco1.getValue());
                    nf.setTelefono(telefono1.getValue());
                    
                    nucleoFamiliarDao.guardar(nf);
                    confirmMessage.closePopup();
                });
                confirmMessage.getCancelButton().addClickListener(e -> {
                    confirmMessage.closePopup();
                });
                
            });
            btnUpdate2.addClickListener(cl -> {
                ConfirmButton confirmMessage = new ConfirmButton("");
                confirmMessage.openInModalPopup("Mensaje de confirmación", "¿Desea confirmar los cambios?", "25%");
                confirmMessage.getOkButton().addClickListener(e -> {
                    NucleoFamiliar nf = nucleoFamiliarDao.getById(Long.parseLong(idFliar2.getValue()));
                    nf.setNombre(nombre2.getValue());
                    nf.setApellido(apellido2.getValue());
                    nf.setCedula(ci2.getValue());
                    nf.setParentesco(cbParentezco2.getValue());
                    nf.setTelefono(telefono2.getValue());
                    
                    nucleoFamiliarDao.guardar(nf);
                    confirmMessage.closePopup();
                });
                confirmMessage.getCancelButton().addClickListener(e -> {
                    confirmMessage.closePopup();
                });
                
            });
            btnUpdate3.addClickListener(cl -> {
                ConfirmButton confirmMessage = new ConfirmButton("");
                confirmMessage.openInModalPopup("Mensaje de confirmación", "¿Desea confirmar los cambios?", "25%");
                confirmMessage.getOkButton().addClickListener(e -> {
                    NucleoFamiliar nf = nucleoFamiliarDao.getById(Long.parseLong(idFliar3.getValue()));
                    nf.setNombre(nombre3.getValue());
                    nf.setApellido(apellido3.getValue());
                    nf.setCedula(ci3.getValue());
                    nf.setParentesco(cbParentezco3.getValue());
                    nf.setTelefono(telefono3.getValue());
                    
                    nucleoFamiliarDao.guardar(nf);
                    confirmMessage.closePopup();
                });
                confirmMessage.getCancelButton().addClickListener(e -> {
                    confirmMessage.closePopup();
                });
                
            });
            btnUpdate4.addClickListener(cl -> {
                ConfirmButton confirmMessage = new ConfirmButton("");
                confirmMessage.openInModalPopup("Mensaje de confirmación", "¿Desea confirmar los cambios?", "25%");
                confirmMessage.getOkButton().addClickListener(e -> {
                    NucleoFamiliar nf = nucleoFamiliarDao.getById(Long.parseLong(idFliar4.getValue()));
                    nf.setNombre(nombre4.getValue());
                    nf.setApellido(apellido4.getValue());
                    nf.setCedula(ci4.getValue());
                    nf.setParentesco(cbParentezco4.getValue());
                    nf.setTelefono(telefono4.getValue());
                    
                    nucleoFamiliarDao.guardar(nf);
                    confirmMessage.closePopup();
                });
                confirmMessage.getCancelButton().addClickListener(e -> {
                    confirmMessage.closePopup();
                });
                
            });
            btnUpdate5.addClickListener(cl -> {
                ConfirmButton confirmMessage = new ConfirmButton("");
                confirmMessage.openInModalPopup("Mensaje de confirmación", "¿Desea confirmar los cambios?", "25%");
                confirmMessage.getOkButton().addClickListener(e -> {
                    NucleoFamiliar nf = nucleoFamiliarDao.getById(Long.parseLong(idFliar5.getValue()));
                    nf.setNombre(nombre5.getValue());
                    nf.setApellido(apellido5.getValue());
                    nf.setCedula(ci5.getValue());
                    nf.setParentesco(cbParentezco5.getValue());
                    nf.setTelefono(telefono5.getValue());
                    
                    nucleoFamiliarDao.guardar(nf);
                    confirmMessage.closePopup();
                });
                confirmMessage.getCancelButton().addClickListener(e -> {
                    confirmMessage.closePopup();
                });
                
            });
            btnUpdate6.addClickListener(cl -> {
                ConfirmButton confirmMessage = new ConfirmButton("");
                confirmMessage.openInModalPopup("Mensaje de confirmación", "¿Desea confirmar los cambios?", "25%");
                confirmMessage.getOkButton().addClickListener(e -> {
                    NucleoFamiliar nf = nucleoFamiliarDao.getById(Long.parseLong(idFliar6.getValue()));
                    nf.setNombre(nombre6.getValue());
                    nf.setApellido(apellido6.getValue());
                    nf.setCedula(ci6.getValue());
                    nf.setParentesco(cbParentezco6.getValue());
                    nf.setTelefono(telefono6.getValue());
                    
                    nucleoFamiliarDao.guardar(nf);
                    confirmMessage.closePopup();
                });
                confirmMessage.getCancelButton().addClickListener(e -> {
                    confirmMessage.closePopup();
                });
                
            });
            btnUpdate7.addClickListener(cl -> {
                ConfirmButton confirmMessage = new ConfirmButton("");
                confirmMessage.openInModalPopup("Mensaje de confirmación", "¿Desea confirmar los cambios?", "25%");
                confirmMessage.getOkButton().addClickListener(e -> {
                    NucleoFamiliar nf = nucleoFamiliarDao.getById(Long.parseLong(idFliar7.getValue()));
                    nf.setNombre(nombre7.getValue());
                    nf.setApellido(apellido7.getValue());
                    nf.setCedula(ci7.getValue());
                    nf.setParentesco(cbParentezco7.getValue());
                    nf.setTelefono(telefono7.getValue());
                    
                    nucleoFamiliarDao.guardar(nf);
                    confirmMessage.closePopup();
                });
                confirmMessage.getCancelButton().addClickListener(e -> {
                    confirmMessage.closePopup();
                });
                
            });
            btnEliminar1.addClickListener(cl -> {
                ConfirmButton confirmMessage = new ConfirmButton("");
                confirmMessage.openInModalPopup("Mensaje de confirmación", "¿Desea eliminar el agendamiento?", "25%");
                confirmMessage.getOkButton().addClickListener(e -> {
                    AgendamientoDetalle ad = AgendamientoDetalleController.getByIdAgendamientoIdNucleo(idAgendamiento.getValue(), idFliar1.getValue());
                    
                    AgendamientoDetalleController.borrar(ad);
                    
                    cbParentezco1.setVisible(false);
                    ci1.setVisible(false);
                    nombre1.setVisible(false);
                    apellido1.setVisible(false);
                    telefono1.setVisible(false);
                    piscina1.setVisible(false);
                    btnUpdate1.setVisible(false);
                    btnDownload1.setVisible(false);
                    btnEliminar1.setVisible(false);
                    confirmMessage.closePopup();
                });
                confirmMessage.getCancelButton().addClickListener(e -> {
                    confirmMessage.closePopup();
                });
                
            });
            btnEliminar2.addClickListener(cl -> {
                ConfirmButton confirmMessage = new ConfirmButton("");
                confirmMessage.openInModalPopup("Mensaje de confirmación", "¿Desea eliminar el agendamiento?", "25%");
                confirmMessage.getOkButton().addClickListener(e -> {
                    AgendamientoDetalle ad = AgendamientoDetalleController.getByIdAgendamientoIdNucleo(idAgendamiento.getValue(), idFliar2.getValue());
                    
                    AgendamientoDetalleController.borrar(ad);
                    
                    cbParentezco2.setVisible(false);
                    ci2.setVisible(false);
                    nombre2.setVisible(false);
                    apellido2.setVisible(false);
                    telefono2.setVisible(false);
                    piscina2.setVisible(false);
                    btnUpdate2.setVisible(false);
                    btnDownload2.setVisible(false);
                    btnEliminar2.setVisible(false);
                    
                    confirmMessage.closePopup();
                });
                confirmMessage.getCancelButton().addClickListener(e -> {
                    confirmMessage.closePopup();
                });
                
            });
            btnEliminar3.addClickListener(cl -> {
                ConfirmButton confirmMessage = new ConfirmButton("");
                confirmMessage.openInModalPopup("Mensaje de confirmación", "¿Desea eliminar el agendamiento?", "25%");
                confirmMessage.getOkButton().addClickListener(e -> {
                    AgendamientoDetalle ad = AgendamientoDetalleController.getByIdAgendamientoIdNucleo(idAgendamiento.getValue(), idFliar3.getValue());
                    
                    AgendamientoDetalleController.borrar(ad);
                    
                    cbParentezco3.setVisible(false);
                    ci3.setVisible(false);
                    nombre3.setVisible(false);
                    apellido3.setVisible(false);
                    telefono3.setVisible(false);
                    piscina3.setVisible(false);
                    btnUpdate3.setVisible(false);
                    btnDownload3.setVisible(false);
                    btnEliminar3.setVisible(false);
                    confirmMessage.closePopup();
                });
                confirmMessage.getCancelButton().addClickListener(e -> {
                    confirmMessage.closePopup();
                });
                
            });
            btnEliminar4.addClickListener(cl -> {
                ConfirmButton confirmMessage = new ConfirmButton("");
                confirmMessage.openInModalPopup("Mensaje de confirmación", "¿Desea eliminar el agendamiento?", "25%");
                confirmMessage.getOkButton().addClickListener(e -> {
                    AgendamientoDetalle ad = AgendamientoDetalleController.getByIdAgendamientoIdNucleo(idAgendamiento.getValue(), idFliar4.getValue());
                    
                    AgendamientoDetalleController.borrar(ad);
                    
                    cbParentezco4.setVisible(false);
                    ci4.setVisible(false);
                    nombre4.setVisible(false);
                    apellido4.setVisible(false);
                    telefono4.setVisible(false);
                    piscina4.setVisible(false);
                    btnUpdate4.setVisible(false);
                    btnDownload4.setVisible(false);
                    btnEliminar4.setVisible(false);
                    confirmMessage.closePopup();
                });
                confirmMessage.getCancelButton().addClickListener(e -> {
                    confirmMessage.closePopup();
                });
                
            });
            btnEliminar5.addClickListener(cl -> {
                ConfirmButton confirmMessage = new ConfirmButton("");
                confirmMessage.openInModalPopup("Mensaje de confirmación", "¿Desea eliminar el agendamiento?", "25%");
                confirmMessage.getOkButton().addClickListener(e -> {
                    AgendamientoDetalle ad = AgendamientoDetalleController.getByIdAgendamientoIdNucleo(idAgendamiento.getValue(), idFliar5.getValue());
                    
                    AgendamientoDetalleController.borrar(ad);
                    
                    cbParentezco5.setVisible(false);
                    ci5.setVisible(false);
                    nombre5.setVisible(false);
                    apellido5.setVisible(false);
                    telefono5.setVisible(false);
                    piscina5.setVisible(false);
                    btnUpdate5.setVisible(false);
                    btnDownload5.setVisible(false);
                    btnEliminar5.setVisible(false);
                    confirmMessage.closePopup();
                });
                confirmMessage.getCancelButton().addClickListener(e -> {
                    confirmMessage.closePopup();
                });
                
            });
            btnEliminar6.addClickListener(cl -> {
                ConfirmButton confirmMessage = new ConfirmButton("");
                confirmMessage.openInModalPopup("Mensaje de confirmación", "¿Desea eliminar el agendamiento?", "25%");
                confirmMessage.getOkButton().addClickListener(e -> {
                    AgendamientoDetalle ad = AgendamientoDetalleController.getByIdAgendamientoIdNucleo(idAgendamiento.getValue(), idFliar6.getValue());
                    
                    AgendamientoDetalleController.borrar(ad);
                    
                    cbParentezco6.setVisible(false);
                    ci6.setVisible(false);
                    nombre6.setVisible(false);
                    apellido6.setVisible(false);
                    telefono6.setVisible(false);
                    piscina6.setVisible(false);
                    btnUpdate6.setVisible(false);
                    btnDownload6.setVisible(false);
                    btnEliminar6.setVisible(false);
                    confirmMessage.closePopup();
                });
                confirmMessage.getCancelButton().addClickListener(e -> {
                    confirmMessage.closePopup();
                });
                
            });
            btnEliminar7.addClickListener(cl -> {
                ConfirmButton confirmMessage = new ConfirmButton("");
                confirmMessage.openInModalPopup("Mensaje de confirmación", "¿Desea eliminar el agendamiento?", "25%");
                confirmMessage.getOkButton().addClickListener(e -> {
                    AgendamientoDetalle ad = AgendamientoDetalleController.getByIdAgendamientoIdNucleo(idAgendamiento.getValue(), idFliar7.getValue());
                    
                    AgendamientoDetalleController.borrar(ad);
                    
                    cbParentezco7.setVisible(false);
                    ci7.setVisible(false);
                    nombre7.setVisible(false);
                    apellido7.setVisible(false);
                    telefono7.setVisible(false);
                    piscina7.setVisible(false);
                    btnUpdate7.setVisible(false);
                    btnDownload7.setVisible(false);
                    btnEliminar7.setVisible(false);
                    confirmMessage.closePopup();
                });
                confirmMessage.getCancelButton().addClickListener(e -> {
                    confirmMessage.closePopup();
                });
                
            });
            btnInvitadoEliminar1.addClickListener(cl -> {
                ConfirmButton confirmMessage = new ConfirmButton("");
                confirmMessage.openInModalPopup("Mensaje de confirmación", "¿Desea eliminar el agendamiento?", "25%");
                confirmMessage.getOkButton().addClickListener(e -> {
                    AgendamientoDetalle ad = AgendamientoDetalleController.getByIdAgendamientoIdInvitado(idAgendamiento.getValue(), idInvitado1.getValue());
                    
                    AgendamientoDetalleController.borrar(ad);
                    
                    cbInvitadoParentezco1.setVisible(false);
                    ciInvitado1.setVisible(false);
                    nombreInvitado1.setVisible(false);
                    apellidoInvitado1.setVisible(false);
                    telefonoInvitado1.setVisible(false);
                    piscinaInvitado1.setVisible(false);
                    btnUpdateInvitado1.setVisible(false);
                    btnInvitadoEliminar1.setVisible(false);
                    confirmMessage.closePopup();
                });
                confirmMessage.getCancelButton().addClickListener(e -> {
                    confirmMessage.closePopup();
                });
                
            });
            btnInvitadoEliminar2.addClickListener(cl -> {
                ConfirmButton confirmMessage = new ConfirmButton("");
                confirmMessage.openInModalPopup("Mensaje de confirmación", "¿Desea eliminar el agendamiento?", "25%");
                confirmMessage.getOkButton().addClickListener(e -> {
                    AgendamientoDetalle ad = AgendamientoDetalleController.getByIdAgendamientoIdInvitado(idAgendamiento.getValue(), idInvitado2.getValue());
                    
                    AgendamientoDetalleController.borrar(ad);
                    
                    cbInvitadoParentezco2.setVisible(false);
                    ciInvitado2.setVisible(false);
                    nombreInvitado2.setVisible(false);
                    apellidoInvitado2.setVisible(false);
                    telefonoInvitado2.setVisible(false);
                    piscinaInvitado2.setVisible(false);
                    btnUpdateInvitado2.setVisible(false);
                    btnInvitadoEliminar2.setVisible(false);
                    confirmMessage.closePopup();
                });
                confirmMessage.getCancelButton().addClickListener(e -> {
                    confirmMessage.closePopup();
                });
                
            });
            btnUpdateInvitado1.addClickListener(cl -> {
                ConfirmButton confirmMessage = new ConfirmButton("");
                confirmMessage.openInModalPopup("Mensaje de confirmación", "¿Desea confirmar los cambios?", "25%");
                confirmMessage.getOkButton().addClickListener(e -> {
                    Invitado nf = invitadoDao.getById(Long.parseLong(idInvitado1.getValue()));
                    nf.setNombre(nombreInvitado1.getValue());
                    nf.setApellido(apellidoInvitado1.getValue());
                    nf.setCedula(ciInvitado1.getValue());
                    nf.setParentesco(cbInvitadoParentezco1.getValue());
                    nf.setCelular(telefonoInvitado1.getValue());
                    
                    invitadoDao.guardar(nf);
                    confirmMessage.closePopup();
                });
                confirmMessage.getCancelButton().addClickListener(e -> {
                    confirmMessage.closePopup();
                });
            });
            btnUpdateInvitado2.addClickListener(cl -> {
                ConfirmButton confirmMessage = new ConfirmButton("");
                confirmMessage.openInModalPopup("Mensaje de confirmación", "¿Desea confirmar los cambios?", "25%");
                confirmMessage.getOkButton().addClickListener(e -> {
                    Invitado nf = invitadoDao.getById(Long.parseLong(idInvitado2.getValue()));
                    nf.setNombre(nombreInvitado2.getValue());
                    nf.setApellido(apellidoInvitado2.getValue());
                    nf.setCedula(ciInvitado2.getValue());
                    nf.setParentesco(cbInvitadoParentezco2.getValue());
                    nf.setCelular(telefonoInvitado2.getValue());
                    
                    invitadoDao.guardar(nf);
                    confirmMessage.closePopup();
                });
                confirmMessage.getCancelButton().addClickListener(e -> {
                    confirmMessage.closePopup();
                });
            });
            btnDownload1.addClickListener(e -> descargarArchivo(new File(Constants.UPLOAD_DIR + "//" + this.solicitud.getSocio().getCedula() + "//1_" + this.solicitud.getSocio().getCedula() + "." + obs1), btnDownload1));
            btnDownload2.addClickListener(e -> descargarArchivo(new File(Constants.UPLOAD_DIR + "//" + this.solicitud.getSocio().getCedula() + "//2_" + this.solicitud.getSocio().getCedula() + "." + obs2), btnDownload2));
            btnDownload3.addClickListener(e -> descargarArchivo(new File(Constants.UPLOAD_DIR + "//" + this.solicitud.getSocio().getCedula() + "//3_" + this.solicitud.getSocio().getCedula() + "." + obs1), btnDownload3));
            btnDownload4.addClickListener(e -> descargarArchivo(new File(Constants.UPLOAD_DIR + "//" + this.solicitud.getSocio().getCedula() + "//4_" + this.solicitud.getSocio().getCedula() + "." + obs2), btnDownload4));
            btnDownload5.addClickListener(e -> descargarArchivo(new File(Constants.UPLOAD_DIR + "//" + this.solicitud.getSocio().getCedula() + "//5_" + this.solicitud.getSocio().getCedula() + "." + obs1), btnDownload5));
            btnDownload6.addClickListener(e -> descargarArchivo(new File(Constants.UPLOAD_DIR + "//" + this.solicitud.getSocio().getCedula() + "//6_" + this.solicitud.getSocio().getCedula() + "." + obs2), btnDownload6));
            btnDownload7.addClickListener(e -> descargarArchivo(new File(Constants.UPLOAD_DIR + "//" + this.solicitud.getSocio().getCedula() + "//7_" + this.solicitud.getSocio().getCedula() + "." + obs2), btnDownload7));
            
            guardar.addClickListener(cl -> {
                try {
                    ConfirmButton confirmMessage = new ConfirmButton("");
                    confirmMessage.openInModalPopup("Mensaje de confirmación", "¿Desea confirmar los cambios?", "25%");
                    confirmMessage.getOkButton().addClickListener(e -> {
                        
                        Agendamiento agend = AgendamientoController.listaAgendamientoById(this.solicitud.getId());
                        agend.setFecha(DateUtils.asDate(fechaAgendamiento.getValue()));
                        
                        agend.setAsiste(asisteSocio.getValue());
                        agend.setUsopiscina(piscinaSocio.getValue());
                        agend.setHorario(cbHorarios.getValue());
                        
                        if (observacionSocio.isVisible()) {
                            agend.setObservacion(observacionSocio.getValue());
                        }
                        
                        AgendamientoController.guardar(agend);
                        
                        saveListener.accept(solicitud);
                        Notification.show("Mensaje del Sistema", "Datos actualizados correctamente", Notification.Type.HUMANIZED_MESSAGE);
                        confirmMessage.closePopup();
                        setVisible(false);
                    });
                    confirmMessage.getCancelButton().addClickListener(e -> {
                        confirmMessage.closePopup();
                    });
                    
                } catch (Exception e) {
                    e.printStackTrace();
                    Notification.show(e.getMessage());
                }
            });
            btnAgregar.addClickListener(cl -> {
                SimpleDateFormat sdfHM = new SimpleDateFormat("HH.mm");
                SimpleDateFormat sdf = new SimpleDateFormat("mm");
                long min = 0l;
            });
//            guardar.setEnabled(false);
            cancelar.addClickListener(clickEvent -> {
                close();
            });

// Create tab content dynamically when tab is selected
            tabsheet.addSelectedTabChangeListener(
                    new TabSheet.SelectedTabChangeListener() {
                public void selectedTabChange(SelectedTabChangeEvent event) {
                    // Find the tabsheet
                    TabSheet tabsheet = event.getTabSheet();

                    // Find the tab (here we know it's a layout)
                    Layout tab = (Layout) tabsheet.getSelectedTab();

                    // Get the tab caption from the tab object
                    String caption = tabsheet.getTab(tab).getCaption();

                    // Fill the tab content
                    tab.removeAllComponents();
                    VerticalLayout vl = new VerticalLayout();
                    vl.setWidth("100%");
//                    vl.addComponent(new Label("HOLA MUNDO ->>"+caption));
//                    if (caption.equalsIgnoreCase("Datos Socio")) {
//                        vl.addComponent(createForm());
//                    } else if (caption.equalsIgnoreCase("Imagenes")) {
//                        vl.addComponent(createHojaImagenes());
//                    } else 
                    if (caption.equalsIgnoreCase("Familiares")) {
                        vl.addComponent(createDatosFliares());
                    } else if (caption.equalsIgnoreCase("Invitados")) {
                        vl.addComponent(createDatosSocioDemografico());
                    } else {
                        vl.addComponent(createFormMotivoSolicitud());
                    }
                    
                    tab.addComponent(vl);
                }
                
                private VerticalLayout createForm() {
                    VerticalLayout layout = new VerticalLayout();
                    layout.addStyleName("crud-view");
                    layout.setMargin(true);
                    layout.setSpacing(true);
                    
                    HorizontalLayout horizontal = new HorizontalLayout();
                    horizontal.addComponent(txtCedulaSocio);
                    horizontal.addComponent(txtNombreSocio);
                    horizontal.addComponent(txtApellido);
                    horizontal.addComponent(txtTelefono);
                    
                    horizontal.setWidth("100%");
                    horizontal.setSpacing(true);
                    horizontal.setStyleName("top-bar");
                    
                    HorizontalLayout horizontalSegundo = new HorizontalLayout();
                    horizontalSegundo.addComponent(ciudad);
                    horizontalSegundo.addComponent(dpto);
                    horizontalSegundo.addComponent(institucion);
                    horizontalSegundo.addComponent(txtCargo);
                    
                    horizontalSegundo.setWidth("100%");
                    horizontalSegundo.setSpacing(true);
                    horizontalSegundo.setStyleName("top-bar");
                    
                    HorizontalLayout horizontalTercero = new HorizontalLayout();
                    horizontalTercero.addComponent(txtTipoVivienda);
                    horizontalTercero.addComponent(txtPin);
                    
                    horizontalTercero.setWidth("50%");
                    horizontalTercero.setSpacing(true);
                    horizontalTercero.setStyleName("top-bar");
                    
                    layout.addComponent(horizontal);
                    layout.addComponent(horizontalSegundo);
                    layout.addComponent(horizontalTercero);
                    
                    HorizontalLayout horizontalButton = new HorizontalLayout();
                    cancelar.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_FRIENDLY);
                    horizontalButton.addComponent(cancelar);
                    horizontalButton.setSpacing(true);
                    layout.addComponent(horizontalButton);
                    return layout;
                }
                
                private VerticalLayout createDatosFliares() {
                    VerticalLayout layout = new VerticalLayout();
                    layout.addStyleName("crud-view");
                    layout.setMargin(true);
                    layout.setSpacing(true);
                    
                    HorizontalLayout horizontal = new HorizontalLayout();
                    horizontal.addComponent(btnEliminar1);
                    horizontal.addComponent(cbParentezco1);
                    cbParentezco1.setWidth("100%");
                    horizontal.addComponent(ci1);
                    ci1.setWidth("100%");
                    horizontal.addComponent(nombre1);
                    nombre1.setWidth("100%");
                    horizontal.addComponent(apellido1);
                    apellido1.setWidth("100%");
                    horizontal.addComponent(telefono1);
                    horizontal.addComponent(piscina1);
                    piscina1.setWidth("40%");
                    //horizontal.addComponent(btnUpdate1);
                    //horizontal.addComponent(btnDownload1);
                    telefono1.setWidth("100%");
                    
                    horizontal.setWidth("100%");
                    horizontal.setSpacing(true);
                    horizontal.setStyleName("top-bar");
                    
                    HorizontalLayout horizontalSegundo = new HorizontalLayout();
                    horizontalSegundo.addComponent(btnEliminar2);
                    horizontalSegundo.addComponent(cbParentezco2);
                    cbParentezco2.setWidth("100%");
                    horizontalSegundo.addComponent(ci2);
                    ci2.setWidth("100%");
                    horizontalSegundo.addComponent(nombre2);
                    nombre2.setWidth("100%");
                    horizontalSegundo.addComponent(apellido2);
                    apellido2.setWidth("100%");
                    horizontalSegundo.addComponent(telefono2);
                    telefono2.setWidth("100%");
                    horizontalSegundo.addComponent(piscina2);
                    piscina2.setWidth("40%");
                    //horizontalSegundo.addComponent(btnUpdate2);
                    //horizontalSegundo.addComponent(btnDownload2);

                    horizontalSegundo.setWidth("100%");
                    horizontalSegundo.setSpacing(true);
                    horizontalSegundo.setStyleName("top-bar");
                    
                    HorizontalLayout horizontalTercero = new HorizontalLayout();
                    horizontalTercero.addComponent(btnEliminar3);
                    horizontalTercero.addComponent(cbParentezco3);
                    cbParentezco3.setWidth("100%");
                    horizontalTercero.addComponent(ci3);
                    ci3.setWidth("100%");
                    horizontalTercero.addComponent(nombre3);
                    nombre3.setWidth("100%");
                    horizontalTercero.addComponent(apellido3);
                    apellido3.setWidth("100%");
                    horizontalTercero.addComponent(telefono3);
                    telefono3.setWidth("100%");
                    horizontalTercero.addComponent(piscina3);
                    piscina3.setWidth("40%");
                    //horizontalTercero.addComponent(btnUpdate3);
                    //horizontalTercero.addComponent(btnDownload3);

                    horizontalTercero.setWidth("100%");
                    horizontalTercero.setSpacing(true);
                    horizontalTercero.setStyleName("top-bar");
                    
                    HorizontalLayout horizontal4to = new HorizontalLayout();
                    horizontal4to.addComponent(btnEliminar4);
                    horizontal4to.addComponent(cbParentezco4);
                    cbParentezco4.setWidth("100%");
                    horizontal4to.addComponent(ci4);
                    ci4.setWidth("100%");
                    horizontal4to.addComponent(nombre4);
                    nombre4.setWidth("100%");
                    horizontal4to.addComponent(apellido4);
                    apellido4.setWidth("100%");
                    horizontal4to.addComponent(telefono4);
                    telefono4.setWidth("100%");
                    
                    horizontal4to.addComponent(piscina4);
                    piscina4.setWidth("40%");
                    //horizontal4to.addComponent(btnUpdate4);
                    //horizontal4to.addComponent(btnDownload4);

                    horizontal4to.setWidth("100%");
                    horizontal4to.setSpacing(true);
                    horizontal4to.setStyleName("top-bar");
                    
                    HorizontalLayout horizontal5to = new HorizontalLayout();
                    //horizontal5to.addComponent(btnEliminar5);
                    //horizontal5to.addComponent(cbParentezco5);
                    cbParentezco5.setWidth("100%");
                    horizontal5to.addComponent(ci5);
                    ci5.setWidth("100%");
                    horizontal5to.addComponent(nombre5);
                    nombre5.setWidth("100%");
                    horizontal5to.addComponent(apellido5);
                    apellido5.setWidth("100%");
                    horizontal5to.addComponent(telefono5);
                    telefono5.setWidth("100%");
                    
                    horizontal5to.addComponent(piscina5);
                    piscina5.setWidth("40%");
                    //horizontal5to.addComponent(btnUpdate5);
                    //horizontal5to.addComponent(btnDownload5);

                    horizontal5to.setWidth("100%");
                    horizontal5to.setSpacing(true);
                    horizontal5to.setStyleName("top-bar");
                    
                    HorizontalLayout horizontal6to = new HorizontalLayout();
                    // horizontal6to.addComponent(btnEliminar6);
                    //horizontal6to.addComponent(cbParentezco6);
                    cbParentezco6.setWidth("100%");
                    horizontal6to.addComponent(ci6);
                    ci6.setWidth("100%");
                    horizontal6to.addComponent(nombre6);
                    nombre6.setWidth("100%");
                    horizontal6to.addComponent(apellido6);
                    apellido6.setWidth("100%");
                    horizontal6to.addComponent(telefono6);
                    telefono6.setWidth("100%");
                    
                    horizontal6to.addComponent(piscina6);
                    piscina6.setWidth("40%");
                    ///horizontal6to.addComponent(btnUpdate6);
                    //horizontal6to.addComponent(btnDownload6);

                    HorizontalLayout horizontal7mo = new HorizontalLayout();
                    horizontal7mo.addComponent(btnEliminar7);
                    horizontal7mo.addComponent(cbParentezco7);
                    horizontal7mo.addComponent(ci7);
                    horizontal7mo.addComponent(nombre7);
                    horizontal7mo.addComponent(apellido7);
                    horizontal7mo.addComponent(telefono7);
                    cbParentezco7.setWidth("100%");
                    ci7.setWidth("100%");
                    nombre7.setWidth("100%");
                    apellido7.setWidth("100%");
                    telefono7.setWidth("100%");
                    
                    horizontal7mo.addComponent(piscina7);
                    piscina7.setWidth("40%");
                    //horizontal7mo.addComponent(btnUpdate7);
                    //horizontal7mo.addComponent(btnDownload7);

                    horizontal6to.setWidth("100%");
                    horizontal6to.setSpacing(true);
                    horizontal6to.setStyleName("top-bar");
                    
                    horizontal7mo.setWidth("100%");
                    horizontal7mo.setSpacing(true);
                    horizontal7mo.setStyleName("top-bar");
                    
                    layout.addComponent(horizontal);
                    layout.addComponent(horizontalSegundo);
                    layout.addComponent(horizontalTercero);
                    layout.addComponent(horizontal4to);
                    layout.addComponent(horizontal5to);
                    layout.addComponent(horizontal6to);
                    layout.addComponent(horizontal7mo);
                    
                    HorizontalLayout horizontalButton = new HorizontalLayout();
                    guardar.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_PRIMARY);
                    horizontalButton.addComponent(guardar);
                    cancelar.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_FRIENDLY);
                    horizontalButton.addComponent(cancelar);
                    horizontalButton.setSpacing(true);
                    layout.addComponent(horizontalButton);
                    return layout;
                }
                
                private VerticalLayout createFormMotivoSolicitud() {
                    guardar.setVisible(true);
                    VerticalLayout layout = new VerticalLayout();
                    layout.addStyleName("crud-view");
                    layout.setMargin(true);
                    layout.setSpacing(true);
                    
                    HorizontalLayout horizontal = new HorizontalLayout();
                    
                    horizontal.addComponent(fechaAgendamiento);
                    horizontal.addComponent(cbHorarios);
                    horizontal.addComponent(nombreSocio);
                    horizontal.addComponent(apellidoSocio);

//                    horizontal.setComponentAlignment(txtMotivoObs, Alignment.TOP_LEFT);
//                    txtMotivoObs.setWidth("80%");
                    layout.addComponent(horizontal);
                    
                    HorizontalLayout horizontalSegundo = new HorizontalLayout();
//                    horizontalSegundo.addComponent(cbHorarios);
                    horizontalSegundo.addComponent(observacionSocio);
                    observacionSocio.setWidth(45f, TextField.UNITS_EM);
                    horizontalSegundo.addComponent(asisteSocio);
                    horizontalSegundo.addComponent(piscinaSocio);
                    horizontalSegundo.setSpacing(true);
                    
                    layout.addComponent(horizontalSegundo);
                    
                    Panel sample1 = new Panel();
                    sample1.setHeight(100.0f, Unit.PERCENTAGE);
                    sample1.setContent(photo1);
                    
                    Panel sample2 = new Panel();
                    sample2.setHeight(100.0f, Unit.PERCENTAGE);
                    sample2.setContent(photo2);
                    
                    HorizontalLayout horizontalFoto = new HorizontalLayout();
                    horizontalFoto.addComponent(sample1);
                    horizontalFoto.addComponent(sample2);
                    
                    layout.addComponent(horizontalFoto);
//                    HorizontalLayout horizontalTercero = new HorizontalLayout();
//                    horizontalTercero.addComponent(asisteSocio);
//                    horizontalTercero.addComponent(piscinaSocio);
//                    horizontalTercero.setSpacing(true);
//
//                    layout.addComponent(horizontalTercero);
                    //layout.addComponent(txtAtendidoPor);
                    //layout.addComponent(h4to);
//                    layout.addComponent(mainLayout);
                    HorizontalLayout horizontalButton = new HorizontalLayout();
                    guardar.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_PRIMARY);
                    horizontalButton.addComponent(guardar);
                    cancelar.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_FRIENDLY);
                    horizontalButton.addComponent(cancelar);
                    horizontalButton.setSpacing(true);
                    layout.addComponent(horizontalButton);
                    return layout;
                }
                
                private VerticalLayout createHojaImagenes() {
                    guardar.setVisible(true);
                    VerticalLayout layout = new VerticalLayout();
                    layout.addStyleName("crud-view");
                    layout.setMargin(true);
                    layout.setSpacing(true);

                    //layout.addComponent(photo);
                    Panel sample1 = new Panel();
                    sample1.setHeight(100.0f, Unit.PERCENTAGE);

//                    final VerticalLayout contentLayout = new VerticalLayout();
//                    contentLayout.setWidth(500, Unit.PIXELS);
//                    contentLayout.setSpacing(false);
//                    contentLayout.addComponent(photo1);
                    sample1.setContent(photo1);
                    
                    HorizontalLayout horizontal = new HorizontalLayout();
                    horizontal.addComponent(sample1);
                    
                    Panel sample2 = new Panel();
                    sample2.setHeight(100.0f, Unit.PERCENTAGE);

//                    final VerticalLayout contentLayout2 = new VerticalLayout();
//                    contentLayout2.setWidth(500, Unit.PIXELS);
//                    contentLayout2.setSpacing(false);
//                    contentLayout2.addComponent(photo2);
                    sample2.setContent(photo2);
                    horizontal.addComponent(sample2);

//                    Panel sample3 = new Panel();
//                    sample3.setHeight(100.0f, Unit.PERCENTAGE);
//
////                    final VerticalLayout contentLayout3 = new VerticalLayout();
////                    contentLayout3.setWidth(500, Unit.PIXELS);
////                    contentLayout3.setSpacing(false);
////                    contentLayout3.addComponent(photo3);
//                    sample3.setContent(photo3);
//                    horizontal.addComponent(sample3);
//
//                    Panel sample4 = new Panel();
//                    sample4.setHeight(100.0f, Unit.PERCENTAGE);
//
////                    final VerticalLayout contentLayout3 = new VerticalLayout();
////                    contentLayout3.setWidth(500, Unit.PIXELS);
////                    contentLayout3.setSpacing(false);
////                    contentLayout3.addComponent(photo3);
//                    sample4.setContent(photo4);
//                    horizontal.addComponent(sample4);
//
//                    horizontal.setWidth("100%");
//                    horizontal.setSpacing(true);
//                    horizontal.setStyleName("top-bar");
//                    HorizontalLayout horizontalSegundo = new HorizontalLayout();
//                    //horizontalSegundo.addComponent(txtOtroMotivo);
//                    HorizontalLayout horizontalTercero = new HorizontalLayout();
////        horizontalSegundo.addComponent(txtOtroMotivo);
//                    //txtOtroMotivo.setWidth("100%");
//
//                    horizontalSegundo.setWidth("100%");
//                    horizontalSegundo.setSpacing(true);
//                    horizontalSegundo.setStyleName("top-bar");
//                    //txtDependencia.setWidth("100%");
//
//                    horizontalTercero.setWidth("100%");
//                    horizontalTercero.setSpacing(true);
//                    horizontalTercero.setStyleName("top-bar");
                    btnAgregar.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_PRIMARY);
                    //form.addComponent(cbFuncionario);
                    form.addComponent(btnAgregar);
                    mainLayout.addComponent(form);
                    mainLayout.setWidth("100%");
                    
                    layout.addComponent(mainLayout);
                    
                    layout.addComponent(horizontal);
                    
                    HorizontalLayout horizontalButton = new HorizontalLayout();
                    guardar.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_PRIMARY);
                    horizontalButton.addComponent(guardar);
                    cancelar.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_FRIENDLY);
                    horizontalButton.addComponent(cancelar);
                    horizontalButton.setSpacing(true);
                    layout.addComponent(horizontalButton);
                    return layout;
                }
                
                private Component createDatosSocioDemografico() {
                    VerticalLayout layout = new VerticalLayout();
                    layout.addStyleName("crud-view");
                    layout.setMargin(true);
                    layout.setSpacing(true);
                    
                    HorizontalLayout horizontal = new HorizontalLayout();
                    horizontal.addComponent(btnInvitadoEliminar1);
                    horizontal.addComponent(cbInvitadoParentezco1);
                    horizontal.addComponent(ciInvitado1);
                    horizontal.addComponent(nombreInvitado1);
                    horizontal.addComponent(apellidoInvitado1);
                    horizontal.addComponent(telefonoInvitado1);
                    
                    cbInvitadoParentezco1.setWidth("100%");
                    ciInvitado1.setWidth("100%");
                    nombreInvitado1.setWidth("100%");
                    apellidoInvitado1.setWidth("100%");
                    telefonoInvitado1.setWidth("100%");
                    
                    horizontal.addComponent(piscinaInvitado1);
                    piscinaInvitado1.setWidth("40%");
                    //horizontal.addComponent(btnUpdateInvitado1);
//                    horizontal.addComponent(btnDownloadInvitado1);

                    horizontal.setWidth("100%");
                    horizontal.setSpacing(true);
                    horizontal.setStyleName("top-bar");
                    
                    HorizontalLayout horizontalSegundo = new HorizontalLayout();
                    horizontalSegundo.addComponent(btnInvitadoEliminar2);
                    horizontalSegundo.addComponent(cbInvitadoParentezco2);
                    horizontalSegundo.addComponent(ciInvitado2);
                    horizontalSegundo.addComponent(nombreInvitado2);
                    horizontalSegundo.addComponent(apellidoInvitado2);
                    horizontalSegundo.addComponent(telefonoInvitado2);
                    
                    horizontalSegundo.addComponent(piscinaInvitado2);
                    piscinaInvitado2.setWidth("40%");
                    //horizontalSegundo.addComponent(btnUpdateInvitado2);
//                    horizontalSegundo.addComponent(btnDownloadInvitado2);

                    horizontalSegundo.setWidth("100%");
                    horizontalSegundo.setSpacing(true);
                    horizontalSegundo.setStyleName("top-bar");
                    
                    layout.addComponent(horizontal);
                    layout.addComponent(horizontalSegundo);
                    
                    HorizontalLayout horizontalButton = new HorizontalLayout();
                    guardar.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_PRIMARY);
                    horizontalButton.addComponent(guardar);
                    cancelar.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_FRIENDLY);
                    horizontalButton.addComponent(cancelar);
                    horizontalButton.setSpacing(true);
                    layout.addComponent(horizontalButton);
                    return layout;
                }
            });

// Have some tabs
//            String[] tabs = {"Datos Socio", "Solicitud", "Imagenes", "Datos Fliares", "Socio Demográfico"};
            String[] tabs = {"Familiares", "Invitados", "Datos"};
            for (String caption : tabs) {
                tabsheet.addTab(new VerticalLayout(), caption);
            }
            
        } catch (Exception e) {
            e.printStackTrace();
        }
        
    }
    
    private void editarSolicitudProduccion(Agendamiento solicitud) {
        labelUrl.setValue("");
        
        upload.setVisible(false);
        
        txtCedulaSocio.setVisible(false);
        txtNombreSocio.setVisible(false);
        txtApellido.setVisible(false);
        txtTelefono.setVisible(false);
        txtObservacion.setVisible(false);
        
        mainLayout.setVisible(true);

        //cbDpto.setItems(dptoDao.listarDepartamentosPadres());
        //cbDpto.setValue(dptoDao.getDependenciaByDescripcion(solicitud.getAreafunc().toLowerCase()));
        //cbCargo.setValue(dptoDao.getDependenciaByDescripcion(solicitud.getCargofunc().toLowerCase()));
        mainLayout.setVisible(true);
        
    }
    
    private void cargarDetalleProduccion(Agendamiento solicitud) {
        SimpleDateFormat formatFec = new SimpleDateFormat("yyyy-MM-dd");
        String patternHora = "HH:mm";
        SimpleDateFormat simpleDateFormatHora = new SimpleDateFormat(patternHora);
        
    }
    
    public void editarRegistro(Agendamiento solicitud) {
        setCaption("Editar Agendamiento");
        this.solicitud = solicitud;
        editar = 1;
        
        idAgendamiento.setValue(this.solicitud.getId() + "");
        idAgendamiento.setVisible(false);
        idSocio.setValue(this.solicitud.getSocio().getId() + "");
        idSocio.setVisible(false);
        fechaAgendamiento.setValue(DateUtils.asLocalDate(this.solicitud.getFecha()));
        nombreSocio.setValue(this.solicitud.getSocio().getNombre());
        apellidoSocio.setValue(this.solicitud.getSocio().getApellido());
        try {
            if (!this.solicitud.getTipo().equalsIgnoreCase("usufructo_sede") && !this.solicitud.getTipo().equalsIgnoreCase("usufructo_quincho_tatare")) {
                observacionSocio.setValue(this.solicitud.getObservacion());
                observacionSocio.setVisible(true);
                asisteSocio.setVisible(false);
                piscinaSocio.setVisible(false);
                photo1.setVisible(false);
                photo2.setVisible(false);
                tabsheet.getTab(0).setVisible(false);
                tabsheet.getTab(1).setVisible(false);
                cbHorarios.setEnabled(false);
            } else {
                observacionSocio.setVisible(false);
                asisteSocio.setVisible(true);
                piscinaSocio.setVisible(true);
                photo1.setVisible(true);
                photo2.setVisible(true);
                tabsheet.getTab(0).setVisible(true);
                tabsheet.getTab(1).setVisible(true);
                cbHorarios.setEnabled(true);
            }
            
        } catch (Exception e) {
        } finally {
        }
        try {
            asisteSocio.setValue(this.solicitud.getAsiste());
        } catch (Exception e) {
        } finally {
        }
        try {
            piscinaSocio.setValue(this.solicitud.getUsopiscina());
        } catch (Exception e) {
        } finally {
        }
        cbHorarios.setValue(this.solicitud.getHorario());
        
        nombreSocio.setEnabled(false);
        apellidoSocio.setEnabled(false);
        
        List<AgendamientoDetalle> listAgendamiento = AgendamientoController.listarPorIdAgendamiento(solicitud.getId(), "INVITADO");
        int num = 0;
        for (AgendamientoDetalle dsd : listAgendamiento) {
            if (num == 0) {
                try {
                    cbInvitadoParentezco1.setValue(dsd.getInvitado().getParentesco());
                } catch (Exception e) {
                } finally {
                }
                try {
                    idInvitado1.setValue(dsd.getInvitado().getId() + "");
                } catch (Exception e) {
                } finally {
                }
                try {
                    nombreInvitado1.setValue(dsd.getInvitado().getNombre());
                } catch (Exception e) {
                } finally {
                }
                try {
                    apellidoInvitado1.setValue(dsd.getInvitado().getApellido());
                } catch (Exception e) {
                } finally {
                }
                try {
                    ciInvitado1.setValue(dsd.getInvitado().getCedula());
                } catch (Exception e) {
                } finally {
                }
                try {
                    telefonoInvitado1.setValue(dsd.getInvitado().getCelular());
                } catch (Exception e) {
                } finally {
                }
                try {
                    if (dsd.getUsopiscina()) {
                        piscinaInvitado1.setValue("SI");
                    } else {
                        piscinaInvitado1.setValue("NO");
                    }
                    
                } catch (Exception e) {
                } finally {
                }
                cbInvitadoParentezco1.setVisible(true);
                ciInvitado1.setVisible(true);
                nombreInvitado1.setVisible(true);
                apellidoInvitado1.setVisible(true);
                telefonoInvitado1.setVisible(true);
                piscinaInvitado1.setVisible(true);
                btnUpdateInvitado1.setVisible(true);
                btnInvitadoEliminar1.setVisible(true);
//                btnDownloadInvitado1.setVisible(true);
            } else {
                try {
                    idInvitado2.setValue(dsd.getInvitado().getId() + "");
                } catch (Exception e) {
                } finally {
                }
                try {
                    cbInvitadoParentezco2.setValue(dsd.getInvitado().getParentesco());
                } catch (Exception e) {
                } finally {
                }
                try {
                    nombreInvitado2.setValue(dsd.getInvitado().getNombre());
                } catch (Exception e) {
                } finally {
                }
                try {
                    apellidoInvitado2.setValue(dsd.getInvitado().getApellido());
                } catch (Exception e) {
                } finally {
                }
                try {
                    ciInvitado2.setValue(dsd.getInvitado().getCedula());
                } catch (Exception e) {
                } finally {
                }
                try {
                    telefonoInvitado2.setValue(dsd.getInvitado().getCelular());
                } catch (Exception e) {
                } finally {
                }
                try {
                    if (dsd.getUsopiscina()) {
                        piscinaInvitado2.setValue("SI");
                    } else {
                        piscinaInvitado2.setValue("NO");
                    }
                    
                } catch (Exception e) {
                } finally {
                }
                cbInvitadoParentezco2.setVisible(true);
                ciInvitado2.setVisible(true);
                nombreInvitado2.setVisible(true);
                apellidoInvitado2.setVisible(true);
                telefonoInvitado2.setVisible(true);
                piscinaInvitado2.setVisible(true);
                btnUpdateInvitado2.setVisible(true);
                btnInvitadoEliminar2.setVisible(true);
//                btnDownloadInvitado2.setVisible(true);
            }
            num++;
        }
        List<AgendamientoDetalle> listAgendamientoFamiliar = AgendamientoController.listarPorIdAgendamiento(solicitud.getId(), "FAMILIAR");
        int numFliar = 0;
        for (AgendamientoDetalle dsd : listAgendamientoFamiliar) {
            switch (numFliar) {
                case 0:
                    try {
                        idFliar1.setValue(dsd.getNucleofamiliar().getId() + "");
                    } catch (Exception e) {
                    } finally {
                    }
                    try {
                        obs1.setValue(dsd.getNucleofamiliar().getDoc1());
                    } catch (Exception e) {
                    } finally {
                    }
                    try {
                        cbParentezco1.setValue(dsd.getNucleofamiliar().getParentesco());
                    } catch (Exception e) {
                    } finally {
                    }
                    try {
                        nombre1.setValue(dsd.getNucleofamiliar().getNombre());
                    } catch (Exception e) {
                    } finally {
                    }
                    try {
                        apellido1.setValue(dsd.getNucleofamiliar().getApellido());
                    } catch (Exception e) {
                    } finally {
                    }
                    try {
                        telefono1.setValue(dsd.getNucleofamiliar().getTelefono());
                    } catch (Exception e) {
                    } finally {
                    }
                    try {
                        ci1.setValue(dsd.getNucleofamiliar().getCedula());
                    } catch (Exception e) {
                    } finally {
                    }
                    try {
                        if (dsd.getUsopiscina()) {
                            piscina1.setValue("SI");
                        } else {
                            piscina1.setValue("NO");
                        }
                        
                    } catch (Exception e) {
                    } finally {
                    }
                    cbParentezco1.setVisible(true);
                    ci1.setVisible(true);
                    nombre1.setVisible(true);
                    apellido1.setVisible(true);
                    telefono1.setVisible(true);
                    piscina1.setVisible(true);
                    btnUpdate1.setVisible(true);
                    btnDownload1.setVisible(true);
                    btnEliminar1.setVisible(true);
                    break;
                case 1:
                    try {
                        idFliar2.setValue(dsd.getNucleofamiliar().getId() + "");
                    } catch (Exception e) {
                    } finally {
                    }
                    try {
                        obs2.setValue(dsd.getNucleofamiliar().getDoc1());
                    } catch (Exception e) {
                    } finally {
                    }
                    try {
                        cbParentezco2.setValue(dsd.getNucleofamiliar().getParentesco());
                    } catch (Exception e) {
                    } finally {
                    }
                    try {
                        nombre2.setValue(dsd.getNucleofamiliar().getNombre());
                    } catch (Exception e) {
                    } finally {
                    }
                    try {
                        apellido2.setValue(dsd.getNucleofamiliar().getApellido());
                    } catch (Exception e) {
                    } finally {
                    }
                    try {
                        telefono2.setValue(dsd.getNucleofamiliar().getTelefono());
                    } catch (Exception e) {
                    } finally {
                    }
                    try {
                        ci2.setValue(dsd.getNucleofamiliar().getCedula());
                    } catch (Exception e) {
                    } finally {
                    }
                    try {
                        if (dsd.getUsopiscina()) {
                            piscina2.setValue("SI");
                        } else {
                            piscina2.setValue("NO");
                        }
                        
                    } catch (Exception e) {
                    } finally {
                    }
                    cbParentezco2.setVisible(true);
                    ci2.setVisible(true);
                    nombre2.setVisible(true);
                    apellido2.setVisible(true);
                    telefono2.setVisible(true);
                    piscina2.setVisible(true);
                    btnUpdate2.setVisible(true);
                    btnDownload2.setVisible(true);
                    btnEliminar2.setVisible(true);
                    break;
                case 2:
                    try {
                        idFliar3.setValue(dsd.getNucleofamiliar().getId() + "");
                    } catch (Exception e) {
                    } finally {
                    }
                    try {
                        obs3.setValue(dsd.getNucleofamiliar().getDoc1());
                    } catch (Exception e) {
                    } finally {
                    }
                    try {
                        cbParentezco3.setValue(dsd.getNucleofamiliar().getParentesco());
                    } catch (Exception e) {
                    } finally {
                    }
                    try {
                        nombre3.setValue(dsd.getNucleofamiliar().getNombre());
                    } catch (Exception e) {
                    } finally {
                    }
                    try {
                        apellido3.setValue(dsd.getNucleofamiliar().getApellido());
                    } catch (Exception e) {
                    } finally {
                    }
                    try {
                        telefono3.setValue(dsd.getNucleofamiliar().getTelefono());
                    } catch (Exception e) {
                    } finally {
                    }
                    try {
                        ci3.setValue(dsd.getNucleofamiliar().getCedula());
                    } catch (Exception e) {
                    } finally {
                    }
                    try {
                        if (dsd.getUsopiscina()) {
                            piscina3.setValue("SI");
                        } else {
                            piscina3.setValue("NO");
                        }
                        
                    } catch (Exception e) {
                    } finally {
                    }
                    cbParentezco3.setVisible(true);
                    ci3.setVisible(true);
                    nombre3.setVisible(true);
                    apellido3.setVisible(true);
                    telefono3.setVisible(true);
                    piscina3.setVisible(true);
                    btnUpdate3.setVisible(true);
                    btnDownload3.setVisible(true);
                    btnEliminar3.setVisible(true);
                    break;
                case 3:
                    try {
                        idFliar4.setValue(dsd.getNucleofamiliar().getId() + "");
                    } catch (Exception e) {
                    } finally {
                    }
                    try {
                        obs4.setValue(dsd.getNucleofamiliar().getDoc1());
                    } catch (Exception e) {
                    } finally {
                    }
                    try {
                        cbParentezco4.setValue(dsd.getNucleofamiliar().getParentesco());
                    } catch (Exception e) {
                    } finally {
                    }
                    try {
                        nombre4.setValue(dsd.getNucleofamiliar().getNombre());
                    } catch (Exception e) {
                    } finally {
                    }
                    try {
                        apellido4.setValue(dsd.getNucleofamiliar().getApellido());
                    } catch (Exception e) {
                    } finally {
                    }
                    try {
                        telefono4.setValue(dsd.getNucleofamiliar().getTelefono());
                    } catch (Exception e) {
                    } finally {
                    }
                    try {
                        ci4.setValue(dsd.getNucleofamiliar().getCedula());
                    } catch (Exception e) {
                    } finally {
                    }
                    try {
                        if (dsd.getUsopiscina()) {
                            piscina4.setValue("SI");
                        } else {
                            piscina4.setValue("NO");
                        }
                        
                    } catch (Exception e) {
                    } finally {
                    }
                    cbParentezco4.setVisible(true);
                    ci4.setVisible(true);
                    nombre4.setVisible(true);
                    apellido4.setVisible(true);
                    telefono4.setVisible(true);
                    piscina4.setVisible(true);
                    btnUpdate4.setVisible(true);
                    btnDownload4.setVisible(true);
                    btnEliminar4.setVisible(true);
                    break;
                case 4:
                    try {
                        idFliar5.setValue(dsd.getNucleofamiliar().getId() + "");
                    } catch (Exception e) {
                    } finally {
                    }
                    try {
                        obs5.setValue(dsd.getNucleofamiliar().getDoc1());
                    } catch (Exception e) {
                    } finally {
                    }
                    try {
                        cbParentezco5.setValue(dsd.getNucleofamiliar().getParentesco());
                    } catch (Exception e) {
                    } finally {
                    }
                    try {
                        nombre5.setValue(dsd.getNucleofamiliar().getNombre());
                    } catch (Exception e) {
                    } finally {
                    }
                    try {
                        apellido5.setValue(dsd.getNucleofamiliar().getApellido());
                    } catch (Exception e) {
                    } finally {
                    }
                    try {
                        telefono5.setValue(dsd.getNucleofamiliar().getTelefono());
                    } catch (Exception e) {
                    } finally {
                    }
                    try {
                        ci5.setValue(dsd.getNucleofamiliar().getCedula());
                    } catch (Exception e) {
                    } finally {
                    }
                    try {
                        if (dsd.getUsopiscina()) {
                            piscina5.setValue("SI");
                        } else {
                            piscina5.setValue("NO");
                        }
                        
                    } catch (Exception e) {
                    } finally {
                    }
                    cbParentezco5.setVisible(true);
                    ci5.setVisible(true);
                    nombre5.setVisible(true);
                    apellido5.setVisible(true);
                    telefono5.setVisible(true);
                    piscina5.setVisible(true);
                    btnUpdate5.setVisible(true);
                    btnDownload5.setVisible(true);
                    btnEliminar5.setVisible(true);
                    break;
                case 5:
                    try {
                        idFliar6.setValue(dsd.getNucleofamiliar().getId() + "");
                    } catch (Exception e) {
                    } finally {
                    }
                    try {
                        obs6.setValue(dsd.getNucleofamiliar().getDoc1());
                    } catch (Exception e) {
                    } finally {
                    }
                    try {
                        cbParentezco6.setValue(dsd.getNucleofamiliar().getParentesco());
                    } catch (Exception e) {
                    } finally {
                    }
                    try {
                        nombre6.setValue(dsd.getNucleofamiliar().getNombre());
                    } catch (Exception e) {
                    } finally {
                    }
                    try {
                        apellido6.setValue(dsd.getNucleofamiliar().getApellido());
                    } catch (Exception e) {
                    } finally {
                    }
                    try {
                        telefono6.setValue(dsd.getNucleofamiliar().getTelefono());
                    } catch (Exception e) {
                    } finally {
                    }
                    try {
                        ci6.setValue(dsd.getNucleofamiliar().getCedula());
                    } catch (Exception e) {
                    } finally {
                    }
                    try {
                        if (dsd.getUsopiscina()) {
                            piscina6.setValue("SI");
                        } else {
                            piscina6.setValue("NO");
                        }
                        
                    } catch (Exception e) {
                    } finally {
                    }
                    cbParentezco6.setVisible(true);
                    ci6.setVisible(true);
                    nombre6.setVisible(true);
                    apellido6.setVisible(true);
                    telefono6.setVisible(true);
                    piscina6.setVisible(true);
                    btnUpdate6.setVisible(true);
                    btnDownload6.setVisible(true);
                    btnEliminar6.setVisible(true);
                    break;
                case 6:
                    try {
                        idFliar7.setValue(dsd.getNucleofamiliar().getId() + "");
                    } catch (Exception e) {
                    } finally {
                    }
                    try {
                        obs7.setValue(dsd.getNucleofamiliar().getDoc1());
                    } catch (Exception e) {
                    } finally {
                    }
                    try {
                        cbParentezco7.setValue(dsd.getNucleofamiliar().getParentesco());
                    } catch (Exception e) {
                    } finally {
                    }
                    try {
                        nombre7.setValue(dsd.getNucleofamiliar().getNombre());
                    } catch (Exception e) {
                    } finally {
                    }
                    try {
                        apellido7.setValue(dsd.getNucleofamiliar().getApellido());
                    } catch (Exception e) {
                    } finally {
                    }
                    try {
                        telefono7.setValue(dsd.getNucleofamiliar().getTelefono());
                    } catch (Exception e) {
                    } finally {
                    }
                    try {
                        ci7.setValue(dsd.getNucleofamiliar().getCedula());
                    } catch (Exception e) {
                    } finally {
                    }
                    try {
                        if (dsd.getUsopiscina()) {
                            piscina7.setValue("SI");
                        } else {
                            piscina7.setValue("NO");
                        }
                        
                    } catch (Exception e) {
                    } finally {
                    }
                    cbParentezco7.setVisible(true);
                    ci7.setVisible(true);
                    nombre7.setVisible(true);
                    apellido7.setVisible(true);
                    telefono7.setVisible(true);
                    piscina7.setVisible(true);
                    btnUpdate7.setVisible(true);
                    btnDownload7.setVisible(true);
                    btnEliminar7.setVisible(true);
                    break;
                default:
                    break;
            }
            numFliar++;
        }
        try {
            FileResource resource = new FileResource(new File("C:\\uploads\\mutual-sedesocial\\" + this.solicitud.getSocio().getCedula() + "\\1_" + this.solicitud.getSocio().getCedula() + ".jpeg"));
            photo1 = new Image("", resource);
            photo1.setWidth("250px");
            photo1.setHeight("200px");
        } catch (Exception e) {
            String basepath = VaadinService.getCurrent()
                    .getBaseDirectory().getAbsolutePath();
            
            FileResource resource = new FileResource(new File(basepath
                    + "/WEB-INF/images/sin-imagen.png"));
            photo1 = new Image("", resource);
            photo1.setWidth("250px");
            photo1.setHeight("200px");
        } finally {
        }
        try {
            FileResource resource2 = new FileResource(new File("C:\\uploads\\mutual-sedesocial\\" + this.solicitud.getSocio().getCedula() + "\\2_" + this.solicitud.getSocio().getCedula() + ".jpeg"));
            photo2 = new Image("", resource2);
            photo2.setWidth("250px");
            photo2.setHeight("200px");
        } catch (Exception e) {
            String basepath = VaadinService.getCurrent()
                    .getBaseDirectory().getAbsolutePath();
            
            FileResource resource = new FileResource(new File(basepath
                    + "/WEB-INF/images/sin-imagen.png"));
            photo2 = new Image("", resource);
            photo2.setWidth("250px");
            photo2.setHeight("200px");
        } finally {
        }
//        try {
//            FileResource resource3 = new FileResource(new File("C:\\uploads\\solicitudAyuda\\" + solicitud.getSocio().getCedula() + "\\3_" + solicitud.getId() + ".jpeg"));
//            photo3 = new Image("", resource3);
//            photo3.setWidth("250px");
//            photo3.setHeight("200px");
//        } catch (Exception e) {
//            String basepath = VaadinService.getCurrent()
//                    .getBaseDirectory().getAbsolutePath();
//
//            FileResource resource = new FileResource(new File(basepath
//                    + "/WEB-INF/images/sin-imagen.png"));
//            photo3 = new Image("", resource);
//            photo3.setWidth("250px");
//            photo3.setHeight("200px");
//        } finally {
//        }
//        try {
//            FileResource resource4 = new FileResource(new File("C:\\uploads\\solicitudAyuda\\" + solicitud.getSocio().getCedula() + "\\4_" + solicitud.getId() + ".jpeg"));
//            photo4 = new Image("", resource4);
//            photo4.setWidth("250px");
//            photo4.setHeight("200px");
//        } catch (Exception e) {
//            String basepath = VaadinService.getCurrent()
//                    .getBaseDirectory().getAbsolutePath();
//
//            FileResource resource = new FileResource(new File(basepath
//                    + "/WEB-INF/images/sin-imagen.png"));
//            photo4 = new Image("", resource);
//            photo4.setWidth("250px");
//            photo4.setHeight("200px");
//        } finally {
//        }

        mainLayout.setVisible(true);
    }
    
    private boolean validate() {
        boolean savedEnabled = true;
//        try {
//
//            if (txtCedulaSocio == null || txtCedulaSocio.isEmpty()) {
        savedEnabled = false;
//            }
//            if (txtNombreSocio == null || txtNombreSocio.isEmpty()) {
//                savedEnabled = false;
//            }
//            if (ciudad == null || ciudad.getValue() == null) {
//                savedEnabled = false;
//            }
//
//        } catch (Exception e) {
//        } finally {
//        }
//        if (encargado == null || encargado.getValue() == null) {
//            savedEnabled = false;
//        }
//        guardar.setEnabled(savedEnabled);
        return savedEnabled;
    }
    
    private Button buildConfirmButton(String url, Button button) {
        button.addStyleName(ValoTheme.BUTTON_SMALL + " " + MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_PRIMARY);
        descargarArchivo(new File(Constants.UPLOAD_DIR + url), button);
        return button;
    }
    
    private void descargarArchivo(File file, Button button) {
        myResource = createResource(file);
        FileDownloader fileDownloader = new FileDownloader(myResource);
        fileDownloader.extend(button);
    }
    
    private StreamResource createResource(File file) {
        return new StreamResource(new StreamResource.StreamSource() {
            @Override
            public InputStream getStream() {
                try {
                    return new FileInputStream(file);
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        }, file.getName());
    }
    
    private void cargarGrilla() {
        SimpleDateFormat formatFecs = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat formatHora = new SimpleDateFormat("HH:mm");
        
    }
    
    public static boolean isWeekendSaturday(LocalDate date) {
        DayOfWeek dayOfWeek = DayOfWeek.of(date.get(ChronoField.DAY_OF_WEEK));
        switch (dayOfWeek) {
            case SATURDAY:
                return true;
            default:
                return false;
        }
    }
    
    private void cargarGrillaSinCompensar() {
        SimpleDateFormat formatFecs = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat formatHora = new SimpleDateFormat("HH:mm");
        boolean val = false;
        
    }
    
    private boolean validateForm() {
        boolean savedEnabled = true;
        
        return savedEnabled;
    }
    
    private boolean validateFormSinCompensar() {
        boolean savedEnabled = true;
        return savedEnabled;
    }
    
    public void setSaveListener(Consumer<Agendamiento> saveListener) {
        this.saveListener = saveListener;
    }
    
    public void setDeleteListener(Consumer<Agendamiento> deleteListener) {
        this.deleteListener = deleteListener;
    }
    
    public void setCancelListener(Consumer<Agendamiento> cancelListener) {
        this.cancelListener = cancelListener;
    }
    
    public boolean isEnviarCorreos() {
        return enviarCorreos;
    }
    
    public void nuevoRegistro() {
        solicitud = new Agendamiento();
        solicitud.setFecha(new Date());
    }
    
    private void guardarDetalleProduccion(Agendamiento solicitud) {
    }
    
    private void editarAgendamientoProduccion(Agendamiento solicitud) {
    }
    
    public static boolean isWeekendSunday(LocalDate date) {
        DayOfWeek dayOfWeek = DayOfWeek.of(date.get(ChronoField.DAY_OF_WEEK));
        switch (dayOfWeek) {
            case SUNDAY:
                return true;
            default:
                return false;
        }
    }
    
    public static long calcWeekDays(final Date start, final Date end) {
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        Date date1 = start;
        Date date2 = end;
        Calendar cal1 = Calendar.getInstance();
        Calendar cal2 = Calendar.getInstance();
        cal1.setTime(date1);
        cal2.setTime(date2);
        
        int numberOfDays = 0;
        while (cal1.before(cal2)) {
//            if ((Calendar.SATURDAY != cal1.get(Calendar.DAY_OF_WEEK)) && (Calendar.SUNDAY != cal1.get(Calendar.DAY_OF_WEEK))) {
            if ((Calendar.SUNDAY != cal1.get(Calendar.DAY_OF_WEEK))) {
                numberOfDays++;
                cal1.add(Calendar.DATE, 1);
            } else {
                cal1.add(Calendar.DATE, 1);
            }
        }
        return numberOfDays;
    }
    
    private void updateList(String value) {
        try {
            if (value.equalsIgnoreCase("")) {
                txtNombreSocio.setValue("");
                txtApellido.setValue("");
                txtTelefono.setValue("");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    private void save() {
        if (validate()) {
            try {
                ConfirmButton confirmMessage = new ConfirmButton("");
                confirmMessage.openInModalPopup("Mensaje de confirmación", "¿Desea enviar un mensaje al socio sobre su Aprobación o Rechazo?", "35%");
                confirmMessage.getOkButton().addClickListener(e -> {
//                        try {
//                            if (estado.getValue().equalsIgnoreCase("APROBADO")) {
//                                enviarSMS(solicitud.getSocio(), solicitud.getTelefono(), solicitud.getId(), solicitud.getUsuario().getIdfuncionario().getId());
//                            } else {
//                                enviarSMSRechazo(solicitud.getSocio(), solicitud.getTelefono(), solicitud.getId(), solicitud.getUsuario().getIdfuncionario().getId());
//                            }
//
//                        } catch (ClassNotFoundException ex) {
//                            System.out.println(ex.fillInStackTrace());
//                            System.out.println(ex.getLocalizedMessage());
//                        }
                    saveListener.accept(solicitud);
                    Notification.show("Mensaje del Sistema", "Datos registrados correctamente", Notification.Type.HUMANIZED_MESSAGE);
                    confirmMessage.closePopup();
                    setVisible(false);
                });
                confirmMessage.getCancelButton().addClickListener(e -> {
                    confirmMessage.closePopup();
                    
                    saveListener.accept(solicitud);
                    Notification.show("Mensaje del Sistema", "Datos registrados correctamente, No se envió mensaje de confirmación al socio", Notification.Type.HUMANIZED_MESSAGE);
                    setVisible(false);
                });
            } catch (Exception e) {
                Notification.show("Todos los campos son obligatorios.",
                        Notification.Type.ERROR_MESSAGE);
            } finally {
            }
        } else {
            Notification.show("Todos los campos son obligatorios.",
                    Notification.Type.ERROR_MESSAGE);
        }
    }
    
    private void guardarDetalle(Agendamiento solicitud) {
        if (editar == 0) {
            
        } else {
            
        }
    }
    
    private void enviarSMS(Socio socio, String telefono, Long id, Long idFuncionario) throws ClassNotFoundException {
        DBConexion dBConnection = new DBConexion();
        String res = "";
        try {
            Statement statement = dBConnection.getConnection().createStatement();
            String sql = "SELECT sms.programar_sms('" + telefono + "',"
                    + "'Sr/a " + socio.getNombre() + " " + socio.getApellido() + " su solicitud ha sido " + estado.getValue() + " con exito su Nro. de Referencia es el " + id + ". Mutual Nac. del MSP Y BS'" + "," + socio.getId() + "," + idFuncionario + ");";
            System.out.println(sql);
            ResultSet resultSet = statement.executeQuery(sql);
            while (resultSet.next()) {
                res = resultSet.getString(1);
                System.out.println("programar_sms: " + res);
            }
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        } catch (Exception ex) {
            Logger.getLogger(AgendamientoForm.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private void enviarSMSRechazo(Socio socio, String telefono, Long id, Long idFuncionario) throws ClassNotFoundException {
        DBConexion dBConnection = new DBConexion();
        String res = "";
        try {
            Statement statement = dBConnection.getConnection().createStatement();
            String sql = "SELECT sms.programar_sms('" + telefono + "',"
                    + "'Estimado/a " + socio.getNombre() + " " + socio.getApellido() + " lamentamos informale que su solicitud ha sido rechazada. Para más información llamar al (021) 233724/5. Gracias, Mutual Nac. del MSP Y BS'" + "," + socio.getId() + "," + idFuncionario + ");";
            System.out.println(sql);
            ResultSet resultSet = statement.executeQuery(sql);
            while (resultSet.next()) {
                res = resultSet.getString(1);
                System.out.println("programar_sms: " + res);
            }
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        } catch (Exception ex) {
            Logger.getLogger(AgendamientoForm.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private void cargarParentezco(Agendamiento solicitud) {
        for (DatosFamiliaresAyuda dfaHere : datoFliarAyudaDao.getListDatosFamiliaresAyudaBySolicitud(solicitud.getId() + "")) {
            datoFliarAyudaDao.borrar(dfaHere);
        }
        try {
            DatosFamiliaresAyuda dfa = new DatosFamiliaresAyuda();
            dfa.setParentezco(cbParentezco1.getValue());
            dfa.setEdad(Integer.parseInt(nombre1.getValue()));
            dfa.setNroDocumento(ci1.getValue());
//            dfa.setAgendamiento(solicitud);

            datoFliarAyudaDao.guardar(dfa);
        } catch (Exception e) {
        } finally {
        }
        try {
            DatosFamiliaresAyuda dfa = new DatosFamiliaresAyuda();
            dfa.setParentezco(cbParentezco2.getValue());
            dfa.setEdad(Integer.parseInt(nombre2.getValue()));
            dfa.setNroDocumento(ci2.getValue());
//            dfa.setAgendamiento(solicitud);

            datoFliarAyudaDao.guardar(dfa);
        } catch (Exception e) {
        } finally {
        }
        try {
            DatosFamiliaresAyuda dfa = new DatosFamiliaresAyuda();
            dfa.setParentezco(cbParentezco3.getValue());
            dfa.setEdad(Integer.parseInt(nombre3.getValue()));
            dfa.setNroDocumento(ci3.getValue());
//            dfa.setAgendamiento(solicitud);

            datoFliarAyudaDao.guardar(dfa);
        } catch (Exception e) {
        } finally {
        }
        
        try {
            DatosFamiliaresAyuda dfa = new DatosFamiliaresAyuda();
            dfa.setParentezco(cbParentezco4.getValue());
            dfa.setEdad(Integer.parseInt(nombre4.getValue()));
            dfa.setNroDocumento(ci4.getValue());
//            dfa.setAgendamiento(solicitud);

            datoFliarAyudaDao.guardar(dfa);
        } catch (Exception e) {
        } finally {
        }
        try {
            DatosFamiliaresAyuda dfa = new DatosFamiliaresAyuda();
            dfa.setParentezco(cbParentezco5.getValue());
            dfa.setEdad(Integer.parseInt(nombre5.getValue()));
            dfa.setNroDocumento(ci5.getValue());
//            dfa.setAgendamiento(solicitud);

            datoFliarAyudaDao.guardar(dfa);
        } catch (Exception e) {
        } finally {
        }
        try {
            DatosFamiliaresAyuda dfa = new DatosFamiliaresAyuda();
            dfa.setParentezco(cbParentezco6.getValue());
            dfa.setEdad(Integer.parseInt(nombre6.getValue()));
            dfa.setNroDocumento(ci6.getValue());
//            dfa.setAgendamiento(solicitud);

            datoFliarAyudaDao.guardar(dfa);
        } catch (Exception e) {
        } finally {
        }
        try {
            DatosFamiliaresAyuda dfa = new DatosFamiliaresAyuda();
            dfa.setParentezco(cbParentezco7.getValue());
            dfa.setEdad(Integer.parseInt(nombre7.getValue()));
            dfa.setNroDocumento(ci7.getValue());
//            dfa.setAgendamiento(solicitud);

            datoFliarAyudaDao.guardar(dfa);
        } catch (Exception e) {
        } finally {
        }
    }

//    private void cargarHistorialEstados(Agendamiento solicitud, String derivacion, String atencion, String necesidad) {
////        estadoAgendamientoDao
//        if (solicitud.getDerivadoAlDpto().toUpperCase().equalsIgnoreCase(derivacion.toUpperCase()) && solicitud.getAtendidopor().toUpperCase().equalsIgnoreCase(atencion.toUpperCase())) {
//        } else {
//            EstadoAgendamiento esa = new EstadoAgendamiento();
//            esa.setFecha(new Date());
//            esa.setUsuario(UserHolder.get());
//            esa.setAgendamiento(solicitud);
//            esa.setEstado(solicitud.getEstado());
//            esa.setDptodesde(derivacion);
//            esa.setDptohasta(solicitud.getDerivadoAlDpto());
//            esa.setObservacion(solicitud.getObsDerivacion());
//            esa.setNecesidad(necesidad);
////            estadoAgendamientoDao.getListEstadoAgendamiento(url);
//            estadoAgendamientoDao.guardar(esa);
//        }
//
//    }
    class ImageReceiver implements Upload.Receiver, Upload.SucceededListener {
        
        private static final long serialVersionUID = -1276759102490466761L;
        
        public OutputStream receiveUpload(String filename,
                String mimeType) {
            
            try {
                // Open the file for writing.
                file = new File(Constants.UPLOAD_DIR + "/mutual-web/" + filename);
                url = Constants.PUBLIC_SERVER_URL + "/mutual-web/" + filename;
                fileName = filename;
                ubicacion = Constants.PUBLIC_SERVER_URL + "/mutual-web/";
                fos = new FileOutputStream(file);
            } catch (final java.io.FileNotFoundException e) {
                new Notification("Could not open file<br/>",
                        e.getMessage(),
                        Notification.Type.ERROR_MESSAGE)
                        .show(Page.getCurrent());
                return null;
            } catch (IOException ex) {
                Logger.getLogger(AgendamientoForm.class.getName()).log(Level.SEVERE, null, ex);
            }
            return fos; // Return the output stream to write to
        }
        
        public void uploadSucceeded(Upload.SucceededEvent event) {
            // Show the uploaded file in the image viewer
            image.setVisible(true);
            image.setSource(new FileResource(file));
            labelUrl.setValue(file.getName());
        }
    };
    
}
