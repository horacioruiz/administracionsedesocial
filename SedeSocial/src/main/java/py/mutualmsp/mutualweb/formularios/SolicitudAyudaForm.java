package py.mutualmsp.mutualweb.formularios;

import com.github.appreciated.material.MaterialTheme;
import com.vaadin.event.ShortcutAction;
import com.vaadin.event.ShortcutListener;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.server.FileResource;
import com.vaadin.server.Page;
import com.vaadin.server.VaadinService;
import com.vaadin.ui.*;
import com.vaadin.ui.Button;
import com.vaadin.ui.TabSheet.SelectedTabChangeEvent;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.Window;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.temporal.ChronoField;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.function.Consumer;
import java.util.logging.Level;
import java.util.logging.Logger;
import py.mutualmsp.mutualweb.dao.CargoDao;
import py.mutualmsp.mutualweb.dao.CiudadDao;
import py.mutualmsp.mutualweb.dao.DatoFamiliarAyudaDao;
import py.mutualmsp.mutualweb.dao.DatoSocioDemograficoDao;
import py.mutualmsp.mutualweb.dao.DepartamentoDao;
import py.mutualmsp.mutualweb.dao.EstadoSolicitudAyudaDao;
import py.mutualmsp.mutualweb.dao.FuncionarioDao;
import py.mutualmsp.mutualweb.dao.InstitucionDao;
import py.mutualmsp.mutualweb.dao.MotivoSolicitudAyudaDao;
import py.mutualmsp.mutualweb.dao.MotivosDao;
import py.mutualmsp.mutualweb.dao.NecesidadAyudaDao;
import py.mutualmsp.mutualweb.dao.SocioDao;
import py.mutualmsp.mutualweb.dao.SolicitudAyudaDao;
import py.mutualmsp.mutualweb.dao.UsuarioDao;
import py.mutualmsp.mutualweb.entities.Ciudad;
import py.mutualmsp.mutualweb.entities.DatosFamiliaresAyuda;
import py.mutualmsp.mutualweb.entities.DatosSocioDemograficos;
import py.mutualmsp.mutualweb.entities.Departamento;
import py.mutualmsp.mutualweb.entities.EstadoSolicitudAyuda;
import py.mutualmsp.mutualweb.entities.Institucion;
import py.mutualmsp.mutualweb.entities.MotivoSolicitudAyuda;
import py.mutualmsp.mutualweb.entities.NecesidadAyuda;
import py.mutualmsp.mutualweb.entities.Socio;
import py.mutualmsp.mutualweb.entities.SolicitudAyuda;
import py.mutualmsp.mutualweb.util.ConfirmButton;
import py.mutualmsp.mutualweb.util.ConfirmData;
import py.mutualmsp.mutualweb.util.Constants;
import py.mutualmsp.mutualweb.util.DBConexion;
import py.mutualmsp.mutualweb.util.ResourceLocator;
import py.mutualmsp.mutualweb.util.UserHolder;

/**
 * Created by Alfre on 28/6/2016.
 */
public class SolicitudAyudaForm extends Window {

    private TextField txtTipoViviendaSocio = new TextField("Tipo vivienda");
    private TextField txtCondicionVivienda = new TextField("Condición vivienda");
    private TextField txtCantPersonasMayores = new TextField("Cant personas mayores a 60 años");
    private TextField txtCantPersonasMenores = new TextField("Cant personas menores a 18 años");
    private TextField txtCantPersonasTrabajando = new TextField("Cant personas trabajando");
    private TextField txtIngresoAproxHogar = new TextField("Ingreso aprox hogar");
    private TextField txtSeguroMedico = new TextField("Seguro médico");
//    private TextField txtPersonaEnferma = new TextField("Persona con enfermedad");
    ComboBox<String> txtPersonaEnferma = new ComboBox<>("Persona con enfermedad");
    private TextArea txtDescripcionEnfermedad = new TextArea("Descripción enfermedad");
    List<EstadoSolicitudAyuda> listEstadiSolicitud = new ArrayList<>();

    ComboBox<String> departametos = new ComboBox<>("Derivar al Dpto.");
    TextArea observacionDerivarPor = new TextArea("Motivo por el cual se deriva a otro Dpto. (Interno)");
    private TextField txtCedulaSocio = new TextField("Ingrese CI socio (*)");
    private TextField txtNombreSocio = new TextField("Nombres");
    private TextField txtApellido = new TextField("Apellidos");
    private TextField txtTelefono = new TextField("Teléfono");

    private TextField txtParentezco1 = new TextField();
    private TextField edad1 = new TextField();
    private TextField ci1 = new TextField();

    private TextField txtParentezco2 = new TextField();
    private TextField edad2 = new TextField();
    private TextField ci2 = new TextField();

    private TextField txtParentezco3 = new TextField();
    private TextField edad3 = new TextField();
    private TextField ci3 = new TextField();

    private TextField txtParentezco4 = new TextField();
    private TextField edad4 = new TextField();
    private TextField ci4 = new TextField();

    private TextField txtParentezco5 = new TextField();
    private TextField edad5 = new TextField();
    private TextField ci5 = new TextField();

    private TextField txtParentezco6 = new TextField();
    private TextField edad6 = new TextField();
    private TextField ci6 = new TextField();

    private TextField txtAtendidoPor = new TextField("Atendido por");

    private ComboBox<Ciudad> ciudad = new ComboBox<>("Ciudad");
    private ComboBox<Departamento> dpto = new ComboBox<>("Departamento");
    private ComboBox<Institucion> institucion = new ComboBox<>("Institución");
    private TextField txtCargo = new TextField("Cargo");

    private TextField txtTipoVivienda = new TextField("Tipo Vivienda");

    private TextArea txtObservacion = new TextArea("Observación (Esto lo visualiza el Socio en la App)");

    private ComboBox<MotivoSolicitudAyuda> motivoSolicitudAyuda = new ComboBox<>("Motivo");
    private TextArea txtMotivoObs = new TextArea("Motivo Obs");
    private ComboBox<NecesidadAyuda> necesidadAyuda = new ComboBox<>("Necesidad");
    private TextArea txtNecesidadObs = new TextArea("Necesidad Obs");
    private ComboBox<String> estado = new ComboBox<>("Estado");
    private TextField txtPin = new TextField("PIN");

    private int editar = 0;

    private Button guardar = new Button("Guardar");
    private Button cancelar = new Button("Cancelar");
    private Button verDerivaciones = new Button("");

    //Grid<SolicitudAyudaProduccionDetalle> gridLicenciaCompensar = new Grid<>(SolicitudAyudaProduccionDetalle.class);
    final FormLayout form = new FormLayout();
    final HorizontalLayout mainLayout = new HorizontalLayout();

    private Button btnAgregar = new Button("Agregar");

//    private TextField txtOtroMotivo = new TextField("Otro motivo");
    //private TextArea txtOtroMotivo = new TextArea("Observación");
    ImageReceiver receiver = new ImageReceiver();
    Upload upload = upload = new Upload("Subir archivo", receiver);
    final Image image = new Image("Imagen");
    Label labelUrl = new Label();
    String fileName = "";
    String ubicacion = "";
    String filename;
    byte[] content;
    public File file;
    String url = "";

    private Consumer<SolicitudAyuda> saveListener;
    private Consumer<SolicitudAyuda> deleteListener;
    private Consumer<SolicitudAyuda> cancelListener;

    HashMap<Long, String> mapeo = new HashMap<>();
//    SolicitudAyudaProduccionDetalle spd = new SolicitudAyudaProduccionDetalle();
    int numRowSelected = 0;

    DatoSocioDemograficoDao datoSocioDemograficoDao = ResourceLocator.locate(DatoSocioDemograficoDao.class);
    DatoFamiliarAyudaDao datoFliarAyudaDao = ResourceLocator.locate(DatoFamiliarAyudaDao.class);
    SocioDao socioDao = ResourceLocator.locate(SocioDao.class);
    CiudadDao ciudadDao = ResourceLocator.locate(CiudadDao.class);
    EstadoSolicitudAyudaDao estadoSolicitudAyudaDao = ResourceLocator.locate(EstadoSolicitudAyudaDao.class);
    DepartamentoDao departamentoDao = ResourceLocator.locate(DepartamentoDao.class);
    InstitucionDao institucionDao = ResourceLocator.locate(InstitucionDao.class);
    MotivoSolicitudAyudaDao motivoSolicitudAyudaDao = ResourceLocator.locate(MotivoSolicitudAyudaDao.class);
    NecesidadAyudaDao necesidadAyudaDao = ResourceLocator.locate(NecesidadAyudaDao.class);
    CargoDao cargoDao = ResourceLocator.locate(CargoDao.class);
    UsuarioDao usuarioDao = ResourceLocator.locate(UsuarioDao.class);
    FuncionarioDao funcDao = ResourceLocator.locate(FuncionarioDao.class);
    //DependenciaDao dptoDao = ResourceLocator.locate(DependenciaDao.class);
    MotivosDao motivosDao = ResourceLocator.locate(MotivosDao.class);
    SolicitudAyudaDao solicitudDao = ResourceLocator.locate(SolicitudAyudaDao.class);
    // SolicitudAyudaProduccionDetalleDao solicitudProduccionDetalleDao = ResourceLocator.locate(SolicitudAyudaProduccionDetalleDao.class);

    private boolean enviarCorreos = false;
    private SolicitudAyuda solicitud;
    String destinarariosString = "";
    TabSheet tabsheet = new TabSheet();

    //List<SolicitudAyudaProduccionDetalle> listProduccionDetalle = new ArrayList<>();
    // Create upload stream
    FileOutputStream fos = null; // Stream to write to
    // Implement both receiver that saves upload in a file and
    // listener for successful upload
    String tmp = "";
    String tmp2 = "";
    String tmp3 = "";
    Image photo1 = new Image();
    Image photo2 = new Image();
    Image photo3 = new Image();
    Image photo4 = new Image();

    public SolicitudAyudaForm() {
        try {
//            VerticalLayout layout = createForm();
            VerticalLayout layout = new VerticalLayout(tabsheet);
            setContent(layout);
            setWidth("90%");
            setHeight("85%");
            setCaption("Agregar Solicitud");
            //setWindowMode(WindowMode.MAXIMIZED);
            setModal(true);
            center();
            listEstadiSolicitud = new ArrayList<>();

            SimpleDateFormat formatFecs = new SimpleDateFormat("yyyy-MM-dd");
            SimpleDateFormat formatHora = new SimpleDateFormat("HH:mm");

            verDerivaciones.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_FRIENDLY);
            verDerivaciones.setIcon(VaadinIcons.EYE);

//            sample.setValue(LocalDateTime.now());
//            sample.setLocale(Locale.US);
//            sample.setResolution(DateTimeResolution.MINUTE);
            txtNombreSocio.setEnabled(false);
            txtApellido.setEnabled(false);
            //txtCargo.setEnabled(false);
            //txtTelefono.setEnabled(false);
            mainLayout.setVisible(false);

            txtCedulaSocio.addBlurListener(e -> {
                validate();
            });

            txtParentezco1.setPlaceholder("Grado parentezo (1)");
            txtParentezco2.setPlaceholder("Grado parentezo (2)");
            txtParentezco3.setPlaceholder("Grado parentezo (3)");
            txtParentezco4.setPlaceholder("Grado parentezo (4)");
            txtParentezco5.setPlaceholder("Grado parentezo (5)");
            txtParentezco6.setPlaceholder("Grado parentezo (6)");

            edad1.setPlaceholder("Edad");
            edad2.setPlaceholder("Edad");
            edad3.setPlaceholder("Edad");
            edad4.setPlaceholder("Edad");
            edad5.setPlaceholder("Edad");
            edad6.setPlaceholder("Edad");

            ci1.setPlaceholder("Nro. Documento");
            ci2.setPlaceholder("Nro. Documento");
            ci3.setPlaceholder("Nro. Documento");
            ci4.setPlaceholder("Nro. Documento");
            ci5.setPlaceholder("Nro. Documento");
            ci6.setPlaceholder("Nro. Documento");

            ciudad.setItems(ciudadDao.listaCiudad());
            ciudad.setItemCaptionGenerator(Ciudad::getDescripcion);

            dpto.setItems(departamentoDao.listaDepartamento());
            dpto.setItemCaptionGenerator(Departamento::getDescripcion);

            institucion.setItems(institucionDao.listaInstitucion());
            institucion.setItemCaptionGenerator(Institucion::getDescripcion);

            motivoSolicitudAyuda.setItems(motivoSolicitudAyudaDao.listaMotivoSolicitudAyuda());
            motivoSolicitudAyuda.setItemCaptionGenerator(MotivoSolicitudAyuda::getDescripcion);

            necesidadAyuda.setItems(necesidadAyudaDao.listaNecesidadAyuda());
            necesidadAyuda.setItemCaptionGenerator(NecesidadAyuda::getDescripcion);

            List<String> listEstado = new ArrayList<>();
            listEstado.add("EN PROCESO DE ANALISIS");
            listEstado.add("APROBADO");
            listEstado.add("RECHAZADO");
            listEstado.add("ANULADO");
            estado.setItems(listEstado);

            List<String> listEfermo = new ArrayList<>();
            listEfermo.add("SI");
            listEfermo.add("NO");
            txtPersonaEnferma.setItems(listEfermo);
            //cargarPorFormulario(ciudadDao.listarPorTipoCodigo("licencias").get(0));

//            rrhh.setItems(socioDao.listaFuncionario());
//            rrhh.setItemCaptionGenerator(Funcionario::getNombreCompleto);
            txtCedulaSocio.focus();

            txtCedulaSocio.addValueChangeListener(e -> updateList(e.getValue()));

            txtCedulaSocio.addShortcutListener(new ShortcutListener("Shortcut", ShortcutAction.KeyCode.ENTER, null) {
                @Override
                public void handleAction(Object sender, Object target) {
                    // Do nice stuff
                    if (txtCedulaSocio.getValue() != null && !txtCedulaSocio.getValue().equals("")) {
                        try {
                            calcularCantDiaFuncionario();
                        } catch (Exception e) {
                        } finally {
                        }

//                    grid.setItems(controller.getFuncionario(filter.getValue().toUpperCase()));
                    }
                }

                private void calcularCantDiaFuncionario() {
                    Socio func = socioDao.listarSocioPorCI(txtCedulaSocio.getValue());
                    txtApellido.setValue(func.getApellido().toUpperCase());
                    txtNombreSocio.setValue(func.getNombre().toUpperCase());
                }
            });

            addCloseListener(closeEvent -> {
                close();
            });

            guardar.addClickListener(cl -> {
                try {
                    ConfirmButton confirmMessage = new ConfirmButton("");
                    confirmMessage.openInModalPopup("Mensaje de confirmación", "¿Desea confirmar los cambios?", "25%");
                    confirmMessage.getOkButton().addClickListener(e -> {
                        save();
                        confirmMessage.closePopup();
                    });
                    confirmMessage.getCancelButton().addClickListener(e -> {
                        confirmMessage.closePopup();
                    });

                } catch (Exception e) {
                    e.printStackTrace();
                    Notification.show(e.getMessage());
                }
            });
            verDerivaciones.addClickListener(cl -> {
                try {
                    Grid<EstadoSolicitudAyuda> grillaAyuda = new Grid<>(EstadoSolicitudAyuda.class);

                    VerticalLayout layoutTest = new VerticalLayout();
                    layoutTest.addStyleName("crud-view");
                    layoutTest.setMargin(true);
                    layoutTest.setSpacing(true);

                    grillaAyuda.setItems(listEstadiSolicitud);
                    grillaAyuda.removeAllColumns();
                    grillaAyuda.addColumn(EstadoSolicitudAyuda::getFecha).setCaption("FECHA");
                    grillaAyuda.addColumn(EstadoSolicitudAyuda::getNecesidad).setCaption("NECESIDAD");
                    grillaAyuda.addColumn(EstadoSolicitudAyuda::getDptodesde).setCaption("DPTO ORIGEN");
                    grillaAyuda.addColumn(EstadoSolicitudAyuda::getDptohasta).setCaption("DPTO DESTINO");
                    grillaAyuda.addColumn(e -> {
                        return e.getUsuario().getUsuario();
                    }).setCaption("Usuario");
                    grillaAyuda.addColumn(EstadoSolicitudAyuda::getObservacion).setCaption("OBSERVACION");
                    grillaAyuda.setSizeFull();
//                    for (EstadoSolicitudAyuda estadoSolicitudAyuda : listEstadiSolicitud) {
//                        layoutTest.addComponent(new Label(estadoSolicitudAyuda.getFecha() + " | " + estadoSolicitudAyuda.getDptodesde() + " | " + estadoSolicitudAyuda.getDptohasta()));
//                        layoutTest.addComponent(new Label(estadoSolicitudAyuda.getUsuario().getIdfuncionario().getNombreCompleto() + " | " + estadoSolicitudAyuda.getObservacion()));
//
//                    }
                    layoutTest.addComponent(grillaAyuda);

                    ConfirmData confirmMessage = new ConfirmData("");
                    confirmMessage
                            .openInModalPopup("Datos encontrados!.", layoutTest, "90%");
                    confirmMessage.getOkButton().addClickListener(e -> {
                        confirmMessage.closePopup();
                    });

                } catch (Exception e) {
                    e.printStackTrace();
                    Notification.show(e.getMessage());
                }
            });
            btnAgregar.addClickListener(cl -> {
                SimpleDateFormat sdfHM = new SimpleDateFormat("HH.mm");
                SimpleDateFormat sdf = new SimpleDateFormat("mm");
                long min = 0l;
            });
//            guardar.setEnabled(false);
            cancelar.addClickListener(clickEvent -> {
                close();
            });

// Create tab content dynamically when tab is selected
            tabsheet.addSelectedTabChangeListener(
                    new TabSheet.SelectedTabChangeListener() {
                public void selectedTabChange(SelectedTabChangeEvent event) {
                    // Find the tabsheet
                    TabSheet tabsheet = event.getTabSheet();

                    // Find the tab (here we know it's a layout)
                    Layout tab = (Layout) tabsheet.getSelectedTab();

                    // Get the tab caption from the tab object
                    String caption = tabsheet.getTab(tab).getCaption();

                    // Fill the tab content
                    tab.removeAllComponents();
                    VerticalLayout vl = new VerticalLayout();
                    vl.setWidth("100%");
//                    vl.addComponent(new Label("HOLA MUNDO ->>"+caption));
                    if (caption.equalsIgnoreCase("Datos Socio")) {
                        vl.addComponent(createForm());
                    } else if (caption.equalsIgnoreCase("Imagenes")) {
                        vl.addComponent(createHojaImagenes());
                    } else if (caption.equalsIgnoreCase("Datos Fliares")) {
                        vl.addComponent(createDatosFliares());
                    } else if (caption.equalsIgnoreCase("Socio Demográfico")) {
                        vl.addComponent(createDatosSocioDemografico());
                    } else {
                        vl.addComponent(createFormMotivoSolicitud());
                    }

                    tab.addComponent(vl);
                }

                private VerticalLayout createForm() {
                    VerticalLayout layout = new VerticalLayout();
                    layout.addStyleName("crud-view");
                    layout.setMargin(true);
                    layout.setSpacing(true);

                    HorizontalLayout horizontal = new HorizontalLayout();
                    horizontal.addComponent(txtCedulaSocio);
                    horizontal.addComponent(txtNombreSocio);
                    horizontal.addComponent(txtApellido);
                    horizontal.addComponent(txtTelefono);

                    horizontal.setWidth("100%");
                    horizontal.setSpacing(true);
                    horizontal.setStyleName("top-bar");

                    HorizontalLayout horizontalSegundo = new HorizontalLayout();
                    horizontalSegundo.addComponent(ciudad);
                    horizontalSegundo.addComponent(dpto);
                    horizontalSegundo.addComponent(institucion);
                    horizontalSegundo.addComponent(txtCargo);

                    horizontalSegundo.setWidth("100%");
                    horizontalSegundo.setSpacing(true);
                    horizontalSegundo.setStyleName("top-bar");

                    HorizontalLayout horizontalTercero = new HorizontalLayout();
                    horizontalTercero.addComponent(txtTipoVivienda);
                    horizontalTercero.addComponent(txtPin);

                    horizontalTercero.setWidth("50%");
                    horizontalTercero.setSpacing(true);
                    horizontalTercero.setStyleName("top-bar");

                    layout.addComponent(horizontal);
                    layout.addComponent(horizontalSegundo);
                    layout.addComponent(horizontalTercero);

                    HorizontalLayout horizontalButton = new HorizontalLayout();
                    cancelar.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_FRIENDLY);
                    horizontalButton.addComponent(cancelar);
                    horizontalButton.setSpacing(true);
                    layout.addComponent(horizontalButton);
                    return layout;
                }

                private VerticalLayout createDatosFliares() {
                    VerticalLayout layout = new VerticalLayout();
                    layout.addStyleName("crud-view");
                    layout.setMargin(true);
                    layout.setSpacing(true);

                    HorizontalLayout horizontal = new HorizontalLayout();
                    horizontal.addComponent(txtParentezco1);
                    horizontal.addComponent(edad1);
                    horizontal.addComponent(ci1);

                    horizontal.setWidth("100%");
                    horizontal.setSpacing(true);
                    horizontal.setStyleName("top-bar");

                    HorizontalLayout horizontalSegundo = new HorizontalLayout();
                    horizontalSegundo.addComponent(txtParentezco2);
                    horizontalSegundo.addComponent(edad2);
                    horizontalSegundo.addComponent(ci2);

                    horizontalSegundo.setWidth("100%");
                    horizontalSegundo.setSpacing(true);
                    horizontalSegundo.setStyleName("top-bar");

                    HorizontalLayout horizontalTercero = new HorizontalLayout();
                    horizontalTercero.addComponent(txtParentezco3);
                    horizontalTercero.addComponent(edad3);
                    horizontalTercero.addComponent(ci3);

                    horizontalTercero.setWidth("100%");
                    horizontalTercero.setSpacing(true);
                    horizontalTercero.setStyleName("top-bar");

                    HorizontalLayout horizontal4to = new HorizontalLayout();
                    horizontal4to.addComponent(txtParentezco4);
                    horizontal4to.addComponent(edad4);
                    horizontal4to.addComponent(ci4);

                    horizontal4to.setWidth("100%");
                    horizontal4to.setSpacing(true);
                    horizontal4to.setStyleName("top-bar");

                    HorizontalLayout horizontal5to = new HorizontalLayout();
                    horizontal5to.addComponent(txtParentezco5);
                    horizontal5to.addComponent(edad5);
                    horizontal5to.addComponent(ci5);

                    horizontal5to.setWidth("100%");
                    horizontal5to.setSpacing(true);
                    horizontal5to.setStyleName("top-bar");

                    HorizontalLayout horizontal6to = new HorizontalLayout();
                    horizontal6to.addComponent(txtParentezco6);
                    horizontal6to.addComponent(edad6);
                    horizontal6to.addComponent(ci6);

                    horizontal6to.setWidth("100%");
                    horizontal6to.setSpacing(true);
                    horizontal6to.setStyleName("top-bar");

                    layout.addComponent(horizontal);
                    layout.addComponent(horizontalSegundo);
                    layout.addComponent(horizontalTercero);
                    layout.addComponent(horizontal4to);
                    layout.addComponent(horizontal5to);
                    layout.addComponent(horizontal6to);

                    HorizontalLayout horizontalButton = new HorizontalLayout();
                    guardar.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_PRIMARY);
                    horizontalButton.addComponent(guardar);
                    cancelar.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_FRIENDLY);
                    horizontalButton.addComponent(cancelar);
                    horizontalButton.setSpacing(true);
                    layout.addComponent(horizontalButton);
                    return layout;
                }

                private VerticalLayout createFormMotivoSolicitud() {
                    guardar.setVisible(true);
                    VerticalLayout layout = new VerticalLayout();
                    layout.addStyleName("crud-view");
                    layout.setMargin(true);
                    layout.setSpacing(true);

                    HorizontalLayout horizontal = new HorizontalLayout();
                    horizontal.addComponent(motivoSolicitudAyuda);
                    horizontal.addComponent(txtMotivoObs);
                    horizontal.setComponentAlignment(motivoSolicitudAyuda, Alignment.TOP_LEFT);
                    horizontal.setComponentAlignment(txtMotivoObs, Alignment.TOP_LEFT);
                    txtMotivoObs.setWidth("80%");
                    layout.addComponent(horizontal);

                    HorizontalLayout horizontalSegundo = new HorizontalLayout();
                    horizontalSegundo.addComponent(necesidadAyuda);
                    horizontalSegundo.addComponent(txtNecesidadObs);
                    horizontalSegundo.setSpacing(true);
                    txtNecesidadObs.setWidth("80%");
//                    horizontalSegundo.setWidth("50%");
                    txtAtendidoPor.setEnabled(false);

                    HorizontalLayout horizontalTercero = new HorizontalLayout();
                    horizontalTercero.addComponent(estado);
                    horizontalTercero.addComponent(txtAtendidoPor);

                    departametos.setItems(Arrays.asList("Créditos", "Cobranzas", "Subsidios"));
                    horizontalTercero.addComponent(departametos);
                    txtAtendidoPor.setEnabled(false);

                    HorizontalLayout horizontalCuarto = new HorizontalLayout();
                    horizontalCuarto.addComponent(observacionDerivarPor);
                    horizontalCuarto.addComponent(verDerivaciones);
                    observacionDerivarPor.setWidth("100%");
                    observacionDerivarPor.setRows(2);
                    HorizontalLayout horizontalQuinto = new HorizontalLayout();
                    horizontalQuinto.addComponent(txtObservacion);
                    //HorizontalLayout h4to = new HorizontalLayout();
                    //h4to.addComponent(txtAtendidoPor);

                    layout.addComponent(horizontalSegundo);
                    //layout.addComponent(txtAtendidoPor);
                    //layout.addComponent(h4to);
                    layout.addComponent(horizontalTercero);
                    layout.addComponent(horizontalCuarto);
                    layout.addComponent(horizontalQuinto);
                    txtObservacion.setWidth("100%");
                    txtObservacion.setRows(2);

                    horizontalTercero.setWidth("100%");
                    horizontalCuarto.setWidth("100%");
                    horizontalQuinto.setWidth("100%");

                    image.setVisible(false);
                    upload.setButtonCaption("Seleccionar");
                    upload.addSucceededListener(receiver);

                    // Prevent too big downloads 0981752315
                    final long UPLOAD_LIMIT = 1000000l;
                    upload.addStartedListener(new Upload.StartedListener() {
                        private static final long serialVersionUID = 4728847902678459488L;

                        @Override
                        public void uploadStarted(Upload.StartedEvent event) {
                            if (event.getContentLength() > UPLOAD_LIMIT) {
                                Notification.show("El archivo es muy grande",
                                        Notification.Type.ERROR_MESSAGE);
                                upload.interruptUpload();
                            }
                        }
                    });

                    // Check the size also during progress
                    upload.addProgressListener(new Upload.ProgressListener() {
                        private static final long serialVersionUID = 8587352676703174995L;

                        @Override
                        public void updateProgress(long readBytes, long contentLength) {
                            if (readBytes > UPLOAD_LIMIT) {
                                Notification.show("El archivo es muy grande",
                                        Notification.Type.ERROR_MESSAGE);
                                upload.interruptUpload();
                            }
                        }
                    });
                    // Create uploads directory
                    File uploads = new File(Constants.UPLOAD_DIR_TEMP);
                    if (!uploads.exists() && !uploads.mkdir()) {
                        horizontal.addComponent(new Label("ERROR: No se pudo crear la carpeta"));
                    }

                    horizontal.setWidth("70%");
                    horizontalSegundo.setWidth("70%");
                    horizontal.setStyleName("top-bar");

                    btnAgregar.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_PRIMARY);
                    //form.addComponent(cbFuncionario);
                    form.addComponent(btnAgregar);
                    mainLayout.addComponent(form);
                    mainLayout.setWidth("100%");

                    layout.addComponent(mainLayout);

                    HorizontalLayout horizontalButton = new HorizontalLayout();
                    guardar.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_PRIMARY);
                    horizontalButton.addComponent(guardar);
                    cancelar.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_FRIENDLY);
                    horizontalButton.addComponent(cancelar);
                    horizontalButton.setSpacing(true);
                    layout.addComponent(horizontalButton);
                    return layout;
                }

                private VerticalLayout createHojaImagenes() {
                    guardar.setVisible(true);
                    VerticalLayout layout = new VerticalLayout();
                    layout.addStyleName("crud-view");
                    layout.setMargin(true);
                    layout.setSpacing(true);

                    //layout.addComponent(photo);
                    Panel sample1 = new Panel();
                    sample1.setHeight(100.0f, Unit.PERCENTAGE);

//                    final VerticalLayout contentLayout = new VerticalLayout();
//                    contentLayout.setWidth(500, Unit.PIXELS);
//                    contentLayout.setSpacing(false);
//                    contentLayout.addComponent(photo1);
                    sample1.setContent(photo1);

                    HorizontalLayout horizontal = new HorizontalLayout();
                    horizontal.addComponent(sample1);

                    Panel sample2 = new Panel();
                    sample2.setHeight(100.0f, Unit.PERCENTAGE);

//                    final VerticalLayout contentLayout2 = new VerticalLayout();
//                    contentLayout2.setWidth(500, Unit.PIXELS);
//                    contentLayout2.setSpacing(false);
//                    contentLayout2.addComponent(photo2);
                    sample2.setContent(photo2);
                    horizontal.addComponent(sample2);

                    Panel sample3 = new Panel();
                    sample3.setHeight(100.0f, Unit.PERCENTAGE);

//                    final VerticalLayout contentLayout3 = new VerticalLayout();
//                    contentLayout3.setWidth(500, Unit.PIXELS);
//                    contentLayout3.setSpacing(false);
//                    contentLayout3.addComponent(photo3);
                    sample3.setContent(photo3);
                    horizontal.addComponent(sample3);

                    Panel sample4 = new Panel();
                    sample4.setHeight(100.0f, Unit.PERCENTAGE);

//                    final VerticalLayout contentLayout3 = new VerticalLayout();
//                    contentLayout3.setWidth(500, Unit.PIXELS);
//                    contentLayout3.setSpacing(false);
//                    contentLayout3.addComponent(photo3);
                    sample4.setContent(photo4);
                    horizontal.addComponent(sample4);

                    horizontal.setWidth("100%");
                    horizontal.setSpacing(true);
                    horizontal.setStyleName("top-bar");

//                    HorizontalLayout horizontalSegundo = new HorizontalLayout();
//                    //horizontalSegundo.addComponent(txtOtroMotivo);
//                    HorizontalLayout horizontalTercero = new HorizontalLayout();
////        horizontalSegundo.addComponent(txtOtroMotivo);
//                    //txtOtroMotivo.setWidth("100%");
//
//                    horizontalSegundo.setWidth("100%");
//                    horizontalSegundo.setSpacing(true);
//                    horizontalSegundo.setStyleName("top-bar");
//                    //txtDependencia.setWidth("100%");
//
//                    horizontalTercero.setWidth("100%");
//                    horizontalTercero.setSpacing(true);
//                    horizontalTercero.setStyleName("top-bar");
                    btnAgregar.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_PRIMARY);
                    //form.addComponent(cbFuncionario);
                    form.addComponent(btnAgregar);
                    mainLayout.addComponent(form);
                    mainLayout.setWidth("100%");

                    layout.addComponent(mainLayout);

                    layout.addComponent(horizontal);

                    HorizontalLayout horizontalButton = new HorizontalLayout();
                    guardar.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_PRIMARY);
                    horizontalButton.addComponent(guardar);
                    cancelar.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_FRIENDLY);
                    horizontalButton.addComponent(cancelar);
                    horizontalButton.setSpacing(true);
                    layout.addComponent(horizontalButton);
                    return layout;
                }

                private Component createDatosSocioDemografico() {
                    VerticalLayout layout = new VerticalLayout();
                    layout.addStyleName("crud-view");
                    layout.setMargin(true);
                    layout.setSpacing(true);

                    HorizontalLayout horizontal = new HorizontalLayout();
                    horizontal.addComponent(txtTipoViviendaSocio);
                    horizontal.addComponent(txtCondicionVivienda);
                    horizontal.addComponent(txtCantPersonasMayores);
                    horizontal.addComponent(txtCantPersonasMenores);

                    horizontal.setWidth("100%");
                    horizontal.setSpacing(true);
                    horizontal.setStyleName("top-bar");

                    HorizontalLayout horizontalSegundo = new HorizontalLayout();
                    horizontalSegundo.addComponent(txtCantPersonasTrabajando);
                    horizontalSegundo.addComponent(txtIngresoAproxHogar);
                    horizontalSegundo.addComponent(txtSeguroMedico);
                    horizontalSegundo.addComponent(txtPersonaEnferma);

                    horizontalSegundo.setWidth("100%");
                    horizontalSegundo.setSpacing(true);
                    horizontalSegundo.setStyleName("top-bar");

                    HorizontalLayout horizontalTercero = new HorizontalLayout();
                    horizontalTercero.addComponent(txtDescripcionEnfermedad);
//                    horizontalTercero.addComponent(txtPin);
                    txtDescripcionEnfermedad.setWidth("50%");
                    horizontalTercero.setWidth("50%");
                    horizontalTercero.setSpacing(true);
                    horizontalTercero.setStyleName("top-bar");

                    layout.addComponent(horizontal);
                    layout.addComponent(horizontalSegundo);
                    layout.addComponent(horizontalTercero);

                    HorizontalLayout horizontalButton = new HorizontalLayout();
                    cancelar.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_FRIENDLY);
                    horizontalButton.addComponent(cancelar);
                    horizontalButton.setSpacing(true);
                    layout.addComponent(horizontalButton);
                    return layout;
                }
            });

// Have some tabs
            String[] tabs = {"Datos Socio", "Solicitud", "Imagenes", "Datos Fliares", "Socio Demográfico"};
            for (String caption : tabs) {
                tabsheet.addTab(new VerticalLayout(), caption);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void editarSolicitudProduccion(SolicitudAyuda solicitud) {
        labelUrl.setValue("");

        upload.setVisible(false);

        txtCedulaSocio.setVisible(false);
        txtNombreSocio.setVisible(false);
        txtApellido.setVisible(false);
        txtTelefono.setVisible(false);
        txtObservacion.setVisible(false);

        mainLayout.setVisible(true);

        //cbDpto.setItems(dptoDao.listarDepartamentosPadres());
        //cbDpto.setValue(dptoDao.getDependenciaByDescripcion(solicitud.getAreafunc().toLowerCase()));
        //cbCargo.setValue(dptoDao.getDependenciaByDescripcion(solicitud.getCargofunc().toLowerCase()));
        mainLayout.setVisible(true);

    }

    private void cargarDetalleProduccion(SolicitudAyuda solicitud) {
        SimpleDateFormat formatFec = new SimpleDateFormat("yyyy-MM-dd");
        String patternHora = "HH:mm";
        SimpleDateFormat simpleDateFormatHora = new SimpleDateFormat(patternHora);

    }

    public void editarRegistro(SolicitudAyuda solicitud) {
        setCaption("Editar Solicitud");
        this.solicitud = solicitud;
        editar = 1;

        //editarSolicitudProduccion(solicitud);
        /* if (solicitud.getParametro().getDescripcion().equalsIgnoreCase("SOLICITUD DE PRODUCCIÓN")) {
            editar = 1;
            editarSolicitudProduccion(solicitud);
        } else {*/
        //List<SolicitudDetalle> listSolicitudDetalle = solicitudDetalleDao.listarPorIdSolicitud(solicitud.getId());
        try {
            txtPin.setValue(solicitud.getPinLiquidacion());
        } catch (Exception e) {
            txtPin.setValue("");
        } finally {
        }
        try {
            listEstadiSolicitud = estadoSolicitudAyudaDao.getListEstadoSolicitudAyuda(solicitud.getId().toString());
            if (listEstadiSolicitud.isEmpty()) {
                verDerivaciones.setVisible(false);
            } else {
                verDerivaciones.setVisible(true);
            }
        } catch (Exception e) {
        } finally {
        }

        try {
            txtCedulaSocio.setValue(solicitud.getSocio().getCedula());
        } catch (Exception e) {
            txtCedulaSocio.setValue("");
        } finally {
        }
        try {
            txtAtendidoPor.setValue(solicitud.getAtendidopor().toUpperCase());
        } catch (Exception e) {
            txtAtendidoPor.setValue("");
        } finally {
        }
        try {
            if (solicitud.getEstado().toUpperCase().equalsIgnoreCase("PENDIENTE")) {
                estado.setValue("EN PROCESO DE ANALISIS");
            } else {
                estado.setValue(solicitud.getEstado().toUpperCase());
            }

        } catch (Exception e) {
            estado.setValue("EN PROCESO DE ANALISIS");
        } finally {
        }
        try {
            txtNombreSocio.setValue(solicitud.getSocio().getNombre());
        } catch (Exception e) {
            txtNombreSocio.setValue("");
        } finally {
        }
        try {
            txtApellido.setValue(solicitud.getSocio().getApellido());
        } catch (Exception e) {
            txtApellido.setValue("");
        } finally {
        }
        try {
            txtTelefono.setValue(solicitud.getTelefono());
        } catch (Exception e) {
            txtTelefono.setValue("");
        } finally {
        }
        try {
            ciudad.setValue(solicitud.getCiudad());
        } catch (Exception e) {
            ciudad.setValue(null);
        } finally {
        }
        try {
            dpto.setValue(solicitud.getDepartamento());
        } catch (Exception e) {
            dpto.setValue(null);
        } finally {
        }
        try {
            institucion.setValue(solicitud.getInstitucion());
        } catch (Exception e) {
            institucion.setValue(null);
        } finally {
        }
        try {
            txtCargo.setValue(solicitud.getCargo());
        } catch (Exception e) {
            txtCargo.setValue("");
        } finally {
        }
        try {
            txtTipoVivienda.setValue(solicitud.getTipoVivienda());
        } catch (Exception e) {
            txtTipoVivienda.setValue("");
        } finally {
        }

        try {
            txtObservacion.setValue(solicitud.getObservacion());
        } catch (Exception e) {
            txtObservacion.setValue("");
        } finally {
        }

        try {
            motivoSolicitudAyuda.setValue(solicitud.getMotivoSolicitudAyuda());
        } catch (Exception e) {
            motivoSolicitudAyuda.setValue(null);
        } finally {
        }
        try {
            txtMotivoObs.setValue(solicitud.getMotivoObs());
        } catch (Exception e) {
            txtMotivoObs.setValue("");
        } finally {
        }
        try {
            necesidadAyuda.setValue(solicitud.getNecesidadAyuda());
        } catch (Exception e) {
            necesidadAyuda.setValue(null);
        } finally {
        }
//        necesidadAyuda.setEnabled(true);
        try {
            txtNecesidadObs.setValue(solicitud.getNecesidadObs());
        } catch (Exception e) {
            txtNecesidadObs.setValue("");
        } finally {
        }
        try {
//            observacionDerivarPor.setValue(solicitud.getObsDerivacion());
            observacionDerivarPor.setValue("");
        } catch (Exception e) {
        } finally {
        }
        try {
            departametos.setValue(solicitud.getDerivadoAlDpto());
        } catch (Exception e) {
        } finally {
        }
//        try {
//            if (departametos.getValue().equals("")) {
//                necesidadAyuda.setEnabled(true);
//                departametos.setEnabled(true);
//                observacionDerivarPor.setEnabled(true);
//            } else {
//                necesidadAyuda.setEnabled(false);
//                departametos.setEnabled(false);
//                observacionDerivarPor.setEnabled(false);
//            }
//        } catch (Exception e) {
//        }
        try {
            if (estado.getValue().equals("ANULADO")) {
                guardar.setEnabled(false);
            } else {
                guardar.setEnabled(true);
            }
        } catch (Exception e) {
        }
        //                private TextField txtTipoViviendaSocio = new TextField("Tipo vivienda");
//    private TextField txtCondicionVivienda = new TextField("Condición vivienda");
//    private TextField txtCantPersonasMayores = new TextField("Cant personas mayores");
//    private TextField txtCantPersonasMenores = new TextField("Cant personas menores");
//    private TextField txtCantPersonasTrabajando = new TextField("Cant personas trabajando");
//    private TextField txtIngresoAproxHogar = new TextField("Ingreso aprox hogar");
//    private TextField txtSeguroMedico = new TextField("Seguro médico");
//    private TextField txtPersonaEnferma = new TextField("Persona con enfermedad");
//    private TextField txtDescripcionEnfermedad = new TextField("Descripción enfermedad");
        List<DatosSocioDemograficos> listDatos = datoSocioDemograficoDao.listarPorIdSolicitud(solicitud.getId());
        for (DatosSocioDemograficos dsd : listDatos) {
            try {
                txtTipoViviendaSocio.setValue(dsd.getTipoVivienda());
            } catch (Exception e) {
            } finally {
            }
            try {
                txtCondicionVivienda.setValue(dsd.getCondicionVivienda());
            } catch (Exception e) {
            } finally {
            }
            try {
                txtCantPersonasMayores.setValue(dsd.getCantPersonasMayores() + "");
            } catch (Exception e) {
            } finally {
            }
            try {
                txtCantPersonasMenores.setValue(dsd.getCantPersonasMenores() + "");
            } catch (Exception e) {
            } finally {
            }

            try {
                txtCantPersonasTrabajando.setValue(dsd.getCantPersonasTrabajan() + "");
            } catch (Exception e) {
            } finally {
            }
            try {
                DecimalFormatSymbols symbols = DecimalFormatSymbols.getInstance();
                symbols.setGroupingSeparator('.');
                DecimalFormat formatter = new DecimalFormat("###,###", symbols);

                txtIngresoAproxHogar.setValue(formatter.format(Math.round(dsd.getIngresoAproxHogar())));
            } catch (Exception e) {
            } finally {
            }
            try {
                txtSeguroMedico.setValue(dsd.getSeguroMedico() + "");
            } catch (Exception e) {
            } finally {
            }
            try {
                if (dsd.getPersonaConEnfermedad().equalsIgnoreCase("SI")) {
                    txtPersonaEnferma.setValue("SI");
                } else if (dsd.getPersonaConEnfermedad().equalsIgnoreCase("NO")) {
                    txtPersonaEnferma.setValue("NO");
                }
            } catch (Exception e) {
            } finally {
            }
            try {
                txtDescripcionEnfermedad.setValue(dsd.getDescripcionEnfermedad());
            } catch (Exception e) {
            } finally {
            }
        }

        List<DatosFamiliaresAyuda> listData = datoFliarAyudaDao.getListDatosFamiliaresAyudaBySolicitud(solicitud.getId() + "");
        int num = 0;
        for (DatosFamiliaresAyuda datosFamiliaresAyuda : listData) {
            num++;
            switch (num) {
                case 1:
                    try {
                        txtParentezco1.setValue(datosFamiliaresAyuda.getParentezco().toUpperCase());
                    } catch (Exception e) {
                    } finally {
                    }
                    try {
                        edad1.setValue(datosFamiliaresAyuda.getEdad() + "");
                    } catch (Exception e) {
                    } finally {
                    }
                    try {
                        ci1.setValue(datosFamiliaresAyuda.getNroDocumento());
                    } catch (Exception e) {
                    } finally {
                    }
                    break;
                case 2:
                    try {
                        txtParentezco2.setValue(datosFamiliaresAyuda.getParentezco().toUpperCase());
                    } catch (Exception e) {
                    } finally {
                    }
                    try {
                        edad2.setValue(datosFamiliaresAyuda.getEdad() + "");
                    } catch (Exception e) {
                    } finally {
                    }
                    try {
                        ci2.setValue(datosFamiliaresAyuda.getNroDocumento());
                    } catch (Exception e) {
                    } finally {
                    }
                    break;
                case 3:
                    try {
                        txtParentezco3.setValue(datosFamiliaresAyuda.getParentezco().toUpperCase());
                    } catch (Exception e) {
                    } finally {
                    }
                    try {
                        edad3.setValue(datosFamiliaresAyuda.getEdad() + "");
                    } catch (Exception e) {
                    } finally {
                    }
                    try {
                        ci3.setValue(datosFamiliaresAyuda.getNroDocumento());
                    } catch (Exception e) {
                    } finally {
                    }
                    break;
                case 4:
                    try {
                        txtParentezco4.setValue(datosFamiliaresAyuda.getParentezco().toUpperCase());
                    } catch (Exception e) {
                    } finally {
                    }
                    try {
                        edad4.setValue(datosFamiliaresAyuda.getEdad() + "");
                    } catch (Exception e) {
                    } finally {
                    }
                    try {
                        ci4.setValue(datosFamiliaresAyuda.getNroDocumento());
                    } catch (Exception e) {
                    } finally {
                    }
                    break;
                case 5:
                    try {
                        txtParentezco5.setValue(datosFamiliaresAyuda.getParentezco().toUpperCase());
                    } catch (Exception e) {
                    } finally {
                    }
                    try {
                        edad5.setValue(datosFamiliaresAyuda.getEdad() + "");
                    } catch (Exception e) {
                    } finally {
                    }
                    try {
                        ci5.setValue(datosFamiliaresAyuda.getNroDocumento());
                    } catch (Exception e) {
                    } finally {
                    }
                    break;
                case 6:
                    try {
                        txtParentezco6.setValue(datosFamiliaresAyuda.getParentezco().toUpperCase());
                    } catch (Exception e) {
                    } finally {
                    }
                    try {
                        edad6.setValue(datosFamiliaresAyuda.getEdad() + "");
                    } catch (Exception e) {
                    } finally {
                    }
                    try {
                        ci6.setValue(datosFamiliaresAyuda.getNroDocumento());
                    } catch (Exception e) {
                    } finally {
                    }
                    break;
                default:
                    break;
            }
        }
        try {
            FileResource resource = new FileResource(new File("C:\\uploads\\solicitudAyuda\\" + solicitud.getSocio().getCedula() + "\\1_" + solicitud.getId() + ".jpeg"));
            photo1 = new Image("", resource);
            photo1.setWidth("250px");
            photo1.setHeight("200px");
        } catch (Exception e) {
            String basepath = VaadinService.getCurrent()
                    .getBaseDirectory().getAbsolutePath();

            FileResource resource = new FileResource(new File(basepath
                    + "/WEB-INF/images/sin-imagen.png"));
            photo1 = new Image("", resource);
            photo1.setWidth("250px");
            photo1.setHeight("200px");
        } finally {
        }
        try {
            FileResource resource2 = new FileResource(new File("C:\\uploads\\solicitudAyuda\\" + solicitud.getSocio().getCedula() + "\\2_" + solicitud.getId() + ".jpeg"));
            photo2 = new Image("", resource2);
            photo2.setWidth("250px");
            photo2.setHeight("200px");
        } catch (Exception e) {
            String basepath = VaadinService.getCurrent()
                    .getBaseDirectory().getAbsolutePath();

            FileResource resource = new FileResource(new File(basepath
                    + "/WEB-INF/images/sin-imagen.png"));
            photo2 = new Image("", resource);
            photo2.setWidth("250px");
            photo2.setHeight("200px");
        } finally {
        }
        try {
            FileResource resource3 = new FileResource(new File("C:\\uploads\\solicitudAyuda\\" + solicitud.getSocio().getCedula() + "\\3_" + solicitud.getId() + ".jpeg"));
            photo3 = new Image("", resource3);
            photo3.setWidth("250px");
            photo3.setHeight("200px");
        } catch (Exception e) {
            String basepath = VaadinService.getCurrent()
                    .getBaseDirectory().getAbsolutePath();

            FileResource resource = new FileResource(new File(basepath
                    + "/WEB-INF/images/sin-imagen.png"));
            photo3 = new Image("", resource);
            photo3.setWidth("250px");
            photo3.setHeight("200px");
        } finally {
        }
        try {
            FileResource resource4 = new FileResource(new File("C:\\uploads\\solicitudAyuda\\" + solicitud.getSocio().getCedula() + "\\4_" + solicitud.getId() + ".jpeg"));
            photo4 = new Image("", resource4);
            photo4.setWidth("250px");
            photo4.setHeight("200px");
        } catch (Exception e) {
            String basepath = VaadinService.getCurrent()
                    .getBaseDirectory().getAbsolutePath();

            FileResource resource = new FileResource(new File(basepath
                    + "/WEB-INF/images/sin-imagen.png"));
            photo4 = new Image("", resource);
            photo4.setWidth("250px");
            photo4.setHeight("200px");
        } finally {
        }

        mainLayout.setVisible(false);
    }

    private boolean validate() {
        boolean savedEnabled = true;
        try {

            if (txtCedulaSocio == null || txtCedulaSocio.isEmpty()) {
                savedEnabled = false;
            }
            if (txtNombreSocio == null || txtNombreSocio.isEmpty()) {
                savedEnabled = false;
            }
            if (ciudad == null || ciudad.getValue() == null) {
                savedEnabled = false;
            }

        } catch (Exception e) {
        } finally {
        }
//        if (encargado == null || encargado.getValue() == null) {
//            savedEnabled = false;
//        }
//        guardar.setEnabled(savedEnabled);
        return savedEnabled;
    }

    private void updateParametro() {

    }

    private void cargarGrilla() {
        SimpleDateFormat formatFecs = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat formatHora = new SimpleDateFormat("HH:mm");

    }

    public static boolean isWeekendSaturday(LocalDate date) {
        DayOfWeek dayOfWeek = DayOfWeek.of(date.get(ChronoField.DAY_OF_WEEK));
        switch (dayOfWeek) {
            case SATURDAY:
                return true;
            default:
                return false;
        }
    }

    private void cargarGrillaSinCompensar() {
        SimpleDateFormat formatFecs = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat formatHora = new SimpleDateFormat("HH:mm");
        boolean val = false;

    }

    private boolean validateForm() {
        boolean savedEnabled = true;

        return savedEnabled;
    }

    private boolean validateFormSinCompensar() {
        boolean savedEnabled = true;
        return savedEnabled;
    }

    public void setSaveListener(Consumer<SolicitudAyuda> saveListener) {
        this.saveListener = saveListener;
    }

    public void setDeleteListener(Consumer<SolicitudAyuda> deleteListener) {
        this.deleteListener = deleteListener;
    }

    public void setCancelListener(Consumer<SolicitudAyuda> cancelListener) {
        this.cancelListener = cancelListener;
    }

    public boolean isEnviarCorreos() {
        return enviarCorreos;
    }

    public void nuevoRegistro() {
        solicitud = new SolicitudAyuda();
        solicitud.setFecha(new Date());
    }

    private void guardarDetalleProduccion(SolicitudAyuda solicitud) {
    }

    private void editarSolicitudAyudaProduccion(SolicitudAyuda solicitud) {
    }

    public static boolean isWeekendSunday(LocalDate date) {
        DayOfWeek dayOfWeek = DayOfWeek.of(date.get(ChronoField.DAY_OF_WEEK));
        switch (dayOfWeek) {
            case SUNDAY:
                return true;
            default:
                return false;
        }
    }

    public static long calcWeekDays(final Date start, final Date end) {
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        Date date1 = start;
        Date date2 = end;
        Calendar cal1 = Calendar.getInstance();
        Calendar cal2 = Calendar.getInstance();
        cal1.setTime(date1);
        cal2.setTime(date2);

        int numberOfDays = 0;
        while (cal1.before(cal2)) {
//            if ((Calendar.SATURDAY != cal1.get(Calendar.DAY_OF_WEEK)) && (Calendar.SUNDAY != cal1.get(Calendar.DAY_OF_WEEK))) {
            if ((Calendar.SUNDAY != cal1.get(Calendar.DAY_OF_WEEK))) {
                numberOfDays++;
                cal1.add(Calendar.DATE, 1);
            } else {
                cal1.add(Calendar.DATE, 1);
            }
        }
        return numberOfDays;
    }

    private void updateList(String value) {
        try {
            if (value.equalsIgnoreCase("")) {
                txtNombreSocio.setValue("");
                txtApellido.setValue("");
                txtTelefono.setValue("");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void save() {
        if (validate()) {
            try {
                if (estado.getValue().equalsIgnoreCase("EN PROCESO DE ANALISIS")) {
                    solicitud.setEstado("EN PROCESO DE ANALISIS");
                } else if (estado.getValue().equalsIgnoreCase("APROBADO")) {
                    solicitud.setEstado("APROBADO");
                } else if (estado.getValue().equalsIgnoreCase("ANULADO")) {
                    solicitud.setEstado("ANULADO");
                } else {
                    solicitud.setEstado("RECHAZADO");
                }
                String derivacion = "";
                try {
                    derivacion = solicitud.getDerivadoAlDpto();
                } catch (Exception e) {
                } finally {
                }
                String necesidad = "";
                try {
                    necesidad = solicitud.getNecesidadAyuda().getDescripcion();
                } catch (Exception e) {
                } finally {
                }
                String atencion = "";
                try {
                    atencion = solicitud.getAtendidopor();
                } catch (Exception e) {
                } finally {
                }

                try {
//                    solicitud.setUsuario();
//                    long user = usuarioDao.getIdFuncionario(UserHolder.get().getId());
                    solicitud.setAtendidopor(UserHolder.get().getIdfuncionario().getNombreCompleto());
                    solicitud.setUsuario(UserHolder.get());
                    solicitud.setDerivadoAlDpto(departametos.getValue() == null ? "" : departametos.getValue());
                    solicitud.setObsDerivacion(observacionDerivarPor.getValue() == null ? "" : observacionDerivarPor.getValue());
                    //solicitud.setAtendidopor(funcDao.gtePorId(user).getNombreCompleto());
                } catch (Exception e) {
                    System.out.println(e.getLocalizedMessage());
                    System.out.println(e.fillInStackTrace());
                } finally {
                }
                try {
                    solicitud.setSocio(socioDao.listarSocioPorCI(txtCedulaSocio.getValue()));
                } catch (Exception e) {
                    solicitud.setSocio(null);
                } finally {
                }
                try {
                    solicitud.setTelefono(txtTelefono.getValue());
                } catch (Exception e) {
                    solicitud.setTelefono("");
                } finally {
                }
                try {
                    solicitud.setCiudad(ciudad.getValue());
                } catch (Exception e) {
                    solicitud.setCiudad(null);
                } finally {
                }
                try {
                    solicitud.setDepartamento(dpto.getValue());
                } catch (Exception e) {
                    solicitud.setDepartamento(null);
                } finally {
                }
                try {
                    solicitud.setInstitucion(institucion.getValue());
                } catch (Exception e) {
                    solicitud.setInstitucion(null);
                } finally {
                }
                try {
                    solicitud.setCargo(txtCargo.getValue());
                } catch (Exception e) {
                    solicitud.setCargo("");
                } finally {
                }
                try {
                    solicitud.setTipoVivienda(txtTipoVivienda.getValue());
                } catch (Exception e) {
                    solicitud.setTipoVivienda("");
                } finally {
                }

                try {
                    solicitud.setObservacion(txtObservacion.getValue());
                } catch (Exception e) {
                    solicitud.setObservacion("");
                } finally {
                }

                try {
                    solicitud.setMotivoSolicitudAyuda(motivoSolicitudAyuda.getValue());
                } catch (Exception e) {
                    solicitud.setMotivoSolicitudAyuda(null);
                } finally {
                }
                try {
                    solicitud.setMotivoObs(txtMotivoObs.getValue());
                } catch (Exception e) {
                    solicitud.setMotivoObs("");
                } finally {
                }
                try {
                    solicitud.setNecesidadAyuda(necesidadAyuda.getValue());
                } catch (Exception e) {
                    solicitud.setNecesidadAyuda(null);
                } finally {
                }
                try {
                    solicitud.setNecesidadObs(txtNecesidadObs.getValue());
                } catch (Exception e) {
                    solicitud.setNecesidadObs("");
                } finally {
                }
                try {
                    solicitud.setObsDerivacion(observacionDerivarPor.getValue() == null ? "" : observacionDerivarPor.getValue());
                } catch (Exception e) {
                } finally {
                }
                try {
                    solicitud.setDerivadoAlDpto(departametos.getValue() == null ? "" : departametos.getValue());
                } catch (Exception e) {
                } finally {
                }
                solicitud = solicitudDao.guardarSolicitudAyuda(solicitud);
                cargarHistorialEstados(solicitud, derivacion, atencion, necesidad);
                cargarParentezco(solicitud);
                if (estado.getValue().equalsIgnoreCase("APROBADO") || estado.getValue().equalsIgnoreCase("RECHAZADO")) {
                    ConfirmButton confirmMessage = new ConfirmButton("");
                    confirmMessage.openInModalPopup("Mensaje de confirmación", "¿Desea enviar un mensaje al socio sobre su Aprobación o Rechazo?", "35%");
                    confirmMessage.getOkButton().addClickListener(e -> {
                        try {
                            if (estado.getValue().equalsIgnoreCase("APROBADO")) {
                                enviarSMS(solicitud.getSocio(), solicitud.getTelefono(), solicitud.getId(), solicitud.getUsuario().getIdfuncionario().getId());
                            } else {
                                enviarSMSRechazo(solicitud.getSocio(), solicitud.getTelefono(), solicitud.getId(), solicitud.getUsuario().getIdfuncionario().getId());
                            }

                        } catch (ClassNotFoundException ex) {
                            System.out.println(ex.fillInStackTrace());
                            System.out.println(ex.getLocalizedMessage());
                        }
                        saveListener.accept(solicitud);
                        Notification.show("Mensaje del Sistema", "Datos registrados correctamente", Notification.Type.HUMANIZED_MESSAGE);
                        confirmMessage.closePopup();
                        setVisible(false);
                    });
                    confirmMessage.getCancelButton().addClickListener(e -> {
                        confirmMessage.closePopup();

                        saveListener.accept(solicitud);
                        Notification.show("Mensaje del Sistema", "Datos registrados correctamente, No se envió mensaje de confirmación al socio", Notification.Type.HUMANIZED_MESSAGE);
                        setVisible(false);
                    });
                } else {
                    saveListener
                            .accept(solicitud);
                    Notification.show("Mensaje del Sistema", "Datos registrados correctamente", Notification.Type.HUMANIZED_MESSAGE);
                    setVisible(false);
                }
            } catch (Exception e) {
                Notification.show("Todos los campos son obligatorios.",
                        Notification.Type.ERROR_MESSAGE);
            } finally {
            }
        } else {
            Notification.show("Todos los campos son obligatorios.",
                    Notification.Type.ERROR_MESSAGE);
        }
    }

    private void guardarDetalle(SolicitudAyuda solicitud) {
        if (editar == 0) {

        } else {

        }
    }

    private void enviarSMS(Socio socio, String telefono, Long id, Long idFuncionario) throws ClassNotFoundException {
        DBConexion dBConnection = new DBConexion();
        String res = "";
        try {
            Statement statement = dBConnection.getConnection().createStatement();
            String sql = "SELECT sms.programar_sms('" + telefono + "',"
                    + "'Sr/a " + socio.getNombre() + " " + socio.getApellido() + " su solicitud ha sido " + estado.getValue() + " con exito su Nro. de Referencia es el " + id + ". Mutual Nac. del MSP Y BS'" + "," + socio.getId() + "," + idFuncionario + ");";
            System.out.println(sql);
            ResultSet resultSet = statement.executeQuery(sql);
            while (resultSet.next()) {
                res = resultSet.getString(1);
                System.out.println("programar_sms: " + res);
            }
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        } catch (Exception ex) {
            Logger.getLogger(SolicitudAyudaForm.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void enviarSMSRechazo(Socio socio, String telefono, Long id, Long idFuncionario) throws ClassNotFoundException {
        DBConexion dBConnection = new DBConexion();
        String res = "";
        try {
            Statement statement = dBConnection.getConnection().createStatement();
            String sql = "SELECT sms.programar_sms('" + telefono + "',"
                    + "'Estimado/a " + socio.getNombre() + " " + socio.getApellido() + " lamentamos informale que su solicitud ha sido rechazada. Para más información llamar al (021) 233724/5. Gracias, Mutual Nac. del MSP Y BS'" + "," + socio.getId() + "," + idFuncionario + ");";
            System.out.println(sql);
            ResultSet resultSet = statement.executeQuery(sql);
            while (resultSet.next()) {
                res = resultSet.getString(1);
                System.out.println("programar_sms: " + res);
            }
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        } catch (Exception ex) {
            Logger.getLogger(SolicitudAyudaForm.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void cargarParentezco(SolicitudAyuda solicitud) {
        for (DatosFamiliaresAyuda dfaHere : datoFliarAyudaDao.getListDatosFamiliaresAyudaBySolicitud(solicitud.getId() + "")) {
            datoFliarAyudaDao.borrar(dfaHere);
        }
        try {
            DatosFamiliaresAyuda dfa = new DatosFamiliaresAyuda();
            dfa.setParentezco(txtParentezco1.getValue());
            dfa.setEdad(Integer.parseInt(edad1.getValue()));
            dfa.setNroDocumento(ci1.getValue());
            dfa.setSolicitudAyuda(solicitud);

            datoFliarAyudaDao.guardar(dfa);
        } catch (Exception e) {
        } finally {
        }
        try {
            DatosFamiliaresAyuda dfa = new DatosFamiliaresAyuda();
            dfa.setParentezco(txtParentezco2.getValue());
            dfa.setEdad(Integer.parseInt(edad2.getValue()));
            dfa.setNroDocumento(ci2.getValue());
            dfa.setSolicitudAyuda(solicitud);

            datoFliarAyudaDao.guardar(dfa);
        } catch (Exception e) {
        } finally {
        }
        try {
            DatosFamiliaresAyuda dfa = new DatosFamiliaresAyuda();
            dfa.setParentezco(txtParentezco3.getValue());
            dfa.setEdad(Integer.parseInt(edad3.getValue()));
            dfa.setNroDocumento(ci3.getValue());
            dfa.setSolicitudAyuda(solicitud);

            datoFliarAyudaDao.guardar(dfa);
        } catch (Exception e) {
        } finally {
        }

        try {
            DatosFamiliaresAyuda dfa = new DatosFamiliaresAyuda();
            dfa.setParentezco(txtParentezco4.getValue());
            dfa.setEdad(Integer.parseInt(edad4.getValue()));
            dfa.setNroDocumento(ci4.getValue());
            dfa.setSolicitudAyuda(solicitud);

            datoFliarAyudaDao.guardar(dfa);
        } catch (Exception e) {
        } finally {
        }
        try {
            DatosFamiliaresAyuda dfa = new DatosFamiliaresAyuda();
            dfa.setParentezco(txtParentezco5.getValue());
            dfa.setEdad(Integer.parseInt(edad5.getValue()));
            dfa.setNroDocumento(ci5.getValue());
            dfa.setSolicitudAyuda(solicitud);

            datoFliarAyudaDao.guardar(dfa);
        } catch (Exception e) {
        } finally {
        }
        try {
            DatosFamiliaresAyuda dfa = new DatosFamiliaresAyuda();
            dfa.setParentezco(txtParentezco6.getValue());
            dfa.setEdad(Integer.parseInt(edad6.getValue()));
            dfa.setNroDocumento(ci6.getValue());
            dfa.setSolicitudAyuda(solicitud);

            datoFliarAyudaDao.guardar(dfa);
        } catch (Exception e) {
        } finally {
        }
    }

    private void cargarHistorialEstados(SolicitudAyuda solicitud, String derivacion, String atencion, String necesidad) {
//        estadoSolicitudAyudaDao
        if (solicitud.getDerivadoAlDpto().toUpperCase().equalsIgnoreCase(derivacion.toUpperCase()) && solicitud.getAtendidopor().toUpperCase().equalsIgnoreCase(atencion.toUpperCase())) {
        } else {
            EstadoSolicitudAyuda esa = new EstadoSolicitudAyuda();
            esa.setFecha(new Date());
            esa.setUsuario(UserHolder.get());
            esa.setSolicitudAyuda(solicitud);
            esa.setEstado(solicitud.getEstado());
            esa.setDptodesde(derivacion);
            esa.setDptohasta(solicitud.getDerivadoAlDpto());
            esa.setObservacion(solicitud.getObsDerivacion());
            esa.setNecesidad(necesidad);
//            estadoSolicitudAyudaDao.getListEstadoSolicitudAyuda(url);
            estadoSolicitudAyudaDao.guardar(esa);
        }

    }

    class ImageReceiver implements Upload.Receiver, Upload.SucceededListener {

        private static final long serialVersionUID = -1276759102490466761L;

        public OutputStream receiveUpload(String filename,
                String mimeType) {

            try {
                // Open the file for writing.
                file = new File(Constants.UPLOAD_DIR + "/mutual-web/" + filename);
                url = Constants.PUBLIC_SERVER_URL + "/mutual-web/" + filename;
                fileName = filename;
                ubicacion = Constants.PUBLIC_SERVER_URL + "/mutual-web/";
                fos = new FileOutputStream(file);
            } catch (final java.io.FileNotFoundException e) {
                new Notification("Could not open file<br/>",
                        e.getMessage(),
                        Notification.Type.ERROR_MESSAGE)
                        .show(Page.getCurrent());
                return null;
            } catch (IOException ex) {
                Logger.getLogger(SolicitudAyudaForm.class.getName()).log(Level.SEVERE, null, ex);
            }
            return fos; // Return the output stream to write to
        }

        public void uploadSucceeded(Upload.SucceededEvent event) {
            // Show the uploaded file in the image viewer
            image.setVisible(true);
            image.setSource(new FileResource(file));
            labelUrl.setValue(file.getName());
        }
    };

}
