package py.mutualmsp.mutualweb.formularios;

import com.github.appreciated.material.MaterialTheme;
import com.vaadin.data.Binder;
import com.vaadin.server.FileResource;
import com.vaadin.server.Page;
import com.vaadin.ui.*;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.math.BigInteger;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import java.util.function.Consumer;
import java.util.logging.Level;
import java.util.logging.Logger;
import py.mutualmsp.mutualweb.dao.NucleoFamiliarDao;
import py.mutualmsp.mutualweb.dao.ParentescoDao;
import py.mutualmsp.mutualweb.dao.SocioDao;
import py.mutualmsp.mutualweb.entities.NucleoFamiliar;
import py.mutualmsp.mutualweb.entities.Socio;
import py.mutualmsp.mutualweb.util.Constants;
import py.mutualmsp.mutualweb.util.ResourceLocator;
import py.mutualmsp.mutualweb.util.Services;

/**
 * Created by Alfre on 15/6/2016.
 */
public class NucleoFliarForm extends CssLayout {

    DateField fechas = new DateField("Fecha Nacimiento");
    CheckBox chequeado = new CheckBox("Verificado");
    TextField ci = new TextField("CI Nucleo Fliar");
    TextField nombres = new TextField("Nombres");
    TextField apellidos = new TextField("Apellidos");
    TextField celular = new TextField("Telefono");
    TextField agendamientos = new TextField("N° Agendamiento");

    ImageReceiver receiver = new ImageReceiver();
    Upload upload = upload = new Upload("Subir archivo", receiver);
    final Image image = new Image("Imagen");

    Label labelUrl = new Label();
    String fileName = "";
    String ubicacion = "";
    byte[] content;
    public File file;
    String url = "";

    FileOutputStream fos = null;

    ComboBox<String> cbParentesco = new ComboBox<>("Parentesco");
    TextField ciSocio = new TextField("CI del Socio");

    Button save = new Button("Guardar");
    Button cancel = new Button("Cancelar");
    //Button delete = new Button("Delete");
    Binder<NucleoFamiliar> binder = new Binder<>(NucleoFamiliar.class);
    NucleoFamiliarDao enfermeriaController = ResourceLocator.locate(NucleoFamiliarDao.class);
    SocioDao socioDao = ResourceLocator.locate(SocioDao.class);
    ParentescoDao parentescoDao = ResourceLocator.locate(ParentescoDao.class);

    private Consumer<NucleoFamiliar> saveListener;
    private Consumer<NucleoFamiliar> deleteListener;
    private Consumer<NucleoFamiliar> cancelListener;
    Services ejb = ResourceLocator.locate(Services.class);
    NucleoFamiliar funcionario;
    String operacion;

    public NucleoFliarForm() {
        addStyleName("product-form-wrapper");
        addStyleName("product-form");
        VerticalLayout verticalLayout = createForm();
        addComponent(verticalLayout);

        fechas.setValue(LocalDate.now());
        binder.forField(fechas).bind(NucleoFamiliar::getFechanacimiento, NucleoFamiliar::setFechanacimiento);
        binder.forField(cbParentesco).asRequired().bind(NucleoFamiliar::getParentesco, NucleoFamiliar::setParentesco);
        binder.bind(ci, NucleoFamiliar::getCedula, NucleoFamiliar::setCedula);
        binder.bind(chequeado, NucleoFamiliar::getChecked, NucleoFamiliar::setChecked);
        binder.bind(nombres, NucleoFamiliar::getNombre, NucleoFamiliar::setNombre);
        binder.bind(apellidos, NucleoFamiliar::getApellido, NucleoFamiliar::setApellido);
        binder.bind(celular, NucleoFamiliar::getTelefono, NucleoFamiliar::setTelefono);
        //binder.bind(agendamientos, NucleoFamiliar::getNumeroagendamiento, NucleoFamiliar::setNumeroagendamiento);
//        binder.bind(celular, NucleoFamiliar::getCelular, NucleoFamiliar::setTelefono);
//        binder.bind(email, NucleoFamiliar::getEmail, NucleoFamiliar::setEmail);
        binder.bindInstanceFields(this);

        List<String> lista = new ArrayList<>();
        lista.add("ESPOSO/A");
        lista.add("HIJO/A");
        lista.add("MADRE");
        lista.add("PADRE");

        cbParentesco.setItems(lista);
        cbParentesco.setValue("ESPOSO/A");

        save.addClickListener(cl -> {
            try {
                if (validate() == false) {
                    save();
                } else {
                    Notification.show("Atención", "Debes completar todos los campos!.", Notification.Type.ERROR_MESSAGE);
                }

            } catch (Exception e) {
                e.printStackTrace();
                Notification.show(e.getMessage());
            }
        });
//        save.setEnabled(false);

        nombres.addBlurListener(e -> {
//            validate();
        });
        fechas.addBlurListener(e -> {
//            validate();
        });
        ci.addBlurListener(e -> {
//            validate();
        });
        agendamientos.addBlurListener(e -> {
//            validate();
        });

        cancel.addClickListener(cl -> {
            try {
                setVisible(false);
                cancelListener.accept(funcionario);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

//        delete.addClickListener(cl -> {
//            enfermeriaController.borrar(funcionario);
//            deleteListener.accept(funcionario);
//            showForm(false);
//        });
    }

    private VerticalLayout createForm() {
        VerticalLayout layout = new VerticalLayout();
        HorizontalLayout hlayout = new HorizontalLayout();
        HorizontalLayout hlayout2 = new HorizontalLayout();
        layout.addStyleName("form-layout");
//        nombres.setWidth("100%");
//        layout.addComponent(fechas);

        ciSocio.setWidth("100%");

        hlayout.addComponent(fechas);
        hlayout.addComponent(ciSocio);
//        hlayout.addComponent(chequeado);

        layout.addComponent(hlayout);

        cbParentesco.setWidth("100%");
//        layout.addComponent(cbParentesco);

        ci.setWidth("100%");
//        layout.addComponent(ci);
        hlayout2.addComponent(ci);
        hlayout2.addComponent(cbParentesco);

        layout.addComponent(hlayout2);

        HorizontalLayout hlayout5 = new HorizontalLayout();
        nombres.setWidth("100%");
        hlayout5.addComponent(nombres);
//        layout.addComponent(nombres);

        apellidos.setWidth("100%");
        hlayout5.addComponent(apellidos);
//        layout.addComponent(apellidos);
        layout.addComponent(hlayout5);

        HorizontalLayout hlayout3 = new HorizontalLayout();
        hlayout3.addComponent(celular);
        hlayout3.addComponent(chequeado);
        celular.setWidth("100%");
        layout.addComponent(hlayout3);

        HorizontalLayout horizontalData = new HorizontalLayout();

        horizontalData.addComponent(upload);
        horizontalData.addComponent(labelUrl);

        image.setVisible(false);
        upload.setButtonCaption("Seleccionar");
        upload.addSucceededListener(receiver);

        // Prevent too big downloads 0981752315
        final long UPLOAD_LIMIT = 1000000l;
        upload.addStartedListener(new Upload.StartedListener() {
            private static final long serialVersionUID = 4728847902678459488L;

            @Override
            public void uploadStarted(Upload.StartedEvent event) {
                if (event.getContentLength() > UPLOAD_LIMIT) {
                    Notification.show("El archivo es muy grande",
                            Notification.Type.ERROR_MESSAGE);
                    upload.interruptUpload();
                }
            }
        });

        // Check the size also during progress
        upload.addProgressListener(new Upload.ProgressListener() {
            private static final long serialVersionUID = 8587352676703174995L;

            @Override
            public void updateProgress(long readBytes, long contentLength) {
                if (readBytes > UPLOAD_LIMIT) {
                    Notification.show("El archivo es muy grande",
                            Notification.Type.ERROR_MESSAGE);
                    upload.interruptUpload();
                }
            }
        });
        // Create uploads directory
        File uploads = new File(Constants.UPLOAD_DIR_TEMP);
        if (!uploads.exists() && !uploads.mkdir()) {
            horizontalData.addComponent(new Label("ERROR: No se pudo crear la carpeta"));
        }

        horizontalData.setWidth("100%");
        horizontalData.setSpacing(true);
        horizontalData.setStyleName("top-bar");

        layout.addComponent(horizontalData);

        CssLayout expander = new CssLayout();
        expander.setSizeFull();
        expander.setStyleName("expander");
        layout.addComponent(expander);
        layout.setExpandRatio(expander, 1.0F);

        save.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_FRIENDLY);
        cancel.addStyleName(MaterialTheme.BUTTON_ROUND);
//        delete.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_DANGER);

        HorizontalLayout hl = new HorizontalLayout();
//        hl.addComponents(save, cancel, delete);
        hl.addComponents(save, cancel);

        layout.addComponent(hl);

        return layout;
    }

    private boolean validate() {
        boolean savedEnabled = false;
        if (nombres == null || nombres.isEmpty()) {
            savedEnabled = true;
        }
        if (apellidos == null || apellidos.isEmpty()) {
            savedEnabled = true;
        }
        if (fechas == null || fechas.isEmpty()) {
            savedEnabled = true;
        }
        if (celular == null || celular.isEmpty()) {
            savedEnabled = true;
        }
        if (celular == null || celular.isEmpty()) {
            savedEnabled = true;
        }
        if (ci == null || ci.isEmpty()) {
            savedEnabled = true;
        }
        if (cbParentesco == null || cbParentesco.isEmpty()) {
            savedEnabled = true;
        }
//        if (operacion.equalsIgnoreCase("A")) {
//            if (ciSocio == null || ciSocio.isEmpty()) {
//                savedEnabled = true;
//            }
//            
//        }
        return savedEnabled;
    }

    private void showForm(boolean show) {
        if (show) {
            addStyleName("visible");
        } else {
            removeStyleName("visible");
        }

        setEnabled(show);
    }

    private void save() {
        try {
            funcionario.setFechanacimiento(fechas.getValue());
            funcionario.setCedula(ci.getValue());
            funcionario.setNombre(nombres.getValue());
            funcionario.setApellido(apellidos.getValue());
            funcionario.setTelefono(celular.getValue());
            System.out.println("EL CI SOCIO ES: " + ciSocio.getValue());
            System.out.println("EL ID SOCIO ES: " + funcionario.getIdsocio());
            funcionario.setDoc1(ciSocio.getValue() + "/" + fileName);
//            if (operacion.equalsIgnoreCase("A")) {
            Socio soc = socioDao.listarSocioPorCI(ciSocio.getValue());
            funcionario.setIdsocio(BigInteger.valueOf(soc.getId()));
//            }
            if (!cbParentesco.getValue().equalsIgnoreCase("")) {
                funcionario.setParentesco(cbParentesco.getValue());
            }
            if (chequeado.isVisible()) {
                funcionario.setChecked(chequeado.getValue());
            } else {
                funcionario.setChecked(true);
            }
            try {
                funcionario.setIdparentesco(BigInteger.valueOf(parentescoDao.getListParentesco(cbParentesco.getValue().toUpperCase()).get(0).getId()));
            } catch (Exception ex) {
            } finally {
            }

            enfermeriaController.guardar(funcionario);
            setVisible(false);
            saveListener.accept(funcionario);
        } catch (Exception ex) {
            Notification.show("Atención", "Ocurio un error al intentar borrar el registro", Notification.Type.ERROR_MESSAGE);
            saveListener.accept(funcionario);
            Logger.getLogger(NucleoFliarForm.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void setNucleoFamiliar(NucleoFamiliar funcionario, String dato, String cedulaSocio) {
        try {
            String select = parentescoDao.getById(funcionario.getIdparentesco().longValue()).getDescripcion();
            cbParentesco.setValue(select);
            funcionario.setParentesco(select);
        } catch (Exception e) {
        } finally {
        }
        this.funcionario = funcionario;
        operacion = dato;
        if (dato.equalsIgnoreCase("A")) {
            ciSocio.setVisible(true);
            chequeado.setVisible(false);
//            cbParentesco.setVisible(true);
        } else {
            ciSocio.setVisible(false);
            chequeado.setVisible(true);
//            cbParentesco.setVisible(false);
        }
        ciSocio.setValue(cedulaSocio);
        try {
            celular.setValue(funcionario.getTelefono());
        } catch (Exception e) {
        } finally {
        }

        cbParentesco.setVisible(true);
        binder.setBean(funcionario);
//        delete.setVisible(((funcionario.getId() != null)));
        setVisible(true);
        nombres.selectAll();
        // !!! ??? Scroll to the top as this is not a Panel, using JavaScript
        Page.getCurrent().getJavaScript().execute("window.document.getElementById('" + getId() + "').scrollTop = 0;");
    }

    public void setSaveListener(Consumer<NucleoFamiliar> saveListener) {
        this.saveListener = saveListener;
    }

    public void setDeleteListener(Consumer<NucleoFamiliar> deleteListener) {
        this.deleteListener = deleteListener;
    }

    public void setCancelListener(Consumer<NucleoFamiliar> cancelListener) {
        this.cancelListener = cancelListener;
    }

    class ImageReceiver implements Upload.Receiver, Upload.SucceededListener {

        private static final long serialVersionUID = -1276759102490466761L;

        public OutputStream receiveUpload(String filename,
                String mimeType) {

            try {
                //mimeType = mimeType.replaceAll("image\\", "");
                // Open the file for writing.
                file = new File(Constants.UPLOAD_DIR + "/" + ciSocio.getValue() + "/" + filename);
                url = Constants.PUBLIC_SERVER_URL + "/mutual-web-rrhh/" + filename;
                fileName = filename;
                ubicacion = Constants.PUBLIC_SERVER_URL;
                fos = new FileOutputStream(file);
            } catch (final java.io.FileNotFoundException e) {
                new Notification("Could not open file<br/>",
                        e.getMessage(),
                        Notification.Type.ERROR_MESSAGE)
                        .show(Page.getCurrent());
                return null;
            } catch (IOException ex) {
                Logger.getLogger(NucleoFamiliar.class.getName()).log(Level.SEVERE, null, ex);
            }
            return fos; // Return the output stream to write to
        }

        public void uploadSucceeded(Upload.SucceededEvent event) {
            // Show the uploaded file in the image viewer
            image.setVisible(true);
            image.setSource(new FileResource(file));
            labelUrl.setValue(file.getName());
        }
    };
}
