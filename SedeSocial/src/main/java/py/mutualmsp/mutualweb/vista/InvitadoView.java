package py.mutualmsp.mutualweb.vista;

import com.github.appreciated.material.MaterialTheme;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.FileDownloader;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.StreamResource;
import com.vaadin.ui.*;
import com.vaadin.ui.themes.ValoTheme;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import py.mutualmsp.mutualweb.dao.InvitadoDao;
import py.mutualmsp.mutualweb.dao.SocioDao;
import py.mutualmsp.mutualweb.dto.InvitadoDto;
import py.mutualmsp.mutualweb.entities.Invitado;
import py.mutualmsp.mutualweb.entities.Socio;
import py.mutualmsp.mutualweb.formularios.InvitadoForm;
import py.mutualmsp.mutualweb.util.Constants;
import py.mutualmsp.mutualweb.util.ResourceLocator;

/**
 * Created by Alfre on 14/6/2016.
 */
public class InvitadoView extends CssLayout implements View {

    public static final String VIEW_NAME = "Invitados";
    Grid<InvitadoDto> grid = new Grid<>(InvitadoDto.class);
    Button newCliente;
    Button searchCliente;
    //Services ejb = ResourceLocator.locate(Services.class);
    TextField filter = new TextField();
    TextField filterCi = new TextField();
    InvitadoForm form = new InvitadoForm();
    StreamResource myResource;
    InvitadoDao controller = ResourceLocator.locate(InvitadoDao.class);
    SocioDao socioDao = ResourceLocator.locate(SocioDao.class);
    private List<InvitadoDto> lista;

    public InvitadoView() {
        System.out.println("Nueva instancia InvitadoView");
        setSizeFull();
        addStyleName("crud-view");
        HorizontalLayout horizontalLayout = createTopBar();

//        filter.addValueChangeListener(vcl -> grid.clearSortOrder());
        form.setVisible(false);

        lista = controller.listaInvitadoDto();
        grid.setItems(lista);
        grid.removeAllColumns();
        grid.addColumn(InvitadoDto::getId).setCaption("Codigo");
        grid.addColumn(InvitadoDto::getCisocio).setCaption("CI Socio");
        grid.addColumn(InvitadoDto::getCedula).setCaption("Nro. Doc");
        grid.addColumn(InvitadoDto::getNombre).setCaption("Nombre");
        grid.addColumn(InvitadoDto::getApellido).setCaption("Apellido");
        grid.addColumn(InvitadoDto::getParentesco).setCaption("Parentesco");
//        grid.addComponentColumn(this::downloadImagen).setCaption("");
//        grid.addColumn(Invitado::get).setCaption("Celular");
//        grid.addColumn(Invitado::getCargo).setCaption("Email");

        grid.setSizeFull();
//        filter.addShortcutListener(new ShortcutListener("Shortcut", KeyCode.ENTER, null) {
//            @Override
//            public void handleAction(Object sender, Object target) {
//                // Do nice stuff
//                
//            }
//        });
//        filterCi.addShortcutListener(new ShortcutListener("Shortcut", KeyCode.ENTER, null) {
//            @Override
//            public void handleAction(Object sender, Object target) {
//                // Do nice stuff
//
//            }
//        });

        HorizontalLayout hl = new HorizontalLayout();
        hl.addComponents(grid, form);
        hl.setSizeFull();
        hl.setExpandRatio(grid, 1);

        VerticalLayout verticalLayout = new VerticalLayout();
        verticalLayout.addComponent(horizontalLayout);
        verticalLayout.addComponent(hl);
        verticalLayout.setMargin(true);
        verticalLayout.setSpacing(true);
        verticalLayout.setSizeFull();
        verticalLayout.setExpandRatio(hl, 1);
        verticalLayout.setStyleName("crud-main-layout");
        addComponent(verticalLayout);

        newCliente.addClickListener(
                clickEvent -> form.setInvitado(new Invitado(), "A", "")
        );
        searchCliente.addClickListener(
                clickEvent -> recuperarDatos()
        );
        filter.setPlaceholder("Filtre CI Socio");
        filterCi.setPlaceholder("Filtre CI Invitado");
        grid.asSingleSelect().addValueChangeListener(e -> {
            if (e.getValue() != null) {

                Invitado inviDto = new Invitado();
                inviDto.setId(e.getValue().getId());
                inviDto.setIdsocio(e.getValue().getIdsocio());
                inviDto.setNombre(e.getValue().getNombre());
                inviDto.setApellido(e.getValue().getApellido());
                inviDto.setFechanacimiento(e.getValue().getFechanacimiento());
                inviDto.setDoc1(e.getValue().getDoc1());
                inviDto.setDoc2(e.getValue().getDoc2());
                inviDto.setDoc3(e.getValue().getDoc3());
                inviDto.setHabilitado(e.getValue().getHabilitado());
                inviDto.setObservacion(e.getValue().getObservacion());
                inviDto.setCelular(e.getValue().getCelular());
                inviDto.setCedula(e.getValue().getCedula());
                inviDto.setParentesco(e.getValue().getParentesco());

                form.setInvitado(inviDto, "E", e.getValue().getCisocio());
            }
        });

        form.setSaveListener(cliente -> {
            grid.clearSortOrder();
            grid.setVisible(true);
            lista = new ArrayList<>();
            for (Invitado invi : controller.listaInvitado()) {
                try {
                    Socio soc = controller.consultarSocio(invi.getIdsocio());

                    InvitadoDto inviDto = new InvitadoDto();
                    inviDto.setId(invi.getId());
                    inviDto.setCisocio(soc.getCedula());
                    inviDto.setIdsocio(BigInteger.valueOf(soc.getId()));
                    inviDto.setCedula(invi.getCedula());
                    inviDto.setParentesco(invi.getParentesco());
                    inviDto.setNombre(invi.getNombre());
                    inviDto.setApellido(invi.getApellido());
                    inviDto.setFechanacimiento(invi.getFechanacimiento());

                    inviDto.setDoc1(invi.getDoc1());
                    inviDto.setDoc2(invi.getDoc2());
                    inviDto.setDoc3(invi.getDoc3());
                    inviDto.setHabilitado(invi.getHabilitado());
                    inviDto.setObservacion(invi.getObservacion());
                    inviDto.setCelular(invi.getCelular());
                    lista.add(inviDto);
                } catch (Exception e) {
                } finally {
                }
            }
            grid.setItems(lista);
            form.setVisible(false);
            Notification.show("Nuevos Datos almacenados!.", Notification.Type.HUMANIZED_MESSAGE);
        });

//        filter.setValue(VIEW_NAME);
        form.setCancelListener(cliente -> {
            grid.clearSortOrder();
            form.setVisible(false);
        });
        form.setDeleteListener(cliente -> {
            grid.clearSortOrder();
            grid.setVisible(true);
            lista = new ArrayList<>();
            for (Invitado invi : controller.listaInvitado()) {
                try {
                    Socio soc = controller.consultarSocio(invi.getIdsocio());

                    InvitadoDto inviDto = new InvitadoDto();
                    inviDto.setId(invi.getId());
                    inviDto.setCisocio(soc.getCedula());
                    inviDto.setIdsocio(BigInteger.valueOf(soc.getId()));
                    inviDto.setCedula(invi.getCedula());
                    inviDto.setParentesco(invi.getParentesco());
                    inviDto.setNombre(invi.getNombre());
                    inviDto.setApellido(invi.getApellido());
                    inviDto.setFechanacimiento(invi.getFechanacimiento());

                    inviDto.setDoc1(invi.getDoc1());
                    inviDto.setDoc2(invi.getDoc2());
                    inviDto.setDoc3(invi.getDoc3());
                    inviDto.setHabilitado(invi.getHabilitado());
                    inviDto.setObservacion(invi.getObservacion());
                    inviDto.setCelular(invi.getCelular());
                    lista.add(inviDto);
                } catch (Exception e) {
                } finally {
                }
            }
            grid.setItems(lista);
            form.setVisible(false);
            Notification.show("Atención", "Registro borrado correctamente", Notification.Type.HUMANIZED_MESSAGE);
        });
    }

    private HorizontalLayout createTopBar() {

        newCliente = new Button("Nuevo Invitado");
        newCliente.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_FRIENDLY);
        newCliente.setIcon(FontAwesome.PLUS_CIRCLE);

        searchCliente = new Button();
        searchCliente.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_FRIENDLY);
        searchCliente.setIcon(FontAwesome.SEARCH);

        HorizontalLayout layout = new HorizontalLayout();
        layout.setSpacing(true);
        layout.setWidth("100%");
        layout.addComponent(filter);
        layout.setExpandRatio(filter, 2);
        layout.addComponent(filterCi);
        layout.setExpandRatio(filterCi, 2);
        layout.addComponent(searchCliente);
        layout.setExpandRatio(searchCliente, 2);
        layout.addComponent(newCliente);
        layout.setExpandRatio(newCliente, 2);
//        layout.setComponentAlignment(filter, Alignment.MIDDLE_LEFT);
//        layout.setExpandRatio(filter, 1);
        layout.setStyleName("top-bar");
        return layout;
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent viewChangeEvent) {

    }

    private Button downloadImagen(Invitado p) {
        Button button = new Button(VaadinIcons.DOWNLOAD);
        button.setEnabled(true);
        button.setVisible(true);
        button.addStyleName(ValoTheme.BUTTON_SMALL + " " + MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_DANGER);
        descargarArchivo(new File(Constants.UPLOAD_DIR_UBI + p.getDoc1()), button);
        return button;
    }

    private void recuperarDatos() {
//        if (filter.getValue() != null) {
//            grid.clearSortOrder();
//            //filterProyecto.setItems(proyectosController.listaProyectosPorInvitado(filterInvitado.getValue()));
//            //filterProyecto.setItemCaptionGenerator(Proyecto::getDescripcion);
//            grid.setItems(controller.listarInvitadoPorID(filter.getValue().toUpperCase()));
//        } else if (filterCi.getValue() != null) {
//            grid.clearSortOrder();
//            //filterProyecto.setItems(proyectosController.listaProyectosPorInvitado(filterInvitado.getValue()));
//            //filterProyecto.setItemCaptionGenerator(Proyecto::getDescripcion);
//            grid.setItems(controller.listarInvitadoPorCI(filterCi.getValue().toUpperCase()));
//        }
        lista = new ArrayList<>();
        for (Invitado invi : controller.listarInvitadoData(filter.getValue().toUpperCase(), filterCi.getValue().toUpperCase())) {
            try {
                Socio soc = controller.consultarSocio(invi.getIdsocio());

                InvitadoDto inviDto = new InvitadoDto();
                inviDto.setId(invi.getId());
                inviDto.setCisocio(soc.getCedula());
                inviDto.setCedula(invi.getCedula());
                inviDto.setParentesco(invi.getParentesco());
                inviDto.setIdsocio(BigInteger.valueOf(soc.getId()));
                inviDto.setNombre(invi.getNombre());
                inviDto.setApellido(invi.getApellido());
                inviDto.setFechanacimiento(invi.getFechanacimiento());

                inviDto.setDoc1(invi.getDoc1());
                inviDto.setDoc2(invi.getDoc2());
                inviDto.setDoc3(invi.getDoc3());
                inviDto.setHabilitado(invi.getHabilitado());
                inviDto.setObservacion(invi.getObservacion());
                inviDto.setCelular(invi.getCelular());
                lista.add(inviDto);
            } catch (Exception e) {
            } finally {
            }
        }
        grid.setItems(lista);
    }

    private void descargarArchivo(File file, Button button) {
        myResource = createResource(file);
        FileDownloader fileDownloader = new FileDownloader(myResource);
        fileDownloader.extend(button);
    }

    private StreamResource createResource(File file) {
        return new StreamResource(new StreamResource.StreamSource() {
            @Override
            public InputStream getStream() {
                try {
                    return new FileInputStream(file);
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        }, file.getName());
    }
}
