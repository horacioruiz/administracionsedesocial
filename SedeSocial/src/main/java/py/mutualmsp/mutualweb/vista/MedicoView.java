package py.mutualmsp.mutualweb.vista;

import com.github.appreciated.material.MaterialTheme;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.navigator.View;
import com.vaadin.shared.ui.ValueChangeMode;
import com.vaadin.ui.*;
import java.util.ArrayList;

import java.util.List;
import py.mutualmsp.mutualweb.dao.MedicoDao;
import py.mutualmsp.mutualweb.entities.Medico;
import py.mutualmsp.mutualweb.formularios.MedicoForm;
import py.mutualmsp.mutualweb.util.ResourceLocator;
import py.mutualmsp.mutualweb.util.UserHolder;

/**
 * @author Dbarreto
 */
public class MedicoView extends CssLayout implements View {

    public static final String VIEW_NAME = "Médico tratante";
    Grid<Medico> grillaMedico = new Grid<>(Medico.class);
    Button btnNuevo = new Button("");
    TextField txtfFiltro = new TextField();
    MedicoDao medicoDao = ResourceLocator.locate(MedicoDao.class);
    MedicoForm medicoForm = new MedicoForm();
    List<Medico> lista = new ArrayList<>();

    public MedicoView() {
        try {
            System.out.print("Nueva instancia de ClienteView");
            setSizeFull();
            addStyleName("crud-view");

            btnNuevo.addStyleName(MaterialTheme.BUTTON_ROUND + " " +MaterialTheme.BUTTON_PRIMARY);
            btnNuevo.setIcon(VaadinIcons.PLUS_CIRCLE);

            medicoForm.setVisible(false);
            HorizontalLayout topLayout = new HorizontalLayout();
            topLayout.addComponents(txtfFiltro, btnNuevo);
            topLayout.setSpacing(true);
            topLayout.setComponentAlignment(txtfFiltro, Alignment.MIDDLE_LEFT);
            topLayout.setComponentAlignment(btnNuevo, Alignment.MIDDLE_RIGHT);
            topLayout.addStyleName("top-bar");

            txtfFiltro.setPlaceholder("Búsqueda por nombre y apellido");
            txtfFiltro.setWidth(15f, TextField.UNITS_EM);
            txtfFiltro.setValueChangeMode(ValueChangeMode.LAZY);
            txtfFiltro.addValueChangeListener(e -> updateList(e.getValue()));

            btnNuevo.addClickListener(e -> {
                grillaMedico.asSingleSelect().clear();
                medicoForm.setMedico(new Medico());
            });
            
            HorizontalLayout horizontalLayout = new HorizontalLayout();
            horizontalLayout.addComponents(grillaMedico, medicoForm);
            horizontalLayout.setSizeFull();
            horizontalLayout.setExpandRatio(grillaMedico, 1);

            VerticalLayout barAndGridLayout = new VerticalLayout();
            barAndGridLayout.addComponent(topLayout);
            barAndGridLayout.addComponent(horizontalLayout);
            barAndGridLayout.setMargin(true);
            barAndGridLayout.setSpacing(true);
            barAndGridLayout.setSizeFull();
            barAndGridLayout.setExpandRatio(horizontalLayout, 1);
            barAndGridLayout.addStyleName("crud-main-layout");
            addComponent(barAndGridLayout);

            lista = medicoDao.listaMedico();
            
            grillaMedico.setItems(lista);
            grillaMedico.removeAllColumns();
            grillaMedico.addColumn(Medico::getId).setCaption("Codigo");
            grillaMedico.addColumn(Medico::getNumeroregistro).setCaption("Nº Registro");
            grillaMedico.addColumn(Medico::getNombre).setCaption("Nombre");
            grillaMedico.addColumn(Medico::getApellido).setCaption("Apellido");
            grillaMedico.addColumn(activo -> { 
                return activo.getActivo()== true ? "SI" : "NO";}).setCaption("Activo");
            grillaMedico.setSizeFull();
            
            grillaMedico.asSingleSelect().addValueChangeListener(e -> {
                if ((UserHolder.get().getIdnivelusuario() == 8 || UserHolder.get().getIdnivelusuario() == 9 || UserHolder.get().getIdnivelusuario() == 10)) {
                    if (e.getValue() == null) {
                        medicoForm.setVisible(false);
                    } else {
                        medicoForm.setMedico(e.getValue());
                        medicoForm.setViejo();
                    }
                }
            });

            
            
            if ((UserHolder.get().getIdnivelusuario() == 8 || UserHolder.get().getIdnivelusuario() == 9 || UserHolder.get().getIdnivelusuario() == 10)) {
                btnNuevo.setEnabled(true);
            } else {
                btnNuevo.setEnabled(false);
            }

            medicoForm.setGuardarListener(medico -> {
                if (medico.getId() == null) {
                    grillaMedico.clearSortOrder();
                    lista = medicoDao.listaMedico();
                    grillaMedico.setItems(lista);
                    Notification.show("Nuevo médico creado", Notification.Type.HUMANIZED_MESSAGE);
                } else {
                    grillaMedico.clearSortOrder();
                }
            });

            medicoForm.setCancelarListener(medico -> {
                grillaMedico.clearSortOrder();
                medicoForm.setVisible(false);
            });

            medicoForm.setBorrarListener(medico -> {
                lista = medicoDao.listaMedico();
                grillaMedico.clearSortOrder();
                grillaMedico.setItems(lista);
                medicoForm.setVisible(false);
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    
    private void updateList(String value) {
        try {
            lista = medicoDao.getListMedico(value);
            grillaMedico.setSizeFull();
            grillaMedico.setItems(lista);
            grillaMedico.clearSortOrder();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    

}
