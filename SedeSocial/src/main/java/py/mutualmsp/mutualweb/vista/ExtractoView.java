/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.vista;

import com.vaadin.annotations.Push;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.navigator.View;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.Sizeable;
import com.vaadin.server.StreamResource;
import com.vaadin.ui.Accordion;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.BrowserFrame;
import com.vaadin.ui.Button;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Panel;
import com.vaadin.ui.ProgressBar;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.UIDetachedException;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.function.Consumer;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperRunManager;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.vaadin.addons.stackpanel.StackPanel;
import py.mutualmsp.mutualweb.dto.AhorroProgramadoCab;
import py.mutualmsp.mutualweb.dto.AhorroProgramadoDetalle;
import py.mutualmsp.mutualweb.dto.AporteDetalle;
import py.mutualmsp.mutualweb.dto.Aportes;
import py.mutualmsp.mutualweb.dto.DetalleMovimiento;
import py.mutualmsp.mutualweb.dto.OrdenCompraCab;
import py.mutualmsp.mutualweb.dto.Prestamo;
import py.mutualmsp.mutualweb.dto.Response;
import py.mutualmsp.mutualweb.dto.Socio;
import py.mutualmsp.mutualweb.http.ApiService;
import py.mutualmsp.mutualweb.http.ApiUtils;
import py.mutualmsp.mutualweb.http.ConsultaWS;
import py.mutualmsp.mutualweb.http.RestAdapter;
import py.mutualmsp.mutualweb.util.StringFunction;
import retrofit2.Call;
import retrofit2.Callback;
/**
 *
 * @author hectorvillalba
 */
@Push
public class ExtractoView extends CssLayout implements View{
    public static final String VIEW_NAME = "Extracto";
    TextField txtNroDocumento = new TextField("Nro. Documento");
    Button btnBuscar = new Button("Consultar");
    Panel panel = new Panel("Información del Socio");
    Button btnReporteExtracto = new Button("Ver en PDF");
    
    //Información Socio
    TextField txtSocio = new TextField("socio");
    TextField txtRegional = new TextField("regional");
    TextField txtInstitucion = new TextField("institución");
    TextField periodo = new TextField();
    ComboBox<String> comboPeriodo = new ComboBox<>("periodo");
    TextField txtCedula = new TextField("cedula");
    TextField txtGiraduria = new TextField("giraduria");
    TextField txtTelefono = new TextField("telefóno");
    TextField txtRubro = new TextField("rubro");
    TextField txtSueldo = new TextField("sueldo");
    ProgressBar spinner = new ProgressBar();
    ProgressBar spinnerAportes = new ProgressBar();
    ProgressBar spinnerOperaciones = new ProgressBar();
    ProgressBar spinnerAhorro = new ProgressBar();
    ProgressBar spinnerPrestamo = new ProgressBar();
    ProgressBar spinnerOrden = new ProgressBar();
    ProgressBar spinnerDetalle = new ProgressBar();
    
    Accordion accordionAportes = new Accordion();
    Accordion accordionOperaciones = new Accordion();
    Accordion accordionAhorro = new Accordion();
    Accordion accordionPrestamos = new Accordion();
    Accordion accordionOrden = new Accordion();
    //RestServices
    ConsultaWS consultaWS;
    private ApiService apiService;
    //Socio
    Socio socio;
    Window window;
    //Grillas
    
    //Aportes
    Grid<AporteDetalle> gridAportesDetalle = new Grid<>();
    DecimalFormat decimalFormat = new DecimalFormat("###,###,###");
    Consumer<Boolean> listenerAportes = aportes -> cargarAportes(true);
    List<AporteDetalle> listaAportes = new ArrayList<>();
    //Operaciones
    Grid<AporteDetalle> gridOperaciones = new Grid<>();
    Consumer<Boolean> listenerOperaciones = aportes -> cargarOperaciones(true);
    List<AporteDetalle> listaOperaciones = new ArrayList<>();
    //Ahorro 
    Grid<AhorroProgramadoCab> gridAhorro= new Grid<>();
    Consumer<Boolean> listenerAhorro = ahorro -> cargarAhorro(true);
    List<AhorroProgramadoCab> listaAhorro = new ArrayList<>();
    //Prestamos
    Grid<Prestamo> gridPrestamo = new Grid<>();
    Consumer<Boolean> listenerPrestamos = prestamos -> cargarPrestamos(true);
    List<Prestamo> listaPrestamo = new ArrayList<>();
    //Orden
    Grid<OrdenCompraCab> gridOrden = new Grid<>();
    Consumer<Boolean> listenerOrden = prestamos -> cargarOrden(true);
    List<OrdenCompraCab> listaOrden = new ArrayList<>();
    private String periodoActual = "";

    
    public ExtractoView() throws ParseException {
        setSizeFull();
        addStyleName("crud-view");
        VerticalLayout topBar = createTopBar();
        topBar.addComponent(spinner);
        topBar.setComponentAlignment(spinner, Alignment.MIDDLE_CENTER);
        //Sacar periodo actual
        SimpleDateFormat sdf = new SimpleDateFormat("EE MMM dd HH:mm:ss z yyyy",
                                            Locale.US);
        btnReporteExtracto.setVisible(false);
        Date parsedDate = sdf.parse(new Date().toString());
        SimpleDateFormat print = new SimpleDateFormat("MMM d, yyyy HH:mm:ss");
        System.out.println("fecha:" + print.format(parsedDate));
        Calendar calendar = Calendar.getInstance();
        Integer mes =calendar.get(Calendar.MONTH) + 1;  
        String mesString = "";
        if (periodoActual.equals("")) {
            if (mes<10)
                mesString = "0" + mes;
            else
                mesString = mes.toString();
            periodoActual += calendar.get(Calendar.YEAR)+ mesString;
        }
        if (periodoActual.contains("-")) {
            periodoActual = periodoActual.replace("-", "");
        }
        System.out.println("periodo: " + periodoActual);
        HorizontalLayout infoSocio =new HorizontalLayout();
        
        btnReporteExtracto.addClickListener(e ->{
            Map pSQL = new HashMap<String, Object>();
            JRBeanCollectionDataSource itemsJRBean = new JRBeanCollectionDataSource(listaAportes);
            pSQL.put("ItemDataSource", itemsJRBean);
                            StreamResource.StreamSource source = new StreamResource.StreamSource() {

                    public InputStream getStream() {
                        byte[] b = null;
                        try {
                            b = JasperRunManager.runReportToPdf(getClass().getClassLoader().getResourceAsStream("report/extracto.jasper"), pSQL);
    //                            b = JasperRunManager.runReportToPdf("C:\\Users\\hruiz\\Documents\\reporte\\ticket_mutual.jasper", pSQL);
                        } catch (JRException ex) {
                            ex.printStackTrace();
                        }
                        return new ByteArrayInputStream(b);
                    }
                };

                StreamResource resource = new StreamResource(source, "extracto.pdf");
                resource.setCacheTime(0);
                resource.setMIMEType("application/pdf");

                Window window = new Window();
                window.setWidth(800, Sizeable.Unit.PIXELS);
                window.setHeight(600, Sizeable.Unit.PIXELS);
                window.setModal(true);
                window.center();
                BrowserFrame pdf = new BrowserFrame("test", resource);
                pdf.setSizeFull();

                window.setContent(pdf);
                getUI().addWindow(window);
        });
       
        FormLayout layout1 = new FormLayout();
        txtSocio.setWidth("130%");
        layout1.addComponent(txtSocio);
        //txtSocio.addStyleName(ValoTheme.TEXTFIELD_LARGE);
        layout1.addComponent(txtRegional);
        layout1.addComponent(txtInstitucion);
        
        FormLayout layout2 = new FormLayout();
        layout2.setWidth("100%");
        layout2.addComponent(comboPeriodo);
        layout2.addComponent(txtCedula);
        layout2.addComponent(txtGiraduria);
        
        FormLayout layout3 = new FormLayout();
        layout3.setWidth("100%");
        layout3.addComponent(txtTelefono);
        layout3.addComponent(txtRubro);
        layout3.addComponent(txtSueldo);
        layout3.addComponent(btnReporteExtracto);
        
        infoSocio.addComponent(layout1);
        infoSocio.addComponent(layout2);
        infoSocio.addComponent(layout3);
        
        Panel panelCab = new Panel("Consulta Extracto");
        panelCab.setContent(topBar);
        panelCab.setSizeUndefined();
        panelCab.setWidth("100%");
        
        panel.setContent(infoSocio);
        panel.setSizeUndefined();
        panel.setWidth("100%");
        //panel.setScrollTop(3540);
        panel.setVisible(false);
        
        spinnerDetalle.setIndeterminate(true);
        spinner.setIndeterminate(true);
        spinnerAportes.setIndeterminate(true);
        spinnerOperaciones.setIndeterminate(true);
        spinner.setVisible(false);
        spinnerOperaciones.setVisible(false);
        spinnerAportes.setVisible(false);
        spinnerAhorro.setVisible(false);
        spinnerAhorro.setIndeterminate(true);
        spinnerPrestamo.setVisible(false);
        spinnerOrden.setVisible(false);
        spinnerPrestamo.setIndeterminate(true);
        spinnerOrden.setIndeterminate(true);
        
        VerticalLayout layout = new VerticalLayout();
        layout.addComponent(panelCab);
        layout.addComponent(panel);
        //layout.addComponent(accordionAportes);
        accordionAportes.setCaption("Aportes");

        Panel section0 = new Panel("Aportes");
        VerticalLayout verticaLayoutAportes = new VerticalLayout();
        verticaLayoutAportes.addComponent(spinnerAportes);
        verticaLayoutAportes.addComponent(gridAportesDetalle);
        verticaLayoutAportes.setComponentAlignment(spinnerAportes, Alignment.TOP_CENTER);
        gridAportesDetalle.setVisible(false);
        gridAportesDetalle.setSizeUndefined();
        gridAportesDetalle.setWidth("100%");
        gridAportesDetalle.setHeightByRows(4);
        verticaLayoutAportes.setSpacing(false);
        verticaLayoutAportes.setMargin(false);
       
        
        //section0.setIcon(VaadinIcons.);
        StackPanel panel0 = StackPanel.extend(section0);
        section0.setContent(verticaLayoutAportes);
//        panel0.close();
//        panel0.addToggleListener((StackPanel source) -> {
//            if (source.isOpen()) {
//                
//            }
//        });
        
        layout.addComponent(section0);
        // panel with individual icons
        gridOperaciones.setVisible(false);
        gridOperaciones.setSizeUndefined();
        gridOperaciones.setWidth("100%");
        gridOperaciones.setHeightByRows(4);
        VerticalLayout verticaLayoutOperaciones = new VerticalLayout();
        verticaLayoutOperaciones.addComponent(spinnerOperaciones);
        verticaLayoutOperaciones.addComponent(gridOperaciones);
        verticaLayoutOperaciones.setComponentAlignment(spinnerOperaciones, Alignment.TOP_CENTER);
        verticaLayoutOperaciones.setSpacing(false);
        verticaLayoutOperaciones.setMargin(false);
        //Operaciones
        Panel section2 = new Panel("Operaciones");
        section2.setContent(verticaLayoutOperaciones);
        StackPanel panel2 = StackPanel.extend(section2);
        panel2.setToggleDownIcon(VaadinIcons.CARET_SQUARE_DOWN_O);
        panel2.setToggleUpIcon(VaadinIcons.CARET_SQUARE_UP_O);
        
        //Ahorro 
        gridAhorro.setVisible(false);
        gridAhorro.setSizeUndefined();
        gridAhorro.setWidth("100%");
        gridAhorro.setHeightByRows(2);
        
        VerticalLayout verticalAhorro = new VerticalLayout();
        verticalAhorro.addComponent(spinnerAhorro);
        verticalAhorro.addComponent(gridAhorro);
        verticalAhorro.setComponentAlignment(spinnerAhorro, Alignment.TOP_CENTER);
        verticalAhorro.setSpacing(false);
        verticalAhorro.setMargin(false);
        Panel section3 = new Panel("Ahorro");
        section3.setContent(verticalAhorro);
        StackPanel panel3 = StackPanel.extend(section3);
        panel3.setToggleDownIcon(VaadinIcons.CARET_SQUARE_DOWN_O);
        panel3.setToggleUpIcon(VaadinIcons.CARET_SQUARE_UP_O);
        
        //Prestamo 
        gridPrestamo.setVisible(false);
        gridPrestamo.setSizeUndefined();
        gridPrestamo.setWidth("100%");
        gridPrestamo.setHeightByRows(4);
        VerticalLayout verticalPrestamo = new VerticalLayout();
        verticalPrestamo.addComponent(spinnerPrestamo);
        verticalPrestamo.addComponent(gridPrestamo);
        verticalPrestamo.setComponentAlignment(spinnerPrestamo, Alignment.TOP_CENTER);
        verticalPrestamo.setSpacing(false);
        verticalPrestamo.setMargin(false);
        Panel section4 = new Panel("Prestamos");
        section4.setContent(verticalPrestamo);
        StackPanel panel4 = StackPanel.extend(section4);
        panel4.setToggleDownIcon(VaadinIcons.CARET_SQUARE_DOWN_O);
        panel4.setToggleUpIcon(VaadinIcons.CARET_SQUARE_UP_O);
        
        //Orden de Credito
        gridOrden.setVisible(false);
        gridOrden.setSizeUndefined();
        gridOrden.setWidth("100%");
        gridOrden.setHeightByRows(4);
        VerticalLayout verticalOrden = new VerticalLayout();
        verticalOrden.addComponent(spinnerOrden);
        verticalOrden.addComponent(gridOrden);
        verticalOrden.setComponentAlignment(spinnerOrden, Alignment.TOP_CENTER);
        verticalOrden.setSpacing(false);
        verticalOrden.setMargin(false);
        Panel section5 = new Panel("Orden de Crédito");
        section5.setContent(verticalOrden);
        StackPanel panel5 = StackPanel.extend(section5);
        panel5.setToggleDownIcon(VaadinIcons.CARET_SQUARE_DOWN_O);
        panel5.setToggleUpIcon(VaadinIcons.CARET_SQUARE_UP_O);
        
        
        //accordionAportes.addComponent(verticalLayoutAportes);
        accordionAportes.setTabsVisible(false);
        layout.addComponent(section2);
        accordionOperaciones.setCaption("Operaciones");
        layout.addComponent(section3);
        layout.addComponent(section4);
        layout.addComponent(section5);
        layout.addComponent(btnReporteExtracto);
        layout.setSpacing(true);
        layout.setMargin(true);
        layout.setSizeUndefined();
        layout.setWidth("100%");
       
        
        Panel panelPrincipal = new Panel();
        panelPrincipal.setContent(layout);
        panelPrincipal.setSizeFull();
        panelPrincipal.setScrollTop(2540);
        addComponent(panelPrincipal);
        
        btnBuscar.addClickListener( e ->{
            try {
                if (txtNroDocumento.getValue()== null || txtNroDocumento.getValue().equals("") ) {
                    txtNroDocumento.focus();
                    Notification.show("El nro de documento no puede estar vacio...!!!", Notification.Type.WARNING_MESSAGE);
                    return;
                }
                spinner.setVisible(true);
                cargarSocio();
            } catch (Exception ee) {
                ee.printStackTrace();
            }            
        });   
        
        gridAportesDetalle.addItemClickListener(e -> {
            if (e.getMouseEventDetails().isDoubleClick()) {
                mostrarDetalle(e.getItem(),"of");
            }
        });
        gridOperaciones.addItemClickListener(e -> {
            if (e.getMouseEventDetails().isDoubleClick()) {
                mostrarDetalle(e.getItem(),"of");
            }
        });
        gridAhorro.addItemClickListener(e -> {
            if (e.getMouseEventDetails().isDoubleClick()) {
                mostrarDetalleAhorro(e.getItem(),"of");
            }
        });
        gridPrestamo.addItemClickListener(e -> {
            if (e.getMouseEventDetails().isDoubleClick()) {
                mostrarPrestamoDetalle(e.getItem(),"mo");
            }
        });
    }    
    
    
    private void spinnerDetalle(){
        if (window == null) 
            window = new Window();
        VerticalLayout verticalLayout = new VerticalLayout();
        verticalLayout.addComponent(new Label("Procesando información..."));
        verticalLayout.addComponent(spinnerDetalle);
        verticalLayout.setComponentAlignment(spinnerDetalle, Alignment.MIDDLE_CENTER);
        window.setWidth(300, Sizeable.Unit.PIXELS);
        window.setHeight(100, Sizeable.Unit.PIXELS);
        window.setModal(true);
        window.center();
        window.setContent(verticalLayout);
        UI.getCurrent().addWindow(window);
    }
    
    private void cargarPeriodo(){
        try {
            System.out.println("cargarPeriodo");
            apiService  = ApiUtils.getAPIService();
            Call<Response<List<String>>> response = apiService.getPeriodo();
            response.enqueue(new Callback<Response<List<String>>>() {
                @Override
                public void onResponse(Call<Response<List<String>>> call, retrofit2.Response<Response<List<String>>> response) {
                    if (response.body() != null){
                        System.out.println(".onResponse(). Periodo");
                        comboPeriodo.setItems(response.body().getData());
                        System.out.println("periodos: " +response.body().getData().size() );
                        //comboPeriodo.setItemCaptionGenerator("descripción");
                    }else{
                        System.out.println(".onResponse(). Periodo: " + response.errorBody());
                    }
                }

                @Override
                public void onFailure(Call<Response<List<String>>> call, Throwable t) {
                    System.out.println(".onFailure()" + call.request().url().toString());
                    Notification.show("Error: " + t.getMessage(), Notification.Type.ERROR_MESSAGE);
                    t.printStackTrace();
                }
            });            
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    private void cargarSocio(){
        //UI ui = UI.getCurrent();
        getUI().setPollInterval(1000);
        consultaWS = RestAdapter.getClient("").create(ConsultaWS.class);
                Call<Response<Socio>> response = consultaWS.getSocio(txtNroDocumento.getValue());
                response.enqueue(new Callback<Response<Socio>>() {
                @Override
                public void onResponse(Call<Response<Socio>> call, retrofit2.Response<Response<Socio>> response) {
                    if (response.body() != null){
                        if (response.body().getCodigo().equals(200)){
                            //ok
                            socio = response.body().getData();
                            System.out.println(".onResponse():" + " success");
                            panel.setVisible(true);
                            txtSocio.setValue(response.body().getData().getNombre() + " " + response.body().getData().getNombre());          
                            txtRegional.setValue(response.body().getData().getRegional() == null ? "" : response.body().getData().getRegional());
                            txtInstitucion.setValue(response.body().getData().getInstitucion() == null ? "" : response.body().getData().getInstitucion());
                            txtCedula.setValue(response.body().getData().getCedula() == null ? "" : response.body().getData().getCedula());
                            txtGiraduria.setValue(response.body().getData().getGiraduria() == null ? "" : response.body().getData().getGiraduria());
                            txtTelefono.setValue(response.body().getData().getTelefono()== null ? "" :response.body().getData().getTelefono());
                            txtRubro.setValue(response.body().getData().getRubro() == null ? "" : response.body().getData().getRubro());
                            DecimalFormat decimalFormat = new DecimalFormat("###,###,###");
                            txtSueldo.setValue(response.body().getData().getSueldo() == null ? "" 
                                            : decimalFormat.format(response.body().getData().getSueldo()));
                            getUI().access(() -> {
                                // Stop polling and hide spinner
                                getUI().setPollInterval(-1);
                                spinner.setVisible(false);       
                            });  
                            cargarPeriodo();
                            accordionAhorro.setVisible(true);
                            accordionOperaciones.setVisible(true);
                            accordionPrestamos.setVisible(true);
                            accordionAportes.setVisible(true);
                            accordionOrden.setVisible(true);
                            listenerAportes.accept(Boolean.TRUE);
                        }else {
                            getUI().access(() -> {
                                // Stop polling and hide spinner
                                getUI().setPollInterval(-1);
                                spinner.setVisible(false);  
                            });
                            Notification.show("Error: " + response.body().getMensaje(), Notification.Type.ERROR_MESSAGE);
                        }
                    }
                }

                @Override
                public void onFailure(Call<Response<Socio>> call, Throwable t) {
                    Notification.show("Error: " + t.getMessage(), Notification.Type.ERROR_MESSAGE);
                }
            });                
                
    }
    
    private VerticalLayout createTopBar() {
        btnBuscar.setIcon(FontAwesome.SEARCH);
        VerticalLayout horizontalLayout = new VerticalLayout();
        horizontalLayout.setWidth("100%");
        horizontalLayout.addComponent(txtNroDocumento);
        horizontalLayout.addComponent(btnBuscar);
        //horizontalLayout.setComponentAlignment(txtNroDocumento, Alignment.MIDDLE_LEFT);
        horizontalLayout.setExpandRatio(txtNroDocumento,05);
        horizontalLayout.setStyleName("top-bar");        
        return horizontalLayout;
    }

    public Consumer<Boolean> getListenerAportes() {
        return listenerAportes;
    }

    public void setListenerAportes(Consumer<Boolean> listenerAportes) {
        System.out.println("setListenerAportes()");
        this.listenerAportes = listenerAportes;
    }

    private void cargarAportes(boolean par) {
//        UI ui = UI.getCurrent();
        getUI().setPollInterval(1000);
        spinnerAportes.setVisible(true);
        consultaWS = RestAdapter.getClient("").create(ConsultaWS.class);
        Call<Response<Aportes>> resp = consultaWS.getAportes(socio.getCedula(), "sheldon",".mutual.2019*",periodoActual);
        resp.enqueue(new Callback<Response<Aportes>>() {
            @Override
            public void onResponse(Call<Response<Aportes>> call, retrofit2.Response<Response<Aportes>> response) {
                if (response != null) { 
                    if (response.body()!= null) {
                        if (response.body().getCodigo().equals(200)) {
                            try {
                                btnReporteExtracto.setVisible(true);
                                gridAportesDetalle.setHeightByRows(response.body().getData().getAporteDetalles().size());
                                gridAportesDetalle.setVisible(true);
                                gridAportesDetalle.setItems(response.body().getData().getAporteDetalles());
                                listaAportes = response.body().getData().getAporteDetalles();
                                gridAportesDetalle.removeAllColumns();
                                gridAportesDetalle.addColumn(a -> {return StringFunction.toCamelCase(a.getDescripcion());})
                                        .setCaption("descripción");
                                gridAportesDetalle.addColumn(a -> {
                                        return a.getNumeroOperacion();})
                                        .setCaption("nro. operación");
                                gridAportesDetalle.addColumn(a -> {
                                        return a.getPlazo()+"/0";})
                                        .setCaption("plazo/ac");                                
                                gridAportesDetalle.addColumn(a -> {
                                    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                                    String formattedDate = formatter.format(a.getFechaOperacion());
                                    return  formattedDate;
                                }).setCaption("f. operación");
                               gridAportesDetalle.addColumn(a -> {return a.getImporte() == null ? "0" : decimalFormat.format(a.getImporte());})
                                        .setCaption("monto")
                                        .setStyleGenerator(item -> "v-align-right");
                                gridAportesDetalle.addColumn(a -> { 
                                    return a.getMontoCobrado() == null ? "0" : decimalFormat.format(a.getMontoCobrado());})
                                        .setCaption("monto aportado")
                                        .setStyleGenerator(item -> "v-align-right");
                                gridAportesDetalle.addColumn(a -> { 
                                    return a.getCerrado() == null ? "0" : decimalFormat.format(a.getCerrado());})
                                        .setCaption("cerrado")
                                        .setStyleGenerator(item -> "v-align-right");
                                gridAportesDetalle.addColumn(a -> { return a.getAtraso() == null ? "0" : decimalFormat.format(a.getAtraso());})
                                        .setCaption("atraso")
                                        .setStyleGenerator(item -> "v-align-right");
                                gridAportesDetalle.addColumn(a -> { return a.getActual() == null ? "0" : decimalFormat.format(a.getActual());})
                                        .setCaption("actual")
                                        .setStyleGenerator(item -> "v-align-right");
                                gridAportesDetalle.addColumn(a -> { return a.getSaldo() == null ? "0" : decimalFormat.format(a.getSaldo());})
                                        .setCaption("saldo")
                                        .setStyleGenerator(item -> "v-align-right");

                                getUI().access(() -> {
                                     // Stop polling and hide spinner
                                     getUI().setPollInterval(-1);
                                     spinnerAportes.setVisible(false);  
                                });
                                //gridAportesDetalle.clearSortOrder();
                                listenerOperaciones.accept(Boolean.TRUE);                             
                            } catch (Exception e) {
                                e.printStackTrace();
                            }                            
                        }else{
                            getUI().access(() -> {
                                 // Stop polling and hide spinner
                                 getUI().setPollInterval(-1);
                                 spinnerAportes.setVisible(false);  
                            });
                            //gridAportesDetalle.clearSortOrder();
                            listenerOperaciones.accept(Boolean.TRUE);                              
                        }
                    }else{
                        getUI().access(() -> {
                             // Stop polling and hide spinner
                             getUI().setPollInterval(-1);
                             spinnerAportes.setVisible(false);  
                        });
                        //gridAportesDetalle.clearSortOrder();
                        listenerOperaciones.accept(Boolean.TRUE);                          
                    }
                }
            }

            @Override
            public void onFailure(Call<Response<Aportes>> call, Throwable t) {
                System.out.println("Exception: " + t.getMessage());
                //Notification.show("" + t.getMessage(), Notification.Type.ERROR_MESSAGE);
                getUI().access(() -> {
                    // Stop polling and hide spinner
                    getUI().setPollInterval(-1);
                    spinner.setVisible(false);  
                });
                listenerOperaciones.accept(Boolean.TRUE); 
            }
        });
    }
    
    private void cargarOperaciones(boolean par) {
        getUI().setPollInterval(1000);
        spinnerOperaciones.setVisible(true);
        consultaWS = RestAdapter.getClient("").create(ConsultaWS.class);
        Call<Response<Aportes>> response = consultaWS.getOperaciones(socio.getCedula(),"sheldon",".mutual.2019*",periodoActual);
        response.enqueue(new Callback<Response<Aportes>>() {
            @Override
            public void onResponse(Call<Response<Aportes>> call, retrofit2.Response<Response<Aportes>> response) {
                if (response.body() !=null){
                    if (response.body().getCodigo().equals(200)){
                       gridOperaciones.setVisible(true);
                       listaOperaciones =  response.body().getData().getAporteDetalles();
                       gridOperaciones.setHeightByRows(listaOperaciones.size()+1);
                       gridOperaciones.setItems(listaOperaciones);
                       gridOperaciones.removeAllColumns();
                       gridOperaciones.addColumn(a -> { return StringFunction.toCamelCase(a.getDescripcion());}).setCaption("descripción");     
                       gridOperaciones.addColumn(a -> {
                                    return a.getNumeroOperacion();
                       }).setCaption("nro. operación");
                       gridOperaciones.addColumn(a -> {
                                    return a.getPlazo()+"/0";
                       }).setCaption("plazo/ac");                       
                       gridOperaciones.addColumn(a -> {
                                SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                                String formattedDate = formatter.format(a.getFechaOperacion());
                                return  formattedDate;
                       }).setCaption("f. operación");
                       gridOperaciones.addColumn(a -> {return a.getImporte() == null ? "0" : decimalFormat.format(a.getImporte());})
                                        .setCaption("monto")
                                        .setStyleGenerator(item -> "v-align-right");
                       gridOperaciones.addColumn(a -> { 
                           return a.getMontoCobrado() == null ? "0" : decimalFormat.format(a.getMontoCobrado());})
                               .setCaption("monto aportado")
                               .setStyleGenerator(item -> "v-align-right");
                        gridOperaciones.addColumn(a -> { 
                                    return a.getCerrado() == null ? "0" : decimalFormat.format(a.getCerrado());})
                                        .setCaption("cerrado")
                                        .setStyleGenerator(item -> "v-align-right");
                       gridOperaciones.addColumn(a -> { return a.getAtraso() == null ? "0" : decimalFormat.format(a.getAtraso());})
                               .setCaption("atraso")
                               .setStyleGenerator(item -> "v-align-right");
                       gridOperaciones.addColumn(a -> { return a.getActual() == null ? "0" : decimalFormat.format(a.getActual());})
                               .setCaption("actual")
                               .setStyleGenerator(item -> "v-align-right");
                       gridOperaciones.addColumn(a -> { return a.getSaldo() == null ? "0" : decimalFormat.format(a.getSaldo());})
                               .setCaption("saldo")
                               .setStyleGenerator(item -> "v-align-right");
                       getUI().access(() -> {
                            // Stop polling and hide spinner
                            getUI().setPollInterval(-1);
                            spinnerOperaciones.setVisible(false);  
                        });
                        cargarFondo();                        
                    }else {
                        //Notification.show("" + response.body().getMensaje(), Notification.Type.ERROR_MESSAGE);
                        getUI().access(() -> {
                            // Stop polling and hide spinner
                            getUI().setPollInterval(-1);
                            spinnerOperaciones.setVisible(false);  
                        });
                        cargarFondo();
                    }
                }
            }
            @Override
            public void onFailure(Call<Response<Aportes>> call, Throwable t) {
               t.printStackTrace();
                //System.out.println(".onFailure()" + t.toString());
               //Notification.show("" + t.getMessage(), Notification.Type.ERROR_MESSAGE);
               getUI().access(() -> {
                    // Stop polling and hide spinner
                    getUI().setPollInterval(-1);
                    spinnerOperaciones.setVisible(false);  
               });
               cargarFondo();
            }
        });
    }
    
    private void cargarAhorro(boolean par) {
        getUI().setPollInterval(1000);
        spinnerAhorro.setVisible(true);
        consultaWS = RestAdapter.getClient("").create(ConsultaWS.class);
        Call<Response<List<AhorroProgramadoCab>>> response = consultaWS.getAhorroCab(socio.getCedula(),"sheldon",".mutual.2019*",periodoActual);
        response.enqueue(new Callback<Response<List<AhorroProgramadoCab>>>() {
            @Override
            public void onResponse(Call<Response<List<AhorroProgramadoCab>>> call, retrofit2.Response<Response<List<AhorroProgramadoCab>>> response) {
                if (response.body() !=null){
                    if (response.body().getCodigo().equals(200)){
                        gridAhorro.setVisible(true);
                        listaAhorro = response.body().getData();
                        gridAhorro.setHeightByRows(listaAhorro.size());
                        gridAhorro.setItems(listaAhorro);
                        gridAhorro.removeAllColumns();
                        gridAhorro.addColumn(a -> {return "Ahorro Programado";}).setCaption("descripción");
                        gridAhorro.addColumn(a -> {return a.getNumeroOperacion().toString();})
                                .setCaption("nro. operación");
                        gridAhorro.addColumn(a -> {return a.getPlazo()+"/"+ a.getCuotasPagadas();})
                                .setCaption("plazo/ac");
                        gridAhorro.addColumn(a -> {
                                SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                                String formattedDate = formatter.format(a.getFechaOperacion());
                                return  formattedDate;    
                        }).setCaption("f. operación");
                        gridAhorro.addColumn(a -> {return a.getImporte() == null ? "0" : decimalFormat.format(a.getImporte());})
                                        .setCaption("monto")
                                        .setStyleGenerator(item -> "v-align-right");
                        gridAhorro.addColumn(a -> { 
                           return a.getMontoAportado() == null ? "0" : decimalFormat.format(a.getMontoAportado());})
                               .setCaption("monto aportado")
                               .setStyleGenerator(item -> "v-align-right");
                        gridAhorro.addColumn(a -> {return a.getCierre() == null ? "0" : decimalFormat.format(a.getCierre());})
                                        .setCaption("cerrado")
                                        .setStyleGenerator(item -> "v-align-right");
                        gridAhorro.addColumn(a -> { return a.getAtraso() == null ? "0" : decimalFormat.format(a.getAtraso());})
                               .setCaption("atraso")
                               .setStyleGenerator(item -> "v-align-right");
                        gridAhorro.addColumn(a -> { return a.getActual() == null ? "0" : decimalFormat.format(a.getActual());})
                               .setCaption("actual")
                               .setStyleGenerator(item -> "v-align-right");
                        gridAhorro.addColumn(a -> { return a.getSaldo() == null ? "0" : decimalFormat.format(a.getSaldo());})
                               .setCaption("saldo")
                               .setStyleGenerator(item -> "v-align-right");
                        getUI().access(() -> {
                            // Stop polling and hide spinner
                            getUI().setPollInterval(-1);
                            spinnerAhorro.setVisible(false);  
                        });   
                        listenerPrestamos.accept(Boolean.TRUE);
                    }else {
                        getUI().access(() -> {  
                            // Stop polling and hide spinner
                            getUI().setPollInterval(-1);
                            spinnerAhorro.setVisible(false);  
                        });   
                        listenerPrestamos.accept(Boolean.TRUE);
                        //Snackbar.make(btnConsultar, response.body().getMensaje(), Snackbar.LENGTH_LONG).show();
                        //Notification.show("" + response.body() == null ? "No tiene ahorro" :response.body().getMensaje(), Notification.Type.ERROR_MESSAGE);
                    }
                }
            }

            @Override
            public void onFailure(Call<Response<List<AhorroProgramadoCab>>> call, Throwable t) {
                //Snackbar.make(btnConsultar, t.getMessage(), Snackbar.LENGTH_LONG).show();
                //Notification.show("" + t.getMessage(), Notification.Type.ERROR_MESSAGE);
                getUI().access(() -> {  
                            // Stop polling and hide spinner
                    getUI().setPollInterval(-1);
                    spinnerAhorro.setVisible(false);  
                });   
                listenerPrestamos.accept(Boolean.TRUE);
            }
        });
    }
    
    private void cargarPrestamos(boolean par) {
        getUI().setPollInterval(500);
        spinnerPrestamo.setVisible(true);
        consultaWS = RestAdapter.getClient("").create(ConsultaWS.class);
        Call<Response<List<Prestamo>>> response = consultaWS.getPrestamo(socio.getCedula(),"sheldon",".mutual.2019*",periodoActual);
        response.enqueue(new Callback<Response<List<Prestamo>>>() {
            @Override
            public void onResponse(Call<Response<List<Prestamo>>> call, retrofit2.Response<Response<List<Prestamo>>> response) {
                if (response.body() !=null){
                    if (response.body().getCodigo().equals(200)){
                        gridPrestamo.setVisible(true);
                        listaPrestamo = response.body().getData();
                        gridPrestamo.setItems(listaPrestamo);
                        gridPrestamo.removeAllColumns();
                        gridPrestamo.addColumn(a -> {return StringFunction.toCamelCase(a.getCuenta()) ;})
                                .setCaption("descripción");
                        gridPrestamo.addColumn(a -> {return a.getNumeroOperacion();}).setCaption("nro. operación");   
                        gridPrestamo.addColumn(a -> {return a.getPlazoAprobado()+ "/" + a.getCuotasPagadas();})
                                .setCaption("plazo/ac");
                        gridPrestamo.addColumn(a -> { 
                            try {
                                SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                                String formattedDate = formatter.format(a.getFechaAprobado());
                                return  formattedDate;
                            } catch (Exception ex) {
                                Logger.getLogger(ExtractoView.class.getName()).log(Level.SEVERE, null, ex);
                            }
                            return  "";
                            }).setCaption("f. emisión");
                        gridPrestamo.addColumn(a -> { 
                           return a.getPagadoCapital()  == null ? "0" : decimalFormat.format(a.getPagadoCapital());})
                               .setCaption("monto pagado")
                               .setStyleGenerator(item -> "v-align-right");
                        gridPrestamo.addColumn(a -> { return a.getCerrado() == null ? "0" : decimalFormat.format(a.getCerrado());})
                               .setCaption("cerrado")
                               .setStyleGenerator(item -> "v-align-right");
                        gridPrestamo.addColumn(a -> { return a.getAtraso() == null ? "0" : decimalFormat.format(a.getAtraso());})
                               .setCaption("atraso")
                               .setStyleGenerator(item -> "v-align-right");
                        gridPrestamo.addColumn(a -> { return a.getActual() == null ? "0" : decimalFormat.format(a.getActual());})
                               .setCaption("actual")
                               .setStyleGenerator(item -> "v-align-right");
                        gridPrestamo.addColumn(a -> { return a.getSaldo() == null ? "0" : decimalFormat.format(a.getSaldo());})
                               .setCaption("saldo")
                               .setStyleGenerator(item -> "v-align-right");
                        getUI().access(() -> {
                            // Stop polling and hide spinner
                            getUI().setPollInterval(-1);
                            spinnerPrestamo.setVisible(false);  
                        }); 
                        listenerOrden.accept(Boolean.TRUE);
                    }else {
                        getUI().access(() -> {
                            // Stop polling and hide spinner
                            getUI().setPollInterval(-1);
                            spinnerPrestamo.setVisible(false);  
                        }); 
                        listenerOrden.accept(Boolean.TRUE);
                        Notification.show("" + response.body() == null ? "No tiene prestamos" : response.body().getMensaje(), Notification.Type.ERROR_MESSAGE);
                    }
                }
            }

            @Override
            public void onFailure(Call<Response<List<Prestamo>>> call, Throwable t) {
                t.printStackTrace();
                //Notification.show("" + t.getMessage(), Notification.Type.ERROR_MESSAGE);
                getUI().access(() -> {
                            // Stop polling and hide spinner
                    getUI().setPollInterval(-1);
                    spinnerPrestamo.setVisible(false);  
                }); 
                listenerOrden.accept(Boolean.TRUE);
            }
        });
    }
    
    private void cargarFondo() {
        getUI().setPollInterval(1000);
        consultaWS = RestAdapter.getClient("").create(ConsultaWS.class);
        Call<Response<Aportes>> response = consultaWS.getFondo(socio.getCedula(),"sheldon",".mutual.2019*",periodoActual);
        response.enqueue(new Callback<Response<Aportes>>() {
            @Override
            public void onResponse(Call<Response<Aportes>> call, retrofit2.Response<Response<Aportes>> response) {
                if (response.body() !=null){
                    if (response.body().getCodigo().equals(200)){
                        listaOperaciones.add(response.body().getData().getAporteDetalles().get(0));
                        gridOperaciones.setItems(listaOperaciones);
                        getUI().access(() -> {
                            // Stop polling and hide spinner
                            getUI().setPollInterval(-1);
                            gridOperaciones.clearSortOrder();    
                        });
                        listenerAhorro.accept(Boolean.TRUE);
                  }else {
                        //Notification.show("" + response.body().getMensaje(), Notification.Type.ERROR_MESSAGE);
                        listenerAhorro.accept(Boolean.TRUE);
                  }
                }
            }
            @Override
            public void onFailure(Call<Response<Aportes>> call, Throwable t) {
               Notification.show("" + t.getMessage(), Notification.Type.ERROR_MESSAGE);
               getUI().access(() -> {
                    // Stop polling and hide spinner
                    getUI().setPollInterval(-1);
                    gridOperaciones.clearSortOrder();    
               });
               listenerAhorro.accept(Boolean.TRUE);               
            }
        });
    }    
    
    private void cargarOrden(boolean par) {
        getUI().setPollInterval(500);
        spinnerOrden.setVisible(true);
        consultaWS = RestAdapter.getClient("").create(ConsultaWS.class);
        Call<Response<List<OrdenCompraCab>>> response = consultaWS.getOrdenCompraCab(socio.getCedula(),"sheldon",".mutual.2019*",periodoActual);
        response.enqueue(new Callback<Response<List<OrdenCompraCab>>>() {
            @Override
            public void onResponse(Call<Response<List<OrdenCompraCab>>> call, retrofit2.Response<Response<List<OrdenCompraCab>>> response) {
                if (response.body() !=null){
                    if (response.body().getCodigo().equals(200)){
                        gridOrden.setVisible(true);
                        listaOrden = response.body().getData();
                        gridOrden.setItems(listaOrden);
                        gridOrden.removeAllColumns();
                        gridOrden.addColumn(a -> { return "" + a.getEntidad();}).setCaption("descripción");
                        gridOrden.addColumn(a -> { return a.getNroOperacion();})
                                .setCaption("nro. operación");
                        gridOrden.addColumn(a -> { return a.getPlazoAprobado()+ "/" + a.getCuotasPagadas();})
                                .setCaption("plazo/ac");
                        gridOrden.addColumn(a -> { 
                            try {
                                SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                                String formattedDate = formatter.format(a.getFechaAprobado());
                                return  formattedDate;
                            } catch (Exception ex) {
                                Logger.getLogger(ExtractoView.class.getName()).log(Level.SEVERE, null, ex);
                            }
                            return  "";
                        }).setCaption("f. emisión");
                        gridOrden.addColumn(a -> { 
                           return a.getMontoAprobado()  == null ? "0" : decimalFormat.format(a.getMontoAprobado());})
                               .setCaption("monto aprobado")
                               .setStyleGenerator(item -> "v-align-right");
                        gridOrden.addColumn(a -> { return a.getAtraso() == null ? "0" : decimalFormat.format(a.getAtraso());})
                               .setCaption("atraso")
                               .setStyleGenerator(item -> "v-align-right");
                        gridOrden.addColumn(a -> { return a.getActual() == null ? "0" : decimalFormat.format(a.getActual());})
                               .setCaption("actual")
                               .setStyleGenerator(item -> "v-align-right");
                        gridOrden.addColumn(a -> { return a.getSaldo() == null ? "0" : decimalFormat.format(a.getSaldo());})
                               .setCaption("saldo")
                               .setStyleGenerator(item -> "v-align-right");
                        getUI().access(() -> {
                            // Stop polling and hide spinner
                            getUI().setPollInterval(-1);
                            spinnerOrden.setVisible(false);  
                        });                         
                    }else {
                        Notification.show("" + response.body().getMensaje(), Notification.Type.ERROR_MESSAGE);
                        getUI().access(() -> {
                            // Stop polling and hide spinner
                            getUI().setPollInterval(-1);
                            spinnerOrden.setVisible(false);  
                        });
                    }
                }
            }

            @Override
            public void onFailure(Call<Response<List<OrdenCompraCab>>> call, Throwable t) {
                Notification.show("" + t.getMessage(), Notification.Type.ERROR_MESSAGE);
                                        getUI().access(() -> {
                    // Stop polling and hide spinner
                    getUI().setPollInterval(-1);
                    spinnerOrden.setVisible(false);  
                });
            }
        });
    }

    
    private void mostrarDetalle(AporteDetalle aporteDetalle, String tipo) {
        spinnerDetalle();
        getUI().setPollInterval(1000);
        consultaWS = RestAdapter.getClient("").create(ConsultaWS.class);
        Call<Response<List<DetalleMovimiento>>> response = consultaWS.getDetalleMovimiento(aporteDetalle.getId(), tipo);
        response.enqueue(new Callback<Response<List<DetalleMovimiento>>>() {
            @Override
            public void onResponse(Call<Response<List<DetalleMovimiento>>> call, retrofit2.Response<Response<List<DetalleMovimiento>>> rspns) {
                if (rspns.body() != null && rspns.body().getCodigo().equals(200)) {

                    window.setVisible(false);
                    window.close();
                    window = null;
                    DetalleWindow detalleWindow =  new DetalleWindow();
                    detalleWindow.poblarGrilla(rspns.body().getData());
                    detalleWindow.setModal(true);
                    detalleWindow.center();
                    detalleWindow.setSizeUndefined();
                    getUI().access(() -> {
                            // Stop polling and hide spinner
                        getUI().setPollInterval(-1);
                        getUI().addWindow(detalleWindow);
                    });                     
                }else{
                    System.err.println("Error " + rspns.message());
                }
            }

            @Override
            public void onFailure(Call<Response<List<DetalleMovimiento>>> call, Throwable thrwbl) {
                System.err.println("Error " + thrwbl.getMessage());
            }
        });
    }
    
    private void mostrarPrestamoDetalle(Prestamo prestamo, String tipo) {
        spinnerDetalle();
        getUI().setPollInterval(1000);
        consultaWS = RestAdapter.getClient("").create(ConsultaWS.class);
        Call<Response<List<DetalleMovimiento>>> response = consultaWS.getDetalleMovimiento(prestamo.getId(), tipo);
        response.enqueue(new Callback<Response<List<DetalleMovimiento>>>() {
            @Override
            public void onResponse(Call<Response<List<DetalleMovimiento>>> call, retrofit2.Response<Response<List<DetalleMovimiento>>> rspns) {
                if (rspns.body() != null && rspns.body().getCodigo().equals(200)) {
                    window.setVisible(false);
                    window.close();
                    window = null;
                    DetalleWindow detalleWindow =  new DetalleWindow();
                    detalleWindow.poblarGrilla(rspns.body().getData());
                    detalleWindow.setModal(true);
                    detalleWindow.center();
                    detalleWindow.setSizeUndefined();
                    getUI().access(() -> {
                            // Stop polling and hide spinner
                        getUI().setPollInterval(-1);
                        getUI().addWindow(detalleWindow);
                    }); 
                }else{
                    System.err.println("Error " + rspns.message());
                }
            }

            @Override
            public void onFailure(Call<Response<List<DetalleMovimiento>>> call, Throwable thrwbl) {
                System.err.println("Error " + thrwbl.getMessage());
            }
        });
    }

    private void mostrarDetalleAhorro(AhorroProgramadoCab item, String of) {
        spinnerDetalle();
        getUI().setPollInterval(1000);
        consultaWS = RestAdapter.getClient("").create(ConsultaWS.class);
        Call<Response<List<AhorroProgramadoDetalle>>> response = consultaWS.getAhorroDetalle(item.getId());
        response.enqueue(new Callback<Response<List<AhorroProgramadoDetalle>>>() {
            @Override
            public void onResponse(Call<Response<List<AhorroProgramadoDetalle>>> call, retrofit2.Response<Response<List<AhorroProgramadoDetalle>>> rspns) {
                if (rspns.body() != null && rspns.body().getCodigo().equals(200)) {
                    window.setVisible(false);
                    window.close();
                    window = null;
                    DetalleWindow detalleWindow =  new DetalleWindow();
                    detalleWindow.poblarGrillaAhorro(rspns.body().getData());
                    detalleWindow.setModal(true);
                    detalleWindow.center();
                    detalleWindow.setSizeUndefined();
                    getUI().access(() -> {
                            // Stop polling and hide spinner
                            getUI().setPollInterval(-1);
                            getUI().addWindow(detalleWindow);
                    });
                }else{
                    System.err.println("Error " + rspns.message());
                }
            }

            @Override
            public void onFailure(Call<Response<List<AhorroProgramadoDetalle>>> call, Throwable thrwbl) {
                System.err.println("Error " + thrwbl.getMessage());
            }
        });    
    }
    
    private class InitializerThread extends Thread {
        @Override
        public void run() {
            // Do initialization which takes some time.
            // Here represented by a 1s sleep
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
            }
            try{
                // Init done, update the UI after doing locking
                UI.getCurrent().access(new Runnable() {
                    @Override
                    public void run() {
                        Notification.show("Hola Grabiel...!!!");
                    }
                });
            } catch (UIDetachedException e) {
                Notification.show("Error: " + e.getMessage());
            }
	  }
    }
    
    public static class SectionPanel extends Panel {
        public SectionPanel(String caption) {
            setCaption(caption);
            setContent(new HorizontalLayout() {
                {
                    setSizeFull();
                    setMargin(true);
                    setSpacing(true);
                    setDefaultComponentAlignment(Alignment.BOTTOM_LEFT);
                    addComponents(
                            new TextField("First Name"),
                            new TextField("Last Name"),
                            new TextField("Phone"),
                            new Button("Save"));
                }
            });
        }
    }     
}
