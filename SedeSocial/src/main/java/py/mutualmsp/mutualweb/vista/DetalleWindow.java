/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.vista;

import com.vaadin.ui.Grid;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.function.Consumer;
import py.mutualmsp.mutualweb.dto.AhorroProgramadoDetalle;
import py.mutualmsp.mutualweb.dto.DetalleMovimiento;

/**
 *
 * @author hectorvillalba
 */
public class DetalleWindow extends Window {
    Grid<DetalleMovimiento> gridDetalleMovimiento = new Grid<>();
    Grid<AhorroProgramadoDetalle> gridDetalleAhorro = new Grid<>();
    Consumer<Boolean> consumer;
    DecimalFormat decimalFormat = new DecimalFormat("###,###,###");
    
    public DetalleWindow(){
        VerticalLayout verticalLayout = new VerticalLayout();
        verticalLayout.addComponent(new Label("Detalle"));
        verticalLayout.addComponent(gridDetalleMovimiento);
        verticalLayout.addComponent(gridDetalleAhorro);
        setContent(verticalLayout);
    }
    
    public void setConsumer(Consumer<Boolean> consumer) {
        this.consumer = consumer;
    }
    
    public void poblarGrilla(List<DetalleMovimiento> lista){
        try {
            gridDetalleAhorro.setVisible(false);
            gridDetalleMovimiento.setVisible(true);
            gridDetalleMovimiento.clearSortOrder();
            gridDetalleMovimiento.removeAllColumns();
            gridDetalleMovimiento.setItems(lista);
            gridDetalleMovimiento.addColumn(DetalleMovimiento::getNumeroCuota).setCaption("nro. cuota");
            gridDetalleMovimiento.addColumn(a -> {
                                        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                                        String formattedDate = formatter.format(a.getFechaVencimiento());
                                        return  formattedDate;
            });
            gridDetalleMovimiento.addColumn(a -> {return decimalFormat.format(a.getCuota());}).setCaption("cuota");
            gridDetalleMovimiento.addColumn(a -> {return decimalFormat.format(a.getPago());}).setCaption("pago");
            gridDetalleMovimiento.setSizeUndefined();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public void poblarGrillaAhorro(List<AhorroProgramadoDetalle> lista){
        try {
            gridDetalleAhorro.setVisible(true);
            gridDetalleMovimiento.setVisible(false);
            gridDetalleAhorro.clearSortOrder();
            gridDetalleAhorro.removeAllColumns();
            gridDetalleAhorro.setItems(lista);
            gridDetalleAhorro.addColumn(AhorroProgramadoDetalle::getNumeroCuota).setCaption("nro. cuota");
            gridDetalleAhorro.addColumn(a -> {
                                        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                                        String formattedDate = formatter.format(a.getFechaVencimiento());
                                        return  formattedDate;
            });
            //gridDetalleAhorro.addColumn(a -> {return decimalFormat.format(a.get);}).setCaption("cuota");
            gridDetalleAhorro.addColumn(a -> {return decimalFormat.format(a.getPago());}).setCaption("pago");
            gridDetalleAhorro.setSizeUndefined();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
