/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.vista;

import com.github.appreciated.material.MaterialTheme;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.navigator.View;
import com.vaadin.shared.ui.ValueChangeMode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import java.util.ArrayList;
import java.util.List;
import py.mutualmsp.mutualweb.dao.AnalisisDao;
import py.mutualmsp.mutualweb.dao.MotivoSolicitudAyudaDao;
import py.mutualmsp.mutualweb.entities.Analisis;
import py.mutualmsp.mutualweb.entities.MotivoSolicitudAyuda;
import py.mutualmsp.mutualweb.formularios.DisponibilidadForm;
import py.mutualmsp.mutualweb.formularios.MotivoAyudaForm;
import py.mutualmsp.mutualweb.util.ResourceLocator;
import py.mutualmsp.mutualweb.util.UserHolder;

/**
 *
 * @author hectorvillalba
 */
public class DisponibilidadView extends CssLayout implements View {
    public static final String VIEW_NAME = "Disponibilidad";
    Grid<Analisis> grillaAnalisis = new Grid<>(Analisis.class);
    TextField txtfFiltro = new TextField("Filtro");
    AnalisisDao analisisDao = ResourceLocator.locate(AnalisisDao.class);
    List<Analisis> lista = new ArrayList<>();
    Button btnNuevo = new Button("");
    DisponibilidadForm disponibilidadForm = new DisponibilidadForm();

    public DisponibilidadView() {
        setSizeFull();
        addStyleName("crud-view");

        btnNuevo.addStyleName(MaterialTheme.BUTTON_ROUND + " " +MaterialTheme.BUTTON_PRIMARY);
        btnNuevo.setIcon(VaadinIcons.PLUS_CIRCLE);

        disponibilidadForm.setVisible(false);
        HorizontalLayout topLayout = new HorizontalLayout();
        topLayout.addComponents(txtfFiltro, btnNuevo);
        topLayout.setSpacing(true);
        topLayout.setComponentAlignment(txtfFiltro, Alignment.MIDDLE_LEFT);
        topLayout.setComponentAlignment(btnNuevo, Alignment.MIDDLE_RIGHT);
        topLayout.addStyleName("top-bar");

        txtfFiltro.setPlaceholder("Filtro de búsqueda");
        txtfFiltro.setValueChangeMode(ValueChangeMode.LAZY);
        //txtfFiltro.addValueChangeListener(e -> updateList(e.getValue()));

        btnNuevo.addClickListener(e -> {
            grillaAnalisis.asSingleSelect().clear();
            UI.getCurrent().addWindow(disponibilidadForm);
            disponibilidadForm.setVisible(true);
            disponibilidadForm.setDisponibilidad(new Analisis());
        });

        if ((UserHolder.get().getIdnivelusuario() == 8 || UserHolder.get().getIdnivelusuario() == 9 || UserHolder.get().getIdnivelusuario() == 10)) {
            btnNuevo.setEnabled(true);
        } else {
            btnNuevo.setEnabled(false);
        }

        HorizontalLayout horizontalLayout = new HorizontalLayout();
        horizontalLayout.addComponents(grillaAnalisis);
        horizontalLayout.setSizeFull();
        horizontalLayout.setExpandRatio(grillaAnalisis, 1);

        VerticalLayout barAndGridLayout = new VerticalLayout();
        barAndGridLayout.addComponent(topLayout);
        barAndGridLayout.addComponent(horizontalLayout);
        barAndGridLayout.setMargin(true);
        barAndGridLayout.setSpacing(true);
        barAndGridLayout.setSizeFull();
        barAndGridLayout.setExpandRatio(horizontalLayout, 1);
        barAndGridLayout.addStyleName("crud-main-layout");

        try {
            lista = analisisDao.listaAnalisis();
            grillaAnalisis.setItems(lista);
            grillaAnalisis.removeAllColumns();
            grillaAnalisis.addColumn(Analisis::getId).setCaption("COD");
            grillaAnalisis.addColumn(Analisis::getIdrubro).setCaption("Descripción");
            grillaAnalisis.setSizeFull();
            grillaAnalisis.asSingleSelect().addValueChangeListener(e -> {
                if ((UserHolder.get().getIdnivelusuario() == 8 || UserHolder.get().getIdnivelusuario() == 9 || UserHolder.get().getIdnivelusuario() == 10)) {
                    if (e.getValue() == null) {
                        disponibilidadForm.setVisible(false);
                    } else {
                        disponibilidadForm.setDisponibilidad(e.getValue());
                        //disponibilidadForm.setViejo();
                    }
                }
            });

            disponibilidadForm.setGuardarListener(r -> {
                grillaAnalisis.clearSortOrder();
                lista = analisisDao.listaAnalisis();
                grillaAnalisis.setItems(lista);
            });
            disponibilidadForm.setBorrarListener(r -> {
                grillaAnalisis.clearSortOrder();
            });
            disponibilidadForm.setCancelarListener(r -> {
                grillaAnalisis.clearSortOrder();
            });

            addComponent(barAndGridLayout);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
