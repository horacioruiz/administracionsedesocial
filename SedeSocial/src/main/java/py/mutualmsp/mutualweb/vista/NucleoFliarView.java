package py.mutualmsp.mutualweb.vista;

import com.github.appreciated.material.MaterialTheme;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.FileDownloader;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.StreamResource;
import com.vaadin.ui.*;
import com.vaadin.ui.themes.ValoTheme;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import py.mutualmsp.mutualweb.dao.InvitadoDao;
import py.mutualmsp.mutualweb.dao.NucleoFamiliarDao;
import py.mutualmsp.mutualweb.dao.ParentescoDao;
import py.mutualmsp.mutualweb.dao.SocioDao;
import py.mutualmsp.mutualweb.dto.NucleoFliarDto;
import py.mutualmsp.mutualweb.entities.NucleoFamiliar;
import py.mutualmsp.mutualweb.entities.Socio;
import py.mutualmsp.mutualweb.formularios.NucleoFliarForm;
import py.mutualmsp.mutualweb.util.Constants;
import py.mutualmsp.mutualweb.util.ResourceLocator;

/**
 * Created by Alfre on 14/6/2016.
 */
public class NucleoFliarView extends CssLayout implements View {

    public static final String VIEW_NAME = "Núcleo Familiar";
    Grid<NucleoFliarDto> grid = new Grid<>(NucleoFliarDto.class);
    Button newCliente;
    Button searchCliente;
    //Services ejb = ResourceLocator.locate(Services.class);
    TextField filter = new TextField();
    TextField filterCi = new TextField();
    NucleoFliarForm form = new NucleoFliarForm();
    StreamResource myResource;
    NucleoFamiliarDao controller = ResourceLocator.locate(NucleoFamiliarDao.class);
    InvitadoDao socioController = ResourceLocator.locate(InvitadoDao.class);
    SocioDao socioDao = ResourceLocator.locate(SocioDao.class);
    ParentescoDao parentescoDao = ResourceLocator.locate(ParentescoDao.class);
    private List<NucleoFliarDto> lista;

    public NucleoFliarView() {
        System.out.println("Nueva instancia NucleoFamiliarView");
        setSizeFull();
        addStyleName("crud-view");
        HorizontalLayout horizontalLayout = createTopBar();

//        filter.addValueChangeListener(vcl -> grid.clearSortOrder());
        form.setVisible(false);
        lista = new ArrayList<>();
        for (NucleoFamiliar invi : controller.listaNucleoFamiliar()) {
            try {
                Socio soc = socioController.consultarSocio(invi.getIdsocio());
                NucleoFliarDto inviDto = new NucleoFliarDto();
                inviDto.setId(invi.getId());
                inviDto.setCisocio(soc.getCedula());
                inviDto.setCedula(invi.getCedula());
                inviDto.setParentesco(invi.getParentesco());
                inviDto.setIdparentesco(invi.getIdparentesco());
                inviDto.setNombre(invi.getNombre());
                inviDto.setApellido(invi.getApellido());
                inviDto.setFechanacimiento(invi.getFechanacimiento());

                inviDto.setDoc1(invi.getDoc1());
                inviDto.setDoc2(invi.getDoc2());
                inviDto.setDoc3(invi.getDoc3());
                inviDto.setHabilitado(invi.getHabilitado());
                inviDto.setObservacion(invi.getObservacion());
                inviDto.setTelefono(invi.getTelefono());
                inviDto.setChecked(invi.getChecked());
                lista.add(inviDto);
            } catch (Exception e) {
            } finally {
            }
        }
        grid.setItems(lista);
        grid.removeAllColumns();
        grid.addColumn(NucleoFliarDto::getId).setCaption("Codigo");
        grid.addComponentColumn(e -> {
            return e.getConfirmadoIcono();
        }).setCaption("Verf.");
        grid.addColumn(NucleoFliarDto::getCisocio).setCaption("CI Socio");
        grid.addColumn(NucleoFliarDto::getCedula).setCaption("CI Fliar");
        grid.addColumn(NucleoFliarDto::getNombre).setCaption("Nombre");
        grid.addColumn(NucleoFliarDto::getApellido).setCaption("Apellido");
        grid.addColumn(e -> {
            String select = "--";
            try {
                select = parentescoDao.getById(e.getIdparentesco().longValue()).getDescripcion();
            } catch (Exception ex) {
            } finally {
            }
            return select;
        }).setCaption("Parentesco");
//        grid.addColumn(NucleoFliarDto::getParentesco).setCaption("Parentesco");
//        grid.addComponentColumn(e -> {
//            
//        }).setCaption("Verf.");
        grid.addColumn(NucleoFliarDto::getEdad).setCaption("Edad");
        grid.addComponentColumn(this::downloadImagen).setCaption("");
//        grid.addColumn(NucleoFamiliar::get).setCaption("Celular");
//        grid.addColumn(NucleoFamiliar::getCargo).setCaption("Email");

        grid.setSizeFull();
//        filter.addShortcutListener(new ShortcutListener("Shortcut", KeyCode.ENTER, null) {
//            @Override
//            public void handleAction(Object sender, Object target) {
//                // Do nice stuff
//                
//            }
//        });
//        filterCi.addShortcutListener(new ShortcutListener("Shortcut", KeyCode.ENTER, null) {
//            @Override
//            public void handleAction(Object sender, Object target) {
//                // Do nice stuff
//
//            }
//        });

        HorizontalLayout hl = new HorizontalLayout();
        hl.addComponents(grid, form);
        hl.setSizeFull();
        hl.setExpandRatio(grid, 1);

        VerticalLayout verticalLayout = new VerticalLayout();
        verticalLayout.addComponent(horizontalLayout);
        verticalLayout.addComponent(hl);
        verticalLayout.setMargin(true);
        verticalLayout.setSpacing(true);
        verticalLayout.setSizeFull();
        verticalLayout.setExpandRatio(hl, 1);
        verticalLayout.setStyleName("crud-main-layout");
        addComponent(verticalLayout);

        newCliente.addClickListener(
                clickEvent -> form.setNucleoFamiliar(new NucleoFamiliar(), "A", "")
        );
        searchCliente.addClickListener(
                clickEvent -> recuperarDatos()
        );
        filter.setPlaceholder("Filtre CI Socio");
        filterCi.setPlaceholder("Filtre CI Nucleo Fliar");
        grid.asSingleSelect().addValueChangeListener(e -> {
            if (e.getValue() != null) {
                NucleoFamiliar inviDto = new NucleoFamiliar();
                inviDto.setId(e.getValue().getId());
                inviDto.setIdsocio(e.getValue().getIdsocio());
                inviDto.setNombre(e.getValue().getNombre());
                inviDto.setApellido(e.getValue().getApellido());
                inviDto.setFechanacimiento(e.getValue().getFechanacimiento());
                inviDto.setDoc1(e.getValue().getDoc1());
                inviDto.setDoc2(e.getValue().getDoc2());
                inviDto.setDoc3(e.getValue().getDoc3());
                inviDto.setHabilitado(e.getValue().getHabilitado());
                inviDto.setObservacion(e.getValue().getObservacion());
                inviDto.setTelefono(e.getValue().getTelefono());
                inviDto.setCedula(e.getValue().getCedula());
                inviDto.setParentesco(e.getValue().getParentesco());
                inviDto.setIdparentesco(e.getValue().getIdparentesco());
                inviDto.setChecked(e.getValue().getChecked());

                form.setNucleoFamiliar(inviDto, "E", e.getValue().getCisocio());
            }
        });

        form.setSaveListener(cliente -> {
            grid.clearSortOrder();
            grid.setVisible(true);
            lista = new ArrayList<>();
            for (NucleoFamiliar invi : controller.listaNucleoFamiliar()) {
                try {
                    Socio soc = socioController.consultarSocio(invi.getIdsocio());

                    NucleoFliarDto inviDto = new NucleoFliarDto();
                    inviDto.setId(invi.getId());
                    inviDto.setCisocio(soc.getCedula());
                    inviDto.setCedula(invi.getCedula());
                    inviDto.setParentesco(invi.getParentesco());
                    inviDto.setIdparentesco(invi.getIdparentesco());
                    inviDto.setNombre(invi.getNombre());
                    inviDto.setApellido(invi.getApellido());
                    inviDto.setFechanacimiento(invi.getFechanacimiento());

                    inviDto.setDoc1(invi.getDoc1());
                    inviDto.setDoc2(invi.getDoc2());
                    inviDto.setDoc3(invi.getDoc3());
                    inviDto.setHabilitado(invi.getHabilitado());
                    inviDto.setObservacion(invi.getObservacion());
                    inviDto.setTelefono(invi.getTelefono());
                    inviDto.setChecked(invi.getChecked());
                    lista.add(inviDto);
                } catch (Exception e) {
                } finally {
                }
            }
            grid.setItems(lista);
            form.setVisible(false);
            Notification.show("Nuevos Datos almacenados!.", Notification.Type.HUMANIZED_MESSAGE);
        });

//        filter.setValue(VIEW_NAME);
        form.setCancelListener(cliente -> {
            grid.clearSortOrder();
            form.setVisible(false);
        });
        form.setDeleteListener(cliente -> {
            grid.clearSortOrder();
            grid.setVisible(true);
            lista = new ArrayList<>();
            for (NucleoFamiliar invi : controller.listaNucleoFamiliar()) {
                try {
                    Socio soc = socioController.consultarSocio(invi.getIdsocio());

                    NucleoFliarDto inviDto = new NucleoFliarDto();
                    inviDto.setId(invi.getId());
                    inviDto.setCisocio(soc.getCedula());
                    inviDto.setCedula(invi.getCedula());
                    inviDto.setParentesco(invi.getParentesco());
                    inviDto.setIdparentesco(invi.getIdparentesco());
                    inviDto.setNombre(invi.getNombre());
                    inviDto.setApellido(invi.getApellido());
                    inviDto.setFechanacimiento(invi.getFechanacimiento());

                    inviDto.setDoc1(invi.getDoc1());
                    inviDto.setDoc2(invi.getDoc2());
                    inviDto.setDoc3(invi.getDoc3());
                    inviDto.setHabilitado(invi.getHabilitado());
                    inviDto.setObservacion(invi.getObservacion());
                    inviDto.setTelefono(invi.getTelefono());
                    inviDto.setChecked(invi.getChecked());
                    lista.add(inviDto);
                } catch (Exception e) {
                } finally {
                }
            }
            grid.setItems(lista);
            form.setVisible(false);
            Notification.show("Atención", "Registro borrado correctamente", Notification.Type.HUMANIZED_MESSAGE);
        });
    }

    private HorizontalLayout createTopBar() {

        newCliente = new Button("Nuevo NucleoFamiliar");
        newCliente.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_FRIENDLY);
        newCliente.setIcon(FontAwesome.PLUS_CIRCLE);

        searchCliente = new Button();
        searchCliente.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_FRIENDLY);
        searchCliente.setIcon(FontAwesome.SEARCH);

        HorizontalLayout layout = new HorizontalLayout();
        layout.setSpacing(true);
        layout.setWidth("100%");
        layout.addComponent(filter);
        layout.setExpandRatio(filter, 2);
        layout.addComponent(filterCi);
        layout.setExpandRatio(filterCi, 2);
        layout.addComponent(searchCliente);
        layout.setExpandRatio(searchCliente, 2);
        layout.addComponent(newCliente);
        layout.setExpandRatio(newCliente, 2);
//        layout.setComponentAlignment(filter, Alignment.MIDDLE_LEFT);
//        layout.setExpandRatio(filter, 1);
        layout.setStyleName("top-bar");
        return layout;
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent viewChangeEvent) {

    }

    private Button downloadImagen(NucleoFliarDto p) {
        Button button = new Button(VaadinIcons.DOWNLOAD);
        button.setEnabled(true);
        button.setVisible(true);
        button.addStyleName(ValoTheme.BUTTON_SMALL + " " + MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_PRIMARY);

        descargarArchivo(new File(Constants.UPLOAD_DIR_UBI + p.getDoc1()), button, p.getDoc1());
        return button;
    }

    private void recuperarDatos() {
//        if (filter.getValue() != null) {
//            grid.clearSortOrder();
//            //filterProyecto.setItems(proyectosController.listaProyectosPorNucleoFamiliar(filterNucleoFamiliar.getValue()));
//            //filterProyecto.setItemCaptionGenerator(Proyecto::getDescripcion);
//            grid.setItems(controller.listarNucleoFamiliarPorID(filter.getValue().toUpperCase()));
//        } else if (filterCi.getValue() != null) {
//            grid.clearSortOrder();
//            //filterProyecto.setItems(proyectosController.listaProyectosPorNucleoFamiliar(filterNucleoFamiliar.getValue()));
//            //filterProyecto.setItemCaptionGenerator(Proyecto::getDescripcion);
//            grid.setItems(controller.listarNucleoFamiliarPorCI(filterCi.getValue().toUpperCase()));
//        }
        lista = new ArrayList<>();
        for (NucleoFamiliar invi : controller.listarNucleoFamiliarData(filter.getValue().toUpperCase(), filterCi.getValue().toUpperCase())) {
            try {
                Socio soc = socioController.consultarSocio(invi.getIdsocio());

                NucleoFliarDto inviDto = new NucleoFliarDto();
                inviDto.setId(invi.getId());
                inviDto.setCisocio(soc.getCedula());
                inviDto.setCedula(invi.getCedula());
                inviDto.setParentesco(invi.getParentesco());
                inviDto.setIdparentesco(invi.getIdparentesco());
                inviDto.setNombre(invi.getNombre());
                inviDto.setApellido(invi.getApellido());
                inviDto.setFechanacimiento(invi.getFechanacimiento());

                inviDto.setDoc1(invi.getDoc1());
                inviDto.setDoc2(invi.getDoc2());
                inviDto.setDoc3(invi.getDoc3());
                inviDto.setHabilitado(invi.getHabilitado());
                inviDto.setObservacion(invi.getObservacion());
                inviDto.setTelefono(invi.getTelefono());
                inviDto.setChecked(invi.getChecked());
                lista.add(inviDto);
            } catch (Exception e) {
            } finally {
            }
        }
        grid.setItems(lista);
    }

    private void descargarArchivo(File file, Button button, String documento) {
        try {
            myResource = createResource(file, documento);
            FileDownloader fileDownloader = new FileDownloader(myResource);
            fileDownloader.extend(button);
        } catch (Exception e) {
            documento = documento.replaceAll(".jpg", ".jpeg");
            System.out.println("DOCUMENTO TWO -->> " + documento);
            myResource = createResource(file, documento);
            FileDownloader fileDownloader = new FileDownloader(myResource);
            fileDownloader.extend(button);
        } finally {
        }

    }

    private StreamResource createResource(File file, String documento) {
        return new StreamResource(new StreamResource.StreamSource() {
            @Override
            public InputStream getStream() {
                try {
                    return new FileInputStream(file);
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                } finally {
                }
            }
        }, file.getName());
    }
}
