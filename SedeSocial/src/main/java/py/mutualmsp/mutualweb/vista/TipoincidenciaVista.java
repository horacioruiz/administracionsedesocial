/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.vista;

import com.github.appreciated.material.MaterialTheme;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.navigator.View;
import com.vaadin.shared.ui.ValueChangeMode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import java.util.ArrayList;
import java.util.List;
import py.mutualmsp.mutualweb.dao.TipoincidenciaDao;
import py.mutualmsp.mutualweb.entities.Tipoincidencia;
import py.mutualmsp.mutualweb.formularios.TipoincidenciaForm;
import py.mutualmsp.mutualweb.util.ResourceLocator;

/**
 *
 * @author Dbarreto
 */

public class TipoincidenciaVista extends CssLayout implements View{
    public static final String VIEW_NAME = "Tipoincidencia";
    Grid<Tipoincidencia> grillaTipoincidencia = new Grid<>(Tipoincidencia.class);
    TextField txtfFiltro = new TextField("Filtro");
    TipoincidenciaDao tipoincidenciaDao = ResourceLocator.locate(TipoincidenciaDao.class);
    List<Tipoincidencia> lista = new ArrayList<>();
    Button btnNuevo = new Button("Nuevo");
    TipoincidenciaForm tipoincidenciaForm = new TipoincidenciaForm();
    
    public TipoincidenciaVista() {
        setSizeFull();
        addStyleName("crud-view");
        
        btnNuevo.addStyleName(MaterialTheme.BUTTON_PRIMARY);
        btnNuevo.setIcon(VaadinIcons.PLUS_CIRCLE);
        
        tipoincidenciaForm.setVisible(false);
        HorizontalLayout topLayout = new HorizontalLayout();
        topLayout.addComponents(txtfFiltro, btnNuevo);
        topLayout.setSpacing(true);
        topLayout.setComponentAlignment(txtfFiltro, Alignment.MIDDLE_LEFT);
        topLayout.setComponentAlignment(btnNuevo, Alignment.MIDDLE_RIGHT);
        topLayout.addStyleName("top-bar");
        
        txtfFiltro.setPlaceholder("Filtro de búsqueda");
        txtfFiltro.setValueChangeMode(ValueChangeMode.LAZY);
        txtfFiltro.addValueChangeListener(e -> updateList(e.getValue()));
        
        btnNuevo.addClickListener(e ->{
            grillaTipoincidencia.asSingleSelect().clear();
            tipoincidenciaForm.setTipoincidencia(new Tipoincidencia());
        });
        
        HorizontalLayout horizontalLayout = new HorizontalLayout();
        horizontalLayout.addComponents(grillaTipoincidencia, tipoincidenciaForm);
        horizontalLayout.setSizeFull();
        horizontalLayout.setExpandRatio(grillaTipoincidencia, 1);
        
        VerticalLayout barAndGridLayout = new VerticalLayout();
        barAndGridLayout.addComponent(topLayout);
        barAndGridLayout.addComponent(horizontalLayout);
        barAndGridLayout.setMargin(true);
        barAndGridLayout.setSpacing(true);
        barAndGridLayout.setSizeFull();
        barAndGridLayout.setExpandRatio(horizontalLayout, 1);
        barAndGridLayout.addStyleName("crud-main-layout");
        
        try {
            lista = tipoincidenciaDao.listaTipoincidencia();
            grillaTipoincidencia.setItems(lista);
            grillaTipoincidencia.removeAllColumns();
            grillaTipoincidencia.addColumn(Tipoincidencia::getId).setCaption("Id Cargo");
            grillaTipoincidencia.addColumn(Tipoincidencia::getDescripcion).setCaption("Descripción");
            grillaTipoincidencia.setSizeFull();
            grillaTipoincidencia.asSingleSelect().addValueChangeListener(e ->{
                if (e.getValue()==null){
                    tipoincidenciaForm.setVisible(false);
                } else {
                    tipoincidenciaForm.setTipoincidencia(e.getValue());
                    tipoincidenciaForm.setViejo();
                }
            });
            
            tipoincidenciaForm.setGuardarListener(r ->{
                grillaTipoincidencia.clearSortOrder();
                lista = tipoincidenciaDao.listaTipoincidencia();
                grillaTipoincidencia.setItems(lista);
            });
            tipoincidenciaForm.setBorrarListener(r ->{
                grillaTipoincidencia.clearSortOrder();
            });
            tipoincidenciaForm.setCancelarListener(r ->{
                grillaTipoincidencia.clearSortOrder();
            });
            
            addComponent(barAndGridLayout);
            
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void updateList(String value) {
        try {
           lista = tipoincidenciaDao.getListTipoincidencia(value);
           grillaTipoincidencia.setSizeFull();
           grillaTipoincidencia.setItems(lista);
           grillaTipoincidencia.clearSortOrder();
       } catch (Exception e) {
           e.printStackTrace();
       }        
    }
    
     
}
