/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.vista;

import com.github.appreciated.material.MaterialTheme;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.navigator.View;
import com.vaadin.shared.ui.ValueChangeMode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import java.util.ArrayList;
import java.util.List;
import py.mutualmsp.mutualweb.dao.NecesidadAyudaDao;
import py.mutualmsp.mutualweb.entities.NecesidadAyuda;
import py.mutualmsp.mutualweb.formularios.NecesidadAyudaForm;
import py.mutualmsp.mutualweb.util.ResourceLocator;
import py.mutualmsp.mutualweb.util.UserHolder;

/**
 *
 * @author Dbarreto
 */
public class NecesidadAyudaVista extends CssLayout implements View {

    public static final String VIEW_NAME = "Necesidades";
    Grid<NecesidadAyuda> grillaNecesidadAyuda = new Grid<>(NecesidadAyuda.class);
    TextField txtfFiltro = new TextField("Filtro");
    NecesidadAyudaDao cargoDao = ResourceLocator.locate(NecesidadAyudaDao.class);
    List<NecesidadAyuda> lista = new ArrayList<>();
    Button btnNuevo = new Button("");
    NecesidadAyudaForm cargoForm = new NecesidadAyudaForm();

    public NecesidadAyudaVista() {
        setSizeFull();
        addStyleName("crud-view");

        btnNuevo.addStyleName(MaterialTheme.BUTTON_ROUND + " " +MaterialTheme.BUTTON_PRIMARY);
        btnNuevo.setIcon(VaadinIcons.PLUS_CIRCLE);

        cargoForm.setVisible(false);
        HorizontalLayout topLayout = new HorizontalLayout();
        topLayout.addComponents(txtfFiltro, btnNuevo);
        topLayout.setSpacing(true);
        topLayout.setComponentAlignment(txtfFiltro, Alignment.MIDDLE_LEFT);
        topLayout.setComponentAlignment(btnNuevo, Alignment.MIDDLE_RIGHT);
        topLayout.addStyleName("top-bar");

        txtfFiltro.setPlaceholder("Filtro de búsqueda");
        txtfFiltro.setValueChangeMode(ValueChangeMode.LAZY);
        txtfFiltro.addValueChangeListener(e -> updateList(e.getValue()));

        btnNuevo.addClickListener(e -> {
            grillaNecesidadAyuda.asSingleSelect().clear();
            cargoForm.setNecesidadAyuda(new NecesidadAyuda());
        });

        if ((UserHolder.get().getIdnivelusuario() == 8 || UserHolder.get().getIdnivelusuario() == 9 || UserHolder.get().getIdnivelusuario() == 10)) {
            btnNuevo.setEnabled(true);
        } else {
            btnNuevo.setEnabled(false);
        }

        HorizontalLayout horizontalLayout = new HorizontalLayout();
        horizontalLayout.addComponents(grillaNecesidadAyuda, cargoForm);
        horizontalLayout.setSizeFull();
        horizontalLayout.setExpandRatio(grillaNecesidadAyuda, 1);

        VerticalLayout barAndGridLayout = new VerticalLayout();
        barAndGridLayout.addComponent(topLayout);
        barAndGridLayout.addComponent(horizontalLayout);
        barAndGridLayout.setMargin(true);
        barAndGridLayout.setSpacing(true);
        barAndGridLayout.setSizeFull();
        barAndGridLayout.setExpandRatio(horizontalLayout, 1);
        barAndGridLayout.addStyleName("crud-main-layout");

        try {
            lista = cargoDao.listaNecesidadAyuda();
            grillaNecesidadAyuda.setItems(lista);
            grillaNecesidadAyuda.removeAllColumns();
            grillaNecesidadAyuda.addColumn(NecesidadAyuda::getId).setCaption("COD");
            grillaNecesidadAyuda.addColumn(NecesidadAyuda::getDescripcion).setCaption("Descripción");
            grillaNecesidadAyuda.setSizeFull();
            grillaNecesidadAyuda.asSingleSelect().addValueChangeListener(e -> {
                if ((UserHolder.get().getIdnivelusuario() == 8 || UserHolder.get().getIdnivelusuario() == 9 || UserHolder.get().getIdnivelusuario() == 10)) {
                    if (e.getValue() == null) {
                        cargoForm.setVisible(false);
                    } else {
                        cargoForm.setNecesidadAyuda(e.getValue());
                        cargoForm.setViejo();
                    }
                }
            });

            cargoForm.setGuardarListener(r -> {
                grillaNecesidadAyuda.clearSortOrder();
                lista = cargoDao.listaNecesidadAyuda();
                grillaNecesidadAyuda.setItems(lista);
            });
            cargoForm.setBorrarListener(r -> {
                grillaNecesidadAyuda.clearSortOrder();
            });
            cargoForm.setCancelarListener(r -> {
                grillaNecesidadAyuda.clearSortOrder();
            });

            addComponent(barAndGridLayout);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void updateList(String value) {
        try {
            lista = cargoDao.getNecesidadAyudaByDescripcion(value);
            grillaNecesidadAyuda.setSizeFull();
            grillaNecesidadAyuda.setItems(lista);
            grillaNecesidadAyuda.clearSortOrder();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
