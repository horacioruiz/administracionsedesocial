package py.mutualmsp.mutualweb.vista;

import com.github.appreciated.material.MaterialTheme;
import com.vaadin.event.ShortcutAction;
import com.vaadin.event.ShortcutListener;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.Sizeable;
import com.vaadin.server.StreamResource;
import com.vaadin.ui.*;
import com.vaadin.ui.themes.ValoTheme;
import java.io.ByteArrayInputStream;
import com.vaadin.server.FileResource;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import com.vaadin.server.Page;
import com.vaadin.server.Resource;
import com.vaadin.server.ResourceReference;
import com.vaadin.server.VaadinService;
import java.io.InputStream;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.Period;
import java.time.temporal.ChronoField;
import static java.time.temporal.TemporalAdjusters.firstDayOfYear;
import static java.time.temporal.TemporalAdjusters.lastDayOfYear;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperRunManager;
import org.apache.poi.hssf.util.CellRangeAddress;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import py.mutualmsp.mutualweb.dao.CiudadDao;
import py.mutualmsp.mutualweb.dao.DatoSocioDemograficoDao;
import py.mutualmsp.mutualweb.dao.DepartamentoDao;
import py.mutualmsp.mutualweb.dao.SocioDao;
import py.mutualmsp.mutualweb.dao.MotivosDao;
import py.mutualmsp.mutualweb.dao.NecesidadAyudaDao;
import py.mutualmsp.mutualweb.dao.SolicitudAyudaDao;
import py.mutualmsp.mutualweb.dao.UsuarioDao;
import py.mutualmsp.mutualweb.entities.Ciudad;
import py.mutualmsp.mutualweb.entities.DatosSocioDemograficos;
import py.mutualmsp.mutualweb.entities.Departamento;
//import py.mutualmsp.mutualweb.entities.Feriado;
import py.mutualmsp.mutualweb.entities.NecesidadAyuda;
import py.mutualmsp.mutualweb.entities.Socio;
import py.mutualmsp.mutualweb.entities.SolicitudAyuda;
//import py.mutualmsp.mutualweb.entities.Vacaciones;
import py.mutualmsp.mutualweb.formularios.SolicitudAyudaForm;
import py.mutualmsp.mutualweb.util.Constants;
import py.mutualmsp.mutualweb.util.DateUtils;
import py.mutualmsp.mutualweb.util.ResourceLocator;
import py.mutualmsp.mutualweb.util.UserHolder;

/**
 * Created by Alfre on 23/6/2016.
 */
public class SolicitudAyudaView extends CssLayout implements View {

    public static final String VIEW_NAME = "Solicitudes de Ayuda";
    //Button recibirMail = new Button("Recibir Mail");
    Button editarSolicitudAyuda = new Button("");
    Button imprimirSolicitudAyuda = new Button("Pdf");
    Button imprimirEvaluacion = new Button("");
    Button exportarExcel = new Button("Excel");
    Button aceptarConformidad = new Button("Aceptar Conformidad");
    Button btnSearch = new Button();
//    ConfirmButton eliminarReclamo = new ConfirmButton("Eliminar Reclamo");
    Label labelTotalizador = new Label();

    private DateField fechaDesde = new DateField();
    private DateField fechaHasta = new DateField();

    ComboBox<Ciudad> filterCiudad = new ComboBox<>();
    ComboBox<Departamento> filterDpto = new ComboBox<>();

//    SolicitudAyudaDetalleView formSolicitudDetalle = new SolicitudAyudaDetalleView();
    Button cerrarTicket = new Button("Cerrar Ticket");
    ComboBox<Socio> filterSocio = new ComboBox<>();
    ComboBox<NecesidadAyuda> filterNecesidadAyuda = new ComboBox<>();
    ComboBox<String> filtreEstado = new ComboBox<>();
    ComboBox<Socio> filterAsignado = new ComboBox<>();
    Grid<SolicitudAyuda> grid = new Grid<>(SolicitudAyuda.class);
    Logger log = Logger.getLogger("UserViews");
    String tmpFile = "";
    SolicitudAyudaForm solicitudForm = new SolicitudAyudaForm();

    SocioDao socioController = ResourceLocator.locate(SocioDao.class);
    NecesidadAyudaDao necesidadAyudaDao = ResourceLocator.locate(NecesidadAyudaDao.class);
    SolicitudAyudaDao solicitudAyudaController = ResourceLocator.locate(SolicitudAyudaDao.class);
    DatoSocioDemograficoDao datoSocioDemograficoDao = ResourceLocator.locate(DatoSocioDemograficoDao.class);
    MotivosDao motivoController = ResourceLocator.locate(MotivosDao.class);
    UsuarioDao usuarioController = ResourceLocator.locate(UsuarioDao.class);
    HashMap<Long, String> mapeo = new HashMap<>();

    CiudadDao ciudadDao = ResourceLocator.locate(CiudadDao.class);
    DepartamentoDao dptoDao = ResourceLocator.locate(DepartamentoDao.class);

    Button nuevaSolicitudAyuda = new Button("Nueva Solicitud");

    TextField filter = new TextField();

    private DateField fechaLog = new DateField();
    private TextArea comentarioLog = new TextArea("Comentarios");
    private Button descargar = new Button("Descargar Archivo");
    String filename;
    byte[] content;
    DecimalFormat decimalFormat = new DecimalFormat("###,###,##0");
    SolicitudAyuda solicitudSeleccionado;
    List<SolicitudAyuda> lista = new ArrayList<>();
    ComboBox<String> filterAprobado = new ComboBox<>();

    public SolicitudAyudaView() {
        try {
            System.out.println("Nueva instancia SolicitudAyudaView");
            setSizeFull();
            addStyleName("crud-view");
            HorizontalLayout horizontalLayout = createHorizontalLayout();

            if (UserHolder.get().getIdfuncionario() == null) {
                UserHolder.get().setIdfuncionario(usuarioController.getByUsuario(UserHolder.get().getId()).getIdfuncionario());
            }

            filterAprobado.setWidth(10f, TextField.UNITS_EM);
            filterAprobado.setVisible(false);
            filterNecesidadAyuda.setWidth(12f, TextField.UNITS_EM);
            filtreEstado.setWidth(12f, TextField.UNITS_EM);
            filter.setWidth(8f, TextField.UNITS_EM);
            fechaDesde.setWidth(8f, TextField.UNITS_EM);
            fechaHasta.setWidth(8f, TextField.UNITS_EM);

            btnSearch.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_FRIENDLY);
            btnSearch.setIcon(VaadinIcons.SEARCH);

            fechaDesde.setPlaceholder("Desde");
            fechaHasta.setPlaceholder("Hasta");

            fechaDesde.setValue(DateUtils.asLocalDate(new Date()));
            fechaHasta.setValue(DateUtils.asLocalDate(new Date()));

            nuevaSolicitudAyuda.setVisible(false);

//            eliminarReclamo.setVisible(false);
            labelTotalizador.setCaption("Total de registros: 0");

            aceptarConformidad.setVisible(false);
            cerrarTicket.setVisible(false);
            editarSolicitudAyuda.setVisible(false);
            imprimirEvaluacion.setVisible(true);
            imprimirSolicitudAyuda.setVisible(true);
            filterSocio.setVisible(true);
//            if (UserHolder.get().getIdnivelusuario() == 8 || UserHolder.get().getIdnivelusuario() == 9 || UserHolder.get().getIdnivelusuario() == 10) {
            filterSocio.setItems(socioController.listaSocio());
//            } else {
//                filterSocio.setItems(socioController.listarSocioPorCI(UserHolder.get().getIdfuncionario().getCedula()));
//            }
            List<String> datos = new ArrayList<>();
            datos.add("PENDIENTE");
            datos.add("EN PROCESO DE ANALISIS");
            datos.add("APROBADO");
            datos.add("RECHAZADO");
            datos.add("ANULADO");

            //Arrays.asList(new String("PENDIENTE", new Icon("")), new String("EN PROCESO DE ANALISIS",new Icon("")),new String("Germany",new Icon("")));
            filtreEstado.setItems(datos);
            filtreEstado.setPlaceholder("Filtre estados");

            filtreEstado.setValue("PENDIENTE");
            filterNecesidadAyuda.setItems(necesidadAyudaDao.listaNecesidadAyuda());
            filterNecesidadAyuda.setPlaceholder("Filtre formularios");
            filter.setPlaceholder("Filtre CI");
            filterNecesidadAyuda.setItemCaptionGenerator(NecesidadAyuda::getDescripcion);
            filterSocio.setItemCaptionGenerator(Socio::getNombreCompleto);

            filterCiudad.setItems(ciudadDao.listaCiudad());
            filterCiudad.setPlaceholder("Filtre Ciudad");
            filterCiudad.setItemCaptionGenerator(Ciudad::getDescripcion);

            filterDpto.setItems(dptoDao.listaDepartamento());
            filterDpto.setPlaceholder("Filtre Dpto");
            filterDpto.setItemCaptionGenerator(Departamento::getDescripcion);

            List<String> listEstados = new ArrayList<>();
            listEstados.add("APROBADO");
            listEstados.add("RECHAZADO");
            listEstados.add("PENDIENTE");
            filterAprobado.setItems(listEstados);
            filterAprobado.setPlaceholder("Filtre aprobados");
            filterSocio.setPlaceholder("Filtre Socio");
            filterNecesidadAyuda.setPlaceholder("Filtre Necesidad");
            filterAsignado.setPlaceholder("Filtre Asignado");

            fechaLog.setVisible(false);
            comentarioLog.setVisible(false);

//            fechaDesde.addValueChangeListener(e -> findByAll());
//            fechaHasta.addValueChangeListener(e -> findByAll());
            btnSearch.addClickListener(e -> findByAll());

            //filter.addValueChangeListener(e -> updateList(e.getValue()));
            filter.addShortcutListener(new ShortcutListener("Shortcut", ShortcutAction.KeyCode.ENTER, null) {
                @Override
                public void handleAction(Object sender, Object target) {
                    // Do nice stuff
//                    String num = filter.getValue();
//                    filterSocio.setValue(null);
//                    filterAprobado.setValue(null);
//                    filterNecesidadAyuda.setValue(null);
//                    fechaDesde.setValue(null);
//                    fechaHasta.setValue(null);
//                    cargarMotivoSolicitudAyuda(num);
//                    generarExcel();
                }

//                private void cargarMotivoSolicitudAyuda(String num) {
//                    grid.clearSortOrder();
//                    filter.setValue(num);
//                    try {
//                        SolicitudAyuda s = solicitudAyudaController.listarPorCiSocio(num);
//                        //if (s.getId() == Long.parseLong(num)) {
//                        grid.setItems(s);
//                        //}
//                    } catch (Exception e) {
//                        grid.clearSortOrder();
//                        grid.setItems(new ArrayList<>());
//                        labelTotalizador.setCaption("Total de registros: " + 0);
//                    } finally {
//                    }
//                }
            });

            filtreEstado.setItemIconGenerator(new IconGenerator<String>() {
                @Override
                public Resource apply(String item) {
                    String basepath = VaadinService.getCurrent()
                            .getBaseDirectory().getAbsolutePath();
                    String flagName = null;
                    switch (item) {
                        case "PENDIENTE":
                            flagName = "Pendiente";
                            break;
                        case "EN PROCESO DE ANALISIS":
                            flagName = "EnProceso";
                            break;
                        case "APROBADO":
                            flagName = "Aprobado";
                            break;
                        case "RECHAZADO":
                            flagName = "Rechazado";
                            break;
                        case "ANULADO":
                            flagName = "Anulado";
                            break;
                    }
                    // Image as a file resource
                    String path = basepath + "/WEB-INF/images/" + flagName + "Chico.png";
                    return new FileResource(new File(path));
                }
            });

            SimpleDateFormat formatSinHora = new SimpleDateFormat("dd-MM-yyyy");
            SimpleDateFormat formatConHora = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");

//            if (UserHolder.get().getIdnivelusuario() == 8 || UserHolder.get().getIdnivelusuario() == 9 || UserHolder.get().getIdnivelusuario() == 10Editar) {
//                
//            } else {
//                lista = new ArrayList<>();
//            }
            lista = solicitudAyudaController.listadeSolicitudAyudaByFuncionarioAndUsuario(0, 0, filterSocio.getValue() == null ? null : filterSocio.getValue().getNombreCompleto(),
                    filtreEstado.getValue() == null ? null : filtreEstado.getValue(), filterNecesidadAyuda.getValue() == null ? null : filterNecesidadAyuda.getValue().getDescripcion(), UserHolder.get(), DateUtils.asDate(fechaDesde.getValue()), DateUtils.asDate(fechaHasta.getValue()),
                    filterCiudad.getValue() == null ? null : filterCiudad.getValue().getDescripcion(),
                    filterDpto.getValue() == null ? null : filterDpto.getValue().getDescripcion(),
                    filter.getValue());

            grid.setItems(lista);
            generarExcel();
            labelTotalizador.setCaption("Total de registros: " + lista.size());
            grid.removeAllColumns();
            //grid.addColumn(e -> {
            //  return e.getId();
            //}).setCaption("Cód");
            grid.addColumn(e -> {
                return e.getFecha() == null ? "--" : formatSinHora.format(e.getFecha());
            }).setCaption("Fecha");
            grid.addComponentColumn(e -> {
                return e.getEstadoIcon();
            }).setCaption("Estado");
            grid.addColumn(e -> {
                return e.getSocio() == null ? "--" : e.getSocio().getCedula();
            }).setCaption("Cedula");
            grid.addColumn(e -> {
                return e.getSocio() == null ? "--" : e.getSocio().getNombre();
            }).setCaption("Nombres");
            grid.addColumn(e -> {
                return e.getSocio() == null ? "--" : e.getSocio().getApellido();
            }).setCaption("Apellidos");
            grid.addColumn(e -> {
                return e.getTelefono();
            }).setCaption("Telef");
            grid.addColumn(e -> {
                return e.getCiudad() == null ? "--" : e.getCiudad().getDescripcion();
            }).setCaption("Ciudad");
            grid.addColumn(e -> {
                return e.getDepartamento() == null ? "--" : e.getDepartamento().getDescripcion();
            }).setCaption("Dpto");
            grid.addColumn(e -> {
                return e.getNecesidadAyuda() == null ? "--" : e.getNecesidadAyuda().getDescripcion();
            }).setCaption("Necesidad");
            grid.addColumn(e -> {
                return e.getMotivoSolicitudAyuda() == null ? "--" : e.getMotivoSolicitudAyuda().getDescripcion();
            }).setCaption("Motivo");
            //if (UserHolder.get().getIdfuncionario().getDependencia().getDescripcion().equalsIgnoreCase("DEPARTAMENTO DE RECURSOS HUMANOS")) {
            //   grid.addComponentColumn(this::buildConfirmButton).setCaption("Confirmar");
            // }
            grid.setSizeFull();
//            filterNecesidadAyuda.addValueChangeListener(vcl -> {
//                filter.setValue("");
//                grid.clearSortOrder();
//                if (filterNecesidadAyuda.getValue() != null) {
//                    List<SolicitudAyuda> listTicket = solicitudAyudaController.listadeSolicitudAyudaBySocioAndUsuario(0, 0, filterSocio.getValue() == null ? null : filterSocio.getValue().getNombreCompleto(),
//                            filterAprobado.getValue() == null ? null : filterAprobado.getValue(),
//                            filterNecesidadAyuda.getValue() == null ? null : filterNecesidadAyuda.getValue().getDescripcion(), UserHolder.get());
//                    grid.setItems(listTicket);
//                    labelTotalizador.setCaption("Total de registros: " + listTicket.size());
//                } else {
//                    List<SolicitudAyuda> listTicket = solicitudAyudaController.listadeSolicitudAyudaBySocioAndUsuario(0, 0, filterSocio.getValue() == null ? null : filterSocio.getValue().getNombreCompleto(),
//                            filterAprobado.getValue() == null ? null : filterAprobado.getValue(), filterNecesidadAyuda.getValue() == null ? null : filterNecesidadAyuda.getValue().getDescripcion(), UserHolder.get());
//                    grid.setItems(listTicket);
//                    labelTotalizador.setCaption("Total de registros: " + listTicket.size());
//                }
//            });
//            filterSocio.addValueChangeListener(vcl -> {
//                filter.setValue("");
//                grid.clearSortOrder();
//                if (filterSocio.getValue() != null) {
//                    List<SolicitudAyuda> listTicket = solicitudAyudaController.listadeSolicitudAyudaBySocioAndUsuario(0, 0, filterSocio.getValue() == null ? null : filterSocio.getValue().getNombreCompleto(),
//                            filterAprobado.getValue() == null ? null : filterAprobado.getValue(),
//                            filterNecesidadAyuda.getValue() == null ? null : filterNecesidadAyuda.getValue().getDescripcion(), UserHolder.get());
//                    grid.setItems(listTicket);
//                    labelTotalizador.setCaption("Total de registros: " + listTicket.size());
//                } else {
//                    List<SolicitudAyuda> listTicket = solicitudAyudaController.listadeSolicitudAyudaBySocioAndUsuario(0, 0, filterSocio.getValue() == null ? null : filterSocio.getValue().getNombreCompleto(),
//                            filterAprobado.getValue() == null ? null : filterAprobado.getValue(), filterNecesidadAyuda.getValue() == null ? null : filterNecesidadAyuda.getValue().getDescripcion(), UserHolder.get());
//                    grid.setItems(listTicket);
//                    labelTotalizador.setCaption("Total de registros: " + listTicket.size());
//                }
//            });
//            filterAprobado.addValueChangeListener(vcl -> {
//                filter.setValue("");
//                grid.clearSortOrder();
//                if (filterSocio.getValue() != null) {
//                    List<SolicitudAyuda> listTicket = solicitudAyudaController.listadeSolicitudAyudaBySocioAndUsuario(0, 0, filterSocio.getValue() == null ? null : filterSocio.getValue().getNombreCompleto(),
//                            filterAprobado.getValue() == null ? null : filterAprobado.getValue(), filterNecesidadAyuda.getValue() == null ? null : filterNecesidadAyuda.getValue().getDescripcion(), UserHolder.get());
//                    grid.setItems(listTicket);
//                    labelTotalizador.setCaption("Total de registros: " + listTicket.size());
//                } else {
//                    List<SolicitudAyuda> listTicket = solicitudAyudaController.listadeSolicitudAyudaBySocioAndUsuario(0, 0, filterSocio.getValue() == null ? null : filterSocio.getValue().getNombreCompleto(),
//                            filterAprobado.getValue() == null ? null : filterAprobado.getValue(), filterNecesidadAyuda.getValue() == null ? null : filterNecesidadAyuda.getValue().getDescripcion(), UserHolder.get());
//                    grid.setItems(listTicket);
//                    labelTotalizador.setCaption("Total de registros: " + listTicket.size());
//                }
////                limpiarLog();
//            });

            descargar.setVisible(false);

            VerticalLayout verticalLayout = new VerticalLayout();
            verticalLayout.addComponent(horizontalLayout);
            grid.setSizeFull();
            verticalLayout.addComponent(grid);
            verticalLayout.setMargin(true);
            verticalLayout.setSpacing(true);
            verticalLayout.setSizeFull();
            verticalLayout.setExpandRatio(grid, 6F);
            verticalLayout.addComponent(labelTotalizador);
            verticalLayout.setComponentAlignment(labelTotalizador, Alignment.BOTTOM_RIGHT);
            HorizontalLayout horizontalLayout1 = crearSegundoGrid();
            horizontalLayout1.setSizeFull();
            verticalLayout.addStyleName("crud-main-layout");
            addComponent(verticalLayout);

            grid.addItemClickListener(listener -> {
                if (listener.getMouseEventDetails().isDoubleClick()) {
                    editarSolicitudAyuda.setVisible(false);
                    imprimirEvaluacion.setVisible(true);
                    imprimirSolicitudAyuda.setVisible(true);
                    cerrarTicket.setVisible(false);

//                    formSolicitudDetalle = new SolicitudAyudaDetalleView();
                    long idTicket = listener.getItem().getId();
//                    formSolicitudDetalle.setIdTicket(idTicket);
//                    formSolicitudDetalle.setIdTicket(listener.getItem());
                    System.out.println("EL TICKET ES EL # " + idTicket);
//                    UI.getCurrent().addWindow(formSolicitudDetalle);
//                    formSolicitudDetalle.setVisible(true);
                }
            });
            grid.asSingleSelect().addValueChangeListener(event -> {
                solicitudSeleccionado = event.getValue();
                editarSolicitudAyuda.setVisible(true);
                imprimirEvaluacion.setVisible(true);
                imprimirSolicitudAyuda.setVisible(true);
            });
            editarSolicitudAyuda.addClickListener(clickEvent -> {
                if (solicitudSeleccionado != null) {
                    try {

                        SolicitudAyudaForm soliForm = new SolicitudAyudaForm();
                        try {
                            SolicitudAyuda lic = solicitudAyudaController.listarPorId(solicitudSeleccionado.getId());
                            if (lic.getId() != null) {
                                soliForm.editarRegistro(lic);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            soliForm.editarRegistro(solicitudAyudaController.listarPorId(solicitudSeleccionado.getId()));
                        } finally {
                        }

                        UI.getCurrent().addWindow(soliForm);
                        soliForm.setVisible(true);

                        soliForm.setCancelListener(reclamo -> {
                            grid.clearSortOrder();
                        });

                        soliForm.setSaveListener(reclamo -> {
                            grid.clearSortOrder();
                            //if (UserHolder.get().getIdnivelusuario() == 8 || UserHolder.get().getIdnivelusuario() == 9 || UserHolder.get().getIdnivelusuario() == 10Editar) {
                            lista = solicitudAyudaController.listadeSolicitudAyudaSinConfirmar();
                            //} else {
                            //    lista = new ArrayList<>();
                            //}
                            grid.setItems(lista);
                            labelTotalizador.setCaption("Total de registros: " + lista.size());
                        });
                    } catch (Exception e) {
                        SolicitudAyudaForm soliForm = new SolicitudAyudaForm();
                        try {
                            //  soliForm.editarRegistro(solicitudAyudaController.listarPorId(solicitudSeleccionado.getId()));
                        } catch (Exception ex) {
                            //  soliForm.editarRegistro(solicitudAyudaController.listarPorIdSocio(solicitudSeleccionado.getId()));
                        } finally {
                        }
                        UI.getCurrent().addWindow(soliForm);
                        soliForm.setVisible(true);

                        soliForm.setCancelListener(reclamo -> {
                            grid.clearSortOrder();
                        });

                        soliForm.setSaveListener(reclamo -> {
                            grid.clearSortOrder();
//                            if (UserHolder.get().getIdnivelusuario() == 8 || UserHolder.get().getIdnivelusuario() == 9 || UserHolder.get().getIdnivelusuario() == 10) {
                            lista = solicitudAyudaController.listadeSolicitudAyudaSinConfirmar();
//                            } else {
//                                lista = new ArrayList<>();
//                            }
                            grid.setItems(lista);
                            labelTotalizador.setCaption("Total de registros: " + lista.size());
                        });
                    } finally {
                    }

                }
            });
            imprimirEvaluacion.addClickListener(clickEvent -> {
                if (solicitudSeleccionado != null) {
                    imprimirEvaluacionSocioeco();
                } else {
                    imprimirEvaluacionSocioecoTodos();
                }
            });
            imprimirSolicitudAyuda.addClickListener(clickEvent -> {
//                    try {
//                        if (solicitudSeleccionado.getConfirmado()) {
//                        if (solicitudSeleccionado.getEncargado() != null) {
                if (lista.isEmpty()) {
                    Notification.show("La grilla debe estar cargada, para realizar la impresion.", Notification.Type.HUMANIZED_MESSAGE);
                } else {
                    imrpimirPDF();
                }

//                        } else {
//                            Notification.show("La licencia no ha sido confirmada, no es posible imprimirla.", Notification.Type.HUMANIZED_MESSAGE);
//                        }
//                    } catch (Exception e) {
//                        imrpimirPDF();
//                    } finally {
//                    }
            });
            exportarExcel.addClickListener(clickEvent -> {
                if (lista.isEmpty()) {
                    Notification.show("La grilla debe estar cargada, para realizar la exportacion.", Notification.Type.HUMANIZED_MESSAGE);
                } else {
                    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd_hhmmss");
                    Date date = new Date(System.currentTimeMillis());
                    //tmpFile = Constants.UPLOAD_DIR + "\\mutual-archivos\\tmp\\" + filterFunci.getValue().getNombreCompleto() + "_" + formatter.format(date) + ".xlsx";

                    // Page.getCurrent().open("http://www.example.com", "_blank", false);
                    final String basepath = tmpFile;

                    Resource pdf = new FileResource(new File(tmpFile));

                    /*Random random = new Random();
                int num = random.nextInt(8000);
                setResource(filterFunci.getValue().getNombreCompleto() + "_" + formatter.format(date) + "_" + num, pdf);
                ResourceReference rr = ResourceReference.create(pdf, this, filterFunci.getValue().getNombreCompleto() + "_" + formatter.format(date) + "_" + random.nextInt(8000));
                String here = rr.getURL();
                Page.getCurrent().open(here, "blank_");*/
                    //setResource("help", pdf);
                    ResourceReference resourceReference = ResourceReference.create(pdf,
                            this, "help");

                    getState().resources.remove("help");
                    getState().resources.put("help", resourceReference);

                    ResourceReference rr = ResourceReference.create(pdf, this, "help");
                    String here = rr.getURL();
                    Page.getCurrent().open(here, "blank_");

                    log.info("Descargando Excel...");
                }
            });

            nuevaSolicitudAyuda.addClickListener(clickEvent -> {
                LocalDate now = LocalDate.now(); // 2015-11-23
                LocalDate firstDay = now.with(firstDayOfYear()); // 2015-01-01
                LocalDate lastDay = now.with(lastDayOfYear());
                LocalDate today = LocalDate.now();

                LocalDate ld = now.plusYears(1L);
                LocalDate firstDaySecond = ld.with(firstDayOfYear()); // 2015-01-01
                LocalDate lastDaySecond = ld.with(lastDayOfYear());

//                List<Feriado> listFeriado = feriadoDao.listarFeriadoPorPeriodo(DateUtils.asDate(firstDay), DateUtils.asDate(lastDay));
//
//                List<Feriado> listFeriadoSecond = feriadoDao.listarFeriadoPorPeriodo(DateUtils.asDate(firstDaySecond), DateUtils.asDate(lastDaySecond));
//                if (listFeriado.size() == 0) {
//                    Notification.show("Mensaje del Sistema", "Es necesario crear los feriados correspondiente al año " + today.getYear() + ".", Notification.Type.HUMANIZED_MESSAGE);
//                } else if (listFeriadoSecond.size() == 0 && today.getMonthValue() == 12) {
//                    Notification.show("Mensaje del Sistema", "Es necesario crear los feriados correspondiente al año " + (today.getYear() + 1) + ".", Notification.Type.HUMANIZED_MESSAGE);
//                } else {
                SolicitudAyudaForm solicitudForm = new SolicitudAyudaForm();
                UI.getCurrent().addWindow(solicitudForm);

                solicitudForm.nuevoRegistro();
                solicitudForm.setVisible(true);
                solicitudForm.setCancelListener(reclamo -> {
                    //form = null;
                    grid.clearSortOrder();
                });
                solicitudForm.setSaveListener(reclamo -> {
                    grid.clearSortOrder();
//                    if (UserHolder.get().getIdnivelusuario() == 8 || UserHolder.get().getIdnivelusuario() == 9 || UserHolder.get().getIdnivelusuario() == 10) {
                    lista = solicitudAyudaController.listadeSolicitudAyudaSinConfirmar();
//                    } else {
//                        lista = new ArrayList<>();
//                    }
                    grid.setItems(lista);
                    labelTotalizador.setCaption("Total de registros: " + lista.size());
                });
//                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String convertStreamToString(java.io.InputStream is) {
        java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
        return s.hasNext() ? s.next() : "";
    }

    private HorizontalLayout crearSegundoGrid() {
        HorizontalLayout layout = new HorizontalLayout();
        VerticalLayout verticalLayout = new VerticalLayout();
        HorizontalLayout horizontalLayout = new HorizontalLayout();
        horizontalLayout.setSpacing(true);

        horizontalLayout.addComponent(descargar);
        verticalLayout.addComponent(horizontalLayout);
        CssLayout expander1 = new CssLayout();
        expander1.setSizeFull();
        expander1.setStyleName("expander");
        verticalLayout.addComponent(expander1);
        verticalLayout.setExpandRatio(expander1, 0.9F);
        verticalLayout.addComponent(fechaLog);
        comentarioLog.setSizeFull();
        verticalLayout.addComponent(comentarioLog);
        verticalLayout.setSpacing(false);

        layout.addComponent(verticalLayout);
        layout.setSpacing(true);
        return layout;
    }

    private void cargarMotivoSolicitudAyuda(String num) {
        grid.clearSortOrder();
        filter.setValue(num);
        try {
            SolicitudAyuda s = solicitudAyudaController.listarPorIdHere(Long.parseLong(num));
            if (s.getId() == Long.parseLong(num)) {
                grid.setItems(s);
                lista.add(s);
            }
        } catch (Exception e) {
            grid.clearSortOrder();
            grid.setItems(new ArrayList<>());
            lista = (new ArrayList<>());
            labelTotalizador.setCaption("Total de registros: " + 0);
        } finally {
        }
    }

    public void generarExcel() {
        try {
            creandoExcel();
        } catch (JSONException e) {
            System.out.println("-> " + e.getLocalizedMessage());
            System.out.println("-> " + e.fillInStackTrace());
            e.printStackTrace();
        }
        // TODO Auto-generated catch block

        // TODO Auto-generated catch block
    }

    private void findByAll() {
//        if (!filter.getValue().trim().equals("")) {
//            //if (UserHolder.get().getIdnivelusuario() == 8 || UserHolder.get().getIdnivelusuario() == 9 || UserHolder.get().getIdnivelusuario() == 10Editar) {
//            if (filter.getValue() != null) {
//                String num = filter.getValue();
//                filterSocio.setValue(null);
//                filterAprobado.setValue(null);
//                filterNecesidadAyuda.setValue(null);
//                fechaDesde.setValue(null);
//                fechaHasta.setValue(null);
//
//                grid.clearSortOrder();
//                filter.setValue(num);
//                lista = new ArrayList<>();
//                try {
//                    SolicitudAyuda s = solicitudAyudaController.listarPorCiSocio(num);
//                    //if (s.getId() == Long.parseLong(num)) {
//                    grid.setItems(s);
//                    lista.add(s);
//                    //}
//                } catch (Exception e) {
//                    grid.clearSortOrder();
//                    grid.setItems(new ArrayList<>());
//                    labelTotalizador.setCaption("Total de registros: " + 0);
//                } finally {
//                }
//
//                generarExcel();
//            }
//            //}
//        } else {
        //        DateUtils.asDate(fechaDesde.getValue())
        if (fechaDesde.getValue() != null && fechaHasta.getValue() != null) {
            if (fechaDesde.getValue().isBefore(fechaHasta.getValue()) || fechaDesde.getValue().isEqual(fechaHasta.getValue())) {
                filtroConFecha();
                generarExcel();
            } else {
                Notification.show("Mensaje del Sistema", "Fecha hasta debe ser mayor o igual a fecha desde.", Notification.Type.HUMANIZED_MESSAGE);
            }
        } else {
            Notification.show("Mensaje del Sistema", "Fecha desde y fecha hasta no deben quedar vacíos.", Notification.Type.HUMANIZED_MESSAGE);
        }
//        }
    }

    private void filtroConFecha() {
//        filter.setValue("");
        grid.clearSortOrder();
        lista = new ArrayList<>();
//        if (filterSocio.getValue() != null) {
//            List<SolicitudAyuda> listTicket = solicitudAyudaController.listadeSolicitudAyudaByFuncionarioAndUsuario(0, 0, filterSocio.getValue() == null ? null : filterSocio.getValue().getNombreCompleto(),
//                    filtreEstado.getValue() == null ? null : filtreEstado.getValue(),
//                    filterNecesidadAyuda.getValue() == null ? null : filterNecesidadAyuda.getValue().getDescripcion(), UserHolder.get(), DateUtils.asDate(fechaDesde.getValue()), DateUtils.asDate(fechaHasta.getValue()),
//                    filterCiudad.getValue() == null ? null : filterCiudad.getValue().getDescripcion(),
//                    filterDpto.getValue() == null ? null : filterDpto.getValue().getDescripcion(),
//                    filter.getValue());
//            try {
//                grid.setItems(listTicket);
//                lista = listTicket;
//            } catch (Exception e) {
//                grid.setItems(new ArrayList<>());
//            } finally {
//            }
//            labelTotalizador.setCaption("Total de registros: " + listTicket.size());
//        } else {
        List<SolicitudAyuda> listTicket = solicitudAyudaController.listadeSolicitudAyudaByFuncionarioAndUsuario(0, 0, filterSocio.getValue() == null ? null : filterSocio.getValue().getNombreCompleto(),
                filtreEstado.getValue() == null ? null : filtreEstado.getValue(), filterNecesidadAyuda.getValue() == null ? null : filterNecesidadAyuda.getValue().getDescripcion(), UserHolder.get(), DateUtils.asDate(fechaDesde.getValue()), DateUtils.asDate(fechaHasta.getValue()),
                filterCiudad.getValue() == null ? null : filterCiudad.getValue().getDescripcion(),
                filterDpto.getValue() == null ? null : filterDpto.getValue().getDescripcion(),
                filter.getValue());
        try {
            grid.setItems(listTicket);
            lista = listTicket;
        } catch (Exception e) {
            grid.setItems(new ArrayList<>());
        } finally {
        }

        labelTotalizador.setCaption("Total de registros: " + listTicket.size());
//        }
    }

    private HorizontalLayout createHorizontalLayout() {
        exportarExcel.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_PRIMARY);
        editarSolicitudAyuda.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_FRIENDLY);
        imprimirEvaluacion.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_FRIENDLY);
        imprimirEvaluacion.setIcon(VaadinIcons.LIST);
        editarSolicitudAyuda.setIcon(VaadinIcons.EDIT);
        imprimirSolicitudAyuda.addStyleName(MaterialTheme.BUTTON_ROUND);
        aceptarConformidad.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_FRIENDLY);
        aceptarConformidad.setIcon(FontAwesome.PARAGRAPH);
        cerrarTicket.addStyleName(MaterialTheme.BUTTON_ROUND + " " + ValoTheme.BUTTON_DANGER);
        cerrarTicket.setIcon(FontAwesome.EDIT);

        HorizontalLayout layout = new HorizontalLayout();
        layout.setSpacing(true);
        layout.setWidth("100%");
        HorizontalLayout layoutIzquierdo = new HorizontalLayout();
        layoutIzquierdo.addComponent(filterSocio);
        layoutIzquierdo.addComponent(filtreEstado);
        layoutIzquierdo.setExpandRatio(filtreEstado, 2);
        layoutIzquierdo.addComponent(filter);
        layoutIzquierdo.setExpandRatio(filter, 2);
        layoutIzquierdo.addComponent(fechaDesde);
        layoutIzquierdo.setExpandRatio(fechaDesde, 2);
        layoutIzquierdo.addComponent(fechaHasta);
        layoutIzquierdo.setExpandRatio(fechaHasta, 2);

        VerticalLayout verticalLayoutIzquierda = new VerticalLayout();
        verticalLayoutIzquierda.addComponent(layoutIzquierdo);

        HorizontalLayout layoutFuncEstado = new HorizontalLayout();
        layoutFuncEstado.addComponent(filterNecesidadAyuda);
        layoutFuncEstado.setExpandRatio(filterNecesidadAyuda, 2);
        layoutFuncEstado.addComponent(filterCiudad);
        layoutFuncEstado.setExpandRatio(filterCiudad, 2);
        layoutFuncEstado.addComponent(filterDpto);
        layoutFuncEstado.setExpandRatio(filterDpto, 2);
        layoutFuncEstado.addComponent(btnSearch);
        layoutFuncEstado.setExpandRatio(btnSearch, 2);
        layoutFuncEstado.addComponent(editarSolicitudAyuda);
        layoutFuncEstado.setExpandRatio(editarSolicitudAyuda, 2);
        layoutFuncEstado.addComponent(imprimirEvaluacion);
        layoutFuncEstado.setExpandRatio(imprimirEvaluacion, 2);
        layoutFuncEstado.addComponent(exportarExcel);
        layoutFuncEstado.setExpandRatio(exportarExcel, 2);
        layoutFuncEstado.addComponent(imprimirSolicitudAyuda);
        layoutFuncEstado.setExpandRatio(imprimirSolicitudAyuda, 2);
        verticalLayoutIzquierda.addComponent(layoutFuncEstado);

        layout.addComponent(verticalLayoutIzquierda);

        layout.setStyleName("top-bar");
        return layout;
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent viewChangeEvent) {

    }

    private void fileReceived(String filename, String mime, byte[] content) {
        this.filename = filename;
        this.content = content;
    }

    public void limpiarLog() {
        grid.clearSortOrder();
        comentarioLog.setValue("");
        fechaLog.setValue(null);
        fechaLog.setEnabled(false);
        comentarioLog.setEnabled(false);
    }

    public static String ordenandoFechaString(String fecha) {
        String[] fechaSplit = fecha.split("-");
        if (fechaSplit[0].length() == 4) {
            return fechaSplit[2] + "-" + fechaSplit[1] + "-" + fechaSplit[0];
        } else {
            return fecha;
        }
    }

    private Button buildConfirmButton(SolicitudAyuda p) {
        Button button = new Button(VaadinIcons.CHECK_SQUARE_O);
        /* if (p.getAprobado() == 0) {
            button.setEnabled(true);
            button.setVisible(true);
        } else {
            button.setEnabled(false);
            button.setVisible(false);
        }*/
        button.addStyleName(ValoTheme.BUTTON_SMALL + " " + MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_PRIMARY);
        button.addClickListener(e -> aprobarSolicitudAyuda(p));
        return button;
    }

    private void cargarVacacionesDisponible(SolicitudAyuda s) {
        // List<Vacaciones> listVacaciones = vacacionesDao.listadeVacaciones(s.getSocio().getIdfuncionario());
        //for (Vacaciones listado : listVacaciones) {
        //  mapeo.put(listado.getId(), listado.getCantdiavaca() + "-" + listado.getCantdiatomada());
        //}
    }

    private void aprobarSolicitudAyuda(SolicitudAyuda p) {

    }

    public static boolean isWeekendSunday(LocalDate date) {
        DayOfWeek dayOfWeek = DayOfWeek.of(date.get(ChronoField.DAY_OF_WEEK));
        switch (dayOfWeek) {
            case SUNDAY:
                return true;
            default:
                return false;
        }
    }

    public static boolean isWeekendSaturday(LocalDate date) {
        DayOfWeek dayOfWeek = DayOfWeek.of(date.get(ChronoField.DAY_OF_WEEK));
        switch (dayOfWeek) {
            case SATURDAY:
                return true;
            default:
                return false;
        }
    }

    private void guardarVacaciones(SolicitudAyuda solicitud, long tomada, long restante) {
        Calendar cal = Calendar.getInstance();

//        Vacaciones vacas = new Vacaciones();
//        vacas.setPeriodo(cal.get(Calendar.YEAR) + "");
//        vacas.setCantdiavaca(0l);
//        vacas.setCantdiatomada(tomada);
//        vacas.setCantdiarestante(restante - tomada);
//        vacas.setSocio(solicitud.getSocio());
//        vacas.setSolicitudAyuda(solicitud);
//        vacacionesDao.guardar(vacas);
    }

    private void imrpimirPDF() {
        Map pSQL = new HashMap<String, Object>();
        InputStream subImg = getClass().getClassLoader().getResourceAsStream("img/logo.png");
//                    String val = "{\"ventas\": " + recuperarTickets() + "}";
//        pSQL.put("repJsonString", "{\"ventas\": []}");

        pSQL.put("repJsonString", "{\"ventas\": " + cargarSolicitudes() + "}");

//                pSQL.put("subRepNomFun", UserHolder.get().getFuncionario().getNombreCompleto());
        Date date = new Date();
        Timestamp ts = new Timestamp(date.getTime());
        String fechaArray[] = ts.toString().split(" ");
        String subRepTimestamp = ordenandoFechaString(fechaArray[0]) + " " + fechaArray[1];
        pSQL.put("subRepTimestamp", subRepTimestamp);
        pSQL.put("subRepNomFun", UserHolder.get().getUsuario());
        pSQL.put("subRepEmpresa", "MUTUAL NAC DE FUNC DEL MINISTERIO DE SALUD PÚBLICA Y BIENESTAR SOCIAL ");
//        pSQL.put("subRepSucursal", "FORMULARIO DE PERMISO");
        pSQL.put("subRepSucursal", "CASA CENTRAL");
        pSQL.put("subRepPathLogo", subImg);
        pSQL.put("subRepPathLogoCP", subImg);
        StreamResource.StreamSource source = new StreamResource.StreamSource() {

            public InputStream getStream() {
                byte[] b = null;
                String archivo = "solicitudayuda";
                try {
                    b = JasperRunManager.runReportToPdf(getClass().getClassLoader().getResourceAsStream(archivo + ".jasper"), pSQL);
//                            b = JasperRunManager.runReportToPdf("C:\\Users\\hruiz\\Documents\\reporte\\ticket_mutual.jasper", pSQL);
                } catch (JRException ex) {
                    ex.printStackTrace();
                }
                return new ByteArrayInputStream(b);
            }
        };

        String archivo = "solicitudayuda";

        StreamResource resource = new StreamResource(source, archivo + ".pdf");
        resource.setCacheTime(0);
        resource.setMIMEType("application/pdf");

        Window window = new Window();
        window.setWidth(800, Sizeable.Unit.PIXELS);
        window.setHeight(600, Sizeable.Unit.PIXELS);
        window.setModal(true);
        window.center();
        BrowserFrame pdf = new BrowserFrame("test", resource);
        pdf.setSizeFull();

        window.setContent(pdf);
        getUI().addWindow(window);
    }

    private JSONArray cargarLicenciaCompensar() {
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        SimpleDateFormat formatHor = new SimpleDateFormat("HH:mm");
        SimpleDateFormat sdfHM = new SimpleDateFormat("HH.mm");
        SimpleDateFormat sdf = new SimpleDateFormat("mm");
        JSONParser parser = new JSONParser();
        JSONArray jsonArrayDato = new JSONArray();
        /*List<SolicitudAyudaCompensar> solicitudDetalle = solicitudDetalleController.listarPorIdLicencia(solicitudSeleccionado.getId());
        int num = 0;
        for (SolicitudAyudaCompensar solicitudProduccionDetalle : solicitudDetalle) {
            JSONObject jsonObj = new JSONObject();

            int minFin = ((solicitudProduccionDetalle.getHorafin().getHours() * 60) + solicitudProduccionDetalle.getHorafin().getMinutes());
            int minInicio = ((solicitudProduccionDetalle.getHoraini().getHours() * 60) + solicitudProduccionDetalle.getHoraini().getMinutes());

            int minFinHere = ((solicitudProduccionDetalle.getLicencia().getHorafin().getHours() * 60) + solicitudProduccionDetalle.getLicencia().getHorafin().getMinutes());
            int minInicioHere = ((solicitudProduccionDetalle.getLicencia().getHoraini().getHours() * 60) + solicitudProduccionDetalle.getLicencia().getHoraini().getMinutes());

            jsonObj.put("codigo", solicitudSeleccionado.getId());
            jsonObj.put("funcionario", solicitudProduccionDetalle.getLicencia().getNombrefuncionario());
            jsonObj.put("area", solicitudProduccionDetalle.getLicencia().getAreafunc().toUpperCase());
            jsonObj.put("seccion", solicitudProduccionDetalle.getLicencia().getCargofunc().toUpperCase());*/
//            jsonObj.put("fechaIngreso", formatter.format(solicitudSeleccionado.getFechacreacion()));

        /* jsonObj.put("fechaPermiso", formatter.format(solicitudProduccionDetalle.getLicencia().getFechaini()));
            jsonObj.put("desdePermiso", formatHor.format(solicitudProduccionDetalle.getLicencia().getHoraini()));
            jsonObj.put("hastaPermiso", formatHor.format(solicitudProduccionDetalle.getLicencia().getHorafin()));
            try {
                long millis = (minFinHere - minInicioHere);
                jsonObj.put("cantPermiso", sdfHM.format(sdf.parse(millis + "")));
            } catch (ParseException ex) {
                jsonObj.put("cantPermiso", "00.00");
            }
            jsonObj.put("motivoPermiso", solicitudProduccionDetalle.getLicencia().getMotivo());

            jsonObj.put("fechaCompensacion", formatter.format(solicitudProduccionDetalle.getFechacompensar()));
            jsonObj.put("desdeCompensacion", formatHor.format(solicitudProduccionDetalle.getHoraini()));
            jsonObj.put("hastaCompensacion", formatHor.format(solicitudProduccionDetalle.getHorafin()));
            try {
                long millis = minFin - minInicio;
                jsonObj.put("cantCompensacion", sdfHM.format(sdf.parse(millis + "")));
            } catch (ParseException ex) {
                jsonObj.put("cantCompensacion", "00.00");
            }
            jsonObj.put("tareaCompensacion", solicitudProduccionDetalle.getObservacion());

            jsonObj.put("num", (num + 1));
            num++;

            jsonArrayDato.add(jsonObj);
        }*/
        return jsonArrayDato;
    }

    private JSONArray cargarLicenciaSinCompensar() {
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        SimpleDateFormat formatHor = new SimpleDateFormat("HH:mm");
        SimpleDateFormat sdfHM = new SimpleDateFormat("HH.mm");
        JSONParser parser = new JSONParser();
        JSONArray jsonArrayDato = new JSONArray();
        /*        List<SolicitudAyudaCompensar> solicitudDetalle = solicitudDetalleController.listarPorIdLicencia(solicitudSeleccionado.getId());
        int num = 0;
        for (SolicitudAyudaCompensar solicitudProduccionDetalle : solicitudDetalle) {
            JSONObject jsonObj = new JSONObject();

            jsonObj.put("codigo", solicitudSeleccionado.getId());
            jsonObj.put("funcionario", solicitudProduccionDetalle.getLicencia().getNombrefuncionario());
            jsonObj.put("area", solicitudProduccionDetalle.getLicencia().getAreafunc().toUpperCase());
            jsonObj.put("seccion", solicitudProduccionDetalle.getLicencia().getCargofunc().toUpperCase());
//            jsonObj.put("fechaIngreso", formatter.format(solicitudSeleccionado.getFechacreacion()));

            switch (solicitudProduccionDetalle.getLicencia().getMotivoSolicitudAyuda().getCodigo().toLowerCase()) {
                case "licencia_matrimonio":
                    jsonObj.put("marca1", "X");
                    jsonObj.put("desde1", formatter.format(solicitudProduccionDetalle.getLicencia().getFechaini()));
                    jsonObj.put("hasta1", formatter.format(solicitudProduccionDetalle.getLicencia().getFechafin()));
                    jsonObj.put("marca2", " ");
                    jsonObj.put("desde2", " ");
                    jsonObj.put("hasta2", " ");
                    jsonObj.put("marca3", " ");
                    jsonObj.put("desde3", " ");
                    jsonObj.put("hasta3", " ");
                    jsonObj.put("marca4", " ");
                    jsonObj.put("desde4", " ");
                    jsonObj.put("hasta4", " ");
                    jsonObj.put("marca5", " ");
                    jsonObj.put("desde5", " ");
                    jsonObj.put("hasta5", " ");
                    jsonObj.put("marca6", " ");
                    jsonObj.put("desde6", " ");
                    jsonObj.put("hasta6", " ");
                    break;
                case "licencia_duelo":
                    jsonObj.put("marca2", "X");
                    jsonObj.put("desde2", formatter.format(solicitudProduccionDetalle.getLicencia().getFechaini()));
                    jsonObj.put("hasta2", formatter.format(solicitudProduccionDetalle.getLicencia().getFechafin()));
                    jsonObj.put("marca1", " ");
                    jsonObj.put("desde1", " ");
                    jsonObj.put("hasta1", " ");
                    jsonObj.put("marca3", " ");
                    jsonObj.put("desde3", " ");
                    jsonObj.put("hasta3", " ");
                    jsonObj.put("marca4", " ");
                    jsonObj.put("desde4", " ");
                    jsonObj.put("hasta4", " ");
                    jsonObj.put("marca5", " ");
                    jsonObj.put("desde5", " ");
                    jsonObj.put("hasta5", " ");
                    jsonObj.put("marca6", " ");
                    jsonObj.put("desde6", " ");
                    jsonObj.put("hasta6", " ");
                    break;
                case "estudio_pap":
                    jsonObj.put("marca3", "X");
                    jsonObj.put("desde3", formatter.format(solicitudProduccionDetalle.getLicencia().getFechaini()));
                    jsonObj.put("hasta3", formatter.format(solicitudProduccionDetalle.getLicencia().getFechafin()));
                    jsonObj.put("marca1", " ");
                    jsonObj.put("desde1", " ");
                    jsonObj.put("hasta1", " ");
                    jsonObj.put("marca2", " ");
                    jsonObj.put("desde2", " ");
                    jsonObj.put("hasta2", " ");
                    jsonObj.put("marca4", " ");
                    jsonObj.put("desde4", " ");
                    jsonObj.put("hasta4", " ");
                    jsonObj.put("marca5", " ");
                    jsonObj.put("desde5", " ");
                    jsonObj.put("hasta5", " ");
                    jsonObj.put("marca6", " ");
                    jsonObj.put("desde6", " ");
                    jsonObj.put("hasta6", " ");
                    break;
                case "reposo_medico":
                    jsonObj.put("marca4", "X");
                    jsonObj.put("desde4", formatter.format(solicitudProduccionDetalle.getLicencia().getFechaini()));
                    jsonObj.put("hasta4", formatter.format(solicitudProduccionDetalle.getLicencia().getFechafin()));
                    jsonObj.put("marca1", " ");
                    jsonObj.put("desde1", " ");
                    jsonObj.put("hasta1", " ");
                    jsonObj.put("marca2", " ");
                    jsonObj.put("desde2", " ");
                    jsonObj.put("hasta2", " ");
                    jsonObj.put("marca3", " ");
                    jsonObj.put("desde3", " ");
                    jsonObj.put("hasta3", " ");
                    jsonObj.put("marca5", " ");
                    jsonObj.put("desde5", " ");
                    jsonObj.put("hasta5", " ");
                    jsonObj.put("marca6", " ");
                    jsonObj.put("desde6", " ");
                    jsonObj.put("hasta6", " ");
                    break;
                case "cambio_rotacion":
                    jsonObj.put("marca5", "X");
                    jsonObj.put("desde5", formatter.format(solicitudProduccionDetalle.getLicencia().getFechaini()));
                    jsonObj.put("hasta5", formatter.format(solicitudProduccionDetalle.getLicencia().getFechafin()));
                    jsonObj.put("marca1", " ");
                    jsonObj.put("desde1", " ");
                    jsonObj.put("hasta1", " ");
                    jsonObj.put("marca2", " ");
                    jsonObj.put("desde2", " ");
                    jsonObj.put("hasta2", " ");
                    jsonObj.put("marca3", " ");
                    jsonObj.put("desde3", " ");
                    jsonObj.put("hasta3", " ");
                    jsonObj.put("marca4", " ");
                    jsonObj.put("desde4", " ");
                    jsonObj.put("hasta4", " ");
                    jsonObj.put("marca6", " ");
                    jsonObj.put("desde6", " ");
                    jsonObj.put("hasta6", " ");
                    break;
                case "permiso_sin_goce_salario":
                    jsonObj.put("marca6", "X");
                    jsonObj.put("desde6", formatter.format(solicitudProduccionDetalle.getLicencia().getFechaini()));
                    jsonObj.put("hasta6", formatter.format(solicitudProduccionDetalle.getLicencia().getFechafin()));
                    jsonObj.put("marca1", " ");
                    jsonObj.put("desde1", " ");
                    jsonObj.put("hasta1", " ");
                    jsonObj.put("marca2", " ");
                    jsonObj.put("desde2", " ");
                    jsonObj.put("hasta2", " ");
                    jsonObj.put("marca3", " ");
                    jsonObj.put("desde3", " ");
                    jsonObj.put("hasta3", " ");
                    jsonObj.put("marca4", " ");
                    jsonObj.put("desde4", " ");
                    jsonObj.put("hasta4", " ");
                    jsonObj.put("marca5", " ");
                    jsonObj.put("desde5", " ");
                    jsonObj.put("hasta5", " ");
                    break;
            }

            jsonObj.put("num", (num + 1));
            num++;

            jsonArrayDato.add(jsonObj);
        }*/
        return jsonArrayDato;
    }

    private void updateList(String value) {
        if (value.equalsIgnoreCase("")) {
//            if (UserHolder.get().getIdnivelusuario() == 8 || UserHolder.get().getIdnivelusuario() == 9 || UserHolder.get().getIdnivelusuario() == 10) {
            lista = solicitudAyudaController.listadeSolicitudAyudaSinConfirmar();
//            } else {
//                lista = new ArrayList<>();
//            }
            grid.clearSortOrder();
            grid.setItems(lista);
        }
    }

    private JSONArray cargarSolicitudes() {
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        SimpleDateFormat formatterHora = new SimpleDateFormat("HH:mm");
        JSONParser parser = new JSONParser();
        JSONArray jsonArrayDato = new JSONArray();
        for (SolicitudAyuda solicitudAyuda : lista) {
            JSONObject jsonObj = new JSONObject();
            try {
                jsonObj.put("fecha", formatter.format(solicitudAyuda.getFecha()));
            } catch (Exception e) {
                jsonObj.put("fecha", "--");
            } finally {
            }
            try {
                jsonObj.put("cedula", solicitudAyuda.getSocio().getCedula());
            } catch (Exception e) {
                jsonObj.put("cedula", "--");
            } finally {
            }
            try {
                jsonObj.put("nombres", solicitudAyuda.getSocio().getNombre().toUpperCase());
            } catch (Exception e) {
                jsonObj.put("nombres", "--");
            } finally {
            }
            try {
                jsonObj.put("apellidos", solicitudAyuda.getSocio().getApellido().toUpperCase());
            } catch (Exception e) {
                jsonObj.put("apellidos", "--");
            } finally {
            }
            try {
                jsonObj.put("telefono", solicitudAyuda.getTelefono());
            } catch (Exception e) {
                jsonObj.put("telefono", "--");
            } finally {
            }
            try {
                jsonObj.put("ciudad", solicitudAyuda.getCiudad().getDescripcion().toUpperCase());
            } catch (Exception e) {
                jsonObj.put("ciudad", "--");
            } finally {
            }
            try {
                jsonObj.put("atendido", solicitudAyuda.getAtendidopor().toUpperCase());
            } catch (Exception e) {
                jsonObj.put("atendido", "--");
            } finally {
            }
            try {
                jsonObj.put("estado", solicitudAyuda.getEstado().toUpperCase());
            } catch (Exception e) {
                jsonObj.put("estado", "--");
            } finally {
            }
            try {
                jsonObj.put("departamento", solicitudAyuda.getDepartamento().getDescripcion().toUpperCase());
            } catch (Exception e) {
                jsonObj.put("departamento", "--");
            } finally {
            }
            try {
                jsonObj.put("necesidad", solicitudAyuda.getNecesidadAyuda().getDescripcion());
            } catch (Exception e) {
                jsonObj.put("necesidad", "--");
            } finally {
            }
            try {
                jsonObj.put("motivo", solicitudAyuda.getMotivoSolicitudAyuda().getDescripcion());
            } catch (Exception e) {
                jsonObj.put("motivo", "--");
            } finally {
            }
            jsonArrayDato.add(jsonObj);
        }

        return jsonArrayDato;
    }

    private void creandoExcel() {
        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet("Report");
        Row row = null;
        int here = 0;

        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        for (SolicitudAyuda solicitudAyuda : lista) {
            try {
                SimpleDateFormat sdf = new SimpleDateFormat("mm");
                SimpleDateFormat sdfHM = new SimpleDateFormat("HH.mm");
                boolean cabecera = false;
                if (here == 0) {
                    //PARA AGREGAR TITULO
                    sheet.addMergedRegion(new CellRangeAddress(0, 0, 1, 9));
                    row = sheet.createRow(here);

                    XSSFCellStyle style = workbook.createCellStyle();
                    XSSFFont font = workbook.createFont();
                    font.setFontHeightInPoints((short) 1);
                    font.setBold(true);
                    style.setFont(font);
                    style.setAlignment(CellStyle.ALIGN_CENTER);

                    Cell cell1 = row.createCell(1);
                    row.setRowStyle(style);
                    cell1.setCellValue("PLANILLA DE SOLICITUDES");
                    cabecera = true;
                }
                if (cabecera) {
                    //PARA AGREGAR NOMBRE DE FUNCIONARIO
                    row = sheet.createRow(++here);
                    Cell cell1 = row.createCell(1);
                    cell1.setCellValue("");
                    Cell cell2 = row.createCell(2);
                    cell2.setCellValue("");
                    Cell cell3 = row.createCell(3);
                    cell3.setCellValue("");
                    Cell cell4 = row.createCell(4);
                    cell4.setCellValue("");
                    Cell cell5 = row.createCell(5);
                    cell5.setCellValue("");
                    Cell cell6 = row.createCell(6);
                    cell6.setCellValue("");
                    Cell cell7 = row.createCell(7);
                    cell7.setCellValue("");
                    Cell cell8 = row.createCell(8);
                    cell8.setCellValue("");
                    Cell cell9 = row.createCell(9);
                    cell9.setCellValue("");
                    Cell cell10 = row.createCell(10);
                    cell10.setCellValue("");
                    Cell cell11 = row.createCell(11);
                    cell11.setCellValue("");
                    cabecera = true;
                }
                if (cabecera) {
                    row = sheet.createRow(++here);
                    Cell cell1 = row.createCell(1);
                    cell1.setCellValue("FECHA");
                    Cell cell2 = row.createCell(2);
                    cell2.setCellValue("CEDULA");
                    Cell cell3 = row.createCell(3);
                    cell3.setCellValue("NOMBRES");
                    Cell cell4 = row.createCell(4);
                    cell4.setCellValue("APELLIDOS");
                    Cell cell5 = row.createCell(5);
                    cell5.setCellValue("TELEFONO");
                    Cell cell6 = row.createCell(6);
                    cell6.setCellValue("CIUDAD");
                    Cell cell7 = row.createCell(7);
                    cell7.setCellValue("DEPARTAMENTO");
                    Cell cell8 = row.createCell(8);
                    cell8.setCellValue("NECESIDAD");
                    Cell cell9 = row.createCell(9);
                    cell9.setCellValue("MOTIVO");
                    Cell cell10 = row.createCell(10);
                    cell10.setCellValue("ATENDIDO POR");
                    Cell cell11 = row.createCell(11);
                    cell11.setCellValue("ESTADO");
                    cabecera = false;
                }
                row = sheet.createRow(++here);
                Cell cell1 = row.createCell(1);
                cell1.setCellValue(formatter.format(solicitudAyuda.getFecha()));
                Cell cell2 = row.createCell(2);
                cell2.setCellValue(solicitudAyuda.getSocio().getCedula());
                Cell cell3 = row.createCell(3);
                cell3.setCellValue(solicitudAyuda.getSocio().getNombre().toUpperCase());
                Cell cell4 = row.createCell(4);
                cell4.setCellValue(solicitudAyuda.getSocio().getApellido().toUpperCase());
                Cell cell5 = row.createCell(5);
                cell5.setCellValue(solicitudAyuda.getTelefono());
                Cell cell6 = row.createCell(6);
                cell6.setCellValue(solicitudAyuda.getCiudad().getDescripcion().toUpperCase());
                Cell cell7 = row.createCell(7);
                cell7.setCellValue(solicitudAyuda.getDepartamento() == null ? "--" : solicitudAyuda.getDepartamento().getDescripcion().toUpperCase());
                Cell cell8 = row.createCell(8);
                cell8.setCellValue(solicitudAyuda.getNecesidadAyuda() == null ? "--" : solicitudAyuda.getNecesidadAyuda().getDescripcion());
                Cell cell9 = row.createCell(9);
                cell9.setCellValue(solicitudAyuda.getMotivoSolicitudAyuda().getDescripcion());
                Cell cell10 = row.createCell(10);
                cell10.setCellValue(solicitudAyuda.getAtendidopor() == null ? "--" : solicitudAyuda.getAtendidopor().toUpperCase());
                Cell cell11 = row.createCell(11);
                cell11.setCellValue(solicitudAyuda.getEstado() == null ? "--" : solicitudAyuda.getEstado().toUpperCase());
            } catch (Exception ex) {
                System.out.println("-->> " + ex.getLocalizedMessage());
                System.out.println("-->> " + ex.fillInStackTrace());
            } finally {
            }
        }
        SimpleDateFormat formatters = new SimpleDateFormat("yyyy-MM-dd_hhmmss");
        Date date = new Date(System.currentTimeMillis());
        tmpFile = Constants.UPLOAD_DIR + "\\temp\\" + formatters.format(date) + ".xlsx";
        try (FileOutputStream outputStream = new FileOutputStream(tmpFile)) {
            workbook.write(outputStream);

            //  outputStream.flush();
            //  outputStream.close();
            // File file = new File(tmpFile);
            // StreamResource myResource;
            // myResource = createResource(file, filterFunci.getValue().getNombreCompleto() + "_" + formatter.format(date) + ".xlsx");
            // fileDownloader = new FileDownloader(myResource);
            //Page.getCurrent().getJavaScript().execute("window.open('https://www.w3schools.com');");
            //UI.getCurrent().getPage().getJavaScript().execute("window.open('https://www.w3schools.com');");
            // Page.getCurrent().setLocation("http://www.google.com");
            //fileDownloader.extend(btnImprimirExcel);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(SolicitudAyudaView.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(SolicitudAyudaView.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void imprimirEvaluacionSocioeco() {
        Map pSQL = new HashMap<String, Object>();
        InputStream subImg = getClass().getClassLoader().getResourceAsStream("img/logo.png");
//                    String val = "{\"ventas\": " + recuperarTickets() + "}";
//        pSQL.put("repJsonString", "{\"ventas\": []}");

        pSQL.put("repJsonString", "{\"ventas\": " + cargarEvaluaciones() + "}");

//                pSQL.put("subRepNomFun", UserHolder.get().getFuncionario().getNombreCompleto());
        Date date = new Date();
        Timestamp ts = new Timestamp(date.getTime());
        String fechaArray[] = ts.toString().split(" ");
        String subRepTimestamp = ordenandoFechaString(fechaArray[0]) + " " + fechaArray[1];
        pSQL.put("subRepTimestamp", subRepTimestamp);
        pSQL.put("subRepNomFun", UserHolder.get().getUsuario());
        pSQL.put("subRepEmpresa", "MUTUAL NAC DE FUNC DEL MINISTERIO DE SALUD PÚBLICA Y BIENESTAR SOCIAL ");
//        pSQL.put("subRepSucursal", "FORMULARIO DE PERMISO");
        pSQL.put("subRepSucursal", "CASA CENTRAL");
        pSQL.put("subRepPathLogo", subImg);
        pSQL.put("subRepPathLogoCP", subImg);
        StreamResource.StreamSource source = new StreamResource.StreamSource() {

            public InputStream getStream() {
                byte[] b = null;
                String archivo = "ficha_evaluacion_socioeconomica";
                try {
                    b = JasperRunManager.runReportToPdf(getClass().getClassLoader().getResourceAsStream(archivo + ".jasper"), pSQL);
//                            b = JasperRunManager.runReportToPdf("C:\\Users\\hruiz\\Documents\\reporte\\ticket_mutual.jasper", pSQL);
                } catch (JRException ex) {
                    ex.printStackTrace();
                }
                return new ByteArrayInputStream(b);
            }
        };

        String archivo = "ficha_evaluacion_socioeconomica";

        StreamResource resource = new StreamResource(source, archivo + ".pdf");
        resource.setCacheTime(0);
        resource.setMIMEType("application/pdf");

        Window window = new Window();
        window.setWidth(800, Sizeable.Unit.PIXELS);
        window.setHeight(600, Sizeable.Unit.PIXELS);
        window.setModal(true);
        window.center();
        BrowserFrame pdf = new BrowserFrame("test", resource);
        pdf.setSizeFull();

        window.setContent(pdf);
        getUI().addWindow(window);
    }

    private void imprimirEvaluacionSocioecoTodos() {
        Map pSQL = new HashMap<String, Object>();

//                    String val = "{\"ventas\": " + recuperarTickets() + "}";
//        pSQL.put("repJsonString", "{\"ventas\": []}");
        pSQL.put("repJsonString", "{\"ventas\": " + cargarEvaluacionesTodo() + "}");

//                pSQL.put("subRepNomFun", UserHolder.get().getFuncionario().getNombreCompleto());
        Date date = new Date();
        Timestamp ts = new Timestamp(date.getTime());
        String fechaArray[] = ts.toString().split(" ");
        String subRepTimestamp = ordenandoFechaString(fechaArray[0]) + " " + fechaArray[1];
        pSQL.put("subRepTimestamp", subRepTimestamp);
        pSQL.put("subRepNomFun", UserHolder.get().getUsuario());
        pSQL.put("subRepEmpresa", "MUTUAL NAC DE FUNC DEL MINISTERIO DE SALUD PÚBLICA Y BIENESTAR SOCIAL ");
//        pSQL.put("subRepSucursal", "FORMULARIO DE PERMISO");
        pSQL.put("subRepSucursal", "CASA CENTRAL");
        InputStream subImg = getClass().getClassLoader().getResourceAsStream("img/logo.png");
        pSQL.put("subRepPathLogo", subImg);
        pSQL.put("subRepPathLogoCP", subImg);
        StreamResource.StreamSource source = new StreamResource.StreamSource() {

            public InputStream getStream() {
                byte[] b = null;
                String archivo = "ficha_evaluacion_socioeconomica_todo";
                try {
                    b = JasperRunManager.runReportToPdf(getClass().getClassLoader().getResourceAsStream(archivo + ".jasper"), pSQL);
//                            b = JasperRunManager.runReportToPdf("C:\\Users\\hruiz\\Documents\\reporte\\ticket_mutual.jasper", pSQL);
                } catch (JRException ex) {
                    ex.printStackTrace();
                }
                return new ByteArrayInputStream(b);
            }
        };

        String archivo = "ficha_evaluacion_socioeconomica_todo";

        StreamResource resource = new StreamResource(source, archivo + ".pdf");
        resource.setCacheTime(0);
        resource.setMIMEType("application/pdf");

        Window window = new Window();
        window.setWidth(800, Sizeable.Unit.PIXELS);
        window.setHeight(600, Sizeable.Unit.PIXELS);
        window.setModal(true);
        window.center();
        BrowserFrame pdf = new BrowserFrame("test", resource);
        pdf.setSizeFull();

        window.setContent(pdf);
        getUI().addWindow(window);
    }

    private JSONArray cargarEvaluaciones() {
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        SimpleDateFormat formatterHora = new SimpleDateFormat("HH:mm");
        JSONParser parser = new JSONParser();
        JSONArray jsonArrayDato = new JSONArray();
        JSONObject jsonObj = new JSONObject();
        try {
            SolicitudAyuda lic = solicitudAyudaController.listarPorId(solicitudSeleccionado.getId());
            List<DatosSocioDemograficos> listDatos = datoSocioDemograficoDao.listarPorIdSolicitud(lic.getId());

            if (lic.getId() != null) {
                try {
                    jsonObj.put("ci", lic.getSocio().getCedula());
                } catch (Exception e) {
                    jsonObj.put("ci", "--");
                } finally {
                }
                try {
                    jsonObj.put("nombres", lic.getSocio().getNombreCompleto());
                } catch (Exception e) {
                    jsonObj.put("nombres", "--");
                } finally {
                }
                try {
                    jsonObj.put("edad", getEdad(lic.getSocio().getFechanacimiento()));
                } catch (Exception e) {
                    jsonObj.put("edad", "--");
                } finally {
                }
                try {
                    jsonObj.put("ciudad", lic.getCiudad().getDescripcion());
                } catch (Exception e) {
                    jsonObj.put("ciudad", "--");
                } finally {
                }
                try {
                    jsonObj.put("departamento", lic.getDepartamento().getDescripcion());
                } catch (Exception e) {
                    jsonObj.put("departamento", "--");
                } finally {
                }
                try {
                    jsonObj.put("casa", listDatos.get(0).getTipoVivienda().equalsIgnoreCase("Casa") ? "   ( X )" : "   (   )");
                } catch (Exception e) {
                    jsonObj.put("casa", "--");
                } finally {
                }
                try {
                    jsonObj.put("dpto", listDatos.get(0).getTipoVivienda().equalsIgnoreCase("Departamento") ? "   ( X )" : "   (   )");
                } catch (Exception e) {
                    jsonObj.put("dpto", "--");
                } finally {
                }
                try {
                    jsonObj.put("pieza", listDatos.get(0).getTipoVivienda().equalsIgnoreCase("Pieza") ? "   ( X )" : "   (   )");
                } catch (Exception e) {
                    jsonObj.put("pieza", "--");
                } finally {
                }
                try {
                    jsonObj.put("precaria", listDatos.get(0).getTipoVivienda().equalsIgnoreCase("Vivienda Precaria") ? "   ( X )" : "   (   )");
                } catch (Exception e) {
                    jsonObj.put("precaria", "--");
                } finally {
                }
                try {
                    jsonObj.put("propia", listDatos.get(0).getCondicionVivienda().equalsIgnoreCase("Propia") ? "   ( X )" : "   (   )");
                } catch (Exception e) {
                    jsonObj.put("propia", "--");
                } finally {
                }
                try {
                    jsonObj.put("cuota", listDatos.get(0).getCondicionVivienda().equalsIgnoreCase("Pagando a Cuotas") ? "   ( X )" : "  (   )");
                } catch (Exception e) {
                    jsonObj.put("cuota", "--");
                } finally {
                }
                try {
                    jsonObj.put("alquilada", listDatos.get(0).getCondicionVivienda().equalsIgnoreCase("Alquilada") ? "   ( X )" : "   (   )");
                } catch (Exception e) {
                    jsonObj.put("alquilada", "--");
                } finally {
                }
                try {
                    jsonObj.put("prestada", listDatos.get(0).getCondicionVivienda().equalsIgnoreCase("Prestada") ? "   ( X )" : "   (   )");
                } catch (Exception e) {
                    jsonObj.put("prestada", "--");
                } finally {
                }
                try {
                    jsonObj.put("familiar", listDatos.get(0).getCondicionVivienda().equalsIgnoreCase("Familiar") ? "   ( X )" : "   (   )");
                } catch (Exception e) {
                    jsonObj.put("familiar", "--");
                } finally {
                }
                try {
                    jsonObj.put("mayor", "   ( " + listDatos.get(0).getCantPersonasMayores() + " )");
                } catch (Exception e) {
                    jsonObj.put("mayor", "--");
                } finally {
                }
                try {
                    jsonObj.put("menor", "   ( " + listDatos.get(0).getCantPersonasMenores() + " )");
                } catch (Exception e) {
                    jsonObj.put("menor", "--");
                } finally {
                }
                try {
                    jsonObj.put("cantTrabaj", "   ( " + listDatos.get(0).getCantPersonasTrabajan() + " )");
                } catch (Exception e) {
                    jsonObj.put("cantTrabaj", "--");
                } finally {
                }
                try {

                    DecimalFormatSymbols symbols = DecimalFormatSymbols.getInstance();
                    symbols.setGroupingSeparator('.');
                    DecimalFormat formatter2 = new DecimalFormat("###,###", symbols);

                    jsonObj.put("ingresoActual", "    " + formatter2.format(Math.round(listDatos.get(0).getIngresoAproxHogar())) + " ");
                } catch (Exception e) {
                    jsonObj.put("ingresoActual", "--");
                } finally {
                }

                try {
                    jsonObj.put("seguroPrivado", listDatos.get(0).getSeguroMedico().equalsIgnoreCase("Privado") ? "   ( X )" : "   (   )");
                } catch (Exception e) {
                    jsonObj.put("seguroPrivado", "--");
                } finally {
                }
                try {
                    jsonObj.put("seguroIps", listDatos.get(0).getSeguroMedico().equalsIgnoreCase("IPS") ? "   ( X )" : "(   )");
                } catch (Exception e) {
                    jsonObj.put("seguroIps", "--");
                } finally {
                }
                try {
                    jsonObj.put("seguroNinguno", listDatos.get(0).getSeguroMedico().equalsIgnoreCase("Ninguno") ? "   ( X )" : "(   )");
                } catch (Exception e) {
                    jsonObj.put("seguroNinguno", "--");
                } finally {
                }
                try {
                    jsonObj.put("enfermoSi", listDatos.get(0).getPersonaConEnfermedad().equalsIgnoreCase("Si") ? "  ( X )" : "(   )");
                } catch (Exception e) {
                    jsonObj.put("enfermoSi", "--");
                } finally {
                }
                try {
                    jsonObj.put("enfermoNo", listDatos.get(0).getPersonaConEnfermedad().equalsIgnoreCase("No") ? "   ( X )" : "(   )");
                } catch (Exception e) {
                    jsonObj.put("enfermoNo", "--");
                } finally {
                }
                try {
                    jsonObj.put("comoAfecto", lic.getNecesidadObs() != null ? lic.getNecesidadObs() : "--");
                } catch (Exception e) {
                    jsonObj.put("comoAfecto", "--");
                } finally {
                }
                try {
                    jsonObj.put("tipoEnfermedad", listDatos.get(0).getDescripcionEnfermedad() != null ? listDatos.get(0).getDescripcionEnfermedad() : "--");
                } catch (Exception e) {
                    jsonObj.put("tipoEnfermedad", "--");
                } finally {
                }
                jsonArrayDato.add(jsonObj);
            }
        } catch (Exception e) {
        } finally {
        }

        return jsonArrayDato;
    }

    private JSONArray cargarEvaluacionesTodo() {
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        SimpleDateFormat formatterHora = new SimpleDateFormat("HH:mm");
        JSONParser parser = new JSONParser();
        JSONArray jsonArrayDato = new JSONArray();
        try {
            for (SolicitudAyuda solicitudAyuda : lista) {
                JSONObject jsonObj = new JSONObject();
                SolicitudAyuda lic = solicitudAyuda;
                List<DatosSocioDemograficos> listDatos = datoSocioDemograficoDao.listarPorIdSolicitud(lic.getId());

                if (lic.getId() != null) {
                    try {
                        jsonObj.put("ci", lic.getSocio().getCedula());
                    } catch (Exception e) {
                        jsonObj.put("ci", "--");
                    } finally {
                    }
                    try {
                        jsonObj.put("nombres", lic.getSocio().getNombreCompleto());
                    } catch (Exception e) {
                        jsonObj.put("nombres", "--");
                    } finally {
                    }
                    try {
                        jsonObj.put("edad", getEdad(lic.getSocio().getFechanacimiento()));
                    } catch (Exception e) {
                        jsonObj.put("edad", "--");
                    } finally {
                    }
                    try {
                        jsonObj.put("ciudad", lic.getCiudad().getDescripcion());
                    } catch (Exception e) {
                        jsonObj.put("ciudad", "--");
                    } finally {
                    }
                    try {
                        jsonObj.put("departamento", lic.getDepartamento().getDescripcion());
                    } catch (Exception e) {
                        jsonObj.put("departamento", "--");
                    } finally {
                    }
                    try {
                        jsonObj.put("casa", listDatos.get(0).getTipoVivienda().equalsIgnoreCase("Casa") ? "   ( X )" : "   (   )");
                    } catch (Exception e) {
                        jsonObj.put("casa", "--");
                    } finally {
                    }
                    try {
                        jsonObj.put("dpto", listDatos.get(0).getTipoVivienda().equalsIgnoreCase("Departamento") ? "   ( X )" : "   (   )");
                    } catch (Exception e) {
                        jsonObj.put("dpto", "--");
                    } finally {
                    }
                    try {
                        jsonObj.put("pieza", listDatos.get(0).getTipoVivienda().equalsIgnoreCase("Pieza") ? "   ( X )" : "   (   )");
                    } catch (Exception e) {
                        jsonObj.put("pieza", "--");
                    } finally {
                    }
                    try {
                        jsonObj.put("precaria", listDatos.get(0).getTipoVivienda().equalsIgnoreCase("Vivienda Precaria") ? "   ( X )" : "   (   )");
                    } catch (Exception e) {
                        jsonObj.put("precaria", "--");
                    } finally {
                    }
                    try {
                        jsonObj.put("propia", listDatos.get(0).getCondicionVivienda().equalsIgnoreCase("Propia") ? "   ( X )" : "   (   )");
                    } catch (Exception e) {
                        jsonObj.put("propia", "--");
                    } finally {
                    }
                    try {
                        jsonObj.put("cuota", listDatos.get(0).getCondicionVivienda().equalsIgnoreCase("Pagando a Cuotas") ? "   ( X )" : "  (   )");
                    } catch (Exception e) {
                        jsonObj.put("cuota", "--");
                    } finally {
                    }
                    try {
                        jsonObj.put("alquilada", listDatos.get(0).getCondicionVivienda().equalsIgnoreCase("Alquilada") ? "   ( X )" : "   (   )");
                    } catch (Exception e) {
                        jsonObj.put("alquilada", "--");
                    } finally {
                    }
                    try {
                        jsonObj.put("prestada", listDatos.get(0).getCondicionVivienda().equalsIgnoreCase("Prestada") ? "   ( X )" : "   (   )");
                    } catch (Exception e) {
                        jsonObj.put("prestada", "--");
                    } finally {
                    }
                    try {
                        jsonObj.put("familiar", listDatos.get(0).getCondicionVivienda().equalsIgnoreCase("Familiar") ? "   ( X )" : "   (   )");
                    } catch (Exception e) {
                        jsonObj.put("familiar", "--");
                    } finally {
                    }
//                    try {
//                        InputStream subImg = getClass().getClassLoader().getResourceAsStream("img/logo.png");
//                        jsonObj.put("subRepPathLogo", convertStreamToString(subImg));
////                        jsonObj.put("subRepPathLogoCP", subImg);
//                    } catch (Exception e) {
//                    } finally {
//                    }
                    try {
                        jsonObj.put("mayor", "   ( " + listDatos.get(0).getCantPersonasMayores() + " )");
                    } catch (Exception e) {
                        jsonObj.put("mayor", "--");
                    } finally {
                    }
                    try {
                        jsonObj.put("menor", "   ( " + listDatos.get(0).getCantPersonasMenores() + " )");
                    } catch (Exception e) {
                        jsonObj.put("menor", "--");
                    } finally {
                    }
                    try {
                        jsonObj.put("cantTrabaj", "   ( " + listDatos.get(0).getCantPersonasTrabajan() + " )");
                    } catch (Exception e) {
                        jsonObj.put("cantTrabaj", "--");
                    } finally {
                    }
                    try {

                        DecimalFormatSymbols symbols = DecimalFormatSymbols.getInstance();
                        symbols.setGroupingSeparator('.');
                        DecimalFormat formatter2 = new DecimalFormat("###,###", symbols);

                        jsonObj.put("ingresoActual", "    " + formatter2.format(Math.round(listDatos.get(0).getIngresoAproxHogar())) + " ");
                    } catch (Exception e) {
                        jsonObj.put("ingresoActual", "--");
                    } finally {
                    }

                    try {
                        jsonObj.put("seguroPrivado", listDatos.get(0).getSeguroMedico().equalsIgnoreCase("Privado") ? "   ( X )" : "   (   )");
                    } catch (Exception e) {
                        jsonObj.put("seguroPrivado", "--");
                    } finally {
                    }
                    try {
                        jsonObj.put("seguroIps", listDatos.get(0).getSeguroMedico().equalsIgnoreCase("IPS") ? "   ( X )" : "(   )");
                    } catch (Exception e) {
                        jsonObj.put("seguroIps", "--");
                    } finally {
                    }
                    try {
                        jsonObj.put("seguroNinguno", listDatos.get(0).getSeguroMedico().equalsIgnoreCase("Ninguno") ? "   ( X )" : "(   )");
                    } catch (Exception e) {
                        jsonObj.put("seguroNinguno", "--");
                    } finally {
                    }
                    try {
                        jsonObj.put("enfermoSi", listDatos.get(0).getPersonaConEnfermedad().equalsIgnoreCase("Si") ? "  ( X )" : "(   )");
                    } catch (Exception e) {
                        jsonObj.put("enfermoSi", "--");
                    } finally {
                    }
                    try {
                        jsonObj.put("enfermoNo", listDatos.get(0).getPersonaConEnfermedad().equalsIgnoreCase("No") ? "   ( X )" : "(   )");
                    } catch (Exception e) {
                        jsonObj.put("enfermoNo", "--");
                    } finally {
                    }
                    try {
                        jsonObj.put("comoAfecto", lic.getNecesidadObs() != null ? lic.getNecesidadObs() : "--");
                    } catch (Exception e) {
                        jsonObj.put("comoAfecto", "--");
                    } finally {
                    }
                    try {
                        jsonObj.put("tipoEnfermedad", listDatos.get(0).getDescripcionEnfermedad() != null ? listDatos.get(0).getDescripcionEnfermedad() : "--");
                    } catch (Exception e) {
                        jsonObj.put("tipoEnfermedad", "--");
                    } finally {
                    }
                    jsonArrayDato.add(jsonObj);
                }
            }
        } catch (Exception e) {
        } finally {
        }

        return jsonArrayDato;
    }

    private String getEdad(Date fecha) {
        //using Calendar Object
        //String s = "1994/06/23";

        StringTokenizer tokenizer = new StringTokenizer(fecha.toString(), " ");
        String s = tokenizer.nextToken();

//        DateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd");
//        String s = dateFormat.format(fec);
//        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
//        Date d = null;
//        try {
//            d = sdf.parse(s);
//        } catch (ParseException ex) {
//            Logger.getLogger(SolicitudAyudaView.class.getName()).log(Level.SEVERE, null, ex);
//        }
        Calendar c = Calendar.getInstance();
        c.setTime(fecha);
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH) + 1;
        int date = c.get(Calendar.DATE);
        LocalDate l1 = LocalDate.of(year, month, date);
        LocalDate now1 = LocalDate.now();
        Period diff1 = Period.between(l1, now1);
        return diff1.getYears() + " años";
    }
}
