package py.mutualmsp.mutualweb.vista;

import com.github.appreciated.material.MaterialTheme;
import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.event.ShortcutListener;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.*;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import py.mutualmsp.mutualweb.dao.EnfermeriaDao;
import py.mutualmsp.mutualweb.dao.TipoincidenciaDao;
import py.mutualmsp.mutualweb.entities.Enfermeria;
import py.mutualmsp.mutualweb.entities.Tipoincidencia;
import py.mutualmsp.mutualweb.formularios.EnfermeriaForm;
import py.mutualmsp.mutualweb.util.ResourceLocator;

/**
 * Created by Alfre on 14/6/2016.
 */
public class EnfermeriaView extends CssLayout implements View {

    public static final String VIEW_NAME = "Enfermerias";
    Grid<Enfermeria> grid = new Grid<>(Enfermeria.class);
    Button newCliente;
    Button btnEnfemeria;
    //Services ejb = ResourceLocator.locate(Services.class);
    TextField filter = new TextField("N° Cédula");
    EnfermeriaForm form = new EnfermeriaForm();
    EnfermeriaDao controller = ResourceLocator.locate(EnfermeriaDao.class);
    TipoincidenciaDao tipoincidenciaDao = ResourceLocator.locate(TipoincidenciaDao.class);
    private List<Enfermeria> lista;

    DateField dtfFechaDesde = new DateField("Fecha Desde");
    DateField dtfFechaHasta = new DateField("Fecha Hasta");
    ComboBox<Tipoincidencia> cmbTipoincidencia = new ComboBox<>("Tipo Incidencia");

    public EnfermeriaView() {
        System.out.println("Nueva instancia EnfermeriaView");
        setSizeFull();
        addStyleName("crud-view");
        HorizontalLayout horizontalLayout = createTopBar();
        SimpleDateFormat formatSinHora = new SimpleDateFormat("dd-MM-yyyy");

//        filter.addValueChangeListener(vcl -> grid.clearSortOrder());
        form.setVisible(false);

        filter.setWidth(10f, TextField.UNITS_EM);
        dtfFechaDesde.setWidth(10f, TextField.UNITS_EM);
        dtfFechaHasta.setWidth(10f, TextField.UNITS_EM);
        cmbTipoincidencia.setWidth(12f, TextField.UNITS_EM);

        lista = controller.listaEnfermeria();
        grid.setItems(lista);
        grid.removeAllColumns();
        grid.addColumn(Enfermeria::getId).setCaption("Codigo");
        grid.addColumn(e -> {
            try {
                DateTimeFormatter formatters = DateTimeFormatter.ofPattern("d-MM-uuuu");
                return e.getFecha() == null ? "--" : e.getFecha().format(formatters);
            } catch (Exception ex) {
                return "--";
            } finally {
            }
        }).setCaption("Fecha");
        grid.addColumn(e -> {
            try {
                DateTimeFormatter formatters = DateTimeFormatter.ofPattern("d-MM-uuuu");
                return e.getFecha() == null ? "--" : e.getFechavigencia().format(formatters);
            } catch (Exception ex) {
                return "--";
            } finally {
            }

        }).setCaption("Fecha Vigencia");
//        grid.addColumn(Enfermeria::getFecha).setCaption("Fecha");
//        grid.addColumn(Enfermeria::getFechavigencia).setCaption("Fecha Vigencia");
        grid.addColumn(Enfermeria::getCedula).setCaption("Nro. Doc");
        grid.addColumn(Enfermeria::getNombre).setCaption("Nombre");
//        grid.addColumn(enfermeria -> {
//            try {
//                return enfermeria.getIdagendamiento().getId();
//            } catch (Exception e) {
//                return 0;
//            }
//
//        }).setCaption("N° Agendamiento");
//        grid.addColumn(Enfermeria::getIdagendamiento).setCaption("N° Agendamiento");
        grid.addColumn(tipoIncidencia -> {
            return tipoIncidencia.getIdtipoIncidencia().getDescripcion();
        }).setCaption("Tipo incidencia");
//        grid.addComponentColumn(e -> {
//            return e.getAproboData();
//        }).setCaption("Aprobó");
//        grid.addColumn(aprobo -> {
//            return aprobo.getAprobo() == true ? "SI" : "NO";
//        }).setCaption("Aprobó");
        grid.setSizeFull();

        filter.addShortcutListener(new ShortcutListener("Shortcut", KeyCode.ENTER, null) {
            @Override
            public void handleAction(Object sender, Object target) {
                // Do nice stuff
                if (filter.getValue() != null) {
                    grid.clearSortOrder();
                    //filterProyecto.setItems(proyectosController.listaProyectosPorEnfermeria(filterEnfermeria.getValue()));
                    //filterProyecto.setItemCaptionGenerator(Proyecto::getDescripcion);
                    grid.setItems(controller.listarEnfermeriaPorCI(filter.getValue().toUpperCase()));
                }
            }
        });

        HorizontalLayout hl = new HorizontalLayout();
        hl.addComponents(grid, form);
        hl.setSizeFull();
        hl.setExpandRatio(grid, 1);

        VerticalLayout verticalLayout = new VerticalLayout();
        verticalLayout.addComponent(horizontalLayout);
        verticalLayout.addComponent(hl);
        verticalLayout.setMargin(true);
        verticalLayout.setSpacing(true);
        verticalLayout.setSizeFull();
        verticalLayout.setExpandRatio(hl, 1);
        verticalLayout.setStyleName("crud-main-layout");
        addComponent(verticalLayout);

//        dtfFechaHasta.addValueChangeListener(e -> {
//            try {
//                if (e.getValue() != null || !e.equals("")) {
//                    java.util.Date fechaDesde = new SimpleDateFormat("yyyy-MM-dd").parse(dtfFechaDesde.getValue().toString());
//                    java.util.Date fechaHasta = new SimpleDateFormat("yyyy-MM-dd").parse(e.getValue().toString());
//                    lista = controller.getEnfermeriaByFecha(fechaDesde, fechaHasta);
//                    grid.setItems(lista);
//                } else {
//                    updateList("");
//                }
//            } catch (Exception ex) {
//                updateList("");
//            }
//        });
        newCliente.addClickListener(
                clickEvent -> form.setEnfermeria(new Enfermeria())
        );
        btnEnfemeria.addClickListener(
                clickEvent -> buscarData()
        );
//        filter.setPlaceholder("Filtre N° CI");
//        grid.asSingleSelect().addValueChangeListener(e -> {
//            if (e.getValue() != null) {
//                form.setEnfermeria(e.getValue());
//            }
//        });

        cmbTipoincidencia.setItems(tipoincidenciaDao.listaTipoincidencia());
        cmbTipoincidencia.setItemCaptionGenerator(Tipoincidencia::getDescripcion);
//        cmbTipoincidencia.addValueChangeListener(e -> {
//            try {
//                if (e.getValue() != null || !e.equals("")) {
//                    filtroTipoIncidencia(e.getValue().getId());
//                } else {
//                    updateList("");
//                }
//            } catch (Exception ex) {
//                updateList("");
//            }
//        });

        grid.addItemClickListener(e -> {
            if (e.getMouseEventDetails().isDoubleClick()) {
                form.setEnfermeria(e.getItem());
                form.setVisible(true);
                verticalLayout.setVisible(false);
                grid.setVisible(false);
            }
        });

        form.setSaveListener(cliente -> {
            grid.clearSortOrder();
            grid.setVisible(true);
            lista = controller.listaEnfermeria();
            grid.setItems(lista);
            form.setVisible(false);
            Notification.show("Nuevos Datos almacenados!.", Notification.Type.HUMANIZED_MESSAGE);
        });
        grid.asSingleSelect().addValueChangeListener(e -> {
            if (e.getValue() != null) {

                Enfermeria inviDto = new Enfermeria();
                inviDto.setId(e.getValue().getId());
                inviDto.setAprobo(e.getValue().getAprobo());
                inviDto.setCedula(e.getValue().getCedula());
                inviDto.setFecha(e.getValue().getFecha());
                inviDto.setFechavigencia(e.getValue().getFechavigencia());
                inviDto.setIdagendamiento(e.getValue().getIdagendamiento());
                inviDto.setIdmedico(e.getValue().getIdmedico());
                inviDto.setIdtipoIncidencia(e.getValue().getIdtipoIncidencia());
                inviDto.setNombre(e.getValue().getNombre());
                inviDto.setObservacion(e.getValue().getObservacion());

                form.setEnfermeria(inviDto, "E", e.getValue().getId());
            }
        });
//        filter.setValue(VIEW_NAME);
        form.setCancelListener(cliente -> {
            grid.clearSortOrder();
            form.setVisible(false);
        });
        form.setDeleteListener(cliente -> {
            grid.clearSortOrder();
            grid.setVisible(true);
            lista = controller.listaEnfermeria();
            grid.setItems(lista);
            form.setVisible(false);
            Notification.show("Atención", "Registro borrado correctamente", Notification.Type.HUMANIZED_MESSAGE);
        });
    }

    private HorizontalLayout createTopBar() {

        newCliente = new Button("Nuevo Enfermeria");
        newCliente.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_FRIENDLY);
        newCliente.setIcon(FontAwesome.PLUS_CIRCLE);

        btnEnfemeria = new Button();
        btnEnfemeria.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_PRIMARY);
        btnEnfemeria.setIcon(FontAwesome.SEARCH);

        HorizontalLayout layout = new HorizontalLayout();
        layout.setSpacing(true);
        layout.setWidth("100%");
        layout.addComponent(filter);
        layout.addComponent(dtfFechaDesde);
        layout.addComponent(dtfFechaHasta);
        layout.addComponent(cmbTipoincidencia);
        layout.addComponent(btnEnfemeria);
        layout.addComponent(newCliente);
        //layout.setComponentAlignment(filter, Alignment.MIDDLE_LEFT);
        //layout.setExpandRatio(filter, 1);
        layout.setSpacing(true);
        layout.setStyleName("top-bar");
        return layout;
    }

    private void updateList(String value) {
        try {
            lista = controller.getListEnfermeria(value);
            grid.setSizeFull();
            grid.setItems(lista);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void filtroTipoIncidencia(Long idtipoIncidencia) {
//        try {
//            lista = controller.getEnfermeriaByTipoIncidencia(idtipoIncidencia);
//            grid.setItems(lista);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent viewChangeEvent) {

    }

    private void buscarData() {
        try {
            lista = new ArrayList<>();
            grid.clearSortOrder();
            lista = controller.getDataFromFilter(filter.getValue(), dtfFechaDesde.getValue(), dtfFechaHasta.getValue(), cmbTipoincidencia.getValue());
            grid.setItems(lista);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
