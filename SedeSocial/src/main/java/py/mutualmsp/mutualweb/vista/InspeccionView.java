package py.mutualmsp.mutualweb.vista;

import com.github.appreciated.material.MaterialTheme;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.Sizeable;
import com.vaadin.server.StreamResource;
import com.vaadin.ui.*;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperRunManager;
import org.json.JSONObject;
import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import py.mutualmsp.mutualweb.dao.InspeccionDTODao;
import py.mutualmsp.mutualweb.dao.InspeccionDao;
import py.mutualmsp.mutualweb.dao.TipoincidenciaDao;
import py.mutualmsp.mutualweb.dto.InspeccionDTO;
import py.mutualmsp.mutualweb.entities.Inspeccion;
import py.mutualmsp.mutualweb.formularios.InspeccionForm;
import py.mutualmsp.mutualweb.util.ResourceLocator;
import py.mutualmsp.mutualweb.util.UserHolder;

/**
 * Created by Alfre on 14/6/2016.
 */
public class InspeccionView extends CssLayout implements View {

    public static final String VIEW_NAME = "Inspeccion";
    Grid<InspeccionDTO> grid = new Grid<>(InspeccionDTO.class);
    Button btnNuevaInspeccion;
    Button btnBuscarInspeccion;
    Button btnEditarInspeccion;
    Button btnImprimirInspeccion;
    //Services ejb = ResourceLocator.locate(Services.class);
    TextField txtNroCi = new TextField("FILTRE CI | NOMBRE | APELLIDO");
    TextField txtSocio = new TextField("FILTRE CI SOCIO | NOMBRE | APELLIDO");

    private DateField desdeVigencia = new DateField("Desde Vigencia");
    private DateField hastaVigencia = new DateField("Hasta Vigencia");
    Button btnLimpiarFecha = new Button("Limpiar Filtro Fecha");
    Button btnImprimirTodo = new Button("Ìmprimir");
    InspeccionForm form = new InspeccionForm();
    InspeccionDTODao controller = ResourceLocator.locate(InspeccionDTODao.class);
    TipoincidenciaDao tipoincidenciaDao = ResourceLocator.locate(TipoincidenciaDao.class);
    private List<InspeccionDTO> lista;
    InspeccionDTO inspeccionSeleccionado;
//    DateField dtfFechaDesde = new DateField("Fecha Desde");
//    DateField dtfFechaHasta = new DateField("Fecha Hasta");
//    ComboBox<Tipoincidencia> cmbTipoincidencia = new ComboBox<>("Tipo Incidencia");

    public InspeccionView() {
        System.out.println("Nueva instancia InspeccionView");
        setSizeFull();
        addStyleName("crud-view");
        VerticalLayout horizontalLayout = createTopBar();
        SimpleDateFormat formatSinHora = new SimpleDateFormat("dd-MM-yyyy");

//        txtNroCi.addValueChangeListener(vcl -> grid.clearSortOrder());
//        form.setVisible(false);
        txtNroCi.setWidth(15f, TextField.UNITS_EM);
        txtSocio.setWidth(15f, TextField.UNITS_EM);
//        dtfFechaDesde.setWidth(10f, TextField.UNITS_EM);
//        dtfFechaHasta.setWidth(10f, TextField.UNITS_EM);
//        cmbTipoincidencia.setWidth(12f, TextField.UNITS_EM);

        lista = controller.listaInspeccionDTO();
        grid.setItems(lista);
        grid.removeAllColumns();
        grid.addColumn(InspeccionDTO::getId).setCaption("Codigo");
        grid.addColumn(InspeccionDTO::getCedula).setCaption("Nro. Doc");
        grid.addColumn(InspeccionDTO::getNombre).setCaption("Nombres");
        grid.addColumn(InspeccionDTO::getApellido).setCaption("Apellidos");
        grid.addColumn(e -> {
            try {
                return e.getParentesco().equalsIgnoreCase("") || e.getParentesco() == null ? "--" : e.getParentesco().toUpperCase();
            } catch (Exception ex) {
                return "--";
            } finally {
            }
        }).setCaption("Parentesco");
        grid.addColumn(e -> {
            try {
                return e.getCedulasocio().equalsIgnoreCase("") || e.getCedulasocio() == null ? "--" : e.getCedulasocio().toUpperCase();
            } catch (Exception ex) {
                return "--";
            } finally {
            }
        }).setCaption("CI Socio");
        grid.addColumn(e -> {
            try {
                return e.getNombresocio().equalsIgnoreCase("") || e.getNombresocio() == null ? "--" : e.getNombresocio().toUpperCase() + " " + e.getApellido().toUpperCase();
            } catch (Exception ex) {
                return "--";
            } finally {
            }
        }).setCaption("Socio");
        grid.addColumn(e -> {
            try {
                return e.getFecharegistro() == null ? "--" : formatSinHora.format(e.getFecharegistro());
            } catch (Exception ex) {
                return "--";
            } finally {
            }
        }).setCaption("Registro");
        grid.addColumn(e -> {
            try {
                return e.getFecha() == null ? "--" : formatSinHora.format(e.getFecha());
            } catch (Exception ex) {
                return "--";
            } finally {
            }
        }).setCaption("Valido Hasta");
        grid.setSizeFull();

        grid.asSingleSelect().addValueChangeListener(event -> {
            inspeccionSeleccionado = event.getValue();
            btnEditarInspeccion.setVisible(true);
            btnImprimirInspeccion.setVisible(true);
        });

        VerticalLayout verticalLayout = new VerticalLayout();
        verticalLayout.addComponent(horizontalLayout);
        grid.setSizeFull();
        verticalLayout.addComponent(grid);
        verticalLayout.setMargin(true);
        verticalLayout.setSpacing(true);
        verticalLayout.setSizeFull();
        verticalLayout.setExpandRatio(grid, 6F);
//        verticalLayout.addComponent(labelTotalizador);
//        verticalLayout.setComponentAlignment(labelTotalizador, Alignment.BOTTOM_RIGHT);
//        HorizontalLayout horizontalLayout1 = crearSegundoGrid();
//        horizontalLayout1.setSizeFull();
        verticalLayout.addStyleName("crud-main-layout");
        addComponent(verticalLayout);


        btnNuevaInspeccion.addClickListener(clickEvent -> {
            InspeccionForm soliForm = new InspeccionForm();

            UI.getCurrent().addWindow(soliForm);
            soliForm.setVisible(true);

            soliForm.setCancelListener(reclamo -> {
                grid.clearSortOrder();
            });

            soliForm.setSaveListener(reclamo -> {
                grid.clearSortOrder();
                grid.setItems(lista);
            });
        });
        btnEditarInspeccion.addClickListener(clickEvent -> {
            if (inspeccionSeleccionado != null) {
                InspeccionForm soliForm = new InspeccionForm();
//            try {
                InspeccionDao controllerDAO = ResourceLocator.locate(InspeccionDao.class);
                soliForm.setConsultaInspeccion(controllerDAO.listarInspeccionPorId(inspeccionSeleccionado.getId() + ""));
//

                UI.getCurrent().addWindow(soliForm);
                soliForm.setVisible(true);

                soliForm.setCancelListener(reclamo -> {
                    grid.clearSortOrder();
                });

                soliForm.setSaveListener(reclamo -> {
                    grid.clearSortOrder();
                    lista = controller.listaInspeccionDTO();
                    grid.setItems(lista);
                });
            }
        });
        btnImprimirInspeccion.addClickListener(clickEvent -> {
            if (inspeccionSeleccionado != null) {
                imrpimirPDF();
            }
        });
        btnImprimirTodo.addClickListener(clickEvent -> {
            if (lista.size() > 0) {
                mostrarListaPDF();
            }
        });
        btnBuscarInspeccion.addClickListener(
                clickEvent -> buscarData()
        );
        btnLimpiarFecha.addClickListener(clickEvent -> {
            desdeVigencia.setValue(null);
            hastaVigencia.setValue(null);
        }
        );

        form.setSaveListener(cliente -> {
            grid.clearSortOrder();
            grid.setVisible(true);
            lista = controller.listaInspeccionDTO();
            grid.setItems(lista);
            form.setVisible(false);
            Notification.show("Nuevos Datos almacenados!.", Notification.Type.HUMANIZED_MESSAGE);
        });
        grid.asSingleSelect().addValueChangeListener(e -> {
            if (e.getValue() != null) {

                Inspeccion inviDto = new Inspeccion();
                inviDto.setId(e.getValue().getId());
                inviDto.setActivo(e.getValue().getActivo());
                inviDto.setAcv(e.getValue().getAcv());
                inviDto.setAlergico(e.getValue().getAlergico());
                inviDto.setApellido(e.getValue().getApellido());
                inviDto.setAsma(e.getValue().getAsma());
                inviDto.setCard(e.getValue().getCard());
                inviDto.setCedula(e.getValue().getCedula());
                inviDto.setComentarioalergico(e.getValue().getComentarioalergico());
                inviDto.setContactoemergencia(e.getValue().getContactoemergencia());
                inviDto.setConv(e.getValue().getConv());
                inviDto.setDst(e.getValue().getDst());
                inviDto.setEdad(e.getValue().getEdad());
                inviDto.setEpoc(e.getValue().getEpoc());
                inviDto.setFc(e.getValue().getFc());
                inviDto.setFecha(e.getValue().getFecha());
                inviDto.setGruposanguineo(e.getValue().getGruposanguineo());
                inviDto.setHallazgos(e.getValue().getHallazgos());
                inviDto.setHta(e.getValue().getHta());
                inviDto.setMotivoconsulta(e.getValue().getMotivoconsulta());
                inviDto.setNombre(e.getValue().getNombre());
                inviDto.setOtros(e.getValue().getOtros());
                inviDto.setPeso(e.getValue().getPeso());
                inviDto.setSeguromedico(e.getValue().getSeguromedico());
                inviDto.setSexo(e.getValue().getSexo());
                inviDto.setT(e.getValue().getT());
                inviDto.setTa(e.getValue().getTa());
                inviDto.setTratamiento(e.getValue().getTratamiento());
                inviDto.setMotivoConsulta(e.getValue().getMotivoConsulta());
                inviDto.setMedicacionConsulta(e.getValue().getMedicacionConsulta());
                inviDto.setObservacionConsulta(e.getValue().getObservacionConsulta());

                form.setConsultaInspeccion(inviDto);
            }
        });
//        txtNroCi.setValue(VIEW_NAME);
        form.setCancelListener(cliente -> {
            grid.clearSortOrder();
            form.setVisible(false);
        });
        form.setDeleteListener(cliente -> {
            grid.clearSortOrder();
            grid.setVisible(true);
            lista = controller.listaInspeccionDTO();
            grid.setItems(lista);
            form.setVisible(false);
            Notification.show("Atención", "Registro borrado correctamente", Notification.Type.HUMANIZED_MESSAGE);
        });
    }

    private VerticalLayout createTopBar() {

        btnNuevaInspeccion = new Button("Nueva Inspeccion");
        btnNuevaInspeccion.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_FRIENDLY);
        btnNuevaInspeccion.setIcon(FontAwesome.PLUS_CIRCLE);

        btnBuscarInspeccion = new Button();
        btnBuscarInspeccion.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_PRIMARY);
        btnBuscarInspeccion.setIcon(FontAwesome.SEARCH);

        btnEditarInspeccion = new Button();
        btnEditarInspeccion.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_FRIENDLY);
        btnEditarInspeccion.setIcon(FontAwesome.EDIT);

        btnImprimirInspeccion = new Button();
        btnImprimirInspeccion.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_FRIENDLY);
        btnImprimirInspeccion.setIcon(FontAwesome.PRINT);

        btnLimpiarFecha.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_FRIENDLY);

        btnImprimirTodo.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_FRIENDLY);

        VerticalLayout layoutVer = new VerticalLayout();
        layoutVer.addStyleName("crud-view");
        layoutVer.setMargin(true);
        layoutVer.setSpacing(true);

        HorizontalLayout layout = new HorizontalLayout();
        layout.setSpacing(true);
        layout.setWidth("100%");
        layout.addComponent(txtNroCi);
        //layout.addComponent(txtSocio);
        layout.addComponent(desdeVigencia);
        layout.addComponent(hastaVigencia);
//        layout.addComponent(cmbTipoincidencia);
        layout.addComponent(btnBuscarInspeccion);
        layout.addComponent(btnLimpiarFecha);

        HorizontalLayout layout1 = new HorizontalLayout();
        layout1.addComponent(btnImprimirTodo);
        layout1.addComponent(btnEditarInspeccion);
        layout1.addComponent(btnImprimirInspeccion);
        layout1.addComponent(btnNuevaInspeccion);

        layoutVer.addComponent(layout);
        layoutVer.addComponent(layout1);
        //layout.setComponentAlignment(txtNroCi, Alignment.MIDDLE_LEFT);
        //layout.setExpandRatio(txtNroCi, 1);
//        layout.setSpacing(true);
//        layout.setStyleName("top-bar");
        return layoutVer;
    }

    private void updateList(String value) {
        try {
            lista = controller.getListInspeccionDTO(value);
            grid.setSizeFull();
            grid.setItems(lista);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void filtroTipoIncidencia(Long idtipoIncidencia) {
//        try {
//            lista = controller.getInspeccionByTipoIncidencia(idtipoIncidencia);
//            grid.setItems(lista);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent viewChangeEvent) {

    }

    private void buscarData() {
        try {
            lista = new ArrayList<>();
            grid.clearSortOrder();
            lista = controller.recuperarInspeccionDTO(txtNroCi.getValue(), txtSocio.getValue(), desdeVigencia.getValue(), hastaVigencia.getValue());
            grid.setItems(lista);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void imrpimirPDF() {
        Map pSQL = new HashMap<String, Object>();
        InputStream subImg = getClass().getClassLoader().getResourceAsStream("img/logo.png");
//                    String val = "{\"ventas\": " + recuperarTickets() + "}";
//        pSQL.put("repJsonString", "{\"ventas\": []}");

        pSQL.put("repJsonString", "{\"ventas\": " + cargarInspeccion() + "}");

//                pSQL.put("subRepNomFun", UserHolder.get().getFuncionario().getNombreCompleto());
        Date date = new Date();
        Timestamp ts = new Timestamp(date.getTime());
        String fechaArray[] = ts.toString().split(" ");
//        String subRepTimestamp = ordenandoFechaString(fechaArray[0]) + " " + fechaArray[1];
//        pSQL.put("subRepTimestamp", subRepTimestamp);
        pSQL.put("subRepEmpresa", "MUTUAL NAC DE FUNC DEL MINISTERIO DE SALUD PÚBLICA Y BIENESTAR SOCIAL");
//        pSQL.put("subRepSucursal", "FORMULARIO DE PERMISO");
//        pSQL.put("subRepSucursal", inspeccionSeleccionado.getFormulario().getDescripcion().toUpperCase());
        pSQL.put("subRepPathLogo", subImg);
        pSQL.put("subRepPathLogoCP", subImg);
        StreamResource.StreamSource source = new StreamResource.StreamSource() {

            public InputStream getStream() {
                byte[] b = null;
                String archivo = "formulario_inspeccion";
                try {
                    b = JasperRunManager.runReportToPdf(getClass().getClassLoader().getResourceAsStream(archivo + ".jasper"), pSQL);
//                            b = JasperRunManager.runReportToPdf("C:\\Users\\hruiz\\Documents\\reporte\\ticket_mutual.jasper", pSQL);
                } catch (JRException ex) {
                    ex.printStackTrace();
                }
                return new ByteArrayInputStream(b);
            }
        };

        String archivo = "formulario_inspeccion";

        StreamResource resource = new StreamResource(source, archivo + ".pdf");
        resource.setCacheTime(0);
        resource.setMIMEType("application/pdf");

        Window window = new Window();
        window.setWidth(800, Sizeable.Unit.PIXELS);
        window.setHeight(600, Sizeable.Unit.PIXELS);
        window.setModal(true);
        window.center();
        BrowserFrame pdf = new BrowserFrame("test", resource);
        pdf.setSizeFull();

        window.setContent(pdf);
        getUI().addWindow(window);
    }

    public static String ordenandoFechaString(String fecha) {
        String[] fechaSplit = fecha.split("-");
        if (fechaSplit[0].length() == 4) {
            return fechaSplit[2] + "-" + fechaSplit[1] + "-" + fechaSplit[0];
        } else {
            return fecha;
        }
    }

    private void mostrarListaPDF() {
        Map pSQL = new HashMap<String, Object>();
        InputStream subImg = getClass().getClassLoader().getResourceAsStream("img/logo.png");
//                    String val = "{\"ventas\": " + recuperarTickets() + "}";
//        pSQL.put("repJsonString", "{\"ventas\": []}");

        pSQL.put("repJsonString", "{\"ventas\": " + cargarParaImpresion() + "}");

//                pSQL.put("subRepNomFun", UserHolder.get().getFuncionario().getNombreCompleto());
        Date date = new Date();
        Timestamp ts = new Timestamp(date.getTime());
        String fechaArray[] = ts.toString().split(" ");
        String subRepTimestamp = ordenandoFechaString(fechaArray[0]) + " " + fechaArray[1];
        pSQL.put("subRepTimestamp", subRepTimestamp);
        pSQL.put("subRepNomFun", UserHolder.get().getIdfuncionario().getNombreCompleto());
        pSQL.put("subRepEmpresa", "MUTUAL NAC DE FUNC DEL MINISTERIO DE SALUD PÚBLICA Y BIENESTAR SOCIAL");
//        pSQL.put("subRepSucursal", "FORMULARIO DE PERMISO");
        pSQL.put("subRepSucursal", "CASA CENTRAL");
        pSQL.put("subRepPathLogo", subImg);
        pSQL.put("subRepPathLogoCP", subImg);
        StreamResource.StreamSource source = new StreamResource.StreamSource() {

            public InputStream getStream() {
                byte[] b = null;
                String archivo = "inspeccion";
                try {
                    b = JasperRunManager.runReportToPdf(getClass().getClassLoader().getResourceAsStream(archivo + ".jasper"), pSQL);
//                            b = JasperRunManager.runReportToPdf("C:\\Users\\hruiz\\Documents\\reporte\\ticket_mutual.jasper", pSQL);
                } catch (JRException ex) {
                    ex.printStackTrace();
                }
                return new ByteArrayInputStream(b);
            }
        };

        String archivo = "inspeccion";

        StreamResource resource = new StreamResource(source, archivo + ".pdf");
        resource.setCacheTime(0);
        resource.setMIMEType("application/pdf");

        Window window = new Window();
        window.setWidth(800, Sizeable.Unit.PIXELS);
        window.setHeight(600, Sizeable.Unit.PIXELS);
        window.setModal(true);
        window.center();
        BrowserFrame pdf = new BrowserFrame("test", resource);
        pdf.setSizeFull();

        window.setContent(pdf);
        getUI().addWindow(window);
    }

    private JSONArray cargarInspeccion() {
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        JSONParser parser = new JSONParser();
        JSONArray jsonArrayDato = new JSONArray();
        InspeccionDTO solicitudDetalle = controller.listarInspeccionDTOPorId(inspeccionSeleccionado.getId() + "");

        JSONObject jsonObj = new JSONObject();
        try {
            jsonObj.put("fechavigencia", formatter.format(inspeccionSeleccionado.getFecha()));
        } catch (Exception e) {
            jsonObj.put("fechavigencia", "");
        } finally {
        }
        try {
            jsonObj.put("nombre", inspeccionSeleccionado.getNombre() + " " + inspeccionSeleccionado.getApellido());
        } catch (Exception e) {
            jsonObj.put("nombre", "");
        } finally {
        }
        try {
            jsonObj.put("cedula", inspeccionSeleccionado.getCedula());
        } catch (Exception e) {
            jsonObj.put("cedula", "");
        } finally {
        }
        try {
            jsonObj.put("edad", inspeccionSeleccionado.getEdad());
        } catch (Exception e) {
            jsonObj.put("edad", "");
        } finally {
        }
        try {
            jsonObj.put("sexo", inspeccionSeleccionado.getSexo().equalsIgnoreCase("M") ? "MASCULINO" : "FEMENINO");
        } catch (Exception e) {
            jsonObj.put("sexo", "");
        } finally {
        }
        try {
            jsonObj.put("sanguineo", inspeccionSeleccionado.getGruposanguineo());
        } catch (Exception e) {
            jsonObj.put("sanguineo", "");
        } finally {
        }
        try {
            jsonObj.put("peso", inspeccionSeleccionado.getPeso() + "");
        } catch (Exception e) {
            jsonObj.put("peso", "");
        } finally {
        }
        try {
            jsonObj.put("seguromedico", inspeccionSeleccionado.getSeguromedico());
        } catch (Exception e) {
            jsonObj.put("seguromedico", "");
        } finally {
        }
        try {
            jsonObj.put("dst", inspeccionSeleccionado.getDst() ? "(  X  )" : "(     )");
        } catch (Exception e) {
        } finally {
        }
        try {
            jsonObj.put("asma", inspeccionSeleccionado.getAsma() ? "(  X  )" : "(     )");
        } catch (Exception e) {
        } finally {
        }
        try {
            jsonObj.put("card", inspeccionSeleccionado.getCard() ? "(  X  )" : "(     )");
        } catch (Exception e) {
        } finally {
        }
        try {
            jsonObj.put("acv", inspeccionSeleccionado.getAcv() ? "(  X  )" : "(     )");
        } catch (Exception e) {
        } finally {
        }
        try {
            jsonObj.put("conv", inspeccionSeleccionado.getConv() ? "(  X  )" : "(     )");
        } catch (Exception e) {
        } finally {
        }
        try {
            jsonObj.put("hta", inspeccionSeleccionado.getHta() ? "(  X  )" : "(     )");
        } catch (Exception e) {
        } finally {
        }
        try {
            jsonObj.put("epoc", inspeccionSeleccionado.getEpoc() ? "(  X  )" : "(     )");
        } catch (Exception e) {
        } finally {
        }
        try {
            jsonObj.put("otros", inspeccionSeleccionado.getOtros() ? "(  X  )" : "(     )");
        } catch (Exception e) {
        } finally {
        }
        try {
            jsonObj.put("alergico", inspeccionSeleccionado.getAlergico() ? "(  X  )" : "(     )");
        } catch (Exception e) {
        } finally {
        }
        try {
            jsonObj.put("alergico", inspeccionSeleccionado.getComentarioalergico());
        } catch (Exception e) {
            jsonObj.put("alergico", "");
        } finally {
        }
        try {
            jsonObj.put("motivoconsulta", inspeccionSeleccionado.getMotivoconsulta());
        } catch (Exception e) {
            jsonObj.put("motivoconsulta", "");
        } finally {
        }
        try {
            jsonObj.put("ta", inspeccionSeleccionado.getTa());
        } catch (Exception e) {
            jsonObj.put("ta", "");
        } finally {
        }
        try {
            jsonObj.put("t", inspeccionSeleccionado.getT());
        } catch (Exception e) {
            jsonObj.put("t", "");
        } finally {
        }
        try {
            jsonObj.put("fc", inspeccionSeleccionado.getFc());
        } catch (Exception e) {
            jsonObj.put("fc", "");
        } finally {
        }
        try {
            jsonObj.put("descripcion1", inspeccionSeleccionado.getHallazgos().substring(0, inspeccionSeleccionado.getHallazgos().length()));
        } catch (Exception e) {
            jsonObj.put("descripcion1", "");
        } finally {
        }
        try {
            jsonObj.put("descripcion2", inspeccionSeleccionado.getHallazgos().substring(101, 200));
        } catch (Exception e) {
            jsonObj.put("descripcion2", "");
        } finally {
        }
        try {
            jsonObj.put("descripcion3", inspeccionSeleccionado.getHallazgos().substring(201, 300));
        } catch (Exception e) {
            jsonObj.put("descripcion3", "");
        } finally {
        }
        try {
            jsonObj.put("descripcion4", inspeccionSeleccionado.getHallazgos().substring(301, 400));
        } catch (Exception e) {
            jsonObj.put("descripcion4", "");
        } finally {
        }
        try {
            jsonObj.put("tratamiento1", inspeccionSeleccionado.getTratamiento().substring(0, inspeccionSeleccionado.getTratamiento().length()));
        } catch (Exception e) {
            jsonObj.put("tratamiento1", "");
        } finally {
        }
        try {
            jsonObj.put("tratamiento2", inspeccionSeleccionado.getTratamiento().substring(101, 200));
        } catch (Exception e) {
            jsonObj.put("tratamiento2", "");
        } finally {
        }
        try {
            jsonObj.put("tratamiento3", inspeccionSeleccionado.getTratamiento().substring(201, 300));
        } catch (Exception e) {
            jsonObj.put("tratamiento3", "");
        } finally {
        }
        try {
            jsonObj.put("contacto1", inspeccionSeleccionado.getContactoemergencia().substring(0, inspeccionSeleccionado.getContactoemergencia().length()));
        } catch (Exception e) {
            jsonObj.put("contacto1", "");
        } finally {
        }
        try {
            jsonObj.put("contacto2", inspeccionSeleccionado.getContactoemergencia().substring(101, 200));
        } catch (Exception e) {
            jsonObj.put("contacto2", "");
        } finally {
        }
        try {
            jsonObj.put("id", inspeccionSeleccionado.getId());
        } catch (Exception e) {
            jsonObj.put("id", "0");
        } finally {
        }
        try {
            jsonObj.put("fechaimpresion", formatter.format(new Date()));
        } catch (Exception e) {
            jsonObj.put("fechaimpresion", "");
        } finally {
        }

        jsonArrayDato.add(jsonObj);
        return jsonArrayDato;
    }

    private JSONArray cargarParaImpresion() {
        JSONArray jsonArrayDato = new JSONArray();
        for (InspeccionDTO ticket : lista) {
            org.json.simple.JSONObject jsonObj = new org.json.simple.JSONObject();
            jsonObj.put("subRepNomFun", UserHolder.get().getIdfuncionario().getNombreCompleto());
            jsonObj.put("subRepTimestamp", "");
            SimpleDateFormat formatFecs = new SimpleDateFormat("dd-MM-yyyy");
            try {
                jsonObj.put("cedula", ticket.getCedula());
            } catch (Exception e) {
                jsonObj.put("cedula", "");
            } finally {
            }
            try {
                jsonObj.put("invitado", ticket.getNombre().toUpperCase() + " " + ticket.getApellido().toUpperCase());
            } catch (Exception e) {
                jsonObj.put("invitado", "");
            } finally {
            }
            try {
                jsonObj.put("parentesco", ticket.getParentesco().toUpperCase());
            } catch (Exception e) {
                jsonObj.put("parentesco", "");
            } finally {
            }
            try {
                jsonObj.put("cisocio", ticket.getCedulasocio());
            } catch (Exception e) {
                jsonObj.put("cisocio", "");
            } finally {
            }
            try {
                jsonObj.put("socio", ticket.getNombresocio().toUpperCase() + " " + ticket.getApellidosocio().toUpperCase());
            } catch (Exception e) {
                jsonObj.put("socio", "");
            } finally {
            }
            try {
                jsonObj.put("desde", formatFecs.format(ticket.getFecharegistro()));
            } catch (Exception e) {
                jsonObj.put("desde", "");
            } finally {
            }
            try {
                jsonObj.put("hasta", formatFecs.format(ticket.getFecha()));
            } catch (Exception e) {
                jsonObj.put("hasta", "");
            } finally {
            }
            //jsonObj.put("fecha", ticket.getFecha());
            //jsonObj.put("numot", marcacionesController.getById(ticket.getId()) == null ? "--" : marcacionesController.getById(ticket.getId()).getSolicitud().getId());
            jsonArrayDato.add(jsonObj);
        }
        return jsonArrayDato;
    }
}
