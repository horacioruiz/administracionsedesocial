/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.vista;

import com.github.appreciated.material.MaterialTheme;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.navigator.View;
import com.vaadin.shared.ui.ValueChangeMode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import java.util.ArrayList;
import java.util.List;
import py.mutualmsp.mutualweb.dao.MotivoSolicitudAyudaDao;
import py.mutualmsp.mutualweb.entities.MotivoSolicitudAyuda;
import py.mutualmsp.mutualweb.formularios.MotivoAyudaForm;
import py.mutualmsp.mutualweb.util.ResourceLocator;
import py.mutualmsp.mutualweb.util.UserHolder;

/**
 *
 * @author Dbarreto
 */
public class MotivoAyudaVista extends CssLayout implements View {

    public static final String VIEW_NAME = "Motivos";
    Grid<MotivoSolicitudAyuda> grillaMotivoSolicitudAyuda = new Grid<>(MotivoSolicitudAyuda.class);
    TextField txtfFiltro = new TextField("Filtro");
    MotivoSolicitudAyudaDao cargoDao = ResourceLocator.locate(MotivoSolicitudAyudaDao.class);
    List<MotivoSolicitudAyuda> lista = new ArrayList<>();
    Button btnNuevo = new Button("");
    MotivoAyudaForm cargoForm = new MotivoAyudaForm();

    public MotivoAyudaVista() {
        setSizeFull();
        addStyleName("crud-view");

        btnNuevo.addStyleName(MaterialTheme.BUTTON_ROUND + " " +MaterialTheme.BUTTON_PRIMARY);
        btnNuevo.setIcon(VaadinIcons.PLUS_CIRCLE);

        cargoForm.setVisible(false);
        HorizontalLayout topLayout = new HorizontalLayout();
        topLayout.addComponents(txtfFiltro, btnNuevo);
        topLayout.setSpacing(true);
        topLayout.setComponentAlignment(txtfFiltro, Alignment.MIDDLE_LEFT);
        topLayout.setComponentAlignment(btnNuevo, Alignment.MIDDLE_RIGHT);
        topLayout.addStyleName("top-bar");

        txtfFiltro.setPlaceholder("Filtro de búsqueda");
        txtfFiltro.setValueChangeMode(ValueChangeMode.LAZY);
        txtfFiltro.addValueChangeListener(e -> updateList(e.getValue()));

        btnNuevo.addClickListener(e -> {
            grillaMotivoSolicitudAyuda.asSingleSelect().clear();
            cargoForm.setMotivoSolicitudAyuda(new MotivoSolicitudAyuda());
        });

        if ((UserHolder.get().getIdnivelusuario() == 8 || UserHolder.get().getIdnivelusuario() == 9 || UserHolder.get().getIdnivelusuario() == 10)) {
            btnNuevo.setEnabled(true);
        } else {
            btnNuevo.setEnabled(false);
        }

        HorizontalLayout horizontalLayout = new HorizontalLayout();
        horizontalLayout.addComponents(grillaMotivoSolicitudAyuda, cargoForm);
        horizontalLayout.setSizeFull();
        horizontalLayout.setExpandRatio(grillaMotivoSolicitudAyuda, 1);

        VerticalLayout barAndGridLayout = new VerticalLayout();
        barAndGridLayout.addComponent(topLayout);
        barAndGridLayout.addComponent(horizontalLayout);
        barAndGridLayout.setMargin(true);
        barAndGridLayout.setSpacing(true);
        barAndGridLayout.setSizeFull();
        barAndGridLayout.setExpandRatio(horizontalLayout, 1);
        barAndGridLayout.addStyleName("crud-main-layout");

        try {
            lista = cargoDao.listaMotivoSolicitudAyuda();
            grillaMotivoSolicitudAyuda.setItems(lista);
            grillaMotivoSolicitudAyuda.removeAllColumns();
            grillaMotivoSolicitudAyuda.addColumn(MotivoSolicitudAyuda::getId).setCaption("COD");
            grillaMotivoSolicitudAyuda.addColumn(MotivoSolicitudAyuda::getDescripcion).setCaption("Descripción");
            grillaMotivoSolicitudAyuda.setSizeFull();
            grillaMotivoSolicitudAyuda.asSingleSelect().addValueChangeListener(e -> {
                if ((UserHolder.get().getIdnivelusuario() == 8 || UserHolder.get().getIdnivelusuario() == 9 || UserHolder.get().getIdnivelusuario() == 10)) {
                    if (e.getValue() == null) {
                        cargoForm.setVisible(false);
                    } else {
                        cargoForm.setMotivoSolicitudAyuda(e.getValue());
                        cargoForm.setViejo();
                    }
                }
            });

            cargoForm.setGuardarListener(r -> {
                grillaMotivoSolicitudAyuda.clearSortOrder();
                lista = cargoDao.listaMotivoSolicitudAyuda();
                grillaMotivoSolicitudAyuda.setItems(lista);
            });
            cargoForm.setBorrarListener(r -> {
                grillaMotivoSolicitudAyuda.clearSortOrder();
            });
            cargoForm.setCancelarListener(r -> {
                grillaMotivoSolicitudAyuda.clearSortOrder();
            });

            addComponent(barAndGridLayout);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void updateList(String value) {
        try {
            lista = cargoDao.getMotivoSolicitudAyudaByDescripcion(value);
            grillaMotivoSolicitudAyuda.setSizeFull();
            grillaMotivoSolicitudAyuda.setItems(lista);
            grillaMotivoSolicitudAyuda.clearSortOrder();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
