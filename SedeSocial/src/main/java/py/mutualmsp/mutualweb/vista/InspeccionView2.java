package py.mutualmsp.mutualweb.vista;

import com.github.appreciated.material.MaterialTheme;
import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.event.ShortcutListener;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.*;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import py.mutualmsp.mutualweb.dao.InspeccionDao;
import py.mutualmsp.mutualweb.dao.TipoincidenciaDao;
import py.mutualmsp.mutualweb.entities.Inspeccion;
import py.mutualmsp.mutualweb.entities.Tipoincidencia;
import py.mutualmsp.mutualweb.formularios.InspeccionForm;
import py.mutualmsp.mutualweb.util.ResourceLocator;

/**
 * Created by Alfre on 14/6/2016.
 */
public class InspeccionView2 extends CssLayout implements View {

    public static final String VIEW_NAME = "Inspección";
    Grid<Inspeccion> grid = new Grid<>(Inspeccion.class);
    Button newCliente;
    Button btnEnfemeria;
    //Services ejb = ResourceLocator.locate(Services.class);
    TextField filter = new TextField("N° Cédula");
    InspeccionForm form = new InspeccionForm();
    InspeccionDao controller = ResourceLocator.locate(InspeccionDao.class);
    TipoincidenciaDao tipoincidenciaDao = ResourceLocator.locate(TipoincidenciaDao.class);
    private List<Inspeccion> lista;

    DateField dtfFechaDesde = new DateField("Fecha Desde");
    DateField dtfFechaHasta = new DateField("Fecha Hasta");
    ComboBox<Tipoincidencia> cmbTipoincidencia = new ComboBox<>("Tipo Incidencia");

    public InspeccionView2() {
        System.out.println("Nueva instancia InspeccionView");
        setSizeFull();
        addStyleName("crud-view");
        HorizontalLayout horizontalLayout = createTopBar();
        SimpleDateFormat formatSinHora = new SimpleDateFormat("dd-MM-yyyy");

//        filter.addValueChangeListener(vcl -> grid.clearSortOrder());
        form.setVisible(false);

        filter.setWidth(10f, TextField.UNITS_EM);
        dtfFechaDesde.setWidth(10f, TextField.UNITS_EM);
        dtfFechaHasta.setWidth(10f, TextField.UNITS_EM);
        cmbTipoincidencia.setWidth(12f, TextField.UNITS_EM);

        lista = controller.listaInspeccion();
        grid.setItems(lista);
        grid.removeAllColumns();
        grid.addColumn(Inspeccion::getId).setCaption("Codigo");
        grid.addColumn(e -> {
//            try {
//                DateTimeFormatter formatters = DateTimeFormatter.ofPattern("d-MM-uuuu");
//                return e.getFecha() == null ? "--" : e.getFecha().format(formatters);
//            } catch (Exception ex) {
                return "--";
//            } finally {
//            }
        }).setCaption("Fecha");
        grid.addColumn(e -> {
//            try {
//                DateTimeFormatter formatters = DateTimeFormatter.ofPattern("d-MM-uuuu");
//                return e.getFecha() == null ? "--" : e.getFecha().format(formatters);
//            } catch (Exception ex) {
                return "--";
//            } finally {
//            }

        }).setCaption("Fecha Vigencia");
//        grid.addColumn(Inspeccion::getFecha).setCaption("Fecha");
//        grid.addColumn(Inspeccion::getFechavigencia).setCaption("Fecha Vigencia");
        grid.addColumn(Inspeccion::getCedula).setCaption("Nro. Doc");
        grid.addColumn(Inspeccion::getNombre).setCaption("Nombre");
        grid.addColumn(Inspeccion::getApellido).setCaption("Apellido");
//        grid.addColumn(enfermeria -> {
//            try {
//                return enfermeria.getIdagendamiento().getId();
//            } catch (Exception e) {
//                return 0;
//            }
//
//        }).setCaption("N° Agendamiento");
//        grid.addColumn(Inspeccion::getIdagendamiento).setCaption("N° Agendamiento");
        grid.addColumn(tipoIncidencia -> {
            return "";
//            return tipoIncidencia. ().getDescripcion();
        }).setCaption("Tipo incidencia");
//        grid.addComponentColumn(e -> {
//            return e.getAproboData();
//        }).setCaption("Aprobó");
//        grid.addColumn(aprobo -> {
//            return aprobo.getAprobo() == true ? "SI" : "NO";
//        }).setCaption("Aprobó");
        grid.setSizeFull();

        filter.addShortcutListener(new ShortcutListener("Shortcut", KeyCode.ENTER, null) {
            @Override
            public void handleAction(Object sender, Object target) {
                // Do nice stuff
                if (filter.getValue() != null) {
                    grid.clearSortOrder();
                    //filterProyecto.setItems(proyectosController.listaProyectosPorInspeccion(filterInspeccion.getValue()));
                    //filterProyecto.setItemCaptionGenerator(Proyecto::getDescripcion);
                    grid.setItems(controller.listarInspeccionPorCI(filter.getValue().toUpperCase()));
                }
            }
        });

        HorizontalLayout hl = new HorizontalLayout();
        hl.addComponents(grid, form);
        hl.setSizeFull();
        hl.setExpandRatio(grid, 1);

        VerticalLayout verticalLayout = new VerticalLayout();
        verticalLayout.addComponent(horizontalLayout);
        verticalLayout.addComponent(hl);
        verticalLayout.setMargin(true);
        verticalLayout.setSpacing(true);
        verticalLayout.setSizeFull();
        verticalLayout.setExpandRatio(hl, 1);
        verticalLayout.setStyleName("crud-main-layout");
        addComponent(verticalLayout);

//        dtfFechaHasta.addValueChangeListener(e -> {
//            try {
//                if (e.getValue() != null || !e.equals("")) {
//                    java.util.Date fechaDesde = new SimpleDateFormat("yyyy-MM-dd").parse(dtfFechaDesde.getValue().toString());
//                    java.util.Date fechaHasta = new SimpleDateFormat("yyyy-MM-dd").parse(e.getValue().toString());
//                    lista = controller.getInspeccionByFecha(fechaDesde, fechaHasta);
//                    grid.setItems(lista);
//                } else {
//                    updateList("");
//                }
//            } catch (Exception ex) {
//                updateList("");
//            }
//        });
//        newCliente.addClickListener(
//                clickEvent -> form.setInspeccion(new Inspeccion())
//        );
        btnEnfemeria.addClickListener(
                clickEvent -> buscarData()
        );
//        filter.setPlaceholder("Filtre N° CI");
//        grid.asSingleSelect().addValueChangeListener(e -> {
//            if (e.getValue() != null) {
//                form.setInspeccion(e.getValue());
//            }
//        });

        cmbTipoincidencia.setItems(tipoincidenciaDao.listaTipoincidencia());
        cmbTipoincidencia.setItemCaptionGenerator(Tipoincidencia::getDescripcion);
//        cmbTipoincidencia.addValueChangeListener(e -> {
//            try {
//                if (e.getValue() != null || !e.equals("")) {
//                    filtroTipoIncidencia(e.getValue().getId());
//                } else {
//                    updateList("");
//                }
//            } catch (Exception ex) {
//                updateList("");
//            }
//        });

        grid.addItemClickListener(e -> {
            if (e.getMouseEventDetails().isDoubleClick()) {
//                form.setInspeccion(e.getItem());
                form.setVisible(true);
                verticalLayout.setVisible(false);
                grid.setVisible(false);
            }
        });

        form.setSaveListener(cliente -> {
            grid.clearSortOrder();
            grid.setVisible(true);
            lista = controller.listaInspeccion();
            grid.setItems(lista);
            form.setVisible(false);
            Notification.show("Nuevos Datos almacenados!.", Notification.Type.HUMANIZED_MESSAGE);
        });
        grid.asSingleSelect().addValueChangeListener(e -> {
            if (e.getValue() != null) {

                Inspeccion inviDto = new Inspeccion();
                inviDto.setId(e.getValue().getId());
                inviDto.setActivo(e.getValue().getActivo());
                inviDto.setAcv(e.getValue().getAcv());
                inviDto.setAlergico(e.getValue().getAlergico());
                inviDto.setApellido(e.getValue().getApellido());
                inviDto.setAsma(e.getValue().getAsma());
                inviDto.setCard(e.getValue().getCard());
                inviDto.setCedula(e.getValue().getCedula());
                inviDto.setComentarioalergico(e.getValue().getComentarioalergico());
                inviDto.setContactoemergencia(e.getValue().getContactoemergencia());
                inviDto.setConv(e.getValue().getConv());
                inviDto.setDst(e.getValue().getDst());
                inviDto.setEdad(e.getValue().getEdad());
                inviDto.setEpoc(e.getValue().getEpoc());
                inviDto.setFc(e.getValue().getFc());
                inviDto.setFecha(e.getValue().getFecha());
                inviDto.setGruposanguineo(e.getValue().getGruposanguineo());
                inviDto.setHallazgos(e.getValue().getHallazgos());
                inviDto.setHta(e.getValue().getHta());
                inviDto.setMotivoconsulta(e.getValue().getMotivoconsulta());
                inviDto.setNombre(e.getValue().getNombre());
                inviDto.setOtros(e.getValue().getOtros());
                inviDto.setPeso(e.getValue().getPeso());
                inviDto.setSeguromedico(e.getValue().getSeguromedico());
                inviDto.setSexo(e.getValue().getSexo());
                inviDto.setT(e.getValue().getT());
                inviDto.setTa(e.getValue().getTa());
                inviDto.setTratamiento(e.getValue().getTratamiento());

                form.setConsultaInspeccion(inviDto);
            }
        });
//        filter.setValue(VIEW_NAME);
        form.setCancelListener(cliente -> {
            grid.clearSortOrder();
            form.setVisible(false);
        });
        form.setDeleteListener(cliente -> {
            grid.clearSortOrder();
            grid.setVisible(true);
            lista = controller.listaInspeccion();
            grid.setItems(lista);
            form.setVisible(false);
            Notification.show("Atención", "Registro borrado correctamente", Notification.Type.HUMANIZED_MESSAGE);
        });
    }

    private HorizontalLayout createTopBar() {

        newCliente = new Button("Nuevo Inspeccion");
        newCliente.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_FRIENDLY);
        newCliente.setIcon(FontAwesome.PLUS_CIRCLE);

        btnEnfemeria = new Button();
        btnEnfemeria.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_PRIMARY);
        btnEnfemeria.setIcon(FontAwesome.SEARCH);

        HorizontalLayout layout = new HorizontalLayout();
        layout.setSpacing(true);
        layout.setWidth("100%");
        layout.addComponent(filter);
        layout.addComponent(dtfFechaDesde);
        layout.addComponent(dtfFechaHasta);
        layout.addComponent(cmbTipoincidencia);
        layout.addComponent(btnEnfemeria);
        layout.addComponent(newCliente);
        //layout.setComponentAlignment(filter, Alignment.MIDDLE_LEFT);
        //layout.setExpandRatio(filter, 1);
        layout.setSpacing(true);
        layout.setStyleName("top-bar");
        return layout;
    }

    private void updateList(String value) {
        try {
            lista = controller.getListInspeccion(value);
            grid.setSizeFull();
            grid.setItems(lista);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void filtroTipoIncidencia(Long idtipoIncidencia) {
//        try {
//            lista = controller.getInspeccionByTipoIncidencia(idtipoIncidencia);
//            grid.setItems(lista);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent viewChangeEvent) {

    }

    private void buscarData() {
        try {
            lista = new ArrayList<>();
            grid.clearSortOrder();
            lista = controller.getDataFromFilter(filter.getValue(), dtfFechaDesde.getValue(), dtfFechaHasta.getValue(), cmbTipoincidencia.getValue());
            grid.setItems(lista);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
