/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author DBarreto
 */
@Entity
@Table(name = "motivo_solicitud_ayuda")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "MotivoSolicitudAyuda.findAll", query = "SELECT c FROM MotivoSolicitudAyuda c")
    , @NamedQuery(name = "MotivoSolicitudAyuda.findById", query = "SELECT c FROM MotivoSolicitudAyuda c WHERE c.id = :id")
    , @NamedQuery(name = "MotivoSolicitudAyuda.findByDescripcion", query = "SELECT c FROM MotivoSolicitudAyuda c WHERE c.descripcion = :descripcion")})
public class MotivoSolicitudAyuda implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;
    @Basic(optional = false)
    @Column(name = "descripcion")
    private String descripcion;

    public MotivoSolicitudAyuda() {
    }

    public MotivoSolicitudAyuda(Long id) {
        this.id = id;
    }

    public MotivoSolicitudAyuda(Long id, String descripcion) {
        this.id = id;
        this.descripcion = descripcion;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MotivoSolicitudAyuda)) {
            return false;
        }
        MotivoSolicitudAyuda other = (MotivoSolicitudAyuda) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.mutualmsp.mutualweb.entities.MotivoSolicitudAyuda[ id=" + id + " ]";
    }

}
