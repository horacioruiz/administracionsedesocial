/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author hectorvillalba
 */
@Entity
@Table(name = "estado_solicitud_ayuda")
@XmlRootElement
public class EstadoSolicitudAyuda implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;

    @Column(name = "fecha")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecha;

    @JoinColumn(name = "id_usuario", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Usuario usuario;

    @JoinColumn(name = "id_solicitud", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private SolicitudAyuda solicitudAyuda;

    @Column(name = "necesidad")
    private String necesidad;

    @Column(name = "estado")
    private String estado;

    @Column(name = "dptodesde")
    private String dptodesde;

    @Column(name = "dptohasta")
    private String dptohasta;

    @Column(name = "observacion")
    private String observacion;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public SolicitudAyuda getSolicitudAyuda() {
        return solicitudAyuda;
    }

    public void setSolicitudAyuda(SolicitudAyuda solicitudAyuda) {
        this.solicitudAyuda = solicitudAyuda;
    }

    public String getNecesidad() {
        return necesidad;
    }

    public void setNecesidad(String necesidad) {
        this.necesidad = necesidad;
    }

    public String getDptodesde() {
        return dptodesde;
    }

    public void setDptodesde(String dptodesde) {
        this.dptodesde = dptodesde;
    }

    public String getDptohasta() {
        return dptohasta;
    }

    public void setDptohasta(String dptohasta) {
        this.dptohasta = dptohasta;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

}
