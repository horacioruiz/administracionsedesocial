/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author hectorvillalba
 */
@Entity
@Table(name = "cuenta")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Cuenta.findAll", query = "SELECT c FROM Cuenta c")
    , @NamedQuery(name = "Cuenta.findById", query = "SELECT c FROM Cuenta c WHERE c.id = :id")
    , @NamedQuery(name = "Cuenta.findByDescripcion", query = "SELECT c FROM Cuenta c WHERE c.descripcion = :descripcion")
    , @NamedQuery(name = "Cuenta.findByIdtipocuenta", query = "SELECT c FROM Cuenta c WHERE c.idtipocuenta = :idtipocuenta")
    , @NamedQuery(name = "Cuenta.findByPrioridad", query = "SELECT c FROM Cuenta c WHERE c.prioridad = :prioridad")
    , @NamedQuery(name = "Cuenta.findByInteresmoratorio", query = "SELECT c FROM Cuenta c WHERE c.interesmoratorio = :interesmoratorio")
    , @NamedQuery(name = "Cuenta.findByActivo", query = "SELECT c FROM Cuenta c WHERE c.activo = :activo")
    , @NamedQuery(name = "Cuenta.findByIdtipodocumento", query = "SELECT c FROM Cuenta c WHERE c.idtipodocumento = :idtipodocumento")
    , @NamedQuery(name = "Cuenta.findByIdimpuesto", query = "SELECT c FROM Cuenta c WHERE c.idimpuesto = :idimpuesto")
    , @NamedQuery(name = "Cuenta.findByCodigo", query = "SELECT c FROM Cuenta c WHERE c.codigo = :codigo")
    , @NamedQuery(name = "Cuenta.findByCuentacontable", query = "SELECT c FROM Cuenta c WHERE c.cuentacontable = :cuentacontable")})
public class Cuenta implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "id")
    private Long id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "descripcion")
    private String descripcion;
    @Basic(optional = false)
    @NotNull
    @Column(name = "idtipocuenta")
    private long idtipocuenta;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 11)
    @Column(name = "prioridad")
    private String prioridad;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Column(name = "interesmoratorio")
    private BigDecimal interesmoratorio;
    @Column(name = "activo")
    private Boolean activo;
    @Basic(optional = false)
    @NotNull
    @Column(name = "idtipodocumento")
    private long idtipodocumento;
    @Basic(optional = false)
    @NotNull
    @Column(name = "idimpuesto")
    private long idimpuesto;
    @Size(max = 3)
    @Column(name = "codigo")
    private String codigo;
    @Size(max = 15)
    @Column(name = "cuentacontable")
    private String cuentacontable;

    public Cuenta() {
    }

    public Cuenta(Long id) {
        this.id = id;
    }

    public Cuenta(Long id, String descripcion, long idtipocuenta, String prioridad, BigDecimal interesmoratorio, long idtipodocumento, long idimpuesto) {
        this.id = id;
        this.descripcion = descripcion;
        this.idtipocuenta = idtipocuenta;
        this.prioridad = prioridad;
        this.interesmoratorio = interesmoratorio;
        this.idtipodocumento = idtipodocumento;
        this.idimpuesto = idimpuesto;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public long getIdtipocuenta() {
        return idtipocuenta;
    }

    public void setIdtipocuenta(long idtipocuenta) {
        this.idtipocuenta = idtipocuenta;
    }

    public String getPrioridad() {
        return prioridad;
    }

    public void setPrioridad(String prioridad) {
        this.prioridad = prioridad;
    }

    public BigDecimal getInteresmoratorio() {
        return interesmoratorio;
    }

    public void setInteresmoratorio(BigDecimal interesmoratorio) {
        this.interesmoratorio = interesmoratorio;
    }

    public Boolean getActivo() {
        return activo;
    }

    public void setActivo(Boolean activo) {
        this.activo = activo;
    }

    public long getIdtipodocumento() {
        return idtipodocumento;
    }

    public void setIdtipodocumento(long idtipodocumento) {
        this.idtipodocumento = idtipodocumento;
    }

    public long getIdimpuesto() {
        return idimpuesto;
    }

    public void setIdimpuesto(long idimpuesto) {
        this.idimpuesto = idimpuesto;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getCuentacontable() {
        return cuentacontable;
    }

    public void setCuentacontable(String cuentacontable) {
        this.cuentacontable = cuentacontable;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Cuenta)) {
            return false;
        }
        Cuenta other = (Cuenta) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.mutualmsp.gestion.ws.entities.Cuenta[ id=" + id + " ]";
    }
    
}
