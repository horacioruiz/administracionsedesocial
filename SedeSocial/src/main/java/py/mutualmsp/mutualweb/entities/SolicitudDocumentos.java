/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.entities;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author hectorvillalba
 */
@Entity
@Table(name = "solicitud_documentos")
@XmlRootElement
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class SolicitudDocumentos implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @JoinColumn(name = "idsolicitudcredito", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private SolicitudCredito solicitudCredito;

    @Column(name = "urlcifrontal")
    private String urlCiFrontal;

    @Column(name = "urlcidorso")
    private String urlCiDorso;

    @Column(name = "urlliquidacion")
    private String urlLiquidacion;

    @Column(name = "urlservicio")
    private String urlServicio;

    @Column(name = "urlsenepa")
    private String urlSenepa;

    @Column(name = "urlcovid1")
    private String urlcovid1;

    @Column(name = "urlcovid2")
    private String urlcovid2;

    public SolicitudDocumentos() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public SolicitudCredito getSolicitudCredito() {
        return solicitudCredito;
    }

    public String getUrlcovid1() {
        return urlcovid1;
    }

    public void setUrlcovid1(String urlcovid1) {
        this.urlcovid1 = urlcovid1;
    }

    public String getUrlcovid2() {
        return urlcovid2;
    }

    public void setUrlcovid2(String urlcovid2) {
        this.urlcovid2 = urlcovid2;
    }

    public void setSolicitudCredito(SolicitudCredito solicitudCredito) {
        this.solicitudCredito = solicitudCredito;
    }

    public String getUrlCiFrontal() {
        return urlCiFrontal;
    }

    public void setUrlCiFrontal(String urlCiFrontal) {
        this.urlCiFrontal = urlCiFrontal;
    }

    public String getUrlCiDorso() {
        return urlCiDorso;
    }

    public void setUrlCiDorso(String urlCiDorso) {
        this.urlCiDorso = urlCiDorso;
    }

    public String getUrlLiquidacion() {
        return urlLiquidacion;
    }

    public String getUrlSenepa() {
        return urlSenepa;
    }

    public void setUrlSenepa(String urlSenepa) {
        this.urlSenepa = urlSenepa;
    }

    public String getUrlServicio() {
        return urlServicio;
    }

    public void setUrlServicio(String urlServicio) {
        this.urlServicio = urlServicio;
    }

    public void setUrlLiquidacion(String urlLiquidacion) {
        this.urlLiquidacion = urlLiquidacion;
    }

}
