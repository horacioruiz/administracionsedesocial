/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.entities;

import com.vaadin.server.FileResource;
import com.vaadin.server.Sizeable;
import com.vaadin.server.VaadinService;
import com.vaadin.ui.Image;
import java.io.File;
import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author DBarreto
 */
@Entity
@Table(name = "formulario")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Formulario.findAll", query = "SELECT d FROM Formulario d")
    , @NamedQuery(name = "Formulario.findById", query = "SELECT d FROM Formulario d WHERE d.id = :id")
    , @NamedQuery(name = "Formulario.findByDescripcion", query = "SELECT d FROM Formulario d WHERE d.descripcion = :descripcion")})
public class Formulario implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;
    @Column(name = "descripcion")
    private String descripcion;
    @Column(name = "abrev")
    private String abrev;
    @Column(name = "estado")
    private Boolean estado;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "formulario", fetch = FetchType.LAZY)
    private List<Motivos> motivoList;

    public Formulario() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Boolean getEstado() {
        return estado;
    }

    public String getAbrev() {
        return abrev;
    }

    public void setAbrev(String abrev) {
        this.abrev = abrev;
    }

    public Image getEstadoIcon() {
        String basepath = VaadinService.getCurrent()
                .getBaseDirectory().getAbsolutePath();
        if (this.estado) {
            FileResource resource = new FileResource(new File(basepath
                    + "/WEB-INF/images/good.png"));
            Image imagen = new Image("Image from file", resource);
            imagen.setWidth(2, Sizeable.Unit.EM);
            return imagen;
        } else {
            FileResource resource = new FileResource(new File(basepath
                    + "/WEB-INF/images/wrong.png"));
            Image imagen = new Image("Image from file", resource);
            imagen.setWidth(2, Sizeable.Unit.EM);
            return imagen;
        }
    }

    public void setEstado(Boolean estado) {
        this.estado = estado;
    }

    @XmlTransient
    public List<Motivos> getMotivoList() {
        return motivoList;
    }

    public void setMotivoList(List<Motivos> motivoList) {
        this.motivoList = motivoList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Formulario)) {
            return false;
        }
        Formulario other = (Formulario) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.mutualmsp.mutualweb.entities.Formulario[ id=" + id + " ]";
    }

}
