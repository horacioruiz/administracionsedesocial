/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.entities;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author DBarreto
 */

@Entity
@Table(name = "inspeccion", schema = "sedesocial")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Inspeccion.findAll", query = "SELECT c FROM Inspeccion c")
    , @NamedQuery(name = "Inspeccion.findById", query = "SELECT c FROM Inspeccion c WHERE c.id = :id")
//    , @NamedQuery(name = "Inspeccion.findByDescripcion", query = "SELECT c FROM Inspeccion c WHERE c.descripcion = :descripcion")
})
public class Inspeccion implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;

    @Column(name = "fecha")
//    @Temporal(TemporalType.TIMESTAMP)
    private Date fecha;

    @Column(name = "fecharegistro")
//    @Temporal(TemporalType.TIMESTAMP)
    private Date fecharegistro;

    @Column(name = "activo")
    private Boolean activo;

    @Column(name = "cedula")
    private String cedula;

    @Column(name = "nombre")
    private String nombre;

    @Column(name = "apellido")
    private String apellido;

    @Column(name = "gruposanguineo")
    private String gruposanguineo;

    @Column(name = "edad")
    private Integer edad;

    @Column(name = "peso")
    private Double peso;

    @Column(name = "sexo")
    private String sexo;

    @Column(name = "seguromedico")
    private String seguromedico;

    @Column(name = "dst")
    private Boolean dst;

    @Column(name = "asma")
    private Boolean asma;

    @Column(name = "card")
    private Boolean card;

    @Column(name = "acv")
    private Boolean acv;

    @Column(name = "conv")
    private Boolean conv;

    @Column(name = "hta")
    private Boolean hta;

    @Column(name = "epoc")
    private Boolean epoc;

    @Column(name = "otros")
    private Boolean otros;

    @Column(name = "alergico")
    private Boolean alergico;

    @Column(name = "comentarioalergico")
    private String comentarioalergico;

    @Column(name = "motivoconsulta")
    private String motivoconsulta;

    @Column(name = "ta")
    private String ta;

    @Column(name = "t")
    private String t;

    @Column(name = "fc")
    private String fc;

    @Column(name = "hallazgos")
    private String hallazgos;

    @Column(name = "tratamiento")
    private String tratamiento;

    @Column(name = "contactoemergencia")
    private String contactoemergencia;

    @Column(name = "cedulasocio")
    private String cedulasocio;

    @Column(name = "nombresocio")
    private String nombresocio;

    @Column(name = "apellidosocio")
    private String apellidosocio;

    @Column(name = "parentesco")
    private String parentesco;
    @Column(name = "motivo_consulta")
    private String motivoConsulta;
    @Column(name = "medicacion_consulta")
    private String medicacionConsulta;
    @Column(name = "observacion_consulta")
    private String observacionConsulta;

    public String getMotivoConsulta() {
        return motivoConsulta;
    }

    public void setMotivoConsulta(String motivoConsulta) {
        this.motivoConsulta = motivoConsulta;
    }

    public String getMedicacionConsulta() {
        return medicacionConsulta;
    }

    public void setMedicacionConsulta(String medicacionConsulta) {
        this.medicacionConsulta = medicacionConsulta;
    }

    public String getObservacionConsulta() {
        return observacionConsulta;
    }

    public void setObservacionConsulta(String observacionConsulta) {
        this.observacionConsulta = observacionConsulta;
    }
    
    

    public Inspeccion() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Boolean getActivo() {
        return activo;
    }

    public void setActivo(Boolean activo) {
        this.activo = activo;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public String getT() {
        return t;
    }

    public void setT(String t) {
        this.t = t;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getGruposanguineo() {
        return gruposanguineo;
    }

    public void setGruposanguineo(String gruposanguineo) {
        this.gruposanguineo = gruposanguineo;
    }

    public Integer getEdad() {
        return edad;
    }

    public void setEdad(Integer edad) {
        this.edad = edad;
    }

    public Double getPeso() {
        return peso;
    }

    public void setPeso(Double peso) {
        this.peso = peso;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public String getSeguromedico() {
        return seguromedico;
    }

    public void setSeguromedico(String seguromedico) {
        this.seguromedico = seguromedico;
    }

    public Boolean getDst() {
        return dst;
    }

    public void setDst(Boolean dst) {
        this.dst = dst;
    }

    public Boolean getAsma() {
        return asma;
    }

    public void setAsma(Boolean asma) {
        this.asma = asma;
    }

    public Boolean getCard() {
        return card;
    }

    public void setCard(Boolean card) {
        this.card = card;
    }

    public Boolean getAcv() {
        return acv;
    }

    public void setAcv(Boolean acv) {
        this.acv = acv;
    }

    public Boolean getConv() {
        return conv;
    }

    public void setConv(Boolean conv) {
        this.conv = conv;
    }

    public Boolean getHta() {
        return hta;
    }

    public void setHta(Boolean hta) {
        this.hta = hta;
    }

    public Boolean getEpoc() {
        return epoc;
    }

    public void setEpoc(Boolean epoc) {
        this.epoc = epoc;
    }

    public Boolean getOtros() {
        return otros;
    }

    public void setOtros(Boolean otros) {
        this.otros = otros;
    }

    public Boolean getAlergico() {
        return alergico;
    }

    public void setAlergico(Boolean alergico) {
        this.alergico = alergico;
    }

    public String getComentarioalergico() {
        return comentarioalergico;
    }

    public void setComentarioalergico(String comentarioalergico) {
        this.comentarioalergico = comentarioalergico;
    }

    public String getMotivoconsulta() {
        return motivoconsulta;
    }

    public void setMotivoconsulta(String motivoconsulta) {
        this.motivoconsulta = motivoconsulta;
    }

    public String getTa() {
        return ta;
    }

    public void setTa(String ta) {
        this.ta = ta;
    }

    public String getFc() {
        return fc;
    }

    public void setFc(String fc) {
        this.fc = fc;
    }

    public String getHallazgos() {
        return hallazgos;
    }

    public void setHallazgos(String hallazgos) {
        this.hallazgos = hallazgos;
    }

    public String getTratamiento() {
        return tratamiento;
    }

    public void setTratamiento(String tratamiento) {
        this.tratamiento = tratamiento;
    }

    public String getContactoemergencia() {
        return contactoemergencia;
    }

    public void setContactoemergencia(String contactoemergencia) {
        this.contactoemergencia = contactoemergencia;
    }

    public String getCedulasocio() {
        return cedulasocio;
    }

    public void setCedulasocio(String cedulasocio) {
        this.cedulasocio = cedulasocio;
    }

    public String getNombresocio() {
        return nombresocio;
    }

    public void setNombresocio(String nombresocio) {
        this.nombresocio = nombresocio;
    }

    public String getApellidosocio() {
        return apellidosocio;
    }

    public void setApellidosocio(String apellidosocio) {
        this.apellidosocio = apellidosocio;
    }

    public Date getFecharegistro() {
        return fecharegistro;
    }

    public void setFecharegistro(Date fecharegistro) {
        this.fecharegistro = fecharegistro;
    }

    public String getParentesco() {
        return parentesco;
    }

    public void setParentesco(String parentesco) {
        this.parentesco = parentesco;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Inspeccion)) {
            return false;
        }
        Inspeccion other = (Inspeccion) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.mutualmsp.mutualweb.entities.Inspeccion[ id=" + id + " ]";
    }

}
