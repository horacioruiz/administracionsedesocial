/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Dbarreto
 */
@Entity
@Table(name = "socio")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Socio.findAll", query = "SELECT u FROM Socio u")
    , @NamedQuery(name = "Socio.findById", query = "SELECT u FROM Socio u WHERE u.id = :id")
})

public class Socio implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;

    @Column(name = "nombre")
    private String nombre;
    @Column(name = "apellido")
    private String apellido;
    @Column(name = "cedula")
    private String cedula;
    @Column(name = "direccion")
    private String direccion;
    
    @Column(name = "telefonolineabaja")
    private String telefonolineabaja;
    
    @Column(name = "telefonocelularprincipal")
    private String telefonocelularprincipal;
    
    @Column(name = "fechanacimiento")
    private Date fechanacimiento;
    @Column(name = "numerocasa")
    private String numerocasa;
    @Transient
    private String nombreCompleto;

    public String getNombreCompleto() {
        nombreCompleto = nombre + " " + apellido;
        return nombreCompleto;
    }

    public Socio() {
    }

    public Socio(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTelefonolineabaja() {
        return telefonolineabaja;
    }

    public void setTelefonolineabaja(String telefonolineabaja) {
        this.telefonolineabaja = telefonolineabaja;
    }

    public String getTelefonocelularprincipal() {
        return telefonocelularprincipal;
    }

    public void setTelefonocelularprincipal(String telefonocelularprincipal) {
        this.telefonocelularprincipal = telefonocelularprincipal;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Date getFechanacimiento() {
        return fechanacimiento;
    }

    public void setFechanacimiento(Date fechanacimiento) {
        this.fechanacimiento = fechanacimiento;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getNumerocasa() {
        return numerocasa;
    }

    public void setNumerocasa(String numerocasa) {
        this.numerocasa = numerocasa;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Socio)) {
            return false;
        }
        Socio other = (Socio) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.mutualmsp.mutualweb.entities.Socio[ id=" + id + " ]";
    }

    public boolean isInAnyRole(List<Boolean> roles) {
        for (Boolean r : roles) {
            if (r) {
                return true;
            }
        }
        return false;
    }

}
