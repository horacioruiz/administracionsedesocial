/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author hectorvillalba
 */
@Entity
@Table(name = "tipocredito")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Tipocredito.findAll", query = "SELECT t FROM Tipocredito t"),
    @NamedQuery(name = "Tipocredito.findById", query = "SELECT t FROM Tipocredito t WHERE t.id = :id"),
    @NamedQuery(name = "Tipocredito.findByDescripcion", query = "SELECT t FROM Tipocredito t WHERE t.descripcion = :descripcion"),
    @NamedQuery(name = "Tipocredito.findByActivo", query = "SELECT t FROM Tipocredito t WHERE t.activo = :activo"),
    @NamedQuery(name = "Tipocredito.findByDescripcionbreve", query = "SELECT t FROM Tipocredito t WHERE t.descripcionbreve = :descripcionbreve"),
    @NamedQuery(name = "Tipocredito.findByCancela", query = "SELECT t FROM Tipocredito t WHERE t.cancela = :cancela"),
    @NamedQuery(name = "Tipocredito.findByExoneracion", query = "SELECT t FROM Tipocredito t WHERE t.exoneracion = :exoneracion"),
    @NamedQuery(name = "Tipocredito.findByPrestamo", query = "SELECT t FROM Tipocredito t WHERE t.prestamo = :prestamo"),
    @NamedQuery(name = "Tipocredito.findByGeneraretencion", query = "SELECT t FROM Tipocredito t WHERE t.generaretencion = :generaretencion"),
    @NamedQuery(name = "Tipocredito.findByRecupero", query = "SELECT t FROM Tipocredito t WHERE t.recupero = :recupero")})
public class Tipocredito implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "id")
    private Long id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "descripcion")
    private String descripcion;
    @Column(name = "activo")
    private Boolean activo;
    @Size(max = 3)
    @Column(name = "descripcionbreve")
    private String descripcionbreve;
    @Column(name = "cancela")
    private Boolean cancela;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Column(name = "exoneracion")
    private BigDecimal exoneracion;
    @Column(name = "prestamo")
    private Boolean prestamo;
    @Column(name = "generaretencion")
    private Boolean generaretencion;
    @Column(name = "recupero")
    private Boolean recupero;

    public Tipocredito() {
    }

    public Tipocredito(Long id) {
        this.id = id;
    }

    public Tipocredito(Long id, String descripcion, BigDecimal exoneracion) {
        this.id = id;
        this.descripcion = descripcion;
        this.exoneracion = exoneracion;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Boolean getActivo() {
        return activo;
    }

    public void setActivo(Boolean activo) {
        this.activo = activo;
    }

    public String getDescripcionbreve() {
        return descripcionbreve;
    }

    public void setDescripcionbreve(String descripcionbreve) {
        this.descripcionbreve = descripcionbreve;
    }

    public Boolean getCancela() {
        return cancela;
    }

    public void setCancela(Boolean cancela) {
        this.cancela = cancela;
    }

    public BigDecimal getExoneracion() {
        return exoneracion;
    }

    public void setExoneracion(BigDecimal exoneracion) {
        this.exoneracion = exoneracion;
    }

    public Boolean getPrestamo() {
        return prestamo;
    }

    public void setPrestamo(Boolean prestamo) {
        this.prestamo = prestamo;
    }

    public Boolean getGeneraretencion() {
        return generaretencion;
    }

    public void setGeneraretencion(Boolean generaretencion) {
        this.generaretencion = generaretencion;
    }

    public Boolean getRecupero() {
        return recupero;
    }

    public void setRecupero(Boolean recupero) {
        this.recupero = recupero;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Tipocredito)) {
            return false;
        }
        Tipocredito other = (Tipocredito) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.mutualmsp.mutual.socios.ws.entities.Tipocredito[ id=" + id + " ]";
    }
    
}
