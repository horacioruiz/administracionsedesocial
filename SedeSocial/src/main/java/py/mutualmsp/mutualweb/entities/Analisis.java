/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author hectorvillalba
 */
@Entity
@Table(name = "analisis")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Analisis.findAll", query = "SELECT a FROM Analisis a")
    , @NamedQuery(name = "Analisis.findById", query = "SELECT a FROM Analisis a WHERE a.id = :id")
    , @NamedQuery(name = "Analisis.findByFecha", query = "SELECT a FROM Analisis a WHERE a.fecha = :fecha")
    , @NamedQuery(name = "Analisis.findByTabla", query = "SELECT a FROM Analisis a WHERE a.tabla = :tabla")
    , @NamedQuery(name = "Analisis.findByIdmovimiento", query = "SELECT a FROM Analisis a WHERE a.idmovimiento = :idmovimiento")
    , @NamedQuery(name = "Analisis.findByIdrubro", query = "SELECT a FROM Analisis a WHERE a.idrubro = :idrubro")
    , @NamedQuery(name = "Analisis.findByIdgiraduria", query = "SELECT a FROM Analisis a WHERE a.idgiraduria = :idgiraduria")
    , @NamedQuery(name = "Analisis.findBySueldo", query = "SELECT a FROM Analisis a WHERE a.sueldo = :sueldo")
    , @NamedQuery(name = "Analisis.findByIdmoneda", query = "SELECT a FROM Analisis a WHERE a.idmoneda = :idmoneda")
    , @NamedQuery(name = "Analisis.findByJubilacion", query = "SELECT a FROM Analisis a WHERE a.jubilacion = :jubilacion")
    , @NamedQuery(name = "Analisis.findByIva", query = "SELECT a FROM Analisis a WHERE a.iva = :iva")
    , @NamedQuery(name = "Analisis.findByDisponible", query = "SELECT a FROM Analisis a WHERE a.disponible = :disponible")
    , @NamedQuery(name = "Analisis.findByPorcentajejubilacion", query = "SELECT a FROM Analisis a WHERE a.porcentajejubilacion = :porcentajejubilacion")
    , @NamedQuery(name = "Analisis.findByPorcentajeiva", query = "SELECT a FROM Analisis a WHERE a.porcentajeiva = :porcentajeiva")
    , @NamedQuery(name = "Analisis.findByPorcentajedisponible", query = "SELECT a FROM Analisis a WHERE a.porcentajedisponible = :porcentajedisponible")})
public class Analisis implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "id")
    private Long id;
    @Column(name = "fecha")
    @Temporal(TemporalType.DATE)
    private Date fecha;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2)
    @Column(name = "tabla")
    private String tabla;
    @Basic(optional = false)
    @NotNull
    @Column(name = "idmovimiento")
    private long idmovimiento;
    @Basic(optional = false)
    @NotNull
    @Column(name = "idrubro")
    private long idrubro;
    @Basic(optional = false)
    @NotNull
    @Column(name = "idgiraduria")
    private long idgiraduria;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Column(name = "sueldo")
    private BigDecimal sueldo;
    @Basic(optional = false)
    @NotNull
    @Column(name = "idmoneda")
    private long idmoneda;
    @Basic(optional = false)
    @NotNull
    @Column(name = "jubilacion")
    private BigDecimal jubilacion;
    @Basic(optional = false)
    @NotNull
    @Column(name = "iva")
    private BigDecimal iva;
    @Basic(optional = false)
    @NotNull
    @Column(name = "disponible")
    private BigDecimal disponible;
    @Basic(optional = false)
    @NotNull
    @Column(name = "porcentajejubilacion")
    private short porcentajejubilacion;
    @Basic(optional = false)
    @NotNull
    @Column(name = "porcentajeiva")
    private short porcentajeiva;
    @Basic(optional = false)
    @NotNull
    @Column(name = "porcentajedisponible")
    private short porcentajedisponible;

    public Analisis() {
    }

    public Analisis(Long id) {
        this.id = id;
    }

    public Analisis(Long id, String tabla, long idmovimiento, long idrubro, long idgiraduria, BigDecimal sueldo, long idmoneda, BigDecimal jubilacion, BigDecimal iva, BigDecimal disponible, short porcentajejubilacion, short porcentajeiva, short porcentajedisponible) {
        this.id = id;
        this.tabla = tabla;
        this.idmovimiento = idmovimiento;
        this.idrubro = idrubro;
        this.idgiraduria = idgiraduria;
        this.sueldo = sueldo;
        this.idmoneda = idmoneda;
        this.jubilacion = jubilacion;
        this.iva = iva;
        this.disponible = disponible;
        this.porcentajejubilacion = porcentajejubilacion;
        this.porcentajeiva = porcentajeiva;
        this.porcentajedisponible = porcentajedisponible;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getTabla() {
        return tabla;
    }

    public void setTabla(String tabla) {
        this.tabla = tabla;
    }

    public long getIdmovimiento() {
        return idmovimiento;
    }

    public void setIdmovimiento(long idmovimiento) {
        this.idmovimiento = idmovimiento;
    }

    public long getIdrubro() {
        return idrubro;
    }

    public void setIdrubro(long idrubro) {
        this.idrubro = idrubro;
    }

    public long getIdgiraduria() {
        return idgiraduria;
    }

    public void setIdgiraduria(long idgiraduria) {
        this.idgiraduria = idgiraduria;
    }

    public BigDecimal getSueldo() {
        return sueldo;
    }

    public void setSueldo(BigDecimal sueldo) {
        this.sueldo = sueldo;
    }

    public long getIdmoneda() {
        return idmoneda;
    }

    public void setIdmoneda(long idmoneda) {
        this.idmoneda = idmoneda;
    }

    public BigDecimal getJubilacion() {
        return jubilacion;
    }

    public void setJubilacion(BigDecimal jubilacion) {
        this.jubilacion = jubilacion;
    }

    public BigDecimal getIva() {
        return iva;
    }

    public void setIva(BigDecimal iva) {
        this.iva = iva;
    }

    public BigDecimal getDisponible() {
        return disponible;
    }

    public void setDisponible(BigDecimal disponible) {
        this.disponible = disponible;
    }

    public short getPorcentajejubilacion() {
        return porcentajejubilacion;
    }

    public void setPorcentajejubilacion(short porcentajejubilacion) {
        this.porcentajejubilacion = porcentajejubilacion;
    }

    public short getPorcentajeiva() {
        return porcentajeiva;
    }

    public void setPorcentajeiva(short porcentajeiva) {
        this.porcentajeiva = porcentajeiva;
    }

    public short getPorcentajedisponible() {
        return porcentajedisponible;
    }

    public void setPorcentajedisponible(short porcentajedisponible) {
        this.porcentajedisponible = porcentajedisponible;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Analisis)) {
            return false;
        }
        Analisis other = (Analisis) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.mutualmsp.mutualweb.entities.Analisis[ id=" + id + " ]";
    }
    
}
