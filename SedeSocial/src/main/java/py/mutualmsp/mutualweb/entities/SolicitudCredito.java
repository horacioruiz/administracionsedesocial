/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.entities;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.vaadin.server.FileResource;
import com.vaadin.server.Sizeable;
import com.vaadin.server.VaadinService;
import com.vaadin.ui.Image;
import java.io.File;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author DBarreto
 */
@Entity
@Table(name = "solicitud_credito")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "SolicitudCredito.findAll", query = "SELECT d FROM SolicitudCredito d")
    , @NamedQuery(name = "SolicitudCredito.findById", query = "SELECT d FROM SolicitudCredito d WHERE d.id = :id")})
public class SolicitudCredito implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;

    @JoinColumn(name = "idsocio", referencedColumnName = "id")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Socio socio;

    @Column(name = "nombre")
    private String nombre;

    @Column(name = "apellido")
    private String apellido;

    @Column(name = "telefono")
    private String telefono;

    @Column(name = "nrodocumento")
    private String nrodocumento;

    @Column(name = "plazo")
    private Long plazo;

    @Column(name = "plazoaprobado")
    private Long plazoaprobado;

    @Column(name = "nropin")
    private String nropin;

    @Column(name = "fecha")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date fecha;

    @Column(name = "estado")
    private String estado;

    @Column(name = "atendidopor")
    private String atendidopor;

    @Column(name = "monto")
    private Double monto;

    @Column(name = "montoaprobado")
    private Double montoaprobado;

    @Column(name = "origen")
    private String origen;

    @Column(name = "obsestado")
    private String obsestado;

    @JoinColumn(name = "idtipoCredito", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Tipocredito tipoCredito;

    @JoinColumn(name = "idcargo", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Cargo cargo;

    @Column(name = "donde_se_entero")
    private String dondeSeEntero;

    @JoinColumn(name = "idrepresentante", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Representante representante;

    @JoinColumn(name = "idinstitucion", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Institucion institucion;

    @JoinColumn(name = "idregional", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Regional regional;

    @Column(name = "rubro")
    private String rubro;

    @Column(name = "retiro")
    private String retiro;

    @JoinColumn(name = "iddestino", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Destino destino;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Socio getSocio() {
        return socio;
    }

    public void setSocio(Socio socio) {
        this.socio = socio;
    }

    public Double getMontoaprobado() {
        return montoaprobado;
    }

    public void setMontoaprobado(Double montoaprobado) {
        this.montoaprobado = montoaprobado;
    }

    public Long getPlazoaprobado() {
        return plazoaprobado;
    }

    public void setPlazoaprobado(Long plazoaprobado) {
        this.plazoaprobado = plazoaprobado;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public String getOrigen() {
        return origen;
    }

    public void setOrigen(String origen) {
        this.origen = origen;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getTelefono() {
        return telefono;
    }

    public String getObsestado() {
        return obsestado;
    }

    public String getDondeSeEntero() {
        return dondeSeEntero;
    }

    public void setDondeSeEntero(String dondeSeEntero) {
        this.dondeSeEntero = dondeSeEntero;
    }

    public void setObsestado(String obsestado) {
        this.obsestado = obsestado;
    }

    public String getRetiro() {
        return retiro;
    }

    public void setRetiro(String retiro) {
        this.retiro = retiro;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getNrodocumento() {
        return nrodocumento;
    }

    public void setNrodocumento(String nrodocumento) {
        this.nrodocumento = nrodocumento;
    }

    public Long getPlazo() {
        return plazo;
    }

    public void setPlazo(Long plazo) {
        this.plazo = plazo;
    }

    public String getNropin() {
        return nropin;
    }

    public void setNropin(String nropin) {
        this.nropin = nropin;
    }

    public String getAtendidopor() {
        return atendidopor;
    }

    public void setAtendidopor(String atendidopor) {
        this.atendidopor = atendidopor;
    }

    public Date getFecha() {
        return fecha;
    }

    public Image getEstadoIcon() {
        String basepath = VaadinService.getCurrent()
                .getBaseDirectory().getAbsolutePath();
        try {
            if (this.estado.equalsIgnoreCase("PENDIENTE")) {
                FileResource resource = new FileResource(new File(basepath
                        + "/WEB-INF/images/Pendiente.png"));
                Image imagen = new Image("Image from file", resource);
                imagen.setWidth(2, Sizeable.Unit.EM);
                return imagen;
            } else if (this.estado.equalsIgnoreCase("EN PROCESO DE ANALISIS")) {
                FileResource resource = new FileResource(new File(basepath
                        + "/WEB-INF/images/EnProceso.PNG"));
                Image imagen = new Image("Image from file", resource);
                imagen.setWidth(2, Sizeable.Unit.EM);
                return imagen;
            } else if (this.estado.equalsIgnoreCase("APROBADO")) {
                FileResource resource = new FileResource(new File(basepath
                        + "/WEB-INF/images/Aprobado.png"));
                Image imagen = new Image("Image from file", resource);
                imagen.setWidth(2, Sizeable.Unit.EM);
                return imagen;
            } else if (this.estado.equalsIgnoreCase("ANULADO")) {
                FileResource resource = new FileResource(new File(basepath
                        + "/WEB-INF/images/Anulado.png"));
                Image imagen = new Image("Image from file", resource);
                imagen.setWidth(2, Sizeable.Unit.EM);
                return imagen;
            } else {
                FileResource resource = new FileResource(new File(basepath
                        + "/WEB-INF/images/Rechazado.png"));
                Image imagen = new Image("Image from file", resource);
                imagen.setWidth(2, Sizeable.Unit.EM);
                return imagen;
            }
        } catch (Exception e) {
            FileResource resource = new FileResource(new File(basepath
                    + "/WEB-INF/images/Pendiente.png"));
            Image imagen = new Image("Image from file", resource);
            imagen.setWidth(2, Sizeable.Unit.EM);
            return imagen;
        } finally {
        }
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public Double getMonto() {
        return monto;
    }

    public void setMonto(Double monto) {
        this.monto = monto;
    }

    public Tipocredito getTipoCredito() {
        return tipoCredito;
    }

    public void setTipoCredito(Tipocredito tipoCredito) {
        this.tipoCredito = tipoCredito;
    }

    public Cargo getCargo() {
        return cargo;
    }

    public void setCargo(Cargo cargo) {
        this.cargo = cargo;
    }

    public Representante getRepresentante() {
        return representante;
    }

    public void setRepresentante(Representante representante) {
        this.representante = representante;
    }

    public Institucion getInstitucion() {
        return institucion;
    }

    public void setInstitucion(Institucion institucion) {
        this.institucion = institucion;
    }

    public Regional getRegional() {
        return regional;
    }

    public void setRegional(Regional regional) {
        this.regional = regional;
    }

    public String getRubro() {
        return rubro;
    }

    public void setRubro(String rubro) {
        this.rubro = rubro;
    }

    public Destino getDestino() {
        return destino;
    }

    public void setDestino(Destino destino) {
        this.destino = destino;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SolicitudCredito)) {
            return false;
        }
        SolicitudCredito other = (SolicitudCredito) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.mutualmsp.mutualweb.entities.SolicitudCredito[ id=" + id + " ]";
    }

}
