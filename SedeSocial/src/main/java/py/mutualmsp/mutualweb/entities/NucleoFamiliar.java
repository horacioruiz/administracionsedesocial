/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.entities;

import com.vaadin.server.FileResource;
import com.vaadin.server.Sizeable;
import com.vaadin.server.VaadinService;
import com.vaadin.ui.Image;
import java.io.File;
import java.io.Serializable;
import java.math.BigInteger;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author hruiz
 */
@Entity
@Table(name = "subordinado", schema = "public")
@NamedQueries({
    @NamedQuery(name = "NucleoFamiliar.findAll", query = "SELECT n FROM NucleoFamiliar n WHERE n.habilitado=TRUE")
})
public class NucleoFamiliar implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;

    @Column(name = "idsocio")
    private BigInteger idsocio;

    @Column(name = "idparentesco")
    private BigInteger idparentesco;

    @Column(name = "nombre")
    private String nombre;

    @Column(name = "apellido")
    private String apellido;

    @Column(name = "fechanacimiento")
//    @Temporal(TemporalType.TIMESTAMP)
    private LocalDate fechanacimiento;

    @Column(name = "cedula")
    private String cedula;

    @Column(name = "checked")
    private Boolean checked;

    @Column(name = "doc1")
    private String doc1;

    @Column(name = "doc2")
    private String doc2;

    @Column(name = "doc3")
    private String doc3;

    @Column(name = "habilitado")
    private Boolean habilitado;

    @Column(name = "parentesco")
    private String parentesco;

    @Column(name = "observacion")
    private String observacion;

    @Column(name = "telefono")
    private String telefono;

    public NucleoFamiliar() {
    }

    public NucleoFamiliar(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigInteger getIdsocio() {
        return idsocio;
    }

    public void setIdsocio(BigInteger idsocio) {
        this.idsocio = idsocio;
    }

    public Boolean getChecked() {
        return checked;
    }

    public void setChecked(Boolean idsocio) {
        this.checked = idsocio;
    }

    public BigInteger getIdparentesco() {
        return idparentesco;
    }

    public void setIdparentesco(BigInteger idparentesco) {
        this.idparentesco = idparentesco;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula.trim();
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public LocalDate getFechanacimiento() {
        return fechanacimiento;
    }

    public void setFechanacimiento(LocalDate fechanacimiento) {
        this.fechanacimiento = fechanacimiento;
    }

    public String getDoc1() {
        return doc1;
    }

    public void setDoc1(String doc1) {
        this.doc1 = doc1;
    }

    public String getDoc2() {
        return doc2;
    }

    public void setDoc2(String doc2) {
        this.doc2 = doc2;
    }

    public String getDoc3() {
        return doc3;
    }

    public void setDoc3(String doc3) {
        this.doc3 = doc3;
    }

    public Boolean getHabilitado() {
        return habilitado;
    }

    public void setHabilitado(Boolean habilitado) {
        this.habilitado = habilitado;
    }

    public String getParentesco() {
        return parentesco;
    }

    public String getEdad() {
        long intervalYears = ChronoUnit.YEARS.between(getFechanacimiento(), LocalDate.now());
        return intervalYears + "";
    }

    public void setParentesco(String parentesco) {
        this.parentesco = parentesco;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public Image getConfirmadoIcono() {
        String basepath = VaadinService.getCurrent()
                .getBaseDirectory().getAbsolutePath();
        try {
            System.out.println("ESTADO HERE ->> " + this.checked);
            if (this.checked == null) {
                FileResource resource = new FileResource(new File(basepath
                        + "/WEB-INF/images/wrong.png"));
                Image imagen = new Image("Image from file", resource);
                imagen.setWidth(2, Sizeable.Unit.EM);
                return imagen;
            } else if (this.checked == true) {
                FileResource resource = new FileResource(new File(basepath
                        + "/WEB-INF/images/good.png"));
                Image imagen = new Image("Image from file", resource);
                imagen.setWidth(2, Sizeable.Unit.EM);
                return imagen;
            } else {
                FileResource resource = new FileResource(new File(basepath
                        + "/WEB-INF/images/wrong.png"));
                Image imagen = new Image("Image from file", resource);
                imagen.setWidth(2, Sizeable.Unit.EM);
                return imagen;
            }
        } catch (Exception e) {
            FileResource resource = new FileResource(new File(basepath
                    + "/WEB-INF/images/wrong.png"));
            Image imagen = new Image("Image from file", resource);
            imagen.setWidth(2, Sizeable.Unit.EM);
            return imagen;
        } finally {
        }
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof NucleoFamiliar)) {
            return false;
        }
        NucleoFamiliar other = (NucleoFamiliar) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.mutualmsp.mutual.sedesocial.ws.entities.NucleoFamiliar[ id=" + id + " ]";
    }

}
