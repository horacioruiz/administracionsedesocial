/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author DBarreto
 */
@Entity
@Table(name = "tipocasa")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Tipocasa.findAll", query = "SELECT t FROM Tipocasa t")
    , @NamedQuery(name = "Tipocasa.findById", query = "SELECT t FROM Tipocasa t WHERE t.id = :id")
    , @NamedQuery(name = "Tipocasa.findByDescripcion", query = "SELECT t FROM Tipocasa t WHERE t.descripcion = :descripcion")})
public class Tipocasa implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;
    @Column(name = "descripcion")
    private String descripcion;
    //@OneToMany(cascade = CascadeType.ALL, mappedBy = "idtipocasa", fetch = FetchType.LAZY)
    //private List<SolicitudAdmision> solicitudAdmisionList;

    public Tipocasa() {
    }

    public Tipocasa(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    /*@XmlTransient
    public List<SolicitudAdmision> getSolicitudAdmisionList() {
        return solicitudAdmisionList;
    }

    public void setSolicitudAdmisionList(List<SolicitudAdmision> solicitudAdmisionList) {
        this.solicitudAdmisionList = solicitudAdmisionList;
    }*/

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Tipocasa)) {
            return false;
        }
        Tipocasa other = (Tipocasa) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.mutualmsp.mutualweb.entities.Tipocasa[ id=" + id + " ]";
    }
    
}
