/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.entities;

import com.vaadin.server.FileResource;
import com.vaadin.server.Sizeable;
import com.vaadin.server.VaadinService;
import com.vaadin.ui.Image;
import java.io.File;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Dbarreto
 */
@Entity
@Table(name = "funcionario")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Funcionario.findAll", query = "SELECT f FROM Funcionario f")
    , @NamedQuery(name = "Funcionario.findByIdfuncionario", query = "SELECT f FROM Funcionario f WHERE f.id = :idfuncionario")
    , @NamedQuery(name = "Funcionario.findByCedula", query = "SELECT f FROM Funcionario f WHERE f.cedula = :cedula")
    , @NamedQuery(name = "Funcionario.findByNombre", query = "SELECT f FROM Funcionario f WHERE f.nombre = :nombre")
    , @NamedQuery(name = "Funcionario.findByApellido", query = "SELECT f FROM Funcionario f WHERE f.apellido = :apellido")})
public class Funcionario implements Serializable {

//    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idverificador", fetch = FetchType.LAZY)
//    private List<Verificacion> verificacionList;
//    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idrevisador", fetch = FetchType.LAZY)
//    private List<Verificacion> verificacionList1;
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;
    @Size(max = 15)
    @Column(name = "cedula")
    private String cedula;
    @Size(max = 150)
    @Column(name = "nombre")
    private String nombre;
    @Size(max = 150)
    @Column(name = "apellido")
    private String apellido;
    @Column(name = "fechaingreso")
    private Date fechaingreso;
    @Column(name = "idestado")
    private Long idestado;

    @JoinColumn(name = "idcargo", referencedColumnName = "id")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Cargo cargo;

//    @JoinColumn(name = "iddependencia", referencedColumnName = "iddependencia")
//    @ManyToOne(optional = false, fetch = FetchType.LAZY)
//    private Dependencia dependencia;
    @Transient
    private String nombreCompleto;

    public String getNombreCompleto() {
        nombreCompleto = nombre + " " + apellido;
        return nombreCompleto;
    }

    public Image getEstadoIcon() {
        String basepath = VaadinService.getCurrent()
                .getBaseDirectory().getAbsolutePath();
        if (this.idestado == 13) {
            FileResource resource = new FileResource(new File(basepath
                    + "/WEB-INF/images/good.png"));
            Image imagen = new Image("Image from file", resource);
            imagen.setWidth(2, Sizeable.Unit.EM);
            return imagen;
        } else //            if (this.idestado == 262) 
        {
            FileResource resource = new FileResource(new File(basepath
                    + "/WEB-INF/images/wrong.png"));
            Image imagen = new Image("Image from file", resource);
            imagen.setWidth(2, Sizeable.Unit.EM);
            return imagen;
        }
        /*else {
            FileResource resource = new FileResource(new File(basepath
                    + "/WEB-INF/images/pending.png"));
            Image imagen = new Image("Image from file", resource);
            imagen.setWidth(2, Sizeable.Unit.EM);
            return imagen;
        }*/
    }

    public void setNombreCompleto(String nombreCompleto) {
        this.nombreCompleto = nombreCompleto;
    }

    public Funcionario() {
    }

    public Funcionario(Long id) {
        this.id = id;
    }

    public Long getIdestado() {
        return idestado;
    }

    public void setIdestado(Long idestado) {
        this.idestado = idestado;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Cargo getCargo() {
        return cargo;
    }

    public void setCargo(Cargo cargo) {
        this.cargo = cargo;
    }

    public String getCedula() {
        return cedula;
    }

    public Date getFechaingreso() {
        return fechaingreso;
    }

    public void setFechaingreso(Date fechaingreso) {
        this.fechaingreso = fechaingreso;
    }

//    public Dependencia getDependencia() {
//        return dependencia;
//    }
//
//    public void setDependencia(Dependencia dependencia) {
//        this.dependencia = dependencia;
//    }
    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Funcionario)) {
            return false;
        }
        Funcionario other = (Funcionario) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.mutualmsp.mutualweb.entities.Funcionario[ id=" + id + " ]";
    }

//    @XmlTransient
//    public List<Verificacion> getVerificacionList() {
//        return verificacionList;
//    }
//
//    public void setVerificacionList(List<Verificacion> verificacionList) {
//        this.verificacionList = verificacionList;
//    }
//
//    @XmlTransient
//    public List<Verificacion> getVerificacionList1() {
//        return verificacionList1;
//    }
//
//    public void setVerificacionList1(List<Verificacion> verificacionList1) {
//        this.verificacionList1 = verificacionList1;
//    }
}
