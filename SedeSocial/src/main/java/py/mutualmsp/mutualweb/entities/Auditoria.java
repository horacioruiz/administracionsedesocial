/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Dbarreto
 */
@Entity
@Table(name = "auditoria")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Auditoria.findAll", query = "SELECT a FROM Auditoria a")
    , @NamedQuery(name = "Auditoria.findById", query = "SELECT a FROM Auditoria a WHERE a.id = :id")
    , @NamedQuery(name = "Auditoria.findByNombretabla", query = "SELECT a FROM Auditoria a WHERE a.nombretabla = :nombretabla")
    , @NamedQuery(name = "Auditoria.findByOperacion", query = "SELECT a FROM Auditoria a WHERE a.operacion = :operacion")
    , @NamedQuery(name = "Auditoria.findByValorviejo", query = "SELECT a FROM Auditoria a WHERE a.valorviejo = :valorviejo")
    , @NamedQuery(name = "Auditoria.findByValornuevo", query = "SELECT a FROM Auditoria a WHERE a.valornuevo = :valornuevo")
    , @NamedQuery(name = "Auditoria.findByFechaactualizacion", query = "SELECT a FROM Auditoria a WHERE a.fechaactualizacion = :fechaactualizacion")
    , @NamedQuery(name = "Auditoria.findByUsuario", query = "SELECT a FROM Auditoria a WHERE a.usuario = :usuario")})
public class Auditoria implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;
    @Size(max = 45)
    @Column(name = "nombretabla")
    private String nombretabla;
    @Size(max = 1)
    @Column(name = "operacion")
    private String operacion;
    @Column(name = "valorviejo")
    private String valorviejo;
    @Column(name = "valornuevo")
    private String valornuevo;
    @Column(name = "fechaactualizacion")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaactualizacion;
    @Size(max = 45)
    @Column(name = "usuario")
    private String usuario;
    
    
    public Auditoria() {
    }

    public Auditoria(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getNombretabla() {
        return nombretabla;
    }

    public String getOperacion() {
        return operacion;
    }

    public String getValorviejo() {
        return valorviejo;
    }

    public String getValornuevo() {
        return valornuevo;
    }

    public Date getFechaactualizacion() {
        return fechaactualizacion;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setNombretabla(String nombretabla) {
        this.nombretabla = nombretabla;
    }

    public void setOperacion(String operacion) {
        this.operacion = operacion;
    }

    public void setValorviejo(String valorviejo) {
        this.valorviejo = valorviejo;
    }

    public void setValornuevo(String valornuevo) {
        this.valornuevo = valornuevo;
    }

    public void setFechaactualizacion(Date fechaactualizacion) {
        this.fechaactualizacion = fechaactualizacion;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Auditoria)) {
            return false;
        }
        Auditoria other = (Auditoria) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.mutualmsp.mutualweb.entities.Auditoria[ id=" + id + " ]";
    }
    
}
