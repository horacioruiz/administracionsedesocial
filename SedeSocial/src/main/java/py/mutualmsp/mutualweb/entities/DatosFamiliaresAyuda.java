/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author DBarreto
 */
@Entity
@Table(name = "datos_familiares_ayuda")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DatosFamiliaresAyuda.findAll", query = "SELECT c FROM DatosFamiliaresAyuda c")
    , @NamedQuery(name = "DatosFamiliaresAyuda.findById", query = "SELECT c FROM DatosFamiliaresAyuda c WHERE c.id = :id")
    , @NamedQuery(name = "DatosFamiliaresAyuda.findByDescripcion", query = "SELECT c FROM DatosFamiliaresAyuda c WHERE c.parentezco = :parentezco")})
public class DatosFamiliaresAyuda implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;
    @Column(name = "parentezco")
    private String parentezco;
    @Column(name = "edad")
    private Integer edad;
    @Column(name = "nro_documento")
    private String nroDocumento;
    @JoinColumn(name = "id_solicitud", referencedColumnName = "id")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private SolicitudAyuda solicitudAyuda;

    //@OneToMany(cascade = CascadeType.ALL, mappedBy = "idcargo", fetch = FetchType.LAZY)
    //private List<SolicitudAdmision> solicitudAdmisionList;
    public DatosFamiliaresAyuda() {
    }

    public DatosFamiliaresAyuda(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getParentezco() {
        return parentezco;
    }

    public void setParentezco(String parentezco) {
        this.parentezco = parentezco;
    }

    public Integer getEdad() {
        return edad;
    }

    public void setEdad(Integer edad) {
        this.edad = edad;
    }

    public String getNroDocumento() {
        return nroDocumento;
    }

    public void setNroDocumento(String nroDocumento) {
        this.nroDocumento = nroDocumento;
    }

    public SolicitudAyuda getSolicitudAyuda() {
        return solicitudAyuda;
    }

    public void setSolicitudAyuda(SolicitudAyuda solicitudAyuda) {
        this.solicitudAyuda = solicitudAyuda;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DatosFamiliaresAyuda)) {
            return false;
        }
        DatosFamiliaresAyuda other = (DatosFamiliaresAyuda) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.mutualmsp.mutualweb.entities.DatosFamiliaresAyuda[ id=" + id + " ]";
    }

}
