/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.entities;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author hectorvillalba
 */
@Entity
@Table(name = "representante")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Representante.findAll", query = "SELECT r FROM Representante r")
    , @NamedQuery(name = "Representante.findById", query = "SELECT r FROM Representante r WHERE r.id = :id")
    , @NamedQuery(name = "Representante.findByNombre", query = "SELECT r FROM Representante r WHERE r.nombre = :nombre")
    , @NamedQuery(name = "Representante.findByApellido", query = "SELECT r FROM Representante r WHERE r.apellido = :apellido")
    , @NamedQuery(name = "Representante.findByIdlocalidad", query = "SELECT r FROM Representante r WHERE r.idlocalidad = :idlocalidad")
    , @NamedQuery(name = "Representante.findByIdregional", query = "SELECT r FROM Representante r WHERE r.idregional = :idregional")
    , @NamedQuery(name = "Representante.findByIdtiporepresentante", query = "SELECT r FROM Representante r WHERE r.idtiporepresentante = :idtiporepresentante")
    , @NamedQuery(name = "Representante.findByCedula", query = "SELECT r FROM Representante r WHERE r.cedula = :cedula")
    , @NamedQuery(name = "Representante.findByTelefono", query = "SELECT r FROM Representante r WHERE r.telefono = :telefono")
    , @NamedQuery(name = "Representante.findByDestinatario", query = "SELECT r FROM Representante r WHERE r.destinatario = :destinatario")})
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Representante implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "id")
    private Long id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "nombre")
    private String nombre;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "apellido")
    private String apellido;
    @Basic(optional = false)
    @NotNull
    @Column(name = "idlocalidad")
    private long idlocalidad;
    @Basic(optional = false)
    @NotNull
    @Column(name = "idregional")
    private long idregional;
    @Basic(optional = false)
    @NotNull
    @Column(name = "idtiporepresentante")
    private long idtiporepresentante;
    @Size(max = 12)
    @Column(name = "cedula")
    private String cedula;
    @Size(max = 40)
    @Column(name = "telefono")
    private String telefono;
    @Column(name = "destinatario")
    private Boolean destinatario;

    public Representante() {
    }

    public Representante(Long id) {
        this.id = id;
    }

    public Representante(Long id, String nombre, String apellido, long idlocalidad, long idregional, long idtiporepresentante) {
        this.id = id;
        this.nombre = nombre;
        this.apellido = apellido;
        this.idlocalidad = idlocalidad;
        this.idregional = idregional;
        this.idtiporepresentante = idtiporepresentante;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public long getIdlocalidad() {
        return idlocalidad;
    }

    public void setIdlocalidad(long idlocalidad) {
        this.idlocalidad = idlocalidad;
    }

    public long getIdregional() {
        return idregional;
    }

    public void setIdregional(long idregional) {
        this.idregional = idregional;
    }

    public long getIdtiporepresentante() {
        return idtiporepresentante;
    }

    public void setIdtiporepresentante(long idtiporepresentante) {
        this.idtiporepresentante = idtiporepresentante;
    }

    public String getCedula() {
        return cedula;
    }

    public String getNombreCompleto() {
        return nombre + " " + apellido;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public Boolean getDestinatario() {
        return destinatario;
    }

    public void setDestinatario(Boolean destinatario) {
        this.destinatario = destinatario;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Representante)) {
            return false;
        }
        Representante other = (Representante) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.mutualmsp.mutual.socios.ws.entities.Representante[ id=" + id + " ]";
    }

}
