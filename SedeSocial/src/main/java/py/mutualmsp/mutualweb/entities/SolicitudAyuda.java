/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.entities;

import com.vaadin.server.FileResource;
import com.vaadin.server.Sizeable;
import com.vaadin.server.VaadinService;
import com.vaadin.ui.Image;
import java.io.File;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author DBarreto
 */
@Entity
@Table(name = "solicitud_ayuda")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "SolicitudAyuda.findAll", query = "SELECT d FROM SolicitudAyuda d")
    , @NamedQuery(name = "SolicitudAyuda.findById", query = "SELECT d FROM SolicitudAyuda d WHERE d.id = :id")})
public class SolicitudAyuda implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;

    @JoinColumn(name = "idsocio", referencedColumnName = "id")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Socio socio;

    @Column(name = "fecha")
    private Date fecha;

    @Column(name = "telefono")
    private String telefono;

    @JoinColumn(name = "iddepartamento", referencedColumnName = "id")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Departamento departamento;

    @JoinColumn(name = "idciudad", referencedColumnName = "id")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Ciudad ciudad;

    @Column(name = "url_foto_1")
    private String urlFoto1;

    @Column(name = "url_foto_2")
    private String urlFoto2;

    @Column(name = "url_foto_3")
    private String urlFoto3;

    @JoinColumn(name = "id_motivo_solicitud", referencedColumnName = "id")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private MotivoSolicitudAyuda motivoSolicitudAyuda;

    @Column(name = "estado")
    private String estado;

    @Column(name = "observacion")
    private String observacion;

    @JoinColumn(name = "id_usuario", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Usuario usuario;

    @Column(name = "atendidopor")
    private String atendidopor;

    @Column(name = "telefonoatencion")
    private String telefonoatencion;

    @Column(name = "motivo_obs")
    private String motivoObs;

    @Column(name = "cargo")
    private String cargo;

    @JoinColumn(name = "idinstitucion", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Institucion institucion;

    @JoinColumn(name = "id_necesidad", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private NecesidadAyuda necesidadAyuda;

    @Column(name = "persona_dep_mayor")
    private Integer personaDepMayor;

    @Column(name = "persona_dep_menor")
    private Integer personaDepMenor;

    @Column(name = "necesidad_obs")
    private String necesidadObs;

    @Column(name = "tipo_vivienda")
    private String tipoVivienda;

    @Size(max = 800)
    @Column(name = "url_liquidacion")
    private String urlLiquidacion;

    @Size(max = 800)
    @Column(name = "pin_liquidacion")
    private String pinLiquidacion;

    @Size(max = 50)
    @Column(name = "derivado_al_dpto")
    private String derivadoAlDpto;

    @Size(max = 800)
    @Column(name = "obs_derivacion")
    private String obsDerivacion;

    public String getDerivadoAlDpto() {
        return derivadoAlDpto;
    }

    public void setDerivadoAlDpto(String derivadoAlDpto) {
        this.derivadoAlDpto = derivadoAlDpto;
    }

    public String getObsDerivacion() {
        return obsDerivacion;
    }

    public void setObsDerivacion(String obsDerivacion) {
        this.obsDerivacion = obsDerivacion;
    }

    public String getUrlLiquidacion() {
        return urlLiquidacion;
    }

    public void setUrlLiquidacion(String urlLiquidacion) {
        this.urlLiquidacion = urlLiquidacion;
    }

    public String getPinLiquidacion() {
        return pinLiquidacion;
    }

    public Image getEstadoIcon() {
        String basepath = VaadinService.getCurrent()
                .getBaseDirectory().getAbsolutePath();

        try {
            if (this.estado.equalsIgnoreCase("PENDIENTE")) {
                FileResource resource = new FileResource(new File(basepath
                        + "/WEB-INF/images/Pendiente.png"));
                Image imagen = new Image("Image from file", resource);
                imagen.setWidth(2, Sizeable.Unit.EM);
                return imagen;
            } else if (this.estado.equalsIgnoreCase("EN PROCESO DE ANALISIS")) {
                FileResource resource = new FileResource(new File(basepath
                        + "/WEB-INF/images/EnProceso.PNG"));
                Image imagen = new Image("Image from file", resource);
                imagen.setWidth(2, Sizeable.Unit.EM);
                return imagen;
            } else if (this.estado.equalsIgnoreCase("APROBADO")) {
                FileResource resource = new FileResource(new File(basepath
                        + "/WEB-INF/images/Aprobado.PNG"));
                Image imagen = new Image("Image from file", resource);
                imagen.setWidth(2, Sizeable.Unit.EM);
                return imagen;
            } else if (this.estado.equalsIgnoreCase("ANULADO")) {
                FileResource resource = new FileResource(new File(basepath
                        + "/WEB-INF/images/Anulado.PNG"));
                Image imagen = new Image("Image from file", resource);
                imagen.setWidth(2, Sizeable.Unit.EM);
                return imagen;
            } else {
                FileResource resource = new FileResource(new File(basepath
                        + "/WEB-INF/images/Rechazado.png"));
                Image imagen = new Image("Image from file", resource);
                imagen.setWidth(2, Sizeable.Unit.EM);
                return imagen;
            }
        } catch (Exception e) {
            FileResource resource = new FileResource(new File(basepath
                    + "/WEB-INF/images/Pendiente.png"));
            Image imagen = new Image("Image from file", resource);
            imagen.setWidth(2, Sizeable.Unit.EM);
            return imagen;
        } finally {
        }
//        try {
//            if (this.estado.equalsIgnoreCase("PENDIENTE")) {
//            FileResource resource = new FileResource(new File(basepath
//                    + "/WEB-INF/images/apagado.jpg"));
//            Image imagen = new Image("Image from file", resource);
//            imagen.setWidth(2, Sizeable.Unit.EM);
//            return imagen;
//        } else if (this.estado.equalsIgnoreCase("EN PROCESO DE ANALISIS")) {
//            FileResource resource = new FileResource(new File(basepath
//                    + "/WEB-INF/images/encendido.jpg"));
//            Image imagen = new Image("Image from file", resource);
//            imagen.setWidth(2, Sizeable.Unit.EM);
//            return imagen;
//        } else if (this.estado.equalsIgnoreCase("APROBADO")) {
//            FileResource resource = new FileResource(new File(basepath
//                    + "/WEB-INF/images/good.png"));
//            Image imagen = new Image("Image from file", resource);
//            imagen.setWidth(2, Sizeable.Unit.EM);
//            return imagen;
//        } else {
//            FileResource resource = new FileResource(new File(basepath
//                    + "/WEB-INF/images/wrong.png"));
//            Image imagen = new Image("Image from file", resource);
//            imagen.setWidth(2, Sizeable.Unit.EM);
//            return imagen;
//        }
//        } catch (Exception e) {
//            FileResource resource = new FileResource(new File(basepath
//                    + "/WEB-INF/images/apagado.jpg"));
//            Image imagen = new Image("Image from file", resource);
//            imagen.setWidth(2, Sizeable.Unit.EM);
//            return imagen;
//        } finally{}
    }

    public void setPinLiquidacion(String pinLiquidacion) {
        this.pinLiquidacion = pinLiquidacion;
    }

    public SolicitudAyuda() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Socio getSocio() {
        return socio;
    }

    public void setSocio(Socio socio) {
        this.socio = socio;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public Departamento getDepartamento() {
        return departamento;
    }

    public void setDepartamento(Departamento departamento) {
        this.departamento = departamento;
    }

    public Ciudad getCiudad() {
        return ciudad;
    }

    public void setCiudad(Ciudad ciudad) {
        this.ciudad = ciudad;
    }

    public String getUrlFoto1() {
        return urlFoto1;
    }

    public void setUrlFoto1(String urlFoto1) {
        this.urlFoto1 = urlFoto1;
    }

    public String getUrlFoto2() {
        return urlFoto2;
    }

    public void setUrlFoto2(String urlFoto2) {
        this.urlFoto2 = urlFoto2;
    }

    public String getUrlFoto3() {
        return urlFoto3;
    }

    public void setUrlFoto3(String urlFoto3) {
        this.urlFoto3 = urlFoto3;
    }

    public MotivoSolicitudAyuda getMotivoSolicitudAyuda() {
        return motivoSolicitudAyuda;
    }

    public void setMotivoSolicitudAyuda(MotivoSolicitudAyuda motivoSolicitudAyuda) {
        this.motivoSolicitudAyuda = motivoSolicitudAyuda;
    }

    public String getEstado() {
        return estado;
    }

    public Integer getPersonaDepMayor() {
        return personaDepMayor;
    }

    public void setPersonaDepMayor(Integer personaDepMayor) {
        this.personaDepMayor = personaDepMayor;
    }

    public Integer getPersonaDepMenor() {
        return personaDepMenor;
    }

    public void setPersonaDepMenor(Integer personaDepMenor) {
        this.personaDepMenor = personaDepMenor;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public String getAtendidopor() {
        return atendidopor;
    }

    public void setAtendidopor(String atendidopor) {
        this.atendidopor = atendidopor;
    }

    public String getTelefonoatencion() {
        return telefonoatencion;
    }

    public void setTelefonoatencion(String telefonoatencion) {
        this.telefonoatencion = telefonoatencion;
    }

    public String getMotivoObs() {
        return motivoObs;
    }

    public void setMotivoObs(String motivoObs) {
        this.motivoObs = motivoObs;
    }

    public String getCargo() {
        return cargo;
    }

    public void setCargo(String cargo) {
        this.cargo = cargo;
    }

    public Institucion getInstitucion() {
        return institucion;
    }

    public void setInstitucion(Institucion institucion) {
        this.institucion = institucion;
    }

    public NecesidadAyuda getNecesidadAyuda() {
        return necesidadAyuda;
    }

    public void setNecesidadAyuda(NecesidadAyuda necesidadAyuda) {
        this.necesidadAyuda = necesidadAyuda;
    }

    public String getNecesidadObs() {
        return necesidadObs;
    }

    public void setNecesidadObs(String necesidadObs) {
        this.necesidadObs = necesidadObs;
    }

    public String getTipoVivienda() {
        return tipoVivienda;
    }

    public void setTipoVivienda(String tipoVivienda) {
        this.tipoVivienda = tipoVivienda;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SolicitudAyuda)) {
            return false;
        }
        SolicitudAyuda other = (SolicitudAyuda) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.mutualmsp.mutualweb.entities.SolicitudAyuda[ id=" + id + " ]";
    }

}
