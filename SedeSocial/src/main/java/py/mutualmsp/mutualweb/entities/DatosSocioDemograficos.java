/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.entities;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author hectorvillalba
 */
@Entity
@Table(name = "datos_socio_demograficos")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DatosSocioDemograficos.findAll", query = "SELECT c FROM DatosSocioDemograficos c")})
public class DatosSocioDemograficos implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "tipo_vivienda")
    private String tipoVivienda;

    @Column(name = "condicion_vivienda")
    private String condicionVivienda;

    @Column(name = "cant_personas_mayores")
    private Integer cantPersonasMayores;

    @Column(name = "cant_personas_menores")
    private Integer cantPersonasMenores;

    @Column(name = "cant_personas_trabajan")
    private Integer cantPersonasTrabajan;

    @Column(name = "ingreso_aprox_hogar")
    private Double ingresoAproxHogar;
    
    @Column(name = "seguro_medico")
    private String seguroMedico;
    
    @Column(name = "persona_con_enfermedad")
    private String personaConEnfermedad;
    
    @Column(name = "descripcion_enfermedad")
    private String descripcionEnfermedad;
    
    @JoinColumn(name = "id_solicitud_ayuda", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private SolicitudAyuda solicitudAyuda;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTipoVivienda() {
        return tipoVivienda;
    }

    public void setTipoVivienda(String tipoVivienda) {
        this.tipoVivienda = tipoVivienda;
    }

    public String getCondicionVivienda() {
        return condicionVivienda;
    }

    public void setCondicionVivienda(String condicionVivienda) {
        this.condicionVivienda = condicionVivienda;
    }

    public Integer getCantPersonasMayores() {
        return cantPersonasMayores;
    }

    public void setCantPersonasMayores(Integer cantPersonasMayores) {
        this.cantPersonasMayores = cantPersonasMayores;
    }

    public Integer getCantPersonasMenores() {
        return cantPersonasMenores;
    }

    public void setCantPersonasMenores(Integer cantPersonasMenores) {
        this.cantPersonasMenores = cantPersonasMenores;
    }

    public Integer getCantPersonasTrabajan() {
        return cantPersonasTrabajan;
    }

    public void setCantPersonasTrabajan(Integer cantPersonasTrabajan) {
        this.cantPersonasTrabajan = cantPersonasTrabajan;
    }

    public Double getIngresoAproxHogar() {
        return ingresoAproxHogar;
    }

    public void setIngresoAproxHogar(Double ingresoAproxHogar) {
        this.ingresoAproxHogar = ingresoAproxHogar;
    }

    public String getSeguroMedico() {
        return seguroMedico;
    }

    public void setSeguroMedico(String seguroMedico) {
        this.seguroMedico = seguroMedico;
    }

    public String getPersonaConEnfermedad() {
        return personaConEnfermedad;
    }

    public void setPersonaConEnfermedad(String personaConEnfermedad) {
        this.personaConEnfermedad = personaConEnfermedad;
    }

    public String getDescripcionEnfermedad() {
        return descripcionEnfermedad;
    }

    public void setDescripcionEnfermedad(String descripcionEnfermedad) {
        this.descripcionEnfermedad = descripcionEnfermedad;
    }

    public SolicitudAyuda getSolicitudAyuda() {
        return solicitudAyuda;
    }

    public void setSolicitudAyuda(SolicitudAyuda solicitudAyuda) {
        this.solicitudAyuda = solicitudAyuda;
    }
    
    
}
