/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.entities;

import com.vaadin.server.FileResource;
import com.vaadin.server.Sizeable;
import com.vaadin.server.VaadinService;
import com.vaadin.ui.Image;
import java.io.File;
import java.io.Serializable;
import java.time.LocalDate;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author DBarreto
 */
@Entity
@Table(name = "enfermeria", schema = "sedesocial")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Enfermeria.findAll", query = "SELECT c FROM Enfermeria c")
    , @NamedQuery(name = "Enfermeria.findById", query = "SELECT c FROM Enfermeria c WHERE c.id = :id")
//    , @NamedQuery(name = "Enfermeria.findByDescripcion", query = "SELECT c FROM Enfermeria c WHERE c.descripcion = :descripcion")
})
public class Enfermeria implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;

    @Column(name = "fecha")
//    @Temporal(TemporalType.TIMESTAMP)
    private LocalDate fecha;

    @Column(name = "observacion")
    private String observacion;

    @Column(name = "cedula")
    private String cedula;

    @Column(name = "nombre")
    private String nombre;

    @JoinColumn(name = "idagendamiento", referencedColumnName = "id")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Agendamiento idagendamiento;
    
    @JoinColumn(name = "idtipoincidencia", referencedColumnName = "id")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Tipoincidencia idtipoIncidencia;
    
    @JoinColumn(name = "idmedico", referencedColumnName = "id")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Medico idmedico;

    @Column(name = "aprobo")
    private Boolean aprobo;

    @Column(name = "fechavigencia")
//    @Temporal(TemporalType.TIMESTAMP)
    private LocalDate fechavigencia;

    public Enfermeria() {
    }

    public Enfermeria(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public LocalDate getFecha() {
        return fecha;
    }

    public void setFecha(LocalDate fecha) {
        this.fecha = fecha;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public Agendamiento getIdagendamiento() {
        return idagendamiento;
    }

    public void setIdagendamiento(Agendamiento idAgendamiento) {
        this.idagendamiento = idAgendamiento;
    }

    public Tipoincidencia getIdtipoIncidencia() {
        return idtipoIncidencia;
    }

    public Medico getIdmedico() {
        return idmedico;
    }
    
    public Image getAproboData() {
        String basepath = VaadinService.getCurrent()
                .getBaseDirectory().getAbsolutePath();
        try {
            if (this.aprobo) {
                FileResource resource = new FileResource(new File(basepath
                        + "/WEB-INF/images/good.png"));
                Image imagen = new Image("Image from file", resource);
                imagen.setWidth(2, Sizeable.Unit.EM);
                return imagen;
            } else {
                FileResource resource = new FileResource(new File(basepath
                        + "/WEB-INF/images/wrong.png"));
                Image imagen = new Image("Image from file", resource);
                imagen.setWidth(2, Sizeable.Unit.EM);
                return imagen;
            }
        } catch (Exception e) {
            FileResource resource = new FileResource(new File(basepath
                    + "/WEB-INF/images/wrong.png"));
            Image imagen = new Image("Image from file", resource);
            imagen.setWidth(2, Sizeable.Unit.EM);
            return imagen;
        } finally {
        }
    }

    public Boolean getAprobo() {
        return aprobo;
    }

    public LocalDate getFechavigencia() {
        return fechavigencia;
    }

    public void setIdtipoIncidencia(Tipoincidencia idtipoIncidencia) {
        this.idtipoIncidencia = idtipoIncidencia;
    }

    public void setIdmedico(Medico idmedico) {
        this.idmedico = idmedico;
    }

    public void setAprobo(Boolean aprobo) {
        this.aprobo = aprobo;
    }

    public void setFechavigencia(LocalDate fechavigencia) {
        this.fechavigencia = fechavigencia;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Enfermeria)) {
            return false;
        }
        Enfermeria other = (Enfermeria) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.mutualmsp.mutualweb.entities.Enfermeria[ id=" + id + " ]";
    }

}