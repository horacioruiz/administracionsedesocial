/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author hruiz
 */
@Entity
@Table(name = "agendamiento_detalle", schema = "sedesocial")
@NamedQueries({
    @NamedQuery(name = "AgendamientoDetalle.findAll", query = "SELECT a FROM AgendamientoDetalle a")})
public class AgendamientoDetalle implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;

    @Column(name = "asistio")
    private Boolean asistio;

    @Column(name = "observacion")
    private String observacion;
    
    @Column(name = "usopiscina")
    private Boolean usopiscina;

    @Column(name = "edad")
    private Integer edad;

    @JoinColumn(name = "idagendamiento", referencedColumnName = "id")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Agendamiento agendamiento;

    @JoinColumn(name = "idinvitado", referencedColumnName = "id")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Invitado invitado;

    @JoinColumn(name = "idsubordinado", referencedColumnName = "id")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private NucleoFamiliar nucleofamiliar;

    public AgendamientoDetalle() {
    }

    public AgendamientoDetalle(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean getAsistio() {
        return asistio;
    }

    public void setAsistio(Boolean asistio) {
        this.asistio = asistio;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public Integer getEdad() {
        return edad;
    }

    public Boolean getUsopiscina() {
        return usopiscina;
    }

    public void setUsopiscina(Boolean usopiscina) {
        this.usopiscina = usopiscina;
    }

    public void setEdad(Integer edad) {
        this.edad = edad;
    }

    public Agendamiento getAgendamiento() {
        return agendamiento;
    }

    public void setAgendamiento(Agendamiento agendamiento) {
        this.agendamiento = agendamiento;
    }

    public Invitado getInvitado() {
        return invitado;
    }

    public void setInvitado(Invitado invitado) {
        this.invitado = invitado;
    }

    public NucleoFamiliar getNucleofamiliar() {
        return nucleofamiliar;
    }

    public void setNucleofamiliar(NucleoFamiliar nucleofamiliar) {
        this.nucleofamiliar = nucleofamiliar;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AgendamientoDetalle)) {
            return false;
        }
        AgendamientoDetalle other = (AgendamientoDetalle) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.mutualmsp.mutual.sedesocial.ws.entities.AgendamientoDetalle[ id=" + id + " ]";
    }

}
