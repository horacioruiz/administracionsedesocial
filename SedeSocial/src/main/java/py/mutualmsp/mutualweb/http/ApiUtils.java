/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.http;

/**
 *
 * @author hectorvillalba
 */
public class ApiUtils {

    private ApiUtils() {}

    public static final String BASE_URL = "http://192.168.10.4:8080/mutual-socios-ws/api/";

    public static ApiService getAPIService() {
        return RestAdapterSocios.getClient(BASE_URL).create(ApiService.class);
    }
}
