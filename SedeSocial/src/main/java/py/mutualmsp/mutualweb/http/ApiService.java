/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.http;

import java.util.List;
import py.mutualmsp.mutualweb.dto.AhorroProgramadoCab;
import py.mutualmsp.mutualweb.dto.AhorroProgramadoDetalle;
import py.mutualmsp.mutualweb.dto.DescuentosCab;
import py.mutualmsp.mutualweb.dto.DetalleMovimiento;
import py.mutualmsp.mutualweb.dto.OrdenCompraCab;
import py.mutualmsp.mutualweb.dto.Prestamo;
import py.mutualmsp.mutualweb.dto.Response;
import py.mutualmsp.mutualweb.dto.Socio;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 *
 * @author hectorvillalba
 */
public interface ApiService {
    
    @GET("consulta/periodo")
    Call<Response<List<String>>> getPeriodo();

    @GET("consulta/socios/{cedula}")
    Call<Response<Socio>> getSocio(@Path("cedula") String cedula);

    @GET("consulta/descuentos/{idsocio}/{periodo}")
    Call<Response<DescuentosCab>> getDescuentos(@Path("idsocio")Long idsocio,
                                                @Path("periodo")String periodo);

    @GET("consulta/ahorro-cab/{cedula}")
    Call<Response<List<AhorroProgramadoCab>>> getAhorroCab(@Path("cedula")String cedula);

    @GET("consulta/ahorro-detalle/{idmovimiento}")
    Call<Response<List<AhorroProgramadoDetalle>>> getAhorroDetalle(@Path("idmovimiento") Integer idMovimiento);

    @GET("consulta/orden-compra-cab/{cedula}")
    Call<Response<List<OrdenCompraCab>>> getOrdenCompraCab(@Path("cedula")String cedula);

    @GET("consulta/prestamo/{cedula}")
    Call<Response<List<Prestamo>>> getPrestamo(@Path("cedula") String cedula);

    @GET("consulta/detalle-movimiento/{idmovimiento}")
    Call<Response<List<DetalleMovimiento>>> getDetalleMovimiento(@Path("idmovimiento") Integer idMovimiento);
    
}
