/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.http;

import java.util.List;
import py.mutualmsp.mutualweb.dto.AhorroProgramadoCab;
import py.mutualmsp.mutualweb.dto.AhorroProgramadoDetalle;
import py.mutualmsp.mutualweb.dto.Aportes;
import py.mutualmsp.mutualweb.dto.DescuentosCab;
import py.mutualmsp.mutualweb.dto.DetalleMovimiento;
import py.mutualmsp.mutualweb.dto.OrdenCompraCab;
import py.mutualmsp.mutualweb.dto.Prestamo;
import py.mutualmsp.mutualweb.dto.Response;
import py.mutualmsp.mutualweb.dto.Socio;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 *
 * @author hectorvillalba
 */
public interface  ConsultaWS  {
    
@GET("consultas/aportes/{cedula}/{user}/{pass}/{periodo}")
Call<Response<Aportes>> getAportes(@Path("cedula") String cedula,
                                       @Path("user") String user,
                                      @Path("pass") String pass,
                                      @Path("periodo")String periodo);

@GET("consultas/operaciones/{cedula}/{user}/{pass}/{periodo}")
Call<Response<Aportes>> getOperaciones(@Path("cedula") String cedula,
                                           @Path("user") String user,
                                           @Path("pass") String pass,
                                           @Path("periodo")String periodo);

@GET("consultas/fondo/{cedula}/{user}/{pass}/{periodo}")
Call<Response<Aportes>> getFondo(@Path("cedula") String cedula,
                                           @Path("user") String user,
                                           @Path("pass") String pass,
                                           @Path("periodo")String periodo);
@GET("datos/socios/{cedula}")
Call<Response<Socio>> getSocio(@Path("cedula") String cedula);

@GET("consulta/descuentos/{idsocio}/{periodo}")
Call<Response<DescuentosCab>> getDescuentos(@Path("idsocio") Long idsocio,
                                                @Path("periodo") String periodo);
@GET("operaciones/ahorro/{cedula}/{user}/{pass}/{periodo}")
Call<Response<List<AhorroProgramadoCab>>> getAhorroCab(@Path("cedula") String cedula,
                                                           @Path("user") String user,
                                                           @Path("pass") String pass,
                                                           @Path("periodo")String periodo);
@GET("operaciones/ahorro-detalle/{idmovimiento}")
Call<Response<List<AhorroProgramadoDetalle>>> getAhorroDetalle(@Path("idmovimiento") Integer idMovimiento);
@GET("operaciones/orden-compra-cab/{cedula}/{user}/{pass}/{periodo}")
Call<Response<List<OrdenCompraCab>>> getOrdenCompraCab(@Path("cedula") String cedula,
                                                           @Path("user")String user,
                                                           @Path("pass")String pass,
                                                           @Path("periodo")String periodo);
    
@GET("operaciones/prestamo/{cedula}/{user}/{pass}/{periodo}")
Call<Response<List<Prestamo>>> getPrestamo(@Path("cedula") String cedula,
                                               @Path("user")String user,
                                               @Path("pass")String passs,
                                               @Path("periodo")String periodo);
    
@GET("operaciones/detalle-movimiento/{idmovimiento}/{tipo}")
Call<Response<List<DetalleMovimiento>>> getDetalleMovimiento(@Path("idmovimiento") Long idMovimiento,
                                                                 @Path("tipo") String tipo);
}
