/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.http;

import java.util.List;
import py.mutualmsp.mutualweb.dto.Barrio;
import py.mutualmsp.mutualweb.dto.Ciudad;
import py.mutualmsp.mutualweb.dto.Institucion;
import py.mutualmsp.mutualweb.dto.Profesion;
import py.mutualmsp.mutualweb.dto.Response;
import retrofit2.Call;
import retrofit2.http.GET;

/**
 *
 * @author hectorvillalba
 */
public interface ConsultaDefinicionesWS {
    
@GET("consulta-definiciones/getBarrios")
Call<Response<List<Barrio>>> getBarrios();
@GET("consulta-definiciones/getCiudades")
Call<Response<List<Ciudad>>> getCiudades();
@GET("consulta-definiciones/getInstitucion")
Call<Response<List<Institucion>>> getInstitucion();
@GET("consulta-definiciones/getProfesion")
Call<Response<List<Profesion>>> getPrefesion();
}

