/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author hectorvillalba
 */
public class Descuentos implements Serializable {
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy", timezone = "America/Asuncion")
    private Date fecha;
    private String periodo;
    private Integer orden;
    private Integer idCuenta;
    private String descripcion;
    private Integer idMovimiento;
    private String tipo;
    private Double atraso;
    private Double cerrado;
    private Double giraduria;
    private Double ventanilla;
    private Double asignacion;
    private Integer idSocio;
    private String cedula;
    private Double diferencia;
    private Double descuentoMutual;

    public Double getDescuentoMutual() {
        return descuentoMutual;
    }

    public void setDescuentoMutual(Double descuentoMutual) {
        this.descuentoMutual = descuentoMutual;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getPeriodo() {
        return periodo;
    }

    public void setPeriodo(String periodo) {
        this.periodo = periodo;
    }

    public Integer getOrden() {
        return orden;
    }

    public void setOrden(Integer orden) {
        this.orden = orden;
    }

    public Integer getIdCuenta() {
        return idCuenta;
    }

    public void setIdCuenta(Integer idCuenta) {
        this.idCuenta = idCuenta;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Integer getIdMovimiento() {
        return idMovimiento;
    }

    public void setIdMovimiento(Integer idMovimiento) {
        this.idMovimiento = idMovimiento;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public Double getAtraso() {
        return atraso;
    }

    public void setAtraso(Double atraso) {
        this.atraso = atraso;
    }

    public Double getCerrado() {
        return cerrado;
    }

    public void setCerrado(Double cerrado) {
        this.cerrado = cerrado;
    }

    public Double getGiraduria() {
        return giraduria;
    }

    public void setGiraduria(Double giraduria) {
        this.giraduria = giraduria;
    }

    public Double getVentanilla() {
        return ventanilla;
    }

    public void setVentanilla(Double ventanilla) {
        this.ventanilla = ventanilla;
    }

    public Double getAsignacion() {
        return asignacion;
    }

    public void setAsignacion(Double asignacion) {
        this.asignacion = asignacion;
    }

    public Integer getIdSocio() {
        return idSocio;
    }

    public void setIdSocio(Integer idSocio) {
        this.idSocio = idSocio;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public Double getDiferencia() {
        return diferencia;
    }

    public void setDiferencia(Double diferencia) {
        this.diferencia = diferencia;
    }
}
