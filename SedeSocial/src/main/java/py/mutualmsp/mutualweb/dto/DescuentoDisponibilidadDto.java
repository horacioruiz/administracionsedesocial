/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.dto;

import java.io.Serializable;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author hectorvillalba
 */
public class DescuentoDisponibilidadDto implements Serializable {
    
    @Getter
    @Setter
    String descripcion;
    @Getter
    @Setter
    Double actual;
    @Getter
    @Setter    
    Double siguiente;
    
    
}
