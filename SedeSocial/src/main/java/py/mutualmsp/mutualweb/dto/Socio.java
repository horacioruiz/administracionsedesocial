/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.dto;

import java.io.Serializable;

/**
 *
 * @author hectorvillalba
 */
public class Socio implements Serializable {

    private Long id;
    private String nombre;
    private String apellido;
    private String cedula;
    private String fcmToken;
    private String pinMutual;
    private Boolean accesoMutual;
    private String email;
    private String direccion;
    private String profesion;
    private String fechaIngreso;
    private String ruc;
    private String telefono;
    private String institucion;
    private String ciudad;
    private String cargo;
    private String rubro;
    private String regional;
    private Double sueldo;
    private String giraduria;

    public String getGiraduria() {
        return giraduria;
    }

    public void setGiraduria(String giraduria) {
        this.giraduria = giraduria;
    }

    public String getRubro() {
        return rubro;
    }

    public void setRubro(String rubro) {
        this.rubro = rubro;
    }

    public String getRegional() {
        return regional;
    }

    public void setRegional(String regional) {
        this.regional = regional;
    }

    public Double getSueldo() {
        return sueldo;
    }

    public void setSueldo(Double sueldo) {
        this.sueldo = sueldo;
    }

    public Boolean getAccesoMutual() {
        return accesoMutual;
    }

    public void setAccesoMutual(Boolean accesoMutual) {
        this.accesoMutual = accesoMutual;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getProfesion() {
        return profesion;
    }

    public void setProfesion(String profesion) {
        this.profesion = profesion;
    }

    public String getFechaIngreso() {
        return fechaIngreso;
    }

    public void setFechaIngreso(String fechaIngreso) {
        this.fechaIngreso = fechaIngreso;
    }

    public String getRuc() {
        return ruc;
    }

    public void setRuc(String ruc) {
        this.ruc = ruc;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getInstitucion() {
        return institucion;
    }

    public void setInstitucion(String institucion) {
        this.institucion = institucion;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public String getCargo() {
        return cargo;
    }

    public void setCargo(String cargo) {
        this.cargo = cargo;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFcmToken() {
        return fcmToken;
    }

    public void setFcmToken(String fcmToken) {
        this.fcmToken = fcmToken;
    }

    public String getPinMutual() {
        return pinMutual;
    }

    public void setPinMutual(String pinMutual) {
        this.pinMutual = pinMutual;
    }

    public boolean isAccesoMutual() {
        return accesoMutual;
    }

    public void setAccesoMutual(boolean accesoMutual) {
        this.accesoMutual = accesoMutual;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }
}