/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.dto;

import com.vaadin.server.FileResource;
import com.vaadin.server.Sizeable;
import com.vaadin.server.VaadinService;
import com.vaadin.ui.Image;
import java.io.File;
import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author hectorvillalba
 */
public class AgendamientoDto implements Serializable {

    private Long id;

    private Date fecha;

    private Boolean confirmado;

    private Boolean asiste;

    private Boolean usopiscina;

    private py.mutualmsp.mutualweb.entities.Socio socio;

    private String tipo;

    private String observacion;

    private String telefono;

    private Long total;

    private Long cantPersonas;

    private String horario;

    private String estado;
    
    private String rechazado;

    public String getHorario() {
        return horario;
    }

    public void setHorario(String horario) {
        this.horario = horario;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getRechazado() {
        return rechazado;
    }

    public void setRechazado(String rechazado) {
        this.rechazado = rechazado;
    }

    public Boolean getConfirmado() {
        return confirmado;
    }

    public void setConfirmado(Boolean confirmado) {
        this.confirmado = confirmado;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public Boolean getAsiste() {
        return asiste;
    }

    public Long getCantPersonas() {
        return cantPersonas;
    }

    public void setCantPersonas(Long cantPersonas) {
        this.cantPersonas = cantPersonas;
    }

    public void setAsiste(Boolean asiste) {
        this.asiste = asiste;
    }

    public Boolean getUsopiscina() {
        return usopiscina;
    }

    public void setUsopiscina(Boolean usopiscina) {
        this.usopiscina = usopiscina;
    }

    public py.mutualmsp.mutualweb.entities.Socio getSocio() {
        return socio;
    }

    public void setSocio(py.mutualmsp.mutualweb.entities.Socio socio) {
        this.socio = socio;
    }

    public String getTipo() {
        return tipo;
    }

    public Image getConfirmadoIcono() {
        String basepath = VaadinService.getCurrent()
                .getBaseDirectory().getAbsolutePath();
        try {
            System.out.println("ESTADO HERE ->> " + this.confirmado);
            if (this.confirmado == null) {
                FileResource resource = new FileResource(new File(basepath
                        + "/WEB-INF/images/pending2.png"));
                Image imagen = new Image("Image from file", resource);
                imagen.setWidth(2, Sizeable.Unit.EM);
                return imagen;
            } else if (this.confirmado == true) {
                FileResource resource = new FileResource(new File(basepath
                        + "/WEB-INF/images/good.png"));
                Image imagen = new Image("Image from file", resource);
                imagen.setWidth(2, Sizeable.Unit.EM);
                return imagen;
            } else {
                FileResource resource = new FileResource(new File(basepath
                        + "/WEB-INF/images/wrong.png"));
                Image imagen = new Image("Image from file", resource);
                imagen.setWidth(2, Sizeable.Unit.EM);
                return imagen;
            }
        } catch (Exception e) {
            FileResource resource = new FileResource(new File(basepath
                    + "/WEB-INF/images/wrong.png"));
            Image imagen = new Image("Image from file", resource);
            imagen.setWidth(2, Sizeable.Unit.EM);
            return imagen;
        } finally {
        }
    }

    public Image getAsisteIcono() {
        String basepath = VaadinService.getCurrent()
                .getBaseDirectory().getAbsolutePath();
        try {
            if (this.asiste) {
                FileResource resource = new FileResource(new File(basepath
                        + "/WEB-INF/images/good.png"));
                Image imagen = new Image("Image from file", resource);
                imagen.setWidth(2, Sizeable.Unit.EM);
                return imagen;
            } else {
                FileResource resource = new FileResource(new File(basepath
                        + "/WEB-INF/images/wrong.png"));
                Image imagen = new Image("Image from file", resource);
                imagen.setWidth(2, Sizeable.Unit.EM);
                return imagen;
            }
        } catch (Exception e) {
            FileResource resource = new FileResource(new File(basepath
                    + "/WEB-INF/images/wrong.png"));
            Image imagen = new Image("Image from file", resource);
            imagen.setWidth(2, Sizeable.Unit.EM);
            return imagen;
        } finally {
        }
    }

    public Image getUsopiscinaIcono() {
        String basepath = VaadinService.getCurrent()
                .getBaseDirectory().getAbsolutePath();
        try {
            if (this.usopiscina) {
                FileResource resource = new FileResource(new File(basepath
                        + "/WEB-INF/images/good.png"));
                Image imagen = new Image("Image from file", resource);
                imagen.setWidth(2, Sizeable.Unit.EM);
                return imagen;
            } else {
                FileResource resource = new FileResource(new File(basepath
                        + "/WEB-INF/images/wrong.png"));
                Image imagen = new Image("Image from file", resource);
                imagen.setWidth(2, Sizeable.Unit.EM);
                return imagen;
            }
        } catch (Exception e) {
            FileResource resource = new FileResource(new File(basepath
                    + "/WEB-INF/images/wrong.png"));
            Image imagen = new Image("Image from file", resource);
            imagen.setWidth(2, Sizeable.Unit.EM);
            return imagen;
        } finally {
        }
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public Long getTotal() {
        return total;
    }

    public void setTotal(Long total) {
        this.total = total;
    }

}
