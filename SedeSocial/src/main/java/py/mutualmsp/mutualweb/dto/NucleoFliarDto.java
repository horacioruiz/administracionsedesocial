/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.dto;

import com.vaadin.server.FileResource;
import com.vaadin.server.Sizeable;
import com.vaadin.server.VaadinService;
import com.vaadin.ui.Image;
import java.io.File;
import java.io.Serializable;
import java.math.BigInteger;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

/**
 *
 * @author hruiz
 */
public class NucleoFliarDto implements Serializable {

    private Long id;

    private BigInteger idsocio;
    
    private BigInteger idparentesco;

    private String nombre;

    private String apellido;

    private LocalDate fechanacimiento;

    private String cedula;

    private Boolean checked;

    private String doc1;

    private String doc2;

    private String doc3;

    private Boolean habilitado;

    private String parentesco;

    private String observacion;

    private String telefono;

    private String cisocio;

    public NucleoFliarDto() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigInteger getIdsocio() {
        return idsocio;
    }

    public void setIdsocio(BigInteger idsocio) {
        this.idsocio = idsocio;
    }

    public String getNombre() {
        return nombre;
    }

    public BigInteger getIdparentesco() {
        return idparentesco;
    }

    public void setIdparentesco(BigInteger idparentesco) {
        this.idparentesco = idparentesco;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public LocalDate getFechanacimiento() {
        return fechanacimiento;
    }

    public void setFechanacimiento(LocalDate fechanacimiento) {
        this.fechanacimiento = fechanacimiento;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public Boolean getChecked() {
        return checked;
    }

    public void setChecked(Boolean checked) {
        this.checked = checked;
    }

    public String getDoc1() {
        return doc1;
    }

    public void setDoc1(String doc1) {
        this.doc1 = doc1;
    }

    public String getDoc2() {
        return doc2;
    }

    public void setDoc2(String doc2) {
        this.doc2 = doc2;
    }

    public String getDoc3() {
        return doc3;
    }

    public void setDoc3(String doc3) {
        this.doc3 = doc3;
    }

    public Boolean getHabilitado() {
        return habilitado;
    }

    public void setHabilitado(Boolean habilitado) {
        this.habilitado = habilitado;
    }

    public String getParentesco() {
        return parentesco;
    }

    public void setParentesco(String parentesco) {
        this.parentesco = parentesco;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getCisocio() {
        return cisocio;
    }

    public void setCisocio(String cisocio) {
        this.cisocio = cisocio;
    }

    public Image getConfirmadoIcono() {
        String basepath = VaadinService.getCurrent()
                .getBaseDirectory().getAbsolutePath();
        try {
            System.out.println("ESTADO HERE ->> " + this.checked);
            if (this.checked == null) {
                FileResource resource = new FileResource(new File(basepath
                        + "/WEB-INF/images/wrong.png"));
                Image imagen = new Image("Image from file", resource);
                imagen.setWidth(2, Sizeable.Unit.EM);
                return imagen;
            } else if (this.checked == true) {
                FileResource resource = new FileResource(new File(basepath
                        + "/WEB-INF/images/good.png"));
                Image imagen = new Image("Image from file", resource);
                imagen.setWidth(2, Sizeable.Unit.EM);
                return imagen;
            } else {
                FileResource resource = new FileResource(new File(basepath
                        + "/WEB-INF/images/wrong.png"));
                Image imagen = new Image("Image from file", resource);
                imagen.setWidth(2, Sizeable.Unit.EM);
                return imagen;
            }
        } catch (Exception e) {
            FileResource resource = new FileResource(new File(basepath
                    + "/WEB-INF/images/wrong.png"));
            Image imagen = new Image("Image from file", resource);
            imagen.setWidth(2, Sizeable.Unit.EM);
            return imagen;
        } finally {
        }
    }

    public String getEdad() {
        long intervalYears = ChronoUnit.YEARS.between(getFechanacimiento(), LocalDate.now());
        return intervalYears + "";
    }

    @Override
    public String toString() {
        return "py.mutualmsp.mutual.sedesocial.ws.entities.NucleoFliar[ id=" + id + " ]";
    }

}
