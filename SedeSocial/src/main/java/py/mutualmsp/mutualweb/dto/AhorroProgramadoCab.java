/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import java.io.Serializable;
import java.util.Date;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author hectorvillalba
 */
public class AhorroProgramadoCab implements Serializable {

    private Integer id;
    private Long numeroOperacion;
    private String contrato;
    private Integer plazo;
    private Double tasainteres;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy", timezone = "America/Asuncion")
    private Date fechaOperacion;
    private Double importe;
    private Double montoAportado;
    private Double atraso;
    private Double actual;
    private Double saldo;
    @Getter
    @Setter
    private Integer cuotasPagadas;
    
    @Getter
    @Setter
    private Double cierre;

    public Double getAtraso() {
        return atraso;
    }

    public void setAtraso(Double atraso) {
        this.atraso = atraso;
    }

    public Double getActual() {
        return actual;
    }

    public void setActual(Double actual) {
        this.actual = actual;
    }

    public Double getSaldo() {
        return saldo;
    }

    public void setSaldo(Double saldo) {
        this.saldo = saldo;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Long getNumeroOperacion() {
        return numeroOperacion;
    }

    public void setNumeroOperacion(Long numeroOperacion) {
        this.numeroOperacion = numeroOperacion;
    }

    public String getContrato() {
        return contrato;
    }

    public void setContrato(String contrato) {
        this.contrato = contrato;
    }

    public Integer getPlazo() {
        return plazo;
    }

    public void setPlazo(Integer plazo) {
        this.plazo = plazo;
    }

    public Double getTasainteres() {
        return tasainteres;
    }

    public void setTasainteres(Double tasainteres) {
        this.tasainteres = tasainteres;
    }

    public Date getFechaOperacion() {
        return fechaOperacion;
    }

    public void setFechaOperacion(Date fechaOperacion) {
        this.fechaOperacion = fechaOperacion;
    }

    public Double getImporte() {
        return importe;
    }

    public void setImporte(Double importe) {
        this.importe = importe;
    }

    public Double getMontoAportado() {
        return montoAportado;
    }

    public void setMontoAportado(Double montoAportado) {
        this.montoAportado = montoAportado;
    }
}
