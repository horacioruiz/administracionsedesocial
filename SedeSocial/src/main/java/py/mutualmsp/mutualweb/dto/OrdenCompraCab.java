/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import java.io.Serializable;
import java.util.Date;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author hectorvillalba
 */
public class OrdenCompraCab implements Serializable {

    private Long id;
    private Integer nroSolicitud;
    private Long nroBoleta;
    private Integer nroOperacion;
    private Integer plazoAprobado;
    private Double tasaInteres;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd", timezone = "America/Asuncion")
    private Date fechaAprobado;
    private Double montoAprobado;
    private String entidad;
    private Double pagado;
    private Double atraso;
    private Double actual;
    private Double saldo;
    @Getter
    @Setter
    private Integer cuotasPagadas;

    public Double getAtraso() {
        return atraso;
    }

    public void setAtraso(Double atraso) {
        this.atraso = atraso;
    }

    public Double getActual() {
        return actual;
    }

    public void setActual(Double actual) {
        this.actual = actual;
    }

    public Double getSaldo() {
        return saldo;
    }

    public void setSaldo(Double saldo) {
        this.saldo = saldo;
    }

    public Long getNroBoleta() {
        return nroBoleta;
    }

    public void setNroBoleta(Long nroBoleta) {
        this.nroBoleta = nroBoleta;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getNroSolicitud() {
        return nroSolicitud;
    }

    public void setNroSolicitud(Integer nroSolicitud) {
        this.nroSolicitud = nroSolicitud;
    }

    public Integer getNroOperacion() {
        return nroOperacion;
    }

    public void setNroOperacion(Integer nroOperacion) {
        this.nroOperacion = nroOperacion;
    }

    public Integer getPlazoAprobado() {
        return plazoAprobado;
    }

    public void setPlazoAprobado(Integer plazoAprobado) {
        this.plazoAprobado = plazoAprobado;
    }

    public Double getTasaInteres() {
        return tasaInteres;
    }

    public void setTasaInteres(Double tasaInteres) {
        this.tasaInteres = tasaInteres;
    }

    public Date getFechaAprobado() {
        return fechaAprobado;
    }

    public void setFechaAprobado(Date fechaAprobado) {
        this.fechaAprobado = fechaAprobado;
    }

    public Double getMontoAprobado() {
        return montoAprobado;
    }

    public void setMontoAprobado(Double montoAprobado) {
        this.montoAprobado = montoAprobado;
    }

    public String getEntidad() {
        return entidad;
    }

    public void setEntidad(String entidad) {
        this.entidad = entidad;
    }

    public Double getPagado() {
        return pagado;
    }

    public void setPagado(Double pagado) {
        this.pagado = pagado;
    }
}
