/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.dto;

import java.io.Serializable;

/**
 *
 * @author hectorvillalba
 */
public class Institucion implements Serializable {
    private Long id;
    private String descripcion;
    private long idciudad;
    private long idregional;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public long getIdciudad() {
        return idciudad;
    }

    public void setIdciudad(long idciudad) {
        this.idciudad = idciudad;
    }

    public long getIdregional() {
        return idregional;
    }

    public void setIdregional(long idregional) {
        this.idregional = idregional;
    }
}
