/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author hectorvillalba
 */
public class Aportes implements Serializable {
    private String cedula;
    private String nombre;
    private String apellido;
    private String tipo;
    private List<AporteDetalle> aporteDetalles;

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public List<AporteDetalle> getAporteDetalles() {
        return aporteDetalles;
    }

    public void setAporteDetalles(List<AporteDetalle> aporteDetalles) {
        this.aporteDetalles = aporteDetalles;
    }
}
