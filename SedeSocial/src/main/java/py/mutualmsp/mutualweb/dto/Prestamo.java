/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import java.io.Serializable;
import java.util.Date;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author hectorvillalba
 */

public class Prestamo implements Serializable {

    private Long id;
    private Long numeroSolicitud;
    private Long numeroOperacion;
    private Integer plazoAprobado;
    private Double tasaInteres;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy", timezone = "America/Asuncion")
    private Date fechaAprobado;
    private Double montoAprobado;
    private Double montoInteres;
    private String entidad;
    private String tipoCredito;
    private Double pagadoCapital;
    private Double pagadoInteres;
    private Boolean cancelacion;
    private Double montoCancelacion;
    private Double montoImpuesto;
    private Double montoaRetirar;
    private Double saldoCapital;
    private Double atraso;
    private Double actual;
    private Double saldo;
    @Getter
    @Setter
    private Integer cuotasPagadas;
    
    @Getter
    @Setter
    private String cuenta;
    
    @Getter
    @Setter
    private Double cerrado;

    public Double getAtraso() {
        return atraso;
    }

    public void setAtraso(Double atraso) {
        this.atraso = atraso;
    }

    public Double getActual() {
        return actual;
    }

    public void setActual(Double actual) {
        this.actual = actual;
    }

    public Double getSaldo() {
        return saldo;
    }

    public void setSaldo(Double saldo) {
        this.saldo = saldo;
    }

    public Double getSaldoCapital() {
        return saldoCapital;
    }

    public void setSaldoCapital(Double saldoCapital) {
        this.saldoCapital = saldoCapital;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getNumeroSolicitud() {
        return numeroSolicitud;
    }

    public void setNumeroSolicitud(Long numeroSolicitud) {
        this.numeroSolicitud = numeroSolicitud;
    }

    public Long getNumeroOperacion() {
        return numeroOperacion;
    }

    public void setNumeroOperacion(Long numeroOperacion) {
        this.numeroOperacion = numeroOperacion;
    }

    public Integer getPlazoAprobado() {
        return plazoAprobado;
    }

    public void setPlazoAprobado(Integer plazoAprobado) {
        this.plazoAprobado = plazoAprobado;
    }

    public Double getTasaInteres() {
        return tasaInteres;
    }

    public void setTasaInteres(Double tasaInteres) {
        this.tasaInteres = tasaInteres;
    }

    public Date getFechaAprobado() {
        return fechaAprobado;
    }

    public void setFechaAprobado(Date fechaAprobado) {
        this.fechaAprobado = fechaAprobado;
    }

    public Double getMontoAprobado() {
        return montoAprobado;
    }

    public void setMontoAprobado(Double montoAprobado) {
        this.montoAprobado = montoAprobado;
    }

    public Double getMontoInteres() {
        return montoInteres;
    }

    public void setMontoInteres(Double montoInteres) {
        this.montoInteres = montoInteres;
    }

    public String getEntidad() {
        return entidad;
    }

    public void setEntidad(String entidad) {
        this.entidad = entidad;
    }

    public String getTipoCredito() {
        return tipoCredito;
    }

    public void setTipoCredito(String tipoCredito) {
        this.tipoCredito = tipoCredito;
    }

    public Double getPagadoCapital() {
        return pagadoCapital;
    }

    public void setPagadoCapital(Double pagadoCapital) {
        this.pagadoCapital = pagadoCapital;
    }

    public Double getPagadoInteres() {
        return pagadoInteres;
    }

    public void setPagadoInteres(Double pagadoInteres) {
        this.pagadoInteres = pagadoInteres;
    }

    public Boolean getCancelacion() {
        return cancelacion;
    }

    public void setCancelacion(Boolean cancelacion) {
        this.cancelacion = cancelacion;
    }

    public Double getMontoCancelacion() {
        return montoCancelacion;
    }

    public void setMontoCancelacion(Double montoCancelacion) {
        this.montoCancelacion = montoCancelacion;
    }

    public Double getMontoImpuesto() {
        return montoImpuesto;
    }

    public void setMontoImpuesto(Double montoImpuesto) {
        this.montoImpuesto = montoImpuesto;
    }

    public Double getMontoaRetirar() {
        return montoaRetirar;
    }

    public void setMontoaRetirar(Double montoaRetirar) {
        this.montoaRetirar = montoaRetirar;
    }
}
