/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.dto;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @author hectorvillalba
 */
public class DescuentosCab implements Serializable {

    private Double totalDescMututal;
    private Double totalGiraduria;
    private Double totalVentanilla;
    private Double totalAsignacion;
    private Double totalDiferencia;
    private List<Descuentos> listaDescuentos;

    public Double getTotalDescMututal() {
        return totalDescMututal;
    }

    public void setTotalDescMututal(Double totalDescMututal) {
        this.totalDescMututal = totalDescMututal;
    }

    public Double getTotalGiraduria() {
        return totalGiraduria;
    }

    public void setTotalGiraduria(Double totalGiraduria) {
        this.totalGiraduria = totalGiraduria;
    }

    public Double getTotalVentanilla() {
        return totalVentanilla;
    }

    public void setTotalVentanilla(Double totalVentanilla) {
        this.totalVentanilla = totalVentanilla;
    }

    public Double getTotalAsignacion() {
        return totalAsignacion;
    }

    public void setTotalAsignacion(Double totalAsignacion) {
        this.totalAsignacion = totalAsignacion;
    }

    public Double getTotalDiferencia() {
        return totalDiferencia;
    }

    public void setTotalDiferencia(Double totalDiferencia) {
        this.totalDiferencia = totalDiferencia;
    }

    public List<Descuentos> getListaDescuentos() {
        return listaDescuentos;
    }

    public void setListaDescuentos(List<Descuentos> listaDescuentos) {
        this.listaDescuentos = listaDescuentos;
    }
}
