/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.util;

import com.vaadin.server.VaadinSession;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import py.mutualmsp.mutualweb.entities.Usuario;

/**
 *
 * @author Dbarreto
 */
public class UserHolder {

    public static Usuario get() {
        Usuario user = (Usuario) VaadinSession.getCurrent().getAttribute(Usuario.class.getName());
        return user;
    }
    public static void setUsuarios(Usuario user) {
        VaadinSession.getCurrent().setAttribute(Usuario.class.getName(), user);
        poblarFormularios();
    }

    public static boolean viewAccesibleToUser(String viewName) {
        return get().isInAnyRole(viewNames_roles.get(viewName));
    }

    public static final Map<String, List<Boolean>> viewNames_roles = new HashMap<>();

    public static void poblarFormularios() {
        //viewNames_roles.put(TicketsView.VIEWNAME, Arrays.asList(get().getAdministrador(),get().getHelpdesk() ,  get().getClientes()));
        //viewNames_roles.put(UsersView.VIEW_NAME, Arrays.asList(get().getAdministrador(), get().getHelpdesk()));
        //viewNames_roles.put(FuncionarioView.VIEW_NAME, Arrays.asList(get().getAdministrador(), get().getHelpdesk()));
        //viewNames_roles.put(ProyectosView.VIEW_NAME, Arrays.asList(get().getAdministrador(), get().getHelpdesk()));
        //viewNames_roles.put(TipoReclamoView.VIEW_NAME, Arrays.asList(get().getAdministrador(), get().getHelpdesk()));
        //viewNames_roles.put(ReportesReclamos.VIEWNAME, Arrays.asList(get().getAdministrador(), get().getHelpdesk()));
    }
//    private void setRolesAllowedInView(String viewName, String ... roles) {
//
//    }
}



