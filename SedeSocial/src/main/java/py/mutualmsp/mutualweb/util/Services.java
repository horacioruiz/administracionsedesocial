package py.mutualmsp.mutualweb.util;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.function.Consumer;
import java.util.function.Function;

/**
 * Created by Alfre on 7/6/2016.
 */
@Stateless
public class Services {

    public Object queryInEjb(Function<EntityManager, Object> qie) {
        return qie.apply(null);
    }

    public void runInTransaction(Consumer<EntityManager> rit) {
        rit.accept(null);
    }

}
