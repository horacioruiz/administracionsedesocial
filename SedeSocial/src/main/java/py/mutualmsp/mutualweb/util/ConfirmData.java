package py.mutualmsp.mutualweb.util;

import com.github.appreciated.material.MaterialTheme;
import com.vaadin.shared.Registration;
import com.vaadin.ui.Button;
import com.vaadin.ui.Component;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

public class ConfirmData extends Button {
    
    private Window popup;
    private VerticalLayout content;
    private Button okButton;
    private Button cancelButton;

    /**
     * @param string
     */
    public ConfirmData(String caption) {
        super(caption);
    }

    /**
     *
     * @return the last Popup into which the Form was opened with
     * #openInModalPopup method or null if the form hasn't been use in window
     */
    public Window getPopup() {
        return popup;
    }

    /**
     * If the form is opened into a popup window using openInModalPopup(), you
     * you can use this method to close the popup.
     */
    public void closePopup() {
        if (popup != null) {
            popup.close();
            popup = null;
        }
    }
    
    public Window openInModalPopup(String titulo, Component contenido, String width) {
        popup = new Window(getModalWindowTitle(titulo), contenido);
        popup.setModal(true);
        popup.setWidth(width);
        popup.setHeight("75%");
        popup.setResizable(false);
        UI.getCurrent().addWindow(popup);
        return popup;
        
    }

    /**
     * @return A default toolbar containing save/cancel/delete buttons
     */
    private HorizontalLayout getToolbar() {
        return new HorizontalLayout(
                getOkButton()//,
//                getCancelButton()
        );
    }

    /**
     * @return
     */
    public Button getCancelButton() {
        if (cancelButton == null) {
            cancelButton = new Button(getCancelCaption());
            cancelButton.setVisible(false);
            cancelButton.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_FRIENDLY);
//            cancelButton.addClickListener(e -> {
//                closePopup();
//            });
        }
        return cancelButton;
    }

    /**
     * @return
     */
    public Button getOkButton() {
        if (okButton == null) {
            okButton = new Button(getOkCaption());
            okButton.setStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_PRIMARY);
        }
        return okButton;
    }

    /**
     * @return
     */
    private Component getContent(String contenido) {
        if (content == null) {
            content = new VerticalLayout();
            content.addComponent(new Label(getConfirmationText(contenido)));
            content.addComponent(getToolbar());
        }
        return content;
    }
    
    public String getModalWindowTitle(String titulo) {
        return (titulo);
    }
    
    public String getConfirmationText(String contenido) {
        return (contenido);
    }
    
    public String getOkCaption() {
        return ("Aceptar");
    }
    
    public String getCancelCaption() {
        return ("Rechazar");
    }

    /* (non-Javadoc)
     * @see com.vaadin.ui.Button#addClickListener(com.vaadin.ui.Button.ClickListener)
     */
    @Override
    public Registration addClickListener(ClickListener listener) {
        getOkButton().addClickListener(listener);
        getOkButton().addClickListener(e -> {
            closePopup();
        });
        return super.addClickListener(e -> {
//            openInModalPopup("", "", "");
        });
    }
}
