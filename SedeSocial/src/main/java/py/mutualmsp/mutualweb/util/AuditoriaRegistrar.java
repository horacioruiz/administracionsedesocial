/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.util;

import py.mutualmsp.mutualweb.dao.AuditoriaDao;
import py.mutualmsp.mutualweb.entities.Auditoria;

/**
 *
 * @author DBarreto
 */
public class AuditoriaRegistrar {
    public static void guardarAuditoria(String nombretabla, String operacion, String vviejo, String vnuevo) {
        try {
            Auditoria auditoria = new Auditoria();
            AuditoriaDao auditoriaDao = ResourceLocator.locate(AuditoriaDao.class);
            java.util.Date fechaactualizacion = new java.util.Date();
            auditoria.setNombretabla(nombretabla);
            auditoria.setOperacion(operacion);
            auditoria.setValorviejo(vviejo);
            auditoria.setValornuevo(vnuevo);
            auditoria.setFechaactualizacion(fechaactualizacion);
            auditoria.setUsuario(UserHolder.get().getUsuario());
            auditoriaDao.guardar(auditoria);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
