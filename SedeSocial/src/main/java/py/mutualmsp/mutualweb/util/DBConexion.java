/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author Hruiz
 */
public class DBConexion {

    private final String url = "jdbc:postgresql://192.168.10.6:5432/MutualMSPBS?currentSchema=sms";
    private final String user = "postgres";
    private final String password = "Draco2019/";
    public static String PGSQL_DRIVER = "org.postgresql.Driver";
    public static Connection con;
    public static Statement st;
    public static PreparedStatement pstm;

    public String getUrl() {
        return url;
    }

    public String getUser() {
        return user;
    }

    public String getPassword() {
        return password;
    }

    public Connection getConnection()
            throws ClassNotFoundException, SQLException {
        Class.forName(PGSQL_DRIVER);
        con = DriverManager.getConnection(url, user, password);
        return con;
    }

    public Statement getStatement() throws SQLException, ClassNotFoundException {
        st = getConnection().createStatement();
        return st;
    }

    public PreparedStatement getPreparedStatement(String sql) throws SQLException, ClassNotFoundException {
        pstm = getConnection().prepareStatement(sql);
        return pstm;
    }
}
